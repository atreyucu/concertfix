$(document).ready(function(){

	$('#carousel-widget').carousel({
   		'interval' : 5000
   	});

   	$(document).on('click', '#signup', function(event) {
   		event.preventDefault();
   		/* Act on the event */
   		var name = $('#ml_name').val();
   		var email = $('#ml_email').val();
   		$.ajax({
   			url: '/ajax/signup',
   			type: 'POST',
   			dataType: 'html',
   			data: {email: email, name : name},
   		})
   		.done(function(data) {
   			// console.log("success");
           $('#signupnotice').html(data);
   		})
   		.fail(function() {
   			// console.log("error");
   		})
   		.always(function() {
			$('#ml_name').val('');
			$('#ml_email').val('');
   		});

   	});

   	$(document).on('click', '.chooseEventBeforeClick', function(event) {
   		event.preventDefault();
      var text = "<p>Please select a date from the list above to view the seating chart for that event</p>";
      if(self.innerWidth > 710) {
        // debugger;
        text = text.replace('below', 'to the left');
      }
   		$('#askToChoose').html(text);
   		$('#askToChoose').addClass('alert alert-danger');
   		$('#askToChooseSidebar').html(text);
   		$('#askToChooseSidebar').addClass('alert alert-danger');
   		/* Act on the event */
   	});
    // More Link
    $('.expand-more').on('click', function() {
      if($(this).hasClass('expanded')) {
        $(this).text('More...');
      } else {
        $(this).text('Less...');
      }
      $(this).toggleClass('expanded');
      $('.more').toggleClass('hide');

    });
	/*
	// carousel demo
    $('#carouselslide').carousel()
	//========================== SlideJS Slider Initiation ============================//

	var block = "<div class='block'></div>";

	var slider = $('#slidejs'),
	 	sliderCaption = $('#bon-slider-caption');

	if (slider.size() > 0) {
		slider.slides({
			preload: true,
			preloadImage: 'img/assets/ajax-loader.gif',
			play: 5000,
			pause: 2500,
			hoverPause: true,
			generateNextPrev: true,
			pagination: true,
			next: 'slide-next',
			prev: 'slide-prev',
			generatePagination: true,
			autoHeight: false,
			effect: 'fade',
			crossfade: true,
			paginationClass : 'slides-paginate',
			container: 'slides-container',
	        slidesLoaded: function() {
	            slider.find('.slides-paginate').addClass('visible-desktop');

	        }
		});
	}
	slider.find('.slide-next').append('<span class="awe-chevron-right"></span>');
    slider.find('.slide-prev').append('<span class="awe-chevron-left"></span>');


	if(slider.find('.slide-outer').children('.block').size() < 1) {
		$('.slide-outer').prepend(block);
	}
	*/
});
