$(document).ready(function(){

	$( document ).tooltip();
	$(document).on('click', '#update-name', function(event) {
		$('#nameUpdateMessage').addClass('hidden');
		event.preventDefault();
		var newName = $('#cfName').val();
 		$.ajax({
			url: '/ajax/update_name',
			type: 'POST',
			dataType: 'json',
			data: {name: newName},
		})
		.done(function(data) {

			$('#nameUpdateMessage').html(data.message);
			$('#nameUpdateMessage').addClass(data.type);
			$('#nameUpdateMessage').removeClass('hidden');
		})
		.fail(function() {

		})
		.always(function() {
		});

		/* Act on the event */
	});

	$(document).on('click', '.notifytype', function(event) {
		$('#notifyUpdateMessage').addClass('hidden');
		var notify = $(this).val();
		$.ajax({
			url: '/ajax/change_notify_type',
			type: 'POST',
			dataType: 'json',
			data: {notify: notify},
		})
		.done(function(data) {
			$('#notifyUpdateMessage').html(data.message);
			$('#notifyUpdateMessage').addClass(data.type);
			$('#notifyUpdateMessage').removeClass('hidden');
		})
		.fail(function() {

		})
		.always(function() {

		});

	});

	$(document).on('click', '.reminder', function(event) {
		$('#reminderUpdateMessage').addClass('hidden');
		var remindtype = $(this).val();
		var status = $(this).attr('checked')?1:0;

		$.ajax({
			url: '/ajax/change_remind_type',
			type: 'POST',
			dataType: 'json',
			data: {remindtype: remindtype, status: status},
		})
		.done(function(data) {
			$('#reminderUpdateMessage').html(data.message);
			$('#reminderUpdateMessage').addClass(data.type);
			$('#reminderUpdateMessage').removeClass('hidden');
		})
		.fail(function() {

		})
		.always(function() {

		});

	});

    $(document).on('click', '.newconcertcity', function(event){
        var frequency = $(this).val();
        $.ajax({
            url: '/ajax/change_new_concert_email_frequency',
            type: 'POST',
            dataType: 'json',
            data: {frequency: frequency}
        })
        .done(function(data) {
            if (data.error == 2)
                $(location).attr('href', '/user/login');
            else {
                $('#newfrequencyUpdateMessage').html(data.message);
                $('#newfrequencyUpdateMessage').addClass(data.type);
                $('#newfrequencyUpdateMessage').fadeIn(500);
                window.setTimeout(function () {
                    $('#newfrequencyUpdateMessage').fadeOut(500)
                }, 3000)
            }
        })
        .fail(function() {

        })
        .always(function() {

        });

    })

	$(document).on('click', '.miles', function(event){
		var miles = $(this).val();
		$.ajax({
			url: '/ajax/change_home_city_miles',
			type: 'POST',
			dataType: 'json',
			data: {miles: miles}
		})
			.done(function(data)
            {
                if (data.error == 2)
                    $(location).attr('href', '/user/login');
                else {
                    $('#homecityUpdateMessage').html(data.message);
                    $('#homecityUpdateMessage').addClass(data.type);
                    $('#homecityUpdateMessage').fadeIn(500);
                    window.setTimeout(function () {
                        $('#homecityUpdateMessage').fadeOut(500)
                    }, 3000)
                }
			})
			.fail(function() {

			})
			.always(function() {

			});

	})

	$(document).on('click', '#addhomecity', function(event){
		var slug = $('#city_slug').val();
		var name = $('#city_name').val();
		$.ajax({
			url: '/ajax/change_home_city_slug',
			type: 'POST',
			dataType: 'json',
			data: {slug: slug, name: name}
		})
			.done(function(data) {
                if (data.error == 2)
                    $(location).attr('href', '/user/login');
                else {
                    $('#homecityUpdateMessage').html(data.message);
                    $('#homecityUpdateMessage').addClass(data.type);
                    $('#homecityUpdateMessage').fadeIn(500);
                    window.setTimeout(function () {
                        $('#homecityUpdateMessage').fadeOut(500)
                    }, 3000)
                }
			})
			.fail(function() {

			})
			.always(function() {

			});

	})

    $(document).on('click', '.upcomingconcertcity', function(event){
        var frequency = $(this).val();
        $.ajax({
            url: '/ajax/change_upcoming_concert_email_frequency',
            type: 'POST',
            dataType: 'json',
            data: {frequency: frequency}
        })
        .done(function(data) {
                if (data.error == 2)
                    $(location).attr('href', '/user/login');
                else {
                    $('#upcomingfrequencyUpdateMessage').html(data.message);
                    $('#upcomingfrequencyUpdateMessage').addClass(data.type);
                    $('#upcomingfrequencyUpdateMessage').fadeIn(500);
                    window.setTimeout(function () {
                        $('#upcomingfrequencyUpdateMessage').fadeOut(500)
                    }, 3000)
                }
        })
        .fail(function() {

        })
        .always(function() {

        });

    })

    $(document).on('click', '.genre',function(event){
        var checked = $(this).is(':checked');
        var action = 0;
        if(checked)
        {
            action = 1;
        }
        else{
            $('.all').removeAttr('checked');
        }
        var genre = $(this).val();

		if(genre == 'all' && action == 1)
		{
			$('.genre').attr('checked', 'checked')
		} else if (genre == 'all' && action == 0){
			$('.genre').removeAttr('checked')
		}
        $.ajax({
            url: '/ajax/change_genre',
            type: 'POST',
            dataType: 'json',
            data: {genre: genre, action: action}
        })
            .done(function(data) {
                /*if(genre == 'all' && action == 1)
                {
                    $('.genre').attr('checked', 'checked')
                } else if (genre == 'all' && action == 0){
					$('.genre').removeAttr('checked')
				}*/
				if (data.error == 2)
					$(location).attr('href', '/user/login');
				else {
					$('#genreUpdateMessage').html(data.message);
					$('#genreUpdateMessage').addClass(data.type);
					$('#genreUpdateMessage').fadeIn(500);
					window.setTimeout(function(){$('#genreUpdateMessage').fadeOut(500)}, 3000)
				}

            })
            .fail(function() {

            })
            .always(function() {

            });

    })

	$(document).on('click', '.remove-link', function(event) {
		event.preventDefault();
		var network = $(this).data('network');
		var remove = (network == 'cf')?'THIS ACCOUNT' : network;
		$('#dialog-confirm').html("Would you like to REMOVE  "+remove+"?")
			$( "#dialog-confirm" ).dialog({
		      resizable: false,
		      height:140,
		      modal: true,
		      title : name,
		      buttons: {
		      	"Yes Remove": function() {
		          $( this ).dialog( "close" );
		          		$('#remove-'+network).submit();
		          },
		        Cancel: function() {
		          $( this ).dialog( "close" );
		        }
		      }
		    }); //dialog
	});


	$(document).on('click', '#delete-account', function(event) {
		event.preventDefault();
		$('#dialog-confirm').html("<span style='color:red;'>Would you like to <strong>COMPLETELY DELETE</strong> this account? It cannot be UNDONE! All your tracking rules and settings will be lost!!!!</span>")
			$( "#dialog-confirm" ).dialog({
		      resizable: false,
		      height:200,
		      modal: true,
		      title : "DELETE ALL ACCOUNTS!",
		      buttons: {
		      	"Yes Remove": function() {
		          $( this ).dialog( "close" );
		          		$('#remove-all').submit();
		          },
		        Cancel: function() {
		          $( this ).dialog( "close" );
		        }
		      }
		    }); //dialog
	});
}); //jQ
