$(document).ready(function(){

    refreshRules('performer');
    refreshRules('city');

    $('.ruleNotification').bind('DOMSubtreeModified', function(event) {
       setTimeout(function () { $(".ruleNotification").html("") }, 5000);
    });
    $(document).unbind('click.refresh-all-rules').on('click.refresh-all-rules', '#refresh-all-rules', function(event) {
        event.preventDefault();
        refreshRules('performer');
        refreshRules('city');
    });

    $('#rule-panels a, #tab-content a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    })

    $(document).on('keydown.performer-search', '.performer-search', function(event) {
        if (event.keyCode == 13) {
            $('#search-performer-results').html('<li><img src="/public/img/icons/loading.gif" /></li>');
            var term = $(this).val();

            $.ajax({
                url: '/ajax/fe_search_performer',
                type: 'GET',
                dataType: 'json',
                data: {term: term},
            })
            .done(function(data) {

                if(data.length)
                {
                    $('#search-performer-results').html('');
                    $.each(data, function(index, item) {
                        var cdn = "https://s3.amazonaws.com/ssglobalcdn/";
                        var pid = item.pId;
                        var pname = item.value;
                        var img = item.img;
                        if(img != null)
                        {
                            pimg = cdn+img;
                        }else
                        {
                            pimg = "/public/img/artist_thumb.jpg";
                        }

                        var pslug = item.slug;
                        var found = '<li class="artist-found"><button class="btn btn-info btn-flatten add-found-performer" data-performerid="'+pid+'" data-performer="'+pname+'" type="button"><span class="awe-plus"></span> Track </button><img src="'+pimg+'" /><span>&nbsp;'+pname+'&nbsp;</span></li>';
                        $('#search-performer-results').append(found);

                    });
                }else
                {
                 $('#search-performer-results').html('<li><p>Sorry, no results for <em>'+term+'</em></p>');
                }
            })
            .fail(function() {

            })
            .always(function() {

            });

        }

        /* Act on the event */
    });

 	$( ".performer-search" ).autocomplete(
 	{
    	source: "/ajax/fe_search_performer",
      	minLength: 2,
      	autoFocus: true,
      	delay : false,
      	messages: {
       		noResults: '',
        	results: function() {}
    	},
    	select: function( event, ui ) {

            $('#search-performer-results').html('<li><img src="/public/img/icons/loading.gif" /></li>');
            $('.performer-search').val('');
            var cdn = "https://s3.amazonaws.com/ssglobalcdn/";
            var pid = ui.item.pId;
            var pname = ui.item.value;
            var img = ui.item.img;
            var img2 = ui.item.limg;
            if(img != null) //uploaded thumb by CF
            {
                pimg = cdn+img;
            }
            else //no thumb uploaded
            {
                if(img2 != null) //LFM image
                {
                    pimg = img2;
                }else
                {
                    pimg = "/public/img/artist_thumb.jpg";
                }
            }

            var pslug = ui.item.slug;
            var found = '<li class="artist-found"><button class="btn btn-info btn-flatten add-found-performer" data-slug="'+pslug+'" data-performerid="'+pid+'" data-performer="'+pname+'" type="button"><span class="awe-plus"></span> Track</button><img src="'+pimg+'" /><span>&nbsp;'+pname+'&nbsp;</span></li>';
            $('#search-performer-results').html('');
            $('#search-performer-results').append(found);

            $.ajax({
                url: '/ajax/get_similar_artists',
                type: 'POST',
                dataType: 'json',
                data: {pid: pid},
            })
            .done(function(data) {

                 var similar = "<li class='similarperformerheader'><h4>Similar Artists</h4></li>";
                $.each(data, function(index, val) {
                    var img = val.img;
                    var img2 = val.limg;

                    if(img != null)
                    {
                        pimg = cdn+img;
                    }else
                    {
                        if(img2 != null) //LFM image
                        {
                            pimg = img2;
                        }else
                        {
                            pimg = "/public/img/artist_thumb.jpg";
                        }
                    }
                    similar += '<li class="similar-performer-li"><button class="btn btn-info btn-flatten add-found-performer similarperformer" data-performer="'+val.performerName+'" data-performerid="'+val.pId+'" data-slug="'+val.pSlug+'" data-name="'+val.performerName+'" type="button"><span class="awe-plus"></span></button><img src="'+pimg+'" /><span>&nbsp;'+val.performerName+'&nbsp;</span></li>';

                });

                similar += '</ul><p style="clear:both;padding:10px 4px;"><button class="btn btn-info btn-flatten add-all-similar">Add All Similar Artists</button></p>';

                $('#search-performer-results').append(similar);

            })
            .fail(function() {

            })
            .always(function() {

            });

        }
    });

    $(document).unbind('click.add-found-performer').on('click.add-found-performer', '.add-found-performer', function(event) {
        event.preventDefault();
        var sendData = {
            ruleType       :   'performer',
            performerId     :   $(this).data('performerid'),
            performerName   :   $(this).data('performer'),
        };
        var $thisli = $(this);

        $.ajax({
            url: '/ajax/add_rule',
            type: 'POST',
            dataType: 'json',
            data: {rule: sendData},
        })
        .done(function(data) {
           if(data.error == 0)
           {
                $('.ruleNotification').html("<div class='alert "+data.type+"'>"+data.message+"</div>");
                $thisli.parent('li').remove();
                var itemsLeft = $('.similarperformer').length;
                if(itemsLeft == 0)
                {
                    $('.similarperformerheader').remove();
                }
                refreshRules('performer');
           }
           else
           {
                $('.ruleNotification').html("<div class='alert "+data.type+"'>"+data.message+"</div>");
                $('.ruleNotification').addClass(data.type);
           }
        })
        .fail(function() {

        })
        .always(function() {

        });

        var pId = $(this).data('perforemrid');
        var pName = $(this).data('performer');
        var pSlug = $(this).data('slug');
        /* Act on the event */
    });

    $( ".city-search" ).autocomplete(
 	{
    	source: "/ajax/fe_search_city",
      	minLength: 3,
      	autoFocus: true,
      	// delay : 300,
      	messages: {
       		noResults: '',
        	results: function() {}
    	},
    	select: function( event, ui ) {
            $('#search-city-results').html('<li><img src="/public/img/icons/loading.gif" /></li>');
            $('.city-search').val('');
            var cname = ui.item.value;
            var cslug = ui.item.slug;
            var found = '<li class="city-found"><button class="btn btn-info btn-flatten add-found-city" data-slug="'+cslug+'" data-city="'+cname+'" type="button"><span class="awe-plus"></span> </button><span>&nbsp;'+cname+'&nbsp;</span></li>';
            $('#search-city-results').html('');
            $('#search-city-results').append(found);

            //get nearby cities
            $.ajax({
              url: '/ajax/get_nearby_cities',
              type: 'POST',
              dataType: 'json',
              data: {slug: cslug},
              complete: function(xhr, textStatus) {
                //called when complete
                //
              },
              success: function(data, textStatus, xhr) {
                //called when successful
                var nearbyhtml = "<li class='nearbyheader'><h4>Cities Nearby</h4></li>";
                $.each(data, function(index, val) {
                    var prettyName = val.city+", "+val.state;
                    nearbyhtml += '<li class="nearby"><button class="btn btn-info btn-flatten add-found-city nearbycity" data-slug="'+val.slug+'" data-city="'+prettyName+'" type="button"><span class="awe-plus"></span> </button><span>&nbsp;'+prettyName+'&nbsp;</span></li>';
          /*          nearbyhtml += "<li><a href='#' class='label pc-add-city' data-reload='0' data-slug='"+val.slug+"' data-name='"+prettyName+"'><span class='awe-plus'> </span> "+prettyName+"</a></li>";*/
                });
                nearbyhtml += "</ul><p style='clear:both;padding:10px 4px;'><button class='btn btn-info btn-flatten add-all-nearby'>Add All Nearby Cities</button></p><ul> <hr />";

                $('#search-city-results').append(nearbyhtml);
              },
              error: function(xhr, textStatus, errorThrown) {
                //called when there is an error
                //
              }
            });


      	}
    });


    $( ".city-search-as" ).autocomplete(
        {
            source: "/ajax/search_city",
            minLength: 3,
            autoFocus: true,
            // delay : 300,
            messages: {
                noResults: '',
                results: function() {}
            },
            select: function( event, ui ) {
                //$('#search-city-results').html('<li><img src="/public/img/icons/loading.gif" /></li>');
                $('.city-search-as').val('');
                var cname = ui.item.value;
                var cslug = ui.item.slug;
                $('input#city_slug').val(cslug);
                $('input#city_name').val(cname);
                /*var found = '<li class="city-found"><button class="btn btn-info btn-flatten" data-slug="'+cslug+'" data-city="'+cname+'" type="button"><span class="awe-plus"></span> </button><span>&nbsp;'+cname+'&nbsp;</span></li>';
                $('#search-city-results').html('');
                $('#search-city-results').append(found);*/

            }
        });


    $(document).unbind('click.add-found-city').on('click.add-found-city', '.add-found-city', function(event) {
        event.preventDefault();
        var sendData = {
            ruleType       :   'city',
            slug     :   $(this).data('slug'),
            city    :   $(this).data('city')
        };
        var $thisli = $(this);

        $.ajax({
            url: '/ajax/add_rule',
            type: 'POST',
            dataType: 'json',
            data: {rule: sendData},
        })
        .done(function(data) {
           if(data.error == 0)
           {
                $('.ruleNotification').html("<div class='alert "+data.type+"'>"+data.message+"</div>");
                $thisli.parent('li').remove();
                var itemsLeft = $('.nearbycity').length;
                if(itemsLeft == 0)
                {
                    $('.nearbyheader').remove();
                }
                refreshRules('city');
           }
           else
           {
                $('.ruleNotification').html("<div class='alert "+data.type+"'>"+data.message+"</div>");
                $('.ruleNotification').addClass(data.type);
           }
        })
        .fail(function() {

        })
        .always(function() {

        });
    });

    $(document).unbind('click.add-all-nearby').on('click.add-all-nearby', '.add-all-nearby', function(event) {
        event.preventDefault();
        $('.nearbyheader').remove();
        $('.nearbycity').each(function(index, val) {
            var sendData = {
                ruleType       :   'city',
                slug     :   $(val).data('slug'),
                city    :   $(val).data('city')
            };
            var $thisli = $(val);

            $.ajax({
                url: '/ajax/add_rule',
                type: 'POST',
                dataType: 'json',
                data: {rule: sendData},
            })
            .done(function(data) {
               if(data.error == 0)
               {
                    $('.ruleNotification').append("<div class='alert "+data.type+"'>"+data.message+"</div>");
                    $thisli.parent('li').remove();

               }
               else
               {
                    $('.ruleNotification').append("<div class='alert "+data.type+"'>"+data.message+"</div>");
                    $('.ruleNotification').addClass(data.type);
               }
            })
            .fail(function() {

            })
            .always(function() {

            });
        });
        refreshRules('city');

    });

    $(document).unbind('click.add-all-similar').on('click.add-all-similar', '.add-all-similar', function(event) {
        event.preventDefault();
        $('.similarperformerheader').remove();
        $('.similarperformer').each(function(index, val) {
            var sendData = {
                ruleType       :   'performer',
                performerId     :   $(this).data('performerid'),
                performerName   :   $(this).data('performer'),
            };
            var $thisli = $(val);

            $.ajax({
                url: '/ajax/add_rule',
                type: 'POST',
                dataType: 'json',
                data: {rule: sendData},
            })
            .done(function(data) {
               if(data.error == 0)
               {
                    $('.ruleNotification').append("<div class='alert "+data.type+"'>"+data.message+"</div>");
                    $thisli.parent('li').remove();

               }
               else
               {
                    $('.ruleNotification').append("<div class='alert "+data.type+"'>"+data.message+"</div>");
                    $('.ruleNotification').addClass(data.type);
               }
            })
            .fail(function() {

            })
            .always(function() {

            });
        });
        refreshRules('performer');

    });

    $(document).on('change.genre-rule', '.genre-rule', function(event) {
        /* Act on the event */
        var thischeck = $(this);
        var data = {
                ruleType        :   'genre',
                on              :   thischeck.prop('checked'),
                gId             :   thischeck.data('genreid')

        };
        $.ajax({
                 url: '/ajax/add_rule',
                type: 'POST',
                dataType: 'json',
            data: {rule: data},
        })
        .done(function() {

        })
        .fail(function() {

        })
        .always(function() {

        });

        if (data.on)
        {
            thischeck.closest('li').addClass('genreActive');
        }
        else
        {
            thischeck.closest('li').removeClass('genreActive');
        }

    });

/*    //legacy
    $(document).unbind('click.addcity').on('click.addcity', '.pc-add-city', function(event)
    {
    	event.preventDefault();
    	var slug = $(this).data('slug');
    	var prettyName = $(this).data('name');
    	var reNearby = $(this).data('reload');
    	if ($('#pc-slug-'+slug).length == 0)
        {
        	$("#pc-city-list").append('<li id="pc-slug-'+slug+'" data-slug="'+slug+'" data-name="'+prettyName+'"><span class="label">'+prettyName+' <a href="#remove" class="pc-remove-city" data-liid="pc-slug-'+slug+'"><span class="awe-trash"></span></a></span></li>');
    	}

        //cleanup
        $('#pc-add-city').data('slug', "");
        $('#pc-add-city').data('name', "");
    	$(this).addClass('hidden');
    	$('.city-search').val('');
    	// $('#pc-cityslug').val('');

    	if(reNearby == 1)
    	{
    		$.ajax({
    		  url: '/ajax/get_nearby_cities',
    		  type: 'POST',
    		  dataType: 'json',
    		  data: {slug: slug},
    		  complete: function(xhr, textStatus) {
    		    //called when complete
    		    //
    		  },
    		  success: function(data, textStatus, xhr) {
    		    //called when successful
    		    var nearbyhtml = "<h4>Also Consider Nearby Cities:</h4><ul class='nearby-city-list'>";
    		    $.each(data, function(index, val) {
    		    	var prettyName = val.city+", "+val.state;
    		    	nearbyhtml += "<li><a href='#' class='label pc-add-city' data-reload='0' data-slug='"+val.slug+"' data-name='"+prettyName+"'><span class='awe-plus'> </span> "+prettyName+"</a></li>";
    		    });
    		    nearbyhtml += "</ul>";
    		    $('.pc-city-nearby').html(nearbyhtml);
    		  },
    		  error: function(xhr, textStatus, errorThrown) {
    		    //called when there is an error
    		    //
    		  }
    		});
    	}

    });*/

/*    //LEGACY
    $(document).unbind('click.addperformer').on('click.addperformer', '.pc-add-performer', function(event)
    {
        event.preventDefault();
        var pid = $(this).data('perforemrid');
        var performer = $(this).data('performer');
        var reNearby = 0; // $(this).data('reload');
        if ($('#pc-pid-'+pid).length == 0) {
          $("#pc-performer-list").append('<li id="pc-pid-'+pid+'" data-performerid="'+pid+'" data-performer="'+performer+'"><span class="label">'+performer+' <a href="#remove" class="pc-remove-performer" data-liid="pc-pid-'+pid+'"><span class="awe-trash"></span></a></span></li>');
        }
        $(this).addClass('hidden');
        $('.performer-search').val('');
    });*/

    $(document).unbind('click.remove-performer-rule').on('click.remove-performer-rule', '.remove-performer-rule', function(event) {
        event.preventDefault();
        var ruleId = $(this).data('ruleid');
        var type = 'performer';
        var title = $(this).data('pname');
        var $thisli = $(this);
        $.ajax({
            url: '/ajax/remove_rule',
            type: 'POST',
            dataType: 'json',
            data: {ruleId: ruleId, type : type, title : title},
        })
        .done(function(data) {
            if(data.error == 0)
           {
                $('.ruleNotification').html("<div class='alert "+data.type+"'>"+data.message+"</div>");
                $thisli.parent('li').remove();
           }
           else
           {
                $('.ruleNotification').html("<div class='alert "+data.type+"'>"+data.message+"</div>");
                $('.ruleNotification').addClass(data.type);
           }
        })
        .fail(function() {

        })
        .always(function() {

        });

    });

    $(document).unbind('click.remove-city-rule').on('click.remove-city-rule', '.remove-city-rule', function(event) {
        event.preventDefault();
        var ruleId = $(this).data('ruleid');
        var type = 'city';
        var title = $(this).data('pname');
        var $thisli = $(this);
        $.ajax({
            url: '/ajax/remove_rule',
            type: 'POST',
            dataType: 'json',
            data: {ruleId: ruleId, type : type, title : title},
        })
        .done(function(data) {
            if(data.error == 0)
           {
                $('.ruleNotification').html("<div class='alert "+data.type+"'>"+data.message+"</div>");
                $thisli.parent('li').remove();
           }
           else
           {
                $('.ruleNotification').html("<div class='alert "+data.type+"'>"+data.message+"</div>");
                $('.ruleNotification').addClass(data.type);
           }
        })
        .fail(function() {

        })
        .always(function() {

        });

    });

/*    $(document).on('click', '.pc-remove-city', function(event)
    {
        event.preventDefault();
        var liId = $(this).data('liid');
        $('#'+liId).remove();
    });

    $(document).on('click', '.pc-remove-performer', function(event)
    {
        event.preventDefault();
        var liId = $(this).data('liid');
        $('#'+liId).remove();
    });*/

/*    $(document).on('change', '#notify-days-before', function(event) {
        event.preventDefault();
        if($('#notify-days-before').is(':checked'))
        {
            $('#notify-days').prop('disabled', false);
        }
        else
        {
            $('#notify-days').prop('disabled', true);
        }
    });*/

 /*   $(document).unbind('click.add-performer-city-rule').on('click.add-performer-city-rule', '#add-performer-city-rule', function(event) {
        event.preventDefault();

        var perfList = [];
        var cityList = [];
        $.each($('#pc-performer-list li'), function(index, val) {

            perfList.push({
                            'name': $(this).data('performer'),
                            'pId' : $(this).data('performerid')
                        });
        });
        $.each($('#pc-city-list li'), function(index, val) {

            cityList.push({
                            'slug': $(this).data('slug'),
                            'prettyName' : $(this).data('name')
                        });
        });
        var ruleType = 'performer-city';
        var notifyFeed = 0;
        var notifyEmail = 0;
        var notifyAs = 0;
        var notifyDays = 0;
        var notifyDaysNum =  $('#notify-days').val();
        if($('#notify-type-email-feed').is(':checked'))
        {
           notifyEmail = 1;
           notifyFeed = 1;
        }
        if($('#notify-type-feed-only').is(':checked'))
        {
            notifyFeed = 1;
            notifyEmail = 0; //just in case
        }
        if($('#notify-as-occurs').is(':checked'))
        {
            notifyAs = 1;
        }
        if($('#notify-days-before').is(':checked'))
        {
            notifyDays = 1;
        }
        var data = {
                'rule'          :   ruleType,
                'performers'    :   perfList,
                'cities'        :   cityList,
                'notifications' :  {
                                    'feed' : notifyFeed,
                                    'email' : notifyEmail,
                                    'as-occurs' : notifyAs,
                                    'days-before': notifyDays,
                                    'days-num'  : notifyDaysNum
                }
        };
        $.ajax({
            url: '/ajax/add_rule',
            type: 'POST',
            dataType: 'json',
            data: {data: data},
        })
        .done(function(result) {
            cleanUp("performer-city");
            refreshRules();
        })
        .fail(function() {

        })
        .always(function() {

        });


    });*/

/*    $(document).unbind('click.refresh').on('click.refresh', '#refreshRules', function (event) {
        refreshRules();
    });*/
/*
    function cleanUp(type)
    {
        switch(type){
            case 'performer-city':
                $('.performer-search').val('');
                $('#pc-add-performer').addClass('hidden');
                $('#pc-performer-list').html('');
                $('#pc-performer-similar').html('');
                $('.city-search').val('');
                $('#pc-add-city').addClass('hidden');
                $('#pc-city-list').html('');
                $('#pc-city-nearby').html('');
                $('#notify-as-occurs').prop('checked', false);
                $('#notify-days-before').prop('checked', false);
                $('#notify-days').val('30');
            break;

            default:

            break;
        }
    }*/
/*    function canAdd(type)
    {
        switch(type){
            case 'pc':

            if($( "#pc-performer-list" ).has( "li" ).length && $('#pc-city-list').has("li").length)
            {
                $('#add-performer-city-rule').prop('disabled',true);
            } else
            {
                $('#add-performer-city-rule').prop('disabled',false);
            }

            break;
            case 'vg':

            break;


        }
    }
*/
    function refreshRules(which)
    {

        $.ajax({
            url: '/ajax/fetch_rules',
            type: 'POST',
            dataType: 'html',
            data: {rule: which},
        })
        .done(function(data) {

            $('#current-rules-'+which).html(data);
        })
        .fail(function() {

        })
        .always(function() {

        });
    }


});

