$(window).resize(function (){
	 $('#main-masonry').masonry({
		  itemSelector: '.masonryitem',
		  isAnimated: true,
		  animationOptions: {
		    duration: 750,
		    easing: 'linear',
		    queue: false
		  }
		});
	 $('#main-masonry2').masonry({
		  itemSelector: '.masonryitem',
		  isAnimated: true,
		  animationOptions: {
		    duration: 750,
		    easing: 'linear',
		    queue: false
		  }
		});

});
$(window).load(function(){



$('#main-masonry').masonry({
  itemSelector: '.masonryitem'
});
		$('.masonryitem').on('click', function(event) {
		// Act on the event
		var t = $(this).attr('id').split('_');
		var slug = t[1];

	 	if($('#p-'+slug).css('bottom') =='-52px' )
		{
			$('#p-'+slug).addClass("block-detials-hover");
		}else
		{
			$('#p-'+slug).removeClass("block-detials-hover");
		}
		//reload masonary
		// $('#main-masonry').masonry({
		//   itemSelector: '.masonryitem',
		//   isAnimated: true,
		//   animationOptions: {
		//     duration: 750,
		//     easing: 'linear',
		//     queue: false
		//   }
		// });
	});
$('#main-masonry2').masonry({
  itemSelector: '.masonryitem'
});
		$('.masonryitem').on('click', function(event) {
		// Act on the event
		var t = $(this).attr('id').split('_');
		var slug = t[1];

	 	if($('#p-'+slug).css('bottom') =='-52px' )
		{
			$('#p-'+slug).addClass("block-detials-hover");
		}else
		{
			$('#p-'+slug).removeClass("block-detials-hover");
		}
		//reload masonary
		// $('#main-masonry2').masonry({
		//   itemSelector: '.masonryitem',
		//   isAnimated: true,
		//   animationOptions: {
		//     duration: 750,
		//     easing: 'linear',
		//     queue: false
		//   }
		// });
	});
	$('#main-masonry3').masonry({
	  itemSelector: '.masonryitem'
	});
		$('.masonryitem').on('click', function(event) {
		// Act on the event
		var t = $(this).attr('id').split('_');
		var slug = t[1];

	 	if($('#p-'+slug).css('bottom') =='-52px' )
		{
			$('#p-'+slug).addClass("block-detials-hover");
		}else
		{
			$('#p-'+slug).removeClass("block-detials-hover");
		}
		//reload masonary
		$('#main-masonry3').masonry({
		  itemSelector: '.masonryitem',
		  isAnimated: true,
		  animationOptions: {
		    duration: 750,
		    easing: 'linear',
		    queue: false
		  }
		});
	});
});

$(document).ready(function(){

	$('#top_search_m').keyup(function() {
		var value = $(this).val()
		if (value.length <= 2)
			$('#jaxResults_topm').slideUp();
	});
	$( "#top_search_m" ).autocomplete({
		source: function( request, response ) {
			$.ajax({
				url: "/ajax/search_pcv",
				dataType: "html",
				type : "POST",
				data: {
					term : $( "#top_search_m" ).val()
				},
				success: function( data ) {
					$('#jaxResults_topm').html(data);
					$('#jaxResults_topm').slideDown();
				}
			});
		},
		minLength: 3,
		select: function( event, ui ) {
			log( ui.item ?
			"Selected: " + ui.item.label :
			"Nothing selected, input was " + this.value);
		},
		open: function() {
			$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
		},
		close: function() {
			$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		}
	});

	$('#top_search_d').keyup(function() {
		var value = $(this).val()
		if (value.length <= 2)
			$('#jaxResults_topd').slideUp();
	});
	$( "#top_search_d" ).autocomplete({
		source: function( request, response ) {
			$.ajax({
				url: "/ajax/search_pcv",
				dataType: "html",
				type : "POST",
				data: {
					term : $( "#top_search_d" ).val()
				},
				success: function( data ) {
					$('#jaxResults_topd').html(data);
					$('#jaxResults_topd').slideDown();
				}
			});
		},
		minLength: 3,
		select: function( event, ui ) {
			log( ui.item ?
			"Selected: " + ui.item.label :
			"Nothing selected, input was " + this.value);
		},
		open: function() {
			$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
		},
		close: function() {
			value.length
			$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		}
	});

	$('#big_search_home').keyup(function() {
		var value = $(this).val()
		if (value.length <= 2)
			$('#big_jaxResults').slideUp();
	});
	$( "#big_search_home" ).autocomplete({
		source: function( request, response ) {
			$.ajax({
				url: "/ajax/search_pcv",
				dataType: "html",
				type : "POST",
				data: {
					term : $( "#big_search_home" ).val()
				},
				success: function( data ) {
					$('#big_jaxResults').html(data);
					$('#big_jaxResults').slideDown();
				}
			});
		},
		minLength: 3,
		select: function( event, ui ) {
			log( ui.item ?
			"Selected: " + ui.item.label :
			"Nothing selected, input was " + this.value);
		},
		open: function() {
			$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
		},
		close: function() {
			$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		}
	});
});
