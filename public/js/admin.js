$(document).ready(function(){

	$(document).on('keyup', '#querysearch', function(event) {
		event.preventDefault();
		var string = $('#querysearch').val();
		if(string.length > 1){
		$.ajax({
				url: '/ajax/search_performer',
				type: 'POST',
				dataType: 'html',
				data: {query: string},
			})
			.done(function(htmlData) {
				$('#search-results').html(htmlData);
			})
			.fail(function() {

			})
			.always(function() {

			});
		}else{
			$('#search-results').html("- no results - ")
		}

		/* Act on the event */
	});


	$(document).on('keyup', '#querysearch_tour', function(event) {
		event.preventDefault();
		var string = $('#querysearch_tour').val();
		if(string.length > 1){
		$.ajax({
				url: '/ajax/search_performer/tour',
				type: 'POST',
				dataType: 'html',
				data: {query: string},
			})
			.done(function(htmlData) {
				$('#search-results').html(htmlData);
			})
			.fail(function() {

			})
			.always(function() {

			});
		}else{
			$('#search-results').html("- no results - ")
		}

		/* Act on the event */
	});

	$(document).on('click', '#lastfm-fb-image', function(event) {
		// event.preventDefault();

		$.ajax({
			url: '/ajax/change_performer_images_by_id',
			type: 'POST',
			dataType: 'html',
			data: {'id': $('#performerid').val(), 'type':'last.fm', 'image': $("#lastfm-image").val()},
		})
			.done(function(htmlData) {
				$('#performer-edit-panel').html(htmlData);
			})
			.fail(function() {

			})
			.always(function() {

			});

		/* Act on the event */
	});

	$(document).on('click', '#btn-fb-image', function(event) {
		// event.preventDefault();

		$.ajax({
			url: '/ajax/change_performer_images_by_id',
			type: 'POST',
			dataType: 'html',
			data: {'id': $('#performerid').val(), 'type':'fb', 'image': $("#fb-image").val()},
		})
			.done(function(htmlData) {
				$('#performer-edit-panel').html(htmlData);
			})
			.fail(function() {

			})
			.always(function() {

			});

		/* Act on the event */
	});

	$(document).on('click', '.performer-result', function(event) {
		// event.preventDefault();
		var performerID = $(this).data('performerid');

		$.ajax({
			url: '/ajax/get_performer_by_id',
			type: 'POST',
			dataType: 'html',
			data: {id: performerID},
		})
		.done(function(htmlData) {
			$('#performer-edit-panel').html(htmlData);
		})
		.fail(function() {

		})
		.always(function() {

		});

		/* Act on the event */
	});
	$(document).on('click', '.resetDefaultText', function () {
		$.ajax({
				url: '/ajax/generate_performer_text',
				type: 'post',
				data: {slug : $(this).data('slug')},
		})
		.done(function(htmlData){
			$('#post-text').val(htmlData);
			$('#post-text').css({'border' : '4px yellow solid'});
			$('.clickToSave').html("<b style='color:red;'>Click the Update button to save text</b>");
		});

	});

	$(document).on('change', '.toggle_area', function () {
		var id = $(this).data('id');
		var area = $(this).val();

		$.ajax({
				url: '/ajax/update_top_city',
				type: 'post',
				data: {id : id, area : area},
		})
		.done(function(htmlData){
			$('#saved-'+id).html(htmlData);
		});

	});

	$(document).on('click', '.removeCity', function () {
		var id = $(this).data('id');
		$.ajax({
				url: '/ajax/delete_city',
				type: 'post',
				data: {id : id},
		})
		.done(function(htmlData){
			$('#tr-'+id).css("background-color", 'red');
			$('#tr-'+id).fadeOut(600, function() { $('#tr-'+id).remove(); });
			 // $('#tr-'+id).remove();
		});

	});



	$(document).on('click', '.feat', function () {
		var feat = $(this).is(':checked');
		var type = $(this).data('feattype');
		var id   = $(this).data('performerid');
		$.ajax({
			url: '/ajax/toggle_featured',
			type: 'POST',
			dataType: 'HTML',
			data: {status: feat, type : type, id : id},
		})
		.done(function(htmlData) {
			$('.featUpdated-'+id).html(htmlData);
		})
		.fail(function() {

		})
		.always(function() {

		});


	});

	$(document).on('click','.delete-picture', function () {
		var pid = $(this).data('pid');
		var img = $(this).data('img');
		var delBtn = $(this);

		// conso.log('d');
		$.ajax({
				url: '/ajax/remove_picture',
				type: 'post',
				data: {pid : pid, type : img},
			})
		.done(function(htmlData) {
			//picture deleted
			delBtn.remove();
			$('#img-'+img).attr('src','/public/img/icons/noimage.png');
			$('.notice-'+img).html(htmlData);
			$('.'+pid+'-img-'+img).attr('src','/public/img/icons/noimage.png');
		})
		.fail(function() {

		})
		.always(function() {

		});
	});

	$(document).on('click','.feat-radio', function () {
		var type = $(this).data('type');
		var id = $(this).data('pid');
		var color = '#FFFFFF';
		switch(type)
		{
		case 'nofeat':
			color = '#FFFFFF';
		  break;
		case 'geo':
			color = '#C1E0FF';
		  break;
		case 'both':
			color = '#CEFFE7';
		  break;
		}
		$.ajax({
				url: '/ajax/toggle_featured',
				type: 'post',
				data: {type : type, id : id},
				})
			.done(function(htmlData){
				$('.featUpdated').html(htmlData);
				$('#th-'+id).css('background-color',color);
				$('#1_th-'+id).css('background-color',color);
			});
	});

	// datepicker
	$( ".datepicker" ).datepicker({
	                                "dateFormat" : "yy-mm-dd"
	                              });

 /*   $('#file_upload').uploadifive({
		'formData'     : {
			'timestamp' : '3245435',
			'token'     : '34543t53regrefdg45wy645rgbvsaf',
		},
		'auto'             : true,
		'queueID'          : 'queue',
		 'multi'        : false,
		'fileType'     : 'image',
		'uploadScript' : '/ajax/upload_image',
		'onUploadComplete' : function(file, data) {

		},
		'onUploadError' : function(file, errorCode, errorMsg, errorString) {
			alert('The file ' + file.name + ' could not be uploaded: ' + errorString);

		 },
		'onUploadSuccess' : function(file, data, response) {


		}
	});*/


/*	$('#post-text').wysiwyg({
    controls: {
      strikeThrough : { visible : true },
      underline     : { visible : true },


      justifyLeft   : { visible : true },
      justifyCenter : { visible : true },
      justifyRight  : { visible : true },
      justifyFull   : { visible : true },


      indent  : { visible : true },
      outdent : { visible : true },


      subscript   : { visible : true },
      superscript : { visible : true },


      undo : { visible : true },
      redo : { visible : true },


      insertImage			: {visible : false,},
      insertLink			: {visible : false,},
      insertOrderedList    : { visible : true },
      insertUnorderedList  : { visible : true },
      insertHorizontalRule : { visible : false },

      h4mozilla : { visible : true && $.browser.mozilla, className : 'h4', command : 'heading', arguments : ['h4'], tags : ['h4'], tooltip : "Header 4" },
      h5mozilla : { visible : true && $.browser.mozilla, className : 'h5', command : 'heading', arguments : ['h5'], tags : ['h5'], tooltip : "Header 5" },
      h6mozilla : { visible : true && $.browser.mozilla, className : 'h6', command : 'heading', arguments : ['h6'], tags : ['h6'], tooltip : "Header 6" },

      // h4 : { visible : true && !( $.browser.mozilla ), className : 'h4', command : 'formatBlock', arguments : ['<H4>'], tags : ['h4'], tooltip : "Header 4" },
      // h5 : { visible : true && !( $.browser.mozilla ), className : 'h5', command : 'formatBlock', arguments : ['<H5>'], tags : ['h5'], tooltip : "Header 5" },
      // h6 : { visible : true && !( $.browser.mozilla ), className : 'h6', command : 'formatBlock', arguments : ['<H6>'], tags : ['h6'], tooltip : "Header 6" },


      cut   : { visible : true },
      copy  : { visible : true },
      paste : { visible : true }
    }
  });
*/
	$('input[name="search"]').keyup(function(){

			var searchterm = $(this).val();

			if(searchterm.length >2) {
				var match = $('tr.data-item:contains("' + searchterm + '")');
				var nomatch = $('td.data-name:not(:contains("' + searchterm + '"))');

				// match.addClass('selected');
				nomatch.parent().css("display", "none");
				$('.heads').css("display", "none");
			} else {
				$('.heads').css("display", "");
				$('tr.data-item').css("display", "");
				// $('tr.data-item').removeClass('selected');
			}

		});

    $('._changeStatus').on('change', function(){
        var item = $(this);

        $.ajax({
            url: '/admin/_ajax/users/status',
            type: 'POST',
            data : {id : item.data('id'), status : item.val()},
            success : function(response){
                if(! response.code) alert('There was an error changing user status');
            },
            beforeSend: function(){},
            complete: function(){}
        });
    });

});


jQuery.expr[':'].contains = function(a,i,m){
    return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase())>=0;
};
