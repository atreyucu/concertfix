var MobileTickets = (function($) {
  var Settings = {},
  Public = {},
  App = {};
  Settings = {
    debug: true
  };
  function _log(what) {
    if(Settings.debug && console.log) {
      console.log(what);
    }
  }

  App = {
    detectCondition: function(condition, callback, interval) {
      var intervalID = setInterval(function() {
        if(condition) {
          clearInterval(intervalID);
          callback();
        }
      }, interval);
    },
    fixCSS: function() {
      $('td.tn_results_header_title,td.tn_results_header_venue,td.tn_results_header_datetime')
      .css('fontSize', '14px');

    },
    fixIMG: function() {
      $('img[src^=tn]').each(function(idx, el) {
        el.src = el.src.replace('tn_images', '../public/img/tn_images');
        el.style.width = '12px';
        el.style.height = '12px';
      });
    },
    init: function() {
      // return;
      $.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
      if(!$.browser.device) return;
      // Fix Map
      App.detectCondition(
        function() {
          return !!$('.tn_results_header_maplink');
        },
        function() {
          var url = $('.tn_results_header_maplink a').attr('onclick');
          var numbers = /(\d+)/g;
          var matches = url.match(numbers);
          $.ajax({
            url: '/ajax/venue_image',
            type: 'GET',
            dataType: 'json',
            data: {
              venue_id: matches[0],
              event_id: matches[1]
            },
          }).done(function(res) {
            $('.tn_results_header_maplink').remove();
            $('td.tn_results_header_divider:eq(1)').html(
              $('<img>').attr('src', res.image)
            ).css('paddingBottom', '20px');
          });
        },
        200
      );
      // Fix Tickets Text
      App.detectCondition(
        function() {
          return !!$('.tn_results_ticket_purchase');
        },
        function() {
          var $el = $('.tn_results_ticket_purchase a');
          $el.text('Buy').addClass('btn');
        },
        200
      );
      App.detectCondition(
        function() {
          console.log(!!$('img[src^=tn]'));
          return !!$('img[src^=tn]');
        },
        App.fixIMG, 200
      );
      App.fixCSS();
    }
  };

  Public = {
    init: function() {
      _log("STARTING MOBILE TICKETS");
      App.init();
    }
  };
  return Public;
})(jQuery);

jQuery(document).ready(MobileTickets.init);



