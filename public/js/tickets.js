jQuery(document).ready(function(){

    $('#top_search_m').keyup(function() {
        var value = $(this).val()
        if (value.length <= 2)
            $('#jaxResults_topm').slideUp();
    });
    $( "#top_search_m" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "/ajax/search_pcv",
                dataType: "html",
                type : "POST",
                data: {
                    term : $( "#top_search_m" ).val()
                },
                success: function( data ) {
                    $('#jaxResults_topm').html(data);
                    $('#jaxResults_topm').slideDown();
                }
            });
        },
        minLength: 3,
        select: function( event, ui ) {
            log( ui.item ?
            "Selected: " + ui.item.label :
            "Nothing selected, input was " + this.value);
        },
        open: function() {
            $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function() {
            $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });

    $('#top_search_d').keyup(function() {
        var value = $(this).val()
        if (value.length <= 2)
            $('#jaxResults_topd').slideUp();
    });
    $( "#top_search_d" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "/ajax/search_pcv",
                dataType: "html",
                type : "POST",
                data: {
                    term : $( "#top_search_d" ).val()
                },
                success: function( data ) {
                    $('#jaxResults_topd').html(data);
                    $('#jaxResults_topd').slideDown();
                }
            });
        },
        minLength: 3,
        select: function( event, ui ) {
            log( ui.item ?
            "Selected: " + ui.item.label :
            "Nothing selected, input was " + this.value);
        },
        open: function() {
            $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function() {
            value.length
            $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
});