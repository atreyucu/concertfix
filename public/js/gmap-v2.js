function initMap() {
	var geocoder;
	var map;

	geocoder = new google.maps.Geocoder();
	var latlng = new google.maps.LatLng(41.879535, -87.624333);
	var mapOptions = {
		zoom: 8,
		center: latlng
	}
	map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);


	var address = document.getElementById('address').value;
	geocoder.geocode({'address': address}, function (results, status) {
		if (status == 'OK') {
			map.setCenter(results[0].geometry.location);
			var marker = new google.maps.Marker({
				map: map,
				position: results[0].geometry.location
			});
		} else {
			alert('Geocode was not successful for the following reason: ' + status);
		}
	});

}
