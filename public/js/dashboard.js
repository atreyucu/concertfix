	jQuery(document).ready(function($) {

		$('.ruleNotification').bind('DOMSubtreeModified', function(event) {
	       setTimeout(function () { $(".ruleNotification").html("") }, 5000);
	    });

	    $(document).on('click.addSingleRule', '.addSingleRule', function(event) {
	    	event.preventDefault();
	    	var sendData = {
	            ruleType       :   'performer',
	            performerId     :   $(this).data('id'),
	            performerName   :   $(this).data('name'),
	        };
	        $.ajax({
	            url: '/ajax/add_rule',
	            type: 'POST',
	            dataType: 'json',
	            data: {rule: sendData},
	        })
	        .done(function(data) {
	           if(data.error == 0)
	           {
	                $('.ruleNotification').html("<div class='alert alert-"+data.type+"'>"+data.message+"</div>");
		                $('#add-'+sendData.performerId).remove();

	           }
	           else
	           {
	                $('.ruleNotification').html("<div class='alert alert-"+data.type+"'>"+data.message+"</div>");
	                $('.ruleNotification').addClass(data.type);
	           }
	        })
	        .fail(function() {

	        })
	        .always(function() {

	        });

	      /*  var pId = $(this).data('perforemrid');
	        var pName = $(this).data('performer');
	        var pSlug = $(this).data('slug');*/
	        /* Act on the event */
	    });

		$(document).on('click.showAll', '#showAll', function(event) {
			event.preventDefault();
			$('.hidden-member').toggle('slow');
			if($(this).data('all') == 0)
			{
				$(this).data('all',1);
				$(this).html("Collapse");
			}else
			{
				$(this).data('all',0);
				$(this).html("Show All");
			}

			/* Act on the event */
		});


		$(document).on('click.dropSingleRule', '.dropSingleRule', function(event) {
			event.preventDefault();
			var rId = $(this).data('id');
			var name = $(this).data('name');

			$('#dialog-confirm').html("Would you like to UNFOLLOW "+name+"?")
			$( "#dialog-confirm" ).dialog({
		      resizable: false,
		      height:140,
		      modal: true,
		      title : name,
		      buttons: {
		        "Stop Tracking": function() {
		          $( this ).dialog( "close" );
		          $.ajax({
		          	url: '/ajax/remove_rule',
		          	type: 'POST',
		          	dataType: 'json',
		          	data: {ruleId: rId, type : 'performer', title : name},
		          })
		          .done(function(data) {
		            if(data.error == 0)
		           {
		                $('.ruleNotification').html("<div class='alert alert-"+data.type+"'>"+data.message+"</div>");
		                $('#'+rId+'-track').remove();
		           }
		           else
		           {
		                $('.ruleNotification').html("<div class='alert alert-"+data.type+"'>"+data.message+"</div>");
		                $('.ruleNotification').addClass(data.type);
		           }
		        })
		          .fail(function() {

		          })
		          .always(function() {

		          });

		        },
		        Cancel: function() {
		          $( this ).dialog( "close" );
		        }
		      }
		    });
			/* Act on the event */
		});
	});