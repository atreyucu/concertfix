$(document).ready(function($) {

	var validated = false;
	$(document).on('click', '#submit-registration-form', function(event) {
		event.preventDefault();
		/* Act on the event */
	});

	$(document).on('blur', '#reg-password2', function(event) {
		$('.reg-password2-group').removeClass('error');
		$('.reg-password2-error').html("");
		var p1 = $('#reg-password').val();
		var p2 = $('#reg-password2').val();
		if(p1 != p2)
		{
			$('.reg-password2-group').addClass('error');
			$('.reg-password2-error').html("Passwords do not match!");
		}
		else
		{
			validated = true;
		}
	});
	$(document).on('blur', '#reg-email', function(event) {
		event.preventDefault();
		$('.reg-email-group').removeClass('error');
		$('.reg-email-error').html("");
		var email = $('#reg-email').val();
		if(email.length > 2)
		{
			$.ajax({
				url: '/ajax/userexist',
				type: 'POST',
				dataType: 'json',
				data: {email: email},
			})
			.done(function(data) {
				if(data > 0)
				{
					$('.reg-email-group').addClass('error');
					$('.reg-email-error').html("This account ("+email+") already exists!");
				}
				else
				{
					validated = true;
				}
			})
			.fail(function() {

			})
			.always(function() {

			});
		}
		/* Act on the event */
	});

	$(document).on('change', '.validator', function(event) {
		event.preventDefault();
		if(validateForm())
		{
			$('#submit-registration-form').prop('disabled', false);

			return true;
		}else
		{
			$('#submit-registration-form').prop('disabled', true);

			return false;
		}

	});

	$(document).on('click', '#submit-registration-form', function(event) {
		event.preventDefault();
		if(validateForm())
		{
			var userdata = {};
			userdata.name = $('#reg-name').val();
			userdata.email = $('#reg-email').val();
			userdata.password = $('#reg-password').val();
			userdata.bday = $('#reg-year').val()+"-"+$('#reg-month').val()+"-"+$('#reg-day').val();
			userdata.gender = $('input[name=gender]:checked', '#reg-form').val();
			$.ajax({
				url: '/ajax/register',
				type: 'post',
				dataType: 'json',
				data: {data: userdata},
			})
			.done(function(data) {
				if(data.error == 0)
				{
					var html = "<div class='alert "+data.type+"'><p>"+data.message+"</p></div>";
					$('#reg-field').html(html);

				}else
				{
					var html = "<div class='alert "+data.type+"'><p>"+data.message+"</p></div>";
					html += $('#reg-field').html();
					$('#reg-field').html(html);
				}
			})
			.fail(function() {

			})
			.always(function() {

			});
			$('#submit-registration-form').prop('disabled', false);
		}else
		{
			$('#submit-registration-form').prop('disabled', true);
			alert("Somthing is missing on the form");
		}
		/* Act on the event */
	});

	function validateForm()
	{
		var name = $('#reg-name').val();
		var email = $('#reg-email').val();
		var p1 = $('#reg-password').val();
		var p2 = $('#reg-password2').val();
		var vName = false;
		var vPassMatch = false;
		var vPass = false;
		var vEmail = false;
		vName = (name.length > 0 ? true : false);
		vEmail = validateEmail(email);
		vPass = (p1.length > 3 ? true : false);
		vPassMatch = ((p1 == p2 && p1.length) ? true : false);
		if(vName && vPass && vEmail && vPassMatch)
		{
			return true;
		}else
		{
			return false;
		}
	}


	function validateEmail($email)
	{
		if($email.length == 0) return false;
		var emailReg = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if( !emailReg.test( $email ) ) {
			return false;
		} else {
			return true;
		}
	}

});