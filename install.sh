yum -y update
yum -y install php php-devel php-pear mysql-server python git ImageMagick php-mcrypt* php-pdo* php-mysql php-curl php-soap
yum -y install gcc gcc-c++ autoconf automake
yum -y install nano mlocate

pecl install Xdebug

curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

updatedb
service httpd start
service mysqld start