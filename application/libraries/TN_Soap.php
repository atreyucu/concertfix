<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Layout class for templating
 *
 */
class TN_soap
{

     private $_CI;
     private $_WSD;
     private $_BID;
     private $_TESTMODE;
     private $_soapy;

    function __construct()
    {
        ini_set('memory_limit', '-1'); // JUST IN CASE WE HIT HUGE DATA
        $this->_obj =& get_instance();
        $this->_obj->load->config('soaptn');

        $this->_WSD = $this->_obj->config->item('wsd');
        $this->_BID = $this->_obj->config->item('bid');
        $this->_webID = $this->_obj->config->item('webconfigID');
        $this->_TESTMODE = $this->_obj->config->item('soapMode');
        try {
                $this->_soapy = new SoapClient($this->_WSD);
              } catch (SoapFault $e) {

                //redirect('/maintenance', 302);
              }
        // $this->_WSD = "http://tnwebservices.ticketnetwork.com/tnwebservice/v3.2/WSDL/tnwebservice.xml";
    }


    public function run_soap($method, $params = array())
    {
        $time_start = microtime(true);

        $params['websiteConfigID'] = (int)$this->_webID;
        switch ($method) {
            case 'SearchEvents':
                try
                {

                    $result = $this->_soapy->__soapCall("SearchEvents", array('parameters' => $params));
                    $holder = $result->SearchEventsResult;
                    if(property_exists($holder, 'Event')):
                        $arrEvents = $holder->Event;
                        //now lets see how many events do we have here
                        if(is_array($arrEvents)): //yep, TN brought us an array
                            $returnValue = $arrEvents;
                        else: //Nope, TN was a cunt and returned and object
                            $temporaryArray[0] = $arrEvents; // simulate a fucking array because TN is a bitch
                            $returnValue = $temporaryArray;
                        endif;
                    else:
                        $returnValue = false; //let's not torture foreach loop in the view
                    endif;
                }
                catch (SoapFault $e)
                {
                //    var_dump($e);
                redirect('/maintenance', 302);

                    // $returnValue = $e;
                }
            break;
            case 'GetEvents':
                try
                {
                    $result = $this->_soapy->__soapCall("GetEvents", array('parameters' => $params));
                    $holder = $result->GetEventsResult;
                    if(property_exists($holder, 'Event')):
                        $arrEvents = $holder->Event;
                        //now lets see how many events do we have here
                        if(is_array($arrEvents)): //yep, TN brought us an array
                            $returnValue = $arrEvents;
                        else: //Nope, TN was a cunt and returned and object
                            $temporaryArray[0] = $arrEvents; // simulate a fucking array because TN is a bitch
                            $returnValue = $temporaryArray;
                        endif;
                    else:
                        $returnValue = false; //let's not torture foreach loop in the view
                    endif;

                }
                catch (SoapFault $e)
                {
                //redirect('/maintenance', 302);
                    if (isset($_SESSION['soap_fault'])) {
                        unset($_SESSION['soap_fault']);
                    }
                    else {
                        $_SESSION['soap_fault'] = true;
                        if (isset($_SESSION['uri_string']))
                            redirect($_SESSION['uri_string']);
                    }
                    var_dump($e);
                    redirect('/maintenance', 302);

                }
            break;
            case 'GetEventPerformers' :
                try
                {
                    $result = $this->_soapy->__soapCall("GetEventPerformers", array('parameters' => $params));
                    $returnValue = $result->GetEventPerformersResult;
                }
                catch (SoapFault $e)
                {
                    redirect('/maintenance', 302);

                    die("SOAP ERROR OCCURRED: ".$e->message);
                }
            break;
            case 'GetPerformerByCategory' :
                $count = 5;
                $message = '';
                do{

                    try{
                        $result = $this->_soapy->__soapCall("GetPerformerByCategory", array('parameters' => $params));
                        $returnValue = $result->GetPerformerByCategoryResult;
                        break 2;
                    } catch (SoapFault $e){
                        $count -= 1;
                        $message = $e->getMessage();
                        sleep(2);
                    }

                } while($count > 0);

                if ($count <= 0) {
                    redirect('/maintenance', 302);
                    die("SOAP ERROR OCCURRED: ".$message);
                }

                /*try
                {

                    $result = $this->_soapy->__soapCall("GetPerformerByCategory", array('parameters' => $params));
                    $returnValue = $result->GetPerformerByCategoryResult;
                }
                catch (SoapFault $e)
                {

                    redirect('/maintenance', 302);

                    die("SOAP ERROR OCCURRED: ".$e->message);
                }*/
            break;
            case 'GetVenue':
                $result = $this->_soapy->__soapCall("GetVenue", array('parameters' => $params));
                $returnValue = $result->GetVenueResult->Venue;

            break;

            case 'GetVenueConfigurations':
                $result = $this->_soapy->__soapCall("GetVenueConfigurations", array('parameters' => $params));
                $returnValue = $result->GetVenueConfigurationsResult;
            break;
            case 'GetEventTickets':
                try {
                    // var_dump($params);
                    // legacy
                    // $result = $this->_soapy->__soapCall("GetTickets", array('parameters' => $params));
                    // $tmp =  $result->GetTicketsResult;
                    // var_dump($tmp);
                    //GetEventTickets - NEW!!!
                    unset($params['websiteConfigID']);
                    $params['websiteConfigId'] = (int)$this->_webID;
                    $params['translationLanguageId'] = 0;
                    $result = $this->_soapy->__soapCall("GetEventTickets2", array('parameters' => $params));
                    // var_dump($result);
                   $tmp =  $result->GetEventTickets2Result->Tickets;
                    // var_dump($tmp);

                    if(is_array($tmp->TicketGroup2)):
                        $returnValue = $tmp->TicketGroup2;
                    else:
                        $holder = array();
                        $holder[0] = $tmp->TicketGroup2;
                        $returnValue = $holder;
                    endif;
                } catch (SoapFault $e) {
                    redirect('/maintenance', 302);

                     var_dump($e);
                }
            break;

            case 'GetPricingInfo':
                try {

                    $params['websiteConfigId'] = (int)$this->_webID;
                    $result = $this->_soapy->__soapCall("GetPricingInfo", array('parameters' => $params));

                    if(isset($result->GetPricingInfoResult)):
                        $returnValue = $result->GetPricingInfoResult;
                    else:
                        $returnValue = false;
                    endif;
                } catch (SoapFault $e) {
                    redirect('/maintenance', 302);

                     var_dump($e);
                }
            break;

            case 'GetCategoriesMasterList':
                try {

                    // $params['websiteConfigId'] = (int)$this->_webID;
                    $result = $this->_soapy->__soapCall("GetCategoriesMasterList", array('parameters' => $params));

                    if(isset($result->GetCategoriesMasterListResult)):
                        $returnValue = $result->GetCategoriesMasterListResult;
                    else:
                        $returnValue = false;
                    endif;
                } catch (SoapFault $e) {
                    redirect('/maintenance', 302);

                     var_dump($e);
                }
            break;
            //GetHighSalesPerformers
            case 'GetHighSalesPerformers':
                try {

                    // $params['websiteConfigId'] = (int)$this->_webID;
                    $result = $this->_soapy->__soapCall("GetHighSalesPerformers", array('parameters' => $params));

                    if(isset($result->GetHighSalesPerformersResult)):
                        $returnValue = $result->GetHighSalesPerformersResult;
                    else:
                        $returnValue = false;
                    endif;
                } catch (SoapFault $e) {
                    echo $e->getMessage();
                    redirect('/maintenance', 302);

                     var_dump($e);
                }
            break;
            //GetHighInventoryPerformers
            case 'GetHighInventoryPerformers':
                try {

                    // $params['websiteConfigId'] = (int)$this->_webID;
                    $result = $this->_soapy->__soapCall("GetHighInventoryPerformers", array('parameters' => $params));

                    if(isset($result->GetHighInventoryPerformersResult)):
                        $returnValue = $result->GetHighInventoryPerformersResult;
                    else:
                        $returnValue = false;
                    endif;
                } catch (SoapFault $e) {
                    redirect('/maintenance', 302);

                     var_dump($e);
                }
            break;
            default:
                # code...
                break;




        }
        $obj =& get_instance();

        $endTime = (microtime(true) - $time_start);
        // echo "SOAP EXEC TIME ".$endTime;
        $obj->config->set_item('soap_exec_time', $endTime);
        return $returnValue;
    }

}

