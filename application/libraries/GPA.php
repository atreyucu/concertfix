<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('vendor/autoload.php');

class GPA extends Google_Client
{
    public function __construct()
    {
        parent::__construct();

    }

    public function factory()
    {
        return new Google_Client();
    }

    public function plusFactory($client)
    {
        return new Google_Oauth2Service($client);
    }
}