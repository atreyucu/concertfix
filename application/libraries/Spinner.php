<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Spinner
{

	public function __construct()
	{
		$CI =& get_instance();
	   	$CI->load->model('site/spinner_m');  //<-------Load the Model first
	}

	public function get_performer_text($pid, $refresh = false)
	{


		//get active guests form today on
 		$guests = $this->spinner_m->get_guests($pid, false);
 		$dbData = $this->spinner_m->get_db_text($pid);

 		if($dbData):
 			//check if guests equal
 			if($dbData->GuestCount != count($guests)):
 				$text = $this->spinner_m->generate_performer_text($pid, $guests);
 				$this->spinner_m->update_performer_text($pid, $text, count($guests));
 			else:
 				if($refresh):
					$text = $this->spinner_m->generate_performer_text($pid, $guests);
 					$this->spinner_m->update_performer_text($pid, $text, count($guests));
 				else:
 					$text = $dbData->PerformerText;
				endif;
 			endif;
 		else:
 			$text =	$this->spinner_m->generate_performer_text($pid, $guests);
 			$this->spinner_m->save_performer_text($pid, $text, count($guests));
 		endif;

 		return $text;
		//return $this->spinner_m->get_performer_text($pid);
	} 
}