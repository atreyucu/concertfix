<?php ob_start(); if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class UserLib
{
  	protected $ci;
  	private $cfUser;

	private $_fb;

	private $_tw;
	private $_ck;
	private $_cs;
	private $_cb;
	private $_twToken;
	private $_twConnection;
	private $_gplus;
	private $userData;


	public function __construct()
	{
		//load CI instance
        $this->ci =& get_instance();

        //get configs
        $this->ci->config->load("facebook",TRUE);
        $this->ci->config->load("twitter", TRUE);
		$fbconfig = $this->ci->config->item('facebook');
		$twconfig = $this->ci->config->item('twitter');

		$this->_ck = $twconfig['consumer_key'];
		$this->_cs = $twconfig['consumer_secret'];
		$this->_cb = $twconfig['callback_url'];
		//load libs
		$this->ci->load->library('facebookapi/Facebook', $fbconfig);
		$this->ci->load->library('twitterapi/Twitteroauth');
        $this->ci->load->library('GPA');

		//set local refs
		$this->_fb = $this->ci->facebook;
		$this->_tw = $this->ci->twitteroauth;
        $this->_gpa = $this->ci->gpa;
        $this->_gpaClient = $this->_gpa->factory();
        $this->_gplus = $this->_gpa->plusFactory($this->_gpaClient);

 		$this->ci->load->model('user_m');
	}

	public function getUser($network = false, $params = false)
	{
		$fbUser = false;
		$twUser = false;
		$cfUser = false;
		$gUser = false;
		$loggedin = false;
		$no_email = false;
		$f_time = 0;
		$fbArray = array();
        switch ($network) :
            case 'facebook':
                echo "ffff";
                //exit;
                // Include the required Composer dependencies.
                require_once __DIR__ . '/Facebook/autoload.php';
                // Initialize the Facebook PHP SDK v5.
                $fb = new Facebook\Facebook([
                    'app_id' => '376715072474405',
                    'app_secret' => 'c10ac685f96fd3a39744aff64b46c35d',
                    'default_graph_version' => 'v3.2',
                ]);
                $helper = $fb->getRedirectLoginHelper();
                //
                $_SESSION['FBRLH_state'] = $_GET['state'];
                try {
                    $accessToken = $helper->getAccessToken();
                } catch (Facebook\Exceptions\FacebookResponseException $e) {
                    // When Graph returns an error
                    echo 'Graph returned an error: ' . $e->getMessage();
                    exit;
                } catch (Facebook\Exceptions\FacebookSDKException $e) {
                    // When validation fails or other local issues
                    echo 'Facebook SDK returned an error: ' . $e->getMessage();
                    exit;
                }

                if (!isset($accessToken)) {
                    if ($helper->getError()) {
                        header('HTTP/1.0 401 Unauthorized');
                        echo "Error: " . $helper->getError() . "\n";
                        echo "Error Code: " . $helper->getErrorCode() . "\n";
                        echo "Error Reason: " . $helper->getErrorReason() . "\n";
                        echo "Error Description: " . $helper->getErrorDescription() . "\n";
                    } else {
                        header('HTTP/1.0 400 Bad Request');
                        echo 'Bad request';
                    }
                    exit;
                }

                // Logged in
                echo '<h3>Access Token</h3>';
                var_dump($accessToken->getValue());
                //echo $accessToken->getValue();

                // The OAuth 2.0 client handler helps us manage access tokens
                $oAuth2Client = $fb->getOAuth2Client();

                // Get the access token metadata from /debug_token

                //$tokenMetadata = $oAuth2Client->debugToken($accessToken);
                /////echo '<h3>Metadata</h3>';
                /////var_dump($tokenMetadata);

                // Validation (these will throw FacebookSDKException's when they fail)
                /////$tokenMetadata->validateAppId($config['app_id']);

                // If you know the user ID this access token belongs to, you can validate it here
                //$tokenMetadata->validateUserId('123');
                //////$tokenMetadata->validateExpiration();

                if (!$accessToken->isLongLived()) {
                    // Exchanges a short-lived access token for a long-lived one
                    try {
                        $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
                    } catch (Facebook\Exceptions\FacebookSDKException $e) {
                        echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
                        exit;
                    }

                    echo '<h3>Long-lived</h3>';
                    var_dump($accessToken->getValue());
                }

                $_SESSION['fb_access_token'] = (string)$accessToken;
                //echo "<br>".$_SESSION['fb_access_token'];

                // User is logged in with a long-lived access token.
                try {
                    // Returns a `Facebook\FacebookResponse` object
                    $response = $fb->get('/me?fields=id,first_name,last_name,email,link,gender,locale', $_SESSION['fb_access_token']);
                } catch (Facebook\Exceptions\FacebookResponseException $e) {
                    echo 'Graph returned an error: ' . $e->getMessage();
                    exit;
                } catch (Facebook\Exceptions\FacebookSDKException $e) {
                    echo 'Facebook SDK returned an error: ' . $e->getMessage();
                    exit;
                }
                $fbUser = $response->getGraphUser();
                echo "tttt";
                //exit;
                //$_SESSION['uid'] = $user['id'];
                //////////////////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////////////////////////
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                /*$user = $this->_fb->getUser();
                if ($user) {
                    try {
                        $fbUser = $this->_fb->api('/me');
                    } catch (FacebookApiException $e) {
                        $this->logErr($e);
                        $fbUser = false;
                    }
                }*/

                if ($fbUser) :
                    if (!isset($fbUser['email'])) {
                        $result = $this->ci->user_m->insert_facebook_id($fbUser['id'], $fbUser['id'] . '@facebook.com');
                        if ($result) {
                            $no_email = true;
                            $fbUser['email'] = $fbUser['id'] . '@facebook.com';
                        } else {
                            $result = $this->ci->user_m->get_facebook_id($fbUser['id']);
                            if (!$result->changed)
                                $no_email = true;
                            $fbUser['email'] = $result->primary_email;
                        }
                    }
                    echo "taaattt";
                    //	exit;
                    $email = $fbUser['email'];
                    //check if this user is already with us:
                    if ($this->ci->user_m->email_exists($fbUser['email'], 'facebook')) :
                        //do nothing
                        echo "nothing";
                    else: //we logging in first time
                        $this->ci->user_m->insert_user($fbUser, 'facebook');
                        $f_time = 1;
                        echo "insert";
                    endif;
                    //log user in
                    echo "loggedin";
                    $loggedin = true;

                    $cfUser = $this->ci->user_m->get_user_by_email($email, false, true);
                    $twUser = $this->ci->user_m->get_user_by_email($email, 'twitter', true);
                    $gUser = $this->ci->user_m->get_user_by_email($email, 'google', true);

                    $fbArray['id'] = $fbUser->getId();
                    $fbArray['first_name'] = $fbUser->getFirstName();
                    $fbArray['last_name'] = $fbUser->getLastName();
                    $fbArray['link'] = $fbUser->getLink();
                    $fbArray['locale'] = $fbUser->getLocation();
                    $fbUser = $fbArray;

                else:
                    $loggedin = false;
                endif;
                break;

            case 'google':
                //$this->_gpaClient->revokeToken();
                $this->_gpaClient->authenticate($_GET['code']);
                $token = $this->_gpaClient->getAccessToken();
                $this->_gpaClient->setAccessToken($token);

                $this->ci->session->set_userdata(array('googleToken' => $token)); // Save to session

                if ($this->_gpaClient->getAccessToken()) {
                    $user = $this->_gplus->userinfo->get();
                    $data = $gUser = array(
                        'gid' => $user['id'],
                        'name' => ucwords(strtolower($user['given_name'] . ' ' . $user['family_name'])),
                        'gender' => $user['gender'],
                        'primary_email' => filter_var($user['email'], FILTER_SANITIZE_EMAIL),
                        'profile_picture' => filter_var($user['picture'], FILTER_VALIDATE_URL),
                        'register_date' => date('Y-m-d H:i:s', time()),
                        'active' => 1
                    );

                    if (!$this->ci->user_m->email_exists($data['primary_email'], 'google')) {
                        $this->ci->user_m->insert_user($data, 'google');
                    }

                    $loggedin = true;
                    $cfUser = $this->ci->user_m->get_user_by_email($data['primary_email'], false, true);
                    $fbUser = $this->ci->user_m->get_user_by_email($data['primary_email'], 'facebook', true);
                    $twUser = $this->ci->user_m->get_user_by_email($data['primary_email'], 'twitter', true);
                } else {
                    $loggedin = false;
                }
                break;

            case 'twitter':

                $sessToken = $this->ci->session->userdata('twtokenz');
                // $this->_twConnection = $this->_tw->create(false, false, $params['oauth_token']);
                $this->_twConnection = $this->_tw->create($this->_ck, $this->_cs, $sessToken['oauth_token'], $sessToken['oauth_token_secret']);
                $token_credentials = $this->_twConnection->getAccessToken($params['oauth_verifier']);
                $this->_twConnection = $this->_tw->create($this->_ck, $this->_cs, $token_credentials['oauth_token'], $token_credentials['oauth_token_secret']);
                $tempUser = $this->_twConnection->get('account/verify_credentials');
                $twUser = array('id' => $tempUser->id,
                    'name' => $tempUser->name,
                    'screen_name' => $tempUser->screen_name,
                    'Location' => $tempUser->location,
                    'thumb' => $tempUser->profile_image_url_https,
                    'picture' => str_replace('_normal', '', $tempUser->profile_image_url_https)
                );
                $twUserExisting = $this->ci->user_m->get_twitter_user($tempUser->id);
                if ($twUserExisting): //lets see if we have existing twitter account
                    $cfUser = $this->ci->user_m->get_user_by_email($twUserExisting['media_email'], false, true);
                    if (count($cfUser) == 0) {
                        $user = array(
                            'email' => $twUserExisting['media_email'],
                            'id' => $twUserExisting['media_user_id']
                        );
                        $this->ci->user_m->insert_user($user, 'twitter');
                        $cfUser = array('id' => $twUserExisting['user_id']);
                        $f_time = 1;
                    }
                    $fbUser = $this->ci->user_m->get_user_by_email($twUserExisting['media_email'], 'facebook', true);
                    $gUser = $this->ci->user_m->get_user_by_email($twUserExisting['media_email'], 'google', true);
                    $twUser['email'] = $twUserExisting['media_email'];
                    $loggedin = true;
                else:
                    $cfUser = "pending";
                    $twUser['email'] = false;
                    $loggedin = true;
                endif;
                //return $twUser;
                break;
            case 'cf':
                $email = strtolower(trim($params['user_email']));
                $cfUser = $this->ci->user_m->cf_user_login($email, md5($params['password']));
                if (!$cfUser):
                    return false;
                endif;
                switch ($cfUser['active']) {
                    case 1: //ok
                        $loggedin = true;
                        $fbUser = $this->ci->user_m->get_user_by_email($email, 'facebook', true);
                        $twUser = $this->ci->user_m->get_user_by_email($email, 'twitter', true);
                        $gUser = $this->ci->user_m->get_user_by_email($email, 'google', true);
                        break;
                    case 2: //inactive
                        $loggedin = false;
                        break;
                    case 0: //deleted
                        $loggedin = false;
                        break;
                    default:
                        $cfUser = false;
                        $loggedin = false;
                        break;
                }
                break;

            default:
                break;
        endswitch;
		$userId = $cfUser['id'];
		$exist = $this->ci->db->select('count(id) cnt')->from('site_user_settings')->where('user_id', $userId)->get()->row()->cnt;

		if(!$exist):

			$userSettings = array(
			                      'user_id' => $userId,
			                      'notify'	=> 2,
			                      'remind60'=> 1,
			                      'remind14'=> 1,
			                      );
			$this->ci->db->insert('site_user_settings', $userSettings);
		endif;

		$userData = array(
						'cf' 		=> $cfUser,
						'facebook' 	=> $fbUser,
						'twitter'	=> $twUser,
						'google'	=> $gUser,
						'loggedin' 	=> $loggedin,
						'network'	=> $network,
						'no_email'	=> $no_email,
						'f_time' => $f_time,
						);
		//$this->ci->session->set_userdata('cfUser', $userData);
		return $userData;

/* 		$facebook = $this->fb->getUser();
 		$twitter = $this->twConnection->get('account/verify_credentials');
 		$tw = ($twitter->errors) ? false : true;
 		$google = false;
 		$cf = false;
		$urls = false;
		$loggedin = false;
		$email = false;
		if($facebook): //logged in with facebook
			$facebook = $this->fb->api('/me');
			if($this->ci->user_m->email_exists($facebook['email'], 'facebook')):
				//do nothing
			else: //we logging in first time
				$this->ci->user_m->insert_user($facebook, 'facebook');
			endif;
			$loggedin = true;
			$email = $facebook['email'];
		elseif($tw): //logged in with twitter


		elseif($google): //logged in with G

		elseif($cf): //user is registered

		else: //no login detected
			$urls = array(
			              'fbLoginUrl' => $this->fb->getLoginUrl(array('scope'=>'email')),
			              'twLoginUrl' => $authUrl = $this->twConnection->getAuthorizeURL($this->twToken)
			              );
		endif;

		$this->userData = array(
									'cf' 		=> $this->ci->user_m->get_user_by_email($facebook['email'], 'facebook', true),
									'facebook' 	=> $facebook,
									'twitter'	=> $twitter,
									'google'	=> $google,
									'urls' 		=> $urls,
									'loggedin' 	=> $loggedin
									);



		return $this->userData;*/

	}

	public function getLoginUrls()
	{
		// Include the required Composer dependencies.
			require_once __DIR__ . '/Facebook/autoload.php';
			// Initialize the Facebook PHP SDK v5.
			$fb = new Facebook\Facebook([
			  'app_id'                => '376715072474405',
			  'app_secret'            => 'c10ac685f96fd3a39744aff64b46c35d',
			  'default_graph_version' => 'v3.2',
			]);
			$helper = $fb->getRedirectLoginHelper();
			$permissions = ['email']; // Optional permissions
			$loginUrl = $helper->getLoginUrl(base_url().'user/process_login/facebook', $permissions);
 		// if(!$oldToken['oauth_token']):
			//Because twitter is a little fucking bitch
			$this->ci->session->unset_userdata('twtokenz');
			$this->_twConnection = $this->_tw->create($this->_ck, $this->_cs);
			$tempCred = $this->_twConnection->getRequestToken($this->_cb);
			$twurl = $this->_twConnection->getAuthorizeURL($tempCred);
			$this->ci->session->set_userdata(array('twtokenz' => $tempCred, 'oldtwUrl' => $twurl));

            $this ->_gpaClient->setScopes(array("https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/userinfo.profile"));

		// else:
			// $twurl = $this->ci->session->userdata('oldtwUrl');
		// endif;
		//$urls = array('fbLoginUrl' => $this->_fb->getLoginUrl(array('scope'=>'email', 'redirect_uri' => base_url()."user/process_login/facebook" )),
		  $urls = array('fbLoginUrl' =>  $loginUrl,
		              'twLoginUrl' => $twurl,
                      'gpLoginUrl' => $this->_gpaClient->createAuthUrl()
		              );
		return $urls;
	}


	public function logoff()
	{
		unset($_SESSION['cfUser']);
		$this->cfUser = false;
		$this->_fb->destroySession();
        $this->ci->session->sess_destroy();  // Assuming you have session helper loaded
	}


}

/* End of file userLib.php */
/* Location: ./application/libraries/userLib.php */


 ?>