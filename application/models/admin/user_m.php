<?php

class User_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function listAll($start = 0, $limit = 20, $key = false, $type = name, $count = false)
    {
        if( ! $count) $this -> db -> limit($limit, $start);

        if($key){
            switch($type)
            {
                case 'name':
                    $this -> db -> like('LOWER(su.name)', strtolower($key));
                break;

                case 'email':
                    $this -> db -> like('LOWER(su.primary_email)', strtolower($key));
                break;
            }
        }

        $result = $this -> db
        -> select('su.id, name, su.primary_email, su.register_date,
        su.confirmed_date, su.birth_date, su.gender, su.profile_picture, su.active,
        (SELECT count(la.id) FROM linked_accounts la WHERE la.user_id = su.id) accounts', false)
        -> from('site_users su')
        -> order_by('su.register_date', 'desc');

        return ( ! $count) ? $result -> get() -> result() : $result -> count_all_results();
    }

    public function get($userId)
    {
        return $this -> db
        -> select('*')
        -> from('site_users su')
        -> where(array('su.id' => (int) $userId))
        -> get()
        -> row();
    }

    public function listLinkedAccounts($userId)
    {
        return $this -> db
        -> select('media_type, media_user_id, media_email')
        -> from('linked_accounts')
        -> where(array('user_id' => (int) $userId))
        -> get()
        -> result();
    }

    public function updateStatus($userId, $status)
    {
        $this -> db
        -> update('site_users', array('active' => (int) $status), array('id' => (int) $userId ));

        return ($this -> db -> affected_rows()) ? true : false;
    }
}