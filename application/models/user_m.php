<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_m extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function email_exists($email = false, $mediaType = false)
	{
		switch ($mediaType) {
			case 'primary':
				$this->db->select('count(*) cnt')->from('site_users su')->where("su.primary_email = '{$email}'");
			break;
			default:
				$this->db->select('count(*) cnt')
						->from('linked_accounts li')
						->where(array('li.media_email' => $email,'li.media_type' => $mediaType));
				break;
		}
		return $this->db->get()->row()->cnt;
	}

	public function insert_user($user = false, $loginType = false)
	{
		// prep primary user
		switch ($loginType) :
			case 'facebook':

				if($uid = $this->get_cf_user_id($user['email'])):
					$userId = $uid->id;
					$updateData = array('active' => 1);
					$this->db->where('id', $userId);
					$this->db->update('site_users', $updateData);
				else:
					$userData = array(
						'primary_email' => $user['email'],
						'register_date'	=> date('Y-m-d H:i:s'),
						'confirmed_date' => ($loginType) ? date('Y-m-d H:i:s') : false,
						'gender' => $user['gender'],
						'confirm_hash' => md5(rand(1,100).$user['email'].date('U')),
						'active'=> 1
						);
					$this->db->insert('site_users', $userData);
					$userId = $this->db->insert_id();
					//prep fb
				endif;
				$mediaData = array(
						'user_id'=> $userId,
						'media_type' => 'facebook',
						'added_date' => date('Y-m-d H:i:s'),
						'media_user_id'	=> $user['id'],
						'media_email'	=> $user['email']
					);
					$this->db->insert('linked_accounts', $mediaData);
			break;
			case 'twitter':
				if($uid = $this->get_cf_user_id($user['email'])): //if we already have this email in the system
					$userId = $uid->id;
					$updateData = array('active' => 1);
					$this->db->where('id', $userId);
					$this->db->update('site_users', $updateData);
				else: //no this email is first
					$userData = array(
						'primary_email' => $user['email'],
						'register_date'	=> date('Y-m-d H:i:s'),
						'confirmed_date' => ($loginType) ? date('Y-m-d H:i:s') : false,
						'gender' => "none",
						'confirm_hash' => md5(rand(1,100).$user['email'].date('U')),
						'active'=> 1
						);
					$this->db->insert('site_users', $userData);
					$userId = $this->db->insert_id();
				endif;
				//prep fb
				$mediaData = array(
					'user_id'=> $userId,
					'media_type' => 'twitter',
					'added_date' => date('Y-m-d H:i:s'),
					'media_user_id'	=> $user['id'],
					'media_email'	=> $user['email']
				);
				$this->db->insert('linked_accounts', $mediaData);

			break;
            case 'google':
                if($uid = $this->get_cf_user_id($user['primary_email'])){
                    $userId = $uid->id;
                    $updateData = array('active' => 1);
					$this->db->where('id', $userId);
					$this->db->update('site_users', $updateData);
                } else {
                    $userData = $user;
                    unset($userData['gid']);
                    $this->db->insert('site_users', $userData);
                    $userId = $this->db->insert_id();
                }

                $mediaData = array(
                    'user_id'       => $userId,
                    'media_type'    => 'google',
                    'added_date'    => date('Y-m-d H:i:s'),
                    'media_user_id' => $user['gid'],
                    'media_email'   => $user['primary_email']
                );

                $this->db->insert('linked_accounts', $mediaData);
            break;
			case 'register':
				$userData = array(
					'primary_email' => $user['email'],
					'name'			=> $user['name'],
					'register_date'	=> date('Y-m-d H:i:s'),
					'password'		=> md5($user['password']),
					'birth_date'		=> $user['bday'],
					'confirmed_date' => false,
					'gender' => $user['gender'],
					'confirm_hash' => $user['hash'],
					'active' => 2
					);
				$this->db->insert('site_users', $userData);
				$userId = $this->db->insert_id();
			default:
				return false;
			break;
			$exist = $this->db->select('count(id) cnt')->from('site_user_settings')->where('user_id', $userId)->get()->row()->cnt;
			echo $this->db->last_query();

			if(!$exist):
				$userSettings = array(
				                      'user_id' => $userId,
				                      'notify'	=> 2,
				                      'remind60'=> 1,
				                      'remind14'=> 1,
				                      );
				$this->db->insert('site_user_settings', $userSettings);
			endif;
			return true;
		endswitch;



		return true;
	}

	public function update_facebook_email($new_email){
		$primary_email = $this->cfUser['cf']['primary_email'];
		$this->db->where('primary_email', $primary_email);
		$data = array('primary_email' => $new_email);
		$this->db->update('site_users', $data);

		$this->db->where('media_email', $primary_email);
		$data = array('media_email' => $new_email);
		$this->db->update('linked_accounts', $data);

		$this->db->where('primary_email', $primary_email);
		$data = array('primary_email' => $new_email, 'changed' => true);
		$this->db->update('facebook_no_email', $data);
	}

	public function insert_facebook_id($fb_id, $email){
		if(!$this->exist_facebook_id($fb_id))
		{
			$data = array('facebook_id' => $fb_id, 'primary_email' => $email);
			$this->db->insert('facebook_no_email',$data);
			return true;
		}
		return false;
	}

	public function exist_facebook_id($fb_id)
	{
		$data = $this->db->select('count(*) as cnt')->from('facebook_no_email')->where(array('facebook_id' => $fb_id,));
		$cnt = $data->get()->row()->cnt;
		if($cnt == 0)
			return false;
		else
			return true;
	}

	public function get_facebook_id($fb_id)
	{
		$data = $this->db->select('primary_email, changed')
			->from('facebook_no_email')
			->where(array('facebook_id' => $fb_id,))
			->get()
			->row();
		return $data;
	}

	public function unlink($network, $userId)
	{
		$this->db->where(array('media_type' => $network,'user_id' => $userId));
		return $this->db->delete('linked_accounts');

	}

	public function get_cf_user_id($email, $activeOnly = false)
	{
		if($activeOnly) $this->db->where('active', 1);
		return $this->db
					->select('id')
					->from('site_users')
					->where(array('primary_email' => $email))
					->limit(1)
					->get()->row();
	}

	public function get_user_by_email($email, $originalCaller = false, $getArray = true)
	{
		if(!$email) return false;
		if($originalCaller):
			return $this->db
					->select('*')
					->from('linked_accounts la')
					->where(array('la.media_email' => $email, 'la.media_type' => $originalCaller))
					->limit(1)
					->get()->row_array();
		else:
			return $this->db
					->select('*')
					->from('site_users su')
					->where(array('su.primary_email' => $email))
					->limit(1)
					->get()->row_array();
		endif;

	}

	public function get_user_by_id($id, $arr = true)
	{
		return $this->db
					->select('*')
					->from('site_users su')
					->where(array('su.id' => $id))
					->limit(1)
					->get()->row_array();
	}

	public function cf_user_login($email, $pass)
	{
		return $this->db
					->select('*')
					->from('site_users su')
					->where(array('su.primary_email' => $email, 'password' => $pass))
					->limit(1)
					->get()->row_array();
	}

	public function get_twitter_user($id)
	{
		return $this->db
					->select('la.*')
					->from('linked_accounts la')
					->where(array('la.media_type' => 'twitter', 'media_user_id' => $id))
					->limit(1)
					->get()->row_array();
	}

	public function verify_email($hash = false)
	{
		if($this->db->select('count(id) cnt')->from('site_users')->where(array('confirm_hash' => $hash, 'active' => 2))->get()->row()->cnt):
			$updatedata = array('active' => 1, 'confirmed_date' => date('Y-m-d H:i:s'));
			$this->db->where('confirm_hash', $hash);
			$this->db->update('site_users', $updatedata);
			return true;
		else:
			return false;
		endif;
	}
	public function update_name($name)
	{
		$id = $this->cfUser['cf']['id'];
		$this->db->where('id', $id);
		$data = array('name' => $name);
		return $this->db->update('site_users', $data);
	}
	public function update_password($pass, $uid = false)
	{
		$id = ($uid) ? $uid : $this->cfUser['cf']['id'];
		$this->db->where('id', $id);
		$data = array('password' => md5($pass));
		return $this->db->update('site_users', $data);
	}
	public function change_notify_type($notify)
	{
		$id = $this->cfUser['cf']['id'];
		$this->db->where('user_id', $id);
		$data = array('notify' => $notify);
		return $this->db->update('site_user_settings', $data);
	}

	public function change_remind_type($data)
	{
		$id = $this->cfUser['cf']['id'];
		$this->db->where('user_id', $id);
		return $this->db->update('site_user_settings', $data);
	}
	public function get_pending_tw_user($email)
	{
		return $this->db->select('*')->from('pending_email_twitter')->where('email', $email)->get()->row();
	}

	public function insert_tw_pending($data)
	{
		$this->db->insert('pending_email_twitter', $data);
	}

	public function pending_exists($confirm_hash, $email_hash)
	{
		$q = $this->db->select('id, twitter_user_id, email')
				->from('pending_email_twitter')->where(array('confirm_hash' => $confirm_hash, 'email_hash' => $email_hash))
				->get()->row();
		if(isset($q->id)):
			$this->db->where('id', $q->id);
			$this->db->delete('pending_email_twitter');
			return $q;
		endif;
		return false;
	}

	public function get_user_settings()
	{

		$id = $this->cfUser['cf']['id'];
		return $this->db->select('*')->from('site_user_settings')->where('user_id', $id)->get()->row();
	}

	public function get_all_users($active = 1, $notify = 2)
	{
		return $this->db->select('su.id, su.name, su.primary_email, sus.notify, sus.remind14, sus.remind60')
					->from('site_users su')
					->join('site_user_settings sus','sus.user_id = su.id')
					//->where(array('su.id' => 22))
					->where(array('su.active' => $active, 'sus.notify' => $notify))
					->get()->result();
	}

	public function get_user($email)
	{
		return $this->db->select('su.id, su.name, su.primary_email, urc.cityName, urc.miles, urc.new_concert, urc.upcoming_concert')
			->from('site_users su')
			->join('user_rules_concerts urc','urc.userid = su.id')
			//->where(array('su.id' => 2233))
			->where(array('su.primary_email' => $email))
			->get()->row();
	}

	public function get_performers_names($ids){
		return $this->db->select('pf.Name performerName')
			->from('performer_full pf')
			->where_in('pf.PerformerID', $ids)
			->get()->result();
	}

	public function get_upcoming_concerts_users($active = 1, $notify = 1)
	{
		$where = 'urc.citySlug is not null and urc.cityName is not null';
		return $this->db->select('su.id, su.name, su.primary_email, urc.citySlug, urc.cityName, urc.miles')
			->from('site_users su')
			->join('user_rules_concerts urc','urc.userid = su.id')
			->where(array('su.active' => $active, 'urc.upcoming_concert' => $notify))
			->where($where)
			->get()->result();
	}

	public function get_new_concerts_users($active = 1)
	{
		$where = "( (urc.new_concert=1 and DATE(DATE_ADD( urc.last_email, INTERVAL 1 DAY )) <= DATE( NOW() )) or (urc.new_concert=2 and DATE(DATE_ADD( urc.last_email, INTERVAL 7 DAY )) <= DATE( NOW() )) or (urc.new_concert=3 and DATE(DATE_ADD( urc.last_email, INTERVAL 30 DAY )) <= DATE( NOW() )) )";
		$where2 = 'urc.citySlug is not null and urc.cityName is not null';
		return $this->db->select('su.id, su.name, su.primary_email, urc.new_concert, urc.citySlug, urc.cityName, urc.miles')
			->from('site_users su')
			->join('user_rules_concerts urc','urc.userid = su.id')
			->where(array('su.active' => $active))
			->where($where)
			->where($where2)
			->get()->result();
	}

	public function get_upcoming_concerts_users_by_id($ids)
	{
		return $this->db->select('su.id, su.name, su.primary_email')
			->from('site_users su')
			->join('user_rules_concerts urc','urc.userid = su.id')
			->where(array('su.active' => 1, 'urc.upcoming_concert' => 1))
			->where_in('su.id', $ids)
			->get()->result();
	}

	public function get_user_rules($id, $ruleType = 'city')
	{
 		switch ($ruleType) {
			case 'city':
				$q = $this->db->select('citySlug')->from('user_rules_city')->where('cfUserId', $id)->get()->result_array();
				$result = array();
				foreach ($q as $d):
					array_push($result, $d['citySlug']);
				endforeach;
				return $result;
				break;
			case 'performer':
				$result = array();
				$q = $this->db->select('performerId')->from('user_rules_performer')->where('cfUserId', $id)->get()->result_array();
 				foreach ($q as $d):
					array_push($result, $d['performerId']);
				endforeach;
				return $result;
				break;
			default:
				return array();
				break;

		}

	}

	public function get_home_nearby_cities($homeCity, $miles){
		$location = array('slug' => $homeCity);
		$this->load->model('global_m');
		$cities = $this->global_m->get_nearby_cities($location, $miles);
		$locations = array();
		$locations[$location['slug']] = $location['slug'];
		foreach($cities as $city){
			$locations[$city['slug']] = $city['slug'];
		}
		return $locations;
	}

	public function  create_reset_link($id)
	{
		$hash = md5($id.rand(1,1000).date('U'));
		$data = array(
		              'user_id'	=> $id,
		              'reset_hash' => $hash,
		              'used'	=> 0,
		              'requested_date'	=> date('Y-m-d H:i:s')
		              );
		if($this->db->insert('password_reset', $data)) return $hash;
		return false;
	}

	public function get_reset_user($link)
	{
		$q = $this->db->select('user_id')->from('password_reset')->where('reset_hash', $link)->get()->row();
		if(isset($q->user_id)):
			return $this->get_user_by_id($q->user_id);
		else:
			return false;
		endif;
	}

	public function remove_reset($uid)
	{
		$this->db->where('user_id', $uid);
		$this->db->delete('password_reset');
	}

	public function remove_user($uId = false)
	{
		//remove rules
		$this->db->where('cfUserId', $uId);
		$this->db->delete('user_rules_city');

		$this->db->where('cfUserId', $uId);
		$this->db->delete('user_rules_performer');
		//remove linked_accounts where user_id = $uId;
		$this->db->where('user_id', $uId);
		$this->db->delete('linked_accounts');
		//remove password_reset where user_id = $uId;
		$this->db->where('user_id', $uId);
		$this->db->delete('password_reset');
		//site_users  active = 0 password = NULL, name = '', confirm_hash = 0;
		$this->db->where('id', $uId);
		$update = array('active' => 0, 'password' => NULL, 'name' => '', 'confirm_hash' => md5(date('U')));
		$this->db->update('site_users', $update);
		//change site_user_settings notify = 0, remind60 = 0, remind14 = 0
		return true;

	}

	public function remove_user_all($uId = false)
	{
		//remove rules
		$this->db->where('cfUserId', $uId);
		$this->db->delete('user_rules_city');

		$this->db->where('cfUserId', $uId);
		$this->db->delete('user_rules_performer');

		//remove linked_accounts where user_id = $uId;
		$this->db->where('user_id', $uId);
		$this->db->delete('linked_accounts');

		//remove password_reset where user_id = $uId;
		$this->db->where('user_id', $uId);
		$this->db->delete('password_reset');

		//site_users id = $uId;
		$this->db->where('id', $uId);
		$this->db->delete('site_users');
		//change site_user_settings notify = 0, remind60 = 0, remind14 = 0
		return true;

	}
}

/* End of file user_m.php */
/* Location: ./application/models/user_m.php */