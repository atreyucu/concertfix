<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notifier_m extends CI_Model {

	public function add_notification_log($events, $id, $reminder = false)
	{
		foreach ($events as $e):
			$data = array('user_id' => $id, 'event_id' => $e->EventID, 'announced' => 1);
			if($reminder):
				$data[$reminder] = 1;
			endif;

			if($this->db->select('count(id) cnt')->from('notification_log')->where(array('user_id' => $id, 'event_id' => $e->EventID))->get()->row()->cnt):
				$this->db->where(array('user_id' => $id, 'event_id' => $e->EventID));
				$this->db->update('notification_log', $data);
			else:
				$this->db->insert('notification_log', $data);
			endif;

		endforeach;
	}
	public function add_email_to_queue($subject, $body, $email)
	{
		$data = array('email' => $email,
		              'subject' => $subject,
		              'body'	=> $body,
		              'added'	=> date('Y-m-d H:i:s'),
		              );
		$this->db->insert('notification_queue', $data);
		return true;
	}

	public function get_emails()
	{
		$q = $this->db
				->select('nq.*, su.name')
				->from('notification_queue nq')
				->join('site_users su','su.primary_email = nq.email')
				->where(array('sent' => '0000-00-00 00:00', 'success' => 0))
				->get()->result();
		return $q;
	}


	public function get_list()
	{
		return $this->db->select('email')->from('mailing_list')->group_by('email')->get()->result_array();
	}
	public function change_status($status, $id)
	{
		if($status == 2):
			$this->db->where('id', $id);
			$data = array('success' => 2);
			$this->db->update('notification_queue', $data);
		elseif($status == 1):
			$this->db->where('id', $id);
			$data = array('sent' => date('Y-m-d H:i:s'), 'success' => 1);
			$this->db->update('notification_queue', $data);
		else:
			//do nothing

		endif;
	return true;
	}



}

/* End of file notifier_m.php */
/* Location: ./application/models/notifier_m.php */