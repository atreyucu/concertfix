<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Spinner_m extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * [get_li_text description]
	 * @param  array  $location
	 * @param  array/bool  $events
	 * @param  string  $page
	 * @param  string  $slug
	 * @param  boolean $filter
	 * @return string
	 */
	public function get_li_text($location, $events, $page, $slug, $filter = false)
	{
		$slug = $this->uri->segment(2);
		$refresh = $this->input->get('refresh');
		$textData = $this->db->select("*")->from('page_text')->where(array('slug' => $slug, 'type' => 'li'))->get()->row();
		// if(falseisset($textData->text)):
		if(isset($textData->text) && !$refresh):
			return $textData->text;
		else:
			$newText=$this->_create_li_text($events, $location, $page, $filter);
			$this->db->where(array('slug' => $slug, 'type' => 'li'));
			$this->db->delete('page_text'); //remove old text
			$data = array('text' => $newText, 'type' => 'li', 'var_data' => '', 'created' => date('Y-m-d H:i:s'),'slug' => $slug);
			$this->db->insert('page_text', $data);
			return $newText;
		endif;
	}


	/**
	 * [_create_li_text creates li's for tours and concerts EMBRACE THIS SHIT]
	 * @param  array/bool  $events
	 * @param  array  $location
	 * @param  string  $page
	 * @param  boolean $filter // this can also be venue object
	 * @return string
	 */
	private function _create_li_text($events, $location, $page, $filter = false)
	{
		switch ($page) {
			case 'concert-city':
				$highEvent = array();
				foreach ($events as $key => $event):
					$highEvent[$key] = ($event->prices) ? (int)$event->prices->highPrice : 0;
				endforeach;

				asort($highEvent,SORT_NUMERIC);
				end($highEvent);
				$topKey = key($highEvent);
				$top_venue_1 = clean_venue($events[$topKey]->Venue);
				foreach ($highEvent as $key => $price) :
					$top_venue_2 = clean_venue($events[$key]->Venue);
					if($top_venue_1 != $top_venue_2) break;
				endforeach;
				$city = $location['city'];
				$text = "<ul class='pageUL'>";
				$middle = array(
					"<li>{$city} ".random_pop(array("is really","remains","continues to be","is always"))." one of the ".random_pop(array("top","best","hottest","most popular"))." ".random_pop(array("cities","places","locations","areas"))." for concert tours in ".random_pop(array("the US","America","North America","the country","the nation")).".</li>",
					"<li>".random_pop(array("These next concerts","The Upcoming Concerts","The Concerts"))." ".random_pop(array("will be","are going to be"))." some of the ".random_pop(array("most anticipated","most popular"))." {$city} concerts ".random_pop(array("in years","on record","by far","in a long time")).".</li>",
					"<li>Schedule for the concerts in {$city} is ".random_pop(array("revised","updated","refreshed"))." ".random_pop(array("daily","up to the minute","constantly","on a regular basis")).".</li>",
					"<li>{$top_venue_1} and {$top_venue_2} both have ".random_pop(array("tons","plenty","lots"))." of concert ".random_pop(array("activity","action","events","dates"))." ".random_pop(array("approaching","forthcoming","coming up","scheduled")).".</li>",
					"<li>Tickets for all {$city} concerts are ".random_pop(array("protected","covered","sold","purchased"))." with a 100% money back guarantee.</li>",
					);
				shuffle($middle);
				$text .= implode(' ', $middle);
				$text .= "</ul><br /><p>{$city} ".random_pop(array("so far","currently","right now"))." has a ".random_pop(array("huge","massive","exciting","busy"))." list of concert tours ".random_pop(array("coming to","entering","visiting"))." the city in the ".random_pop(array("coming weeks","next few months","near future")).". ".random_pop(array("Many","Some","A selection"))." of music's top ".random_pop(array("artists","performers","tours"))." are ".random_pop(array("about to have","planning on having","making plans to have"))." concerts in {$city}. ".random_pop(array("You cannot miss","Do not miss","Make sure to catch"))." some of the concert ".random_pop(array("events","excitement","buzz","fever"))." that {$city} has to offer at some of the ".random_pop(array("most prolific","best","most magnificant"))." venues ".random_pop(array("around","in the nation","in the country","in the world")).". ".random_pop(array("Simply","Just"))." ".random_pop(array("check out","view","see","browse through"))." the ".random_pop(array("list","schedule","dates"))." ".random_pop(array("here","above","provided"))." for all the ".random_pop(array("upcoming","future","best"))." {$city} concerts. Our {$city} concert schedule is ".random_pop(array("revised","updated","refreshed"))." ".random_pop(array("daily","up to the minute","constantly","on a regular basis"))." in case their are any ".random_pop(array("changes","modifications","adjustments","revisions")).".</p>";
				return $text;
			break;

			case 'concert-venue' :
				$city = $location['city'];
 				$venue = clean_venue($filter->Name);
				$text = "<ul class='pageUL'>";
				$middle = array(
					"<li>{$venue} ".random_pop(array("is really","remains","continues to be","is always"))." one of the ".random_pop(array("top","best","hottest","most popular"))." ".random_pop(array("venues","places","locations"))." for concert tours in {$city}.</li>",
					"<li>Concert Schedule for {$venue} is ".random_pop(array("revised","updated","refreshed"))." ".random_pop(array("daily","up to the minute","constantly","on a regular basis")).".</li>",
					"<li>".random_pop(array("These next concerts","The Upcoming Concerts","{$city} Concert Schedule"))." ".random_pop(array("will be","is going to be"))." some of the ".random_pop(array("most anticipated","most popular"))." {$venue} concerts ".random_pop(array("in years","on record","by far","in a long time")).".</li>",
					"<li>Tickets for all {$venue} concerts are ".random_pop(array("protected","covered","sold","purchased"))." with a 100% moneyback guarantee.</li>",
					"<li>ETickets and last minute tickets for {$venue} ".random_pop(array("are now on sale","are in stock","are now available","can be found here","can now be purchased")).".</li>",
				);
				shuffle($middle);
				$text .= implode(' ', $middle);
				$text .= "</ul><br /><p>{$venue} ".random_pop(array("so far","currently","right now"))." has a ".random_pop(array("huge","massive","exciting","busy"))." list of concert tours ".random_pop(array("coming to","entering","visiting"))." the venue in the ".random_pop(array("coming weeks","next few months","near future")).". ".random_pop(array("Many","Some","A selection"))." of music's top ".random_pop(array("artists","performers","tours"))." are ".random_pop(array("about to have","planning on having","making plans to have"))." concerts at {$venue} in {$city}. ".random_pop(array("You cannot miss","Do not miss","Make sure to catch"))." some of the concert ".random_pop(array("events","excitement","buzz","fever"))." that {$venue} has to offer. {$venue} is one of the ".random_pop(array("most prolific","best","most magnificant"))." venues that {$city} has to offer. ".random_pop(array("Simply","Just"))." ".random_pop(array("check out","view","see","browse through"))." the ".random_pop(array("list","schedule","dates"))." ".random_pop(array("here","above","provided"))." for all the ".random_pop(array("upcoming","future","best"))." {$venue} concerts. Our {$venue} concert schedule is ".random_pop(array("revised","updated","refreshed"))." ".random_pop(array("daily","up to the minute","constantly","on a regular basis"))." in case their are any ".random_pop(array("changes","modifications","adjustments","revisions")).".</p>";
				return $text;
			break;

			case 'concert-city-date':
				$topVs = $this->db->select("Name")->from('tn_venues')->where("RegionSlug = '{$location['slug']}'")->order_by('EventCount', 'desc')->limit(2)->get()->result();

				$lis = array(
				"<li>{$location['city']} ".random_pop(array("is really","remains","continues to be","is always"))." one of the ".random_pop(array("top","best","hottest","most popular"))." ".random_pop(array("cities","places","locations","areas"))." for concert tours in {$filter}.</li>",
				"<li>".random_pop(array("These next {$filter} concerts","The Upcoming {$filter} Concerts","{$location['city']} Concert Schedule in {$filter}"))." ".random_pop(array("will be","is going to be"))." some of the ".random_pop(array("most anticipated","most popular"))." {$location['city']}  concerts ".random_pop(array("in years","on record","by far","in a long time")).".</li>",
				"<li>The {$location['city']} {$filter} concert schedule is ".random_pop(array("revised","updated","refreshed"))." ".random_pop(array("daily","up to the minute","constantly","on a regular basis")).".</li>",
				"<li>{$topVs[0]->Name} and {$topVs[1]->Name} both have ".random_pop(array("tons","plenty","lots"))." of concert ".random_pop(array("activity","action","events","dates"))." ".random_pop(array("approaching","forthcoming","coming up","scheduled"))." in {$filter}.</li>",
				"<li>Tickets for all {$location['city']} concerts in {$filter} are ".random_pop(array("protected","covered","sold","purchased"))." with a 100% moneyback guarantee.</li>",
				);
				shuffle($lis);
				$text = "<ul class='pageUL'>".implode('', $lis)."</ul><br /><p>{$location['city']} ".random_pop(array("so far","currently","right now"))." has a ".random_pop(array("huge","massive","exciting","busy"))." list of concert tours ".random_pop(array("coming to","entering","visiting"))." the {$location['city']} in {$filter}. ".random_pop(array("Many","Some","A selection"))." of  music's top ".random_pop(array("artists","performers","tours"))." are ".random_pop(array("about to have","planning on having","making plans to have"))." concerts in {$location['city']} in {$filter}. ".random_pop(array("You cannot miss","Do not miss","Make sure to catch"))." some of the  concert ".random_pop(array("events","excitement","buzz","fever"))." that {$location['city']} has to offer at some of the ".random_pop(array("most prolific","best","most magnificant"))." venues ".random_pop(array("around","in the nation","in the country","in the world")).". ".random_pop(array("Simply","Just"))." ".random_pop(array("check out","view","see","browse through"))." the ".random_pop(array("list","schedule","dates"))." ".random_pop(array("here","above","provided"))." for all the ".random_pop(array("upcoming","future","best"))." {$location['city']} concerts in {$filter}. Our {$location['city']} concert schedule is ".random_pop(array("revised","updated","refreshed"))." ".random_pop(array("daily","up to the minute","constantly","on a regular basis"))." in case their are any ".random_pop(array("changes","modifications","adjustments","revisions")).".</p>";
				return $text;
			break;

			case 'concert-city-genre':
				$city = $location['city'];
				$filter = ucwords(strtolower($filter));
				$topVs = $this->db->select("Name")->from('tn_venues')->where("RegionSlug = '{$location['slug']}'")->order_by('EventCount', 'desc')->limit(2)->get()->result();

				$middle = array(
					"<li>{$city} ".random_pop(array("is really","remains","continues to be","is always"))." one of the ".random_pop(array("top","best","hottest","most popular"))." ".random_pop(array("cities","places","locations","areas"))." for {$filter} concert tours in ".random_pop(array("the US","America","the country","the nation")).".</li>",
					"<li>".random_pop(array("These next {$filter} concerts","The Upcoming {$filter} Concerts","{$filter} Concert Schedule"))." ".random_pop(array("will be","is going to be"))." some of the ".random_pop(array("most anticipated","most popular"))." {$city} {$filter} concerts ".random_pop(array("in years","on record","by far","in a long time")).".</li>",
					"<li>Schedule for the {$filter} concerts in {$city} is ".random_pop(array("revised","updated","refreshed"))." ".random_pop(array("daily","up to the minute","constantly","on a regular basis")).".</li>",
					"<li>{$topVs[0]->Name} and {$topVs[1]->Name} both have ".random_pop(array("tons","plenty","lots"))." of {$filter} concert ".random_pop(array("activity","action","events","dates"))." ".random_pop(array("approaching","forthcoming","coming up","scheduled")).".</li>",
					"<li>Tickets for all {$city} {$filter} concerts are ".random_pop(array("protected","covered","sold","purchased"))." with a 100% moneyback guarantee.</li>"
					);
					shuffle($middle);

					$text = "<ul class='pageUL'> ".implode('', $middle)."</ul><br /><p> {$city} ".random_pop(array("so far","currently","right now"))." has a ".random_pop(array("huge","massive","exciting","busy"))." list of {$filter} concert tours ".random_pop(array("coming to","entering","visiting"))." $city in the ".random_pop(array("coming weeks","next few months","near future")).". ".random_pop(array("Many","Some","A selection"))." of {$filter} music's top ".random_pop(array("artists","performers","tours"))." are ".random_pop(array("about to have","planning on having","making plans to have"))." concerts in {$city}. ".random_pop(array("You cannot miss","Do not miss","Make sure to catch"))." some of the {$filter} concert ".random_pop(array("events","excitement","buzz","fever"))." that {$city} has to offer at some of the ".random_pop(array("most prolific","best","most magnificant"))." venues ".random_pop(array("around","in the nation","in the country","in the world")).". ".random_pop(array("Simply","Just"))." ".random_pop(array("check out","view","see","browse through"))." the ".random_pop(array("list","schedule","dates"))." ".random_pop(array("here","above","provided"))." for all the ".random_pop(array("upcoming","future","best"))." {$city} {$filter} concerts. Our {$city} concert schedule is ".random_pop(array("revised","updated","refreshed"))." ".random_pop(array("daily","up to the minute","constantly","on a regular basis"))." in case their are any ".random_pop(array("changes","modifications","adjustments","revisions")).".</p>";
				return $text;
			break;

			default:
				return false;
				break;
		}
	}

	/**
	 * Gets Text for Main Paragraphs in Concerts
	 * @param  string $section
	 * @param  [type] $type //NOT USED
	 * @param  array/bool $cityEvents
	 * @param  string $slug
	 * @return string
	 */
	public function get_text($section, $type, $cityEvents, $slug)
	{
		if($cityEvents):
			$slug = $this->uri->segment(2);
			if($textData = $this->_fetch_text($slug, $type)):
				$vars = unserialize($textData->var_data);
				if($vars['event_count'] != count($cityEvents)):
					//refresh vars
					$vars = $this->_generate_text_vars($cityEvents, $section);
					// update vars only
					$this->_create_update_text_vars($slug, false, serialize($vars));
				endif;
				return $this->_replace_text($textData->text, $vars, $cityEvents, $section);
			else:
				$vars = $this->_generate_text_vars($cityEvents, $section);
				$text = $this->_generate_text($cityEvents, $section);
				$this->_create_update_text_vars($slug, $text, serialize($vars));
				return $this->_replace_text($text, $vars, $cityEvents, $section);
			endif;
		else:
			return "Sorry, there are currently no events in this category";
		endif;
	}

	public function get_city_events_text($location, $type = 'secondary', $events, $slug)
	{
		$slug = $this->uri->segment(2);
		$refresh = $this->input->get('refresh');
		$textData = $this->db->select("*")->from('page_text')->where(array('slug' => $slug, 'type' => 'secondary'))->where("created > NOW() - INTERVAL 2 DAY")->get()->row();
		// if(falseisset($textData->text)):
		if(isset($textData->text) && !$refresh):
			return $textData->text;
		else:
			$newText=$this->_create_city_page_text($events, $location);
			$this->db->where(array('slug' => $slug, 'type' => 'secondary'));
			$this->db->delete('page_text'); //remove old text
			$data = array('text' => $newText, 'type' => 'secondary', 'var_data' => '', 'created' => date('Y-m-d H:i:s'),'slug' => $slug);
			$this->db->insert('page_text', $data);
			return $newText;
		endif;
	}


	/**
	 * generates text variables (based on events)
	 * @param  array/bool $events
	 * @param  string $section
	 * @return string
	 */
	private function _generate_text_vars($events, $section = 'city')
	{
		if($events):
		// $var['event_city'] =  $events[0]->City;
			$var['event_start'] =  $events[0]->Name;
			$var['event_date_start'] =  date('F j, Y', strtotime($events[0]->Date));
			$var['event_venue_start'] =  clean_venue($events[0]->Venue);
			$var['event_count'] =  count($events);

			$rand = rand(0, count($events)-1);
			$var['event_hottest'] =  $events[$rand]->Name;
			$var['event_date_hottest'] =  date('F j, Y', strtotime($events[$rand]->Date));
			$var['event_venue_hottest'] = clean_venue($events[$rand]->Venue);
			$var['ss_twitter_hash'] =  twHash("ConcertFix");
			$var['ss_twitter_page'] =  twFollow('concertFix');
		else: //if there are no events, no need to generate variables, they will be regenerated on the next run
			// $var['event_city'] =  '';
			$var['event_start'] =  '';
			$var['event_date_start'] =  '';
			$var['event_venue_start'] =  '';
			$var['event_count'] =  count($events);
			$var['event_hottest'] =  '';
			$var['event_date_hottest'] =  '';
			$var['event_venue_hottest'] = '';
			$var['ss_twitter_hash'] =  twHash("concertfix");
			$var['ss_twitter_page'] =  twFollow('concertFix');

		endif;
		return $var;
	}

	/**
	 * replaces variables within text (WHY IS THIS EVEN NEEDED????)
	 * @param  string $text
	 * @param  array $replaceArray
	 * @param  array/bool $events
	 * @param  string $section
	 * @return string
	 */
	private function _replace_text($text, $replaceArray, $events, $section)
	{
		if(count($events) > 2):
			$replaceArray['other_events_insert'] = $this->_generate_other_events($events, $section);
		else:
			$replaceArray['other_events_insert'] = '';
		endif;
		// var_dump($replaceArray);
		foreach ($replaceArray as $find => $replace) :
			$text = str_replace("{".$find."}", $replace, $text);
		endforeach;
		return $text;
	}

	/**
	 * generates other events within events...again why is this needed???
	 * @param  array/bool $events
	 * @param  string $section
	 * @return string
	 */
	private function _generate_other_events($events, $section = "city")
	{
		$other = array();
		for ($i=0; $i < 6; $i++):
			if(isset($events[$i])):
				array_push($other, $events[$i]);
			endif;
		endfor;
		$otherCount = count($other);
		$list = '';
		for ($i=0; $i < $otherCount; $i++):

			$venue = ($section == 'city_venue') ? '' : " at  ".clean_venue($events[$i]->Venue);
			$list .= " {$events[$i]->Name} on ".date('F j, Y', strtotime($events[$i]->Date)).$venue;
			if($i != ($otherCount -1)):
				$list .= ($i == ($otherCount -2)) ? ' and ' : ', ';
			endif;
			// $list .= ($i == ($otherCount -1)) ? '' : '';
		endfor;
		switch (count($events)) :
			case 3:
				// $v = ($section == 'city_venue') ? '' : ;
				$list = "{$events[1]->Name} on ".date('F j, Y', strtotime($events[1]->Date))." at  ".clean_venue($events[1]->Venue);
				break;
			case 4:
				$list = "{$events[1]->Name} on ".date('F j, Y', strtotime($events[1]->Date))." at  ".clean_venue($events[1]->Venue)." and ";
				$list .= "{$events[2]->Name} on ".date('F j, Y', strtotime($events[2]->Date))." at  ".clean_venue($events[2]->Venue);
				break;
			case 5:
				$list = "{$events[1]->Name} on ".date('F j, Y', strtotime($events[1]->Date))." at  ".clean_venue($events[1]->Venue).", ";
				$list .= "{$events[2]->Name} on ".date('F j, Y', strtotime($events[2]->Date))." at  ".clean_venue($events[2]->Venue)." and ";
				$list .= "{$events[3]->Name} on ".date('F j, Y', strtotime($events[3]->Date))." at  ".clean_venue($events[3]->Venue);
				break;
			default: //5 or more
				$list = "{$events[1]->Name} on ".date('F j, Y', strtotime($events[1]->Date))." at  ".clean_venue($events[1]->Venue).", ";
				$list .= "{$events[2]->Name} on ".date('F j, Y', strtotime($events[2]->Date))." at  ".clean_venue($events[2]->Venue).", ";
				$list .= "{$events[3]->Name} on ".date('F j, Y', strtotime($events[3]->Date))." at  ".clean_venue($events[3]->Venue)." and ";
				$list .= "{$events[4]->Name} on ".date('F j, Y', strtotime($events[4]->Date))." at  ".clean_venue($events[4]->Venue);
				break;
		endswitch;
		$sent = array('Other popular concerts scheduled', 'Other hot concerts', 'List of other concerts booked', 'Many other events');
		return random_pop($sent)." in {event_city} include {$list}";

	}

	/**
	 * creates and updates vars in text
	 * @param  string $slug
	 * @param  string $text
	 * @param  array $vars
	 * @param  string $type
	 * @return nothing
	 */
	private function _create_update_text_vars($slug, $text, $vars, $type = "main")
	{
		$slug = $this->uri->segment(2);
		if($this->_fetch_text($slug, $type)):
			$obj = array('type' => $type, 'created' => date('Y-m-d H:i:s'));
			if($text) $obj['text'] = $text;
			if($vars) $obj['var_data'] = $vars;
			$this->db->where('type', $type);
			return $this->db->update('page_text', $obj);
		else:
			//insert
			$obj = array('slug' => $slug, 'type' => $type, 'text' => $text, 'var_data' => $vars, 'created' => date('Y-m-d H:i:s'));
			return $this->db->insert('page_text', $obj);
		endif;
	}

	/**
	 * creates city page text
	 * @param  array/bool  $events
	 * @param  array $location
	 * @return text
	 */
	private function _create_city_page_text($events, $location = false)
	{
		$city = $location['city'];
		$SScity = twHash('concertFix');
		$sitename = twFollow('concertfix');
		switch (count($events)) :
			case 3:
				$performer1 = isset($events[0]->performers[0]) ? $events[0]->performers[0]->PerformerName : $events[0]->Name;
				$date1 = date('F j, Y', strtotime($events[0]->Date));
				$venue1 = clean_venue($events[0]->Venue);

				$performer2 = isset($events[1]->performers[0]) ? $events[1]->performers[0]->PerformerName : $events[1]->Name;
				$date2 = date('F j, Y', strtotime($events[1]->Date));
				$venue2 = clean_venue($events[1]->Venue);

				$performer3 = isset($events[2]->performers[0]) ? $events[2]->performers[0]->PerformerName : $events[2]->Name;
				$date3 = date('F j, Y', strtotime($events[2]->Date));
				$venue3 = clean_venue($events[2]->Venue);

				$text = "{$city} is ".random_pop(array("a great","a perfect","an amazing","an ideal"))." ".random_pop(array("location","place","city"))." to ".random_pop(array("attend","go to","see","be at"))." a concert. ".random_pop(array("At this time","Currently","As of today"))." there are a total of ".random_pop(array("three","3"))." concerts ".random_pop(array("scheduled in","penciled in for","ready to play in"))." {$city}. ".random_pop(array("Beginning on","Starting on","Starting with","First, on"))." {$date1}, {$performer1} ".random_pop(array("will be making an appearance at","will be seen at","will be visiting"))." {$venue1} for a very ".random_pop(array("exciting","popular","saught after"))." ".random_pop(array("show","concert","performance")).". ".random_pop(array("After that","Following this one","Additionally","Next")).", {$performer2} will be ".random_pop(array("visiting","coming to","making a stop in","performing in"))." {$city} on {$date2} at {$venue2}. The ".random_pop(array("third","final","last","3rd"))." concert in {$city} will be {$performer3} on {$date3} at {$venue3}. ".random_pop(array("We also carry","We also have","We sometimes carry","Often times we have","In certain instances we carry","In some cases we carry an inventory of"))." parking passes, VIP ".random_pop(array("passes","packages","tickets"))." and front row ".random_pop(array("passes","packages","tickets"))." for ".random_pop(array("certain","some","many"))." venues in {$city}. ".random_pop(array("Visit","Search","Check out","See"))." {$SScity} or follow {$sitename} for ".random_pop(array("more information about any","info about","news about any"))." ".random_pop(array("upcoming","new","newly added","newly scheduled"))." concerts in {$city} or any ".random_pop(array("updates","changes","modifications"))." to ".random_pop(array("any of these currently scheduled concerts","these scheduled concerts","any of the concerts already scheduled","the current list of concerts")).".";

				# code...
			break;

			case 2:
				$performer1 = isset($events[0]->performers[0]) ? $events[0]->performers[0]->PerformerName : $events[0]->Name;
				$date1 = date('F j, Y', strtotime($events[0]->Date));
				$venue1 = clean_venue($events[0]->Venue);

				$performer2 = isset($events[1]->performers[0]) ? $events[1]->performers[0]->PerformerName : $events[1]->Name;
				$date2 = date('F j, Y', strtotime($events[1]->Date));
				$venue2 = clean_venue($events[1]->Venue);

				$text = "{$city} is ".random_pop(array("a great","a perfect","an amazing","an ideal"))." ".random_pop(array("location","place","city"))." to ".random_pop(array("attend","go to","see","be at"))." a concert. ".random_pop(array("At this time","Currently","As of today"))." there are only ".random_pop(array("two","2"))." concerts ".random_pop(array("scheduled in","penciled in for","ready to play in"))." {$city}. ".random_pop(array("Beginning on","Starting on","Starting with","First, on"))." {$date1}, {$performer1} ".random_pop(array("will be making an appearance at","will be seen at","will be visiting"))." {$venue1} for a very ".random_pop(array("exciting","popular","saught after"))." ".random_pop(array("show","concert","performance")).". The ".random_pop(array("second","final","last","2nd"))." concert in {$city} will be {$performer2} on {$date2} at {$venue2}. ".random_pop(array("For everything you need","For more information","To get more information"))." about these ".random_pop(array("current concerts","currently scheduled concerts","upcoming concerts"))." in {$city}, ".random_pop(array("visit","search","check out","see"))." {$SScity} or follow {$sitename}. ".random_pop(array("We also carry","We also have","We sometimes carry","Often times we have","In certain instances we carry","In some cases there are"))." parking passes, VIP ".random_pop(array("passes","packages","tickets"))." and front row ".random_pop(array("passes","packages","tickets"))." for ".random_pop(array("certain","some","many"))." venues in {$city}.";
			break;

			case 1:
				$performer1 = isset($events[0]->performers[0]) ? $events[0]->performers[0]->PerformerName : $events[0]->Name;
				$date1 = date('F j, Y', strtotime($events[0]->Date));
				$venue1 = clean_venue($events[0]->Venue);

				$text = "{$city} is ".random_pop(array("a great","a perfect","an amazing","an ideal"))." ".random_pop(array("location","place","city"))." to ".random_pop(array("attend","go to","see","be at"))." a concert. ".random_pop(array("At this time","Currently","As of today"))." there is only ".random_pop(array("one","1","a single"))." concert ".random_pop(array("scheduled in","penciled in for","ready to play in"))." {$city}, which will feature {$performer1}. This ".random_pop(array("exciting","popular","saught after"))." event ".random_pop(array("will be making an appearance at","will be seen at","will be visiting"))." {$venue1} and will take place on {$date1}. Look for our selection of parking passes, VIP ".random_pop(array("passes","packages","tickets"))." and front row ".random_pop(array("passes","packages","tickets"))." for the venues in {$city}. ".random_pop(array("For everything you need","For more information","To get more information"))." about any ".random_pop(array("additional","new","newly scheduled"))." concerts coming to {$city}, ".random_pop(array("visit","search","check out","see"))." {$SScity} or ".random_pop(array("make sure to follow","follow us","be sure to follow us on Twitter","follow us on Twitter"))." {$sitename}.";
			break;

			case 0:
				$text = "{$city} is ".random_pop(array("a great","a perfect","an amazing","an ideal"))." ".random_pop(array("location","place","city"))." to ".random_pop(array("attend","go to","see","be at"))." a concert. Unfortunately, ".random_pop(array("at this time","currently","as of today"))." there are no concerts ".random_pop(array("scheduled in","penciled in for","playing in"))." {$city}. But ".random_pop(array("be sure","make sure","continue"))." to visit this page often and ".random_pop(array("visit","search","check out","see"))." {$SScity} or follow {$sitename} for ".random_pop(array("more information about any","info about","news about any"))." ".random_pop(array("upcoming","new","newly added","newly scheduled"))." concerts in {$city}.";
			break;

			default: //more than 3
			$highEvent = array();
				foreach ($events as $key => $event):
					$highEvent[$key] = ($event->prices) ? (int)$event->prices->highPrice : 0;
				endforeach;

				asort($highEvent,SORT_NUMERIC);
				end($highEvent);
				$topKey = key($highEvent);

				$top_performer = isset($events[$topKey]->performers[0]) ? $events[$topKey]->performers[0]->PerformerName : $events[$topKey]->Name;
				$top_venue = clean_venue($events[$topKey]->Venue);
				$top_event_date = date('F j, Y', strtotime($events[$topKey]->Date));

				$performer1 = isset($events[0]->performers[0]) ? $events[0]->performers[0]->PerformerName : $events[0]->Name;
				$date1 = date('F j, Y', strtotime($events[0]->Date));
				$venue1 = clean_venue($events[0]->Venue);

				$performer2 = isset($events[1]->performers[0]) ? $events[1]->performers[0]->PerformerName : $events[1]->Name;
				$date2 = date('F j, Y', strtotime($events[1]->Date));
				$venue2 = clean_venue($events[1]->Venue);

				$performer3 = isset($events[2]->performers[0]) ? $events[2]->performers[0]->PerformerName : $events[2]->Name;
				$date3 = date('F j, Y', strtotime($events[2]->Date));
				$venue3 = clean_venue($events[2]->Venue);
				$name3 = $events[2]->Name;

				$performer4 = isset($events[4]->performers[0]) ? $events[4]->performers[0]->PerformerName : $events[4]->Name;
				$date4 = date('F j, Y', strtotime($events[4]->Date));
				$venue4 = clean_venue($events[4]->Venue);

				$text = random_pop(array(
		                          " {$city} ".random_pop(array("has","will be having","curretly has","right now has"))." ".count($events)." concerts ".random_pop(array("penciled in","scheduled","ready to go","in the works"))." ".random_pop(array("so far","up to this point","at the moment")).", ".random_pop(array("begining with","starting with","featuring","leading with"))." {$performer1} on {$date1} at {$venue1}. ".random_pop(array("Other","Some more","Additional","More","Some other"))." ".random_pop(array("popular","much-anticipated","featured","exciting","big"))." concerts ".random_pop(array("coming up","scheduled","upcoming"))." in {$city} ".random_pop(array("will include","will be","will feature"))." {$performer2} on {$date2} in {$venue2}, {$name3} on {$date3} at {$venue3}, and {$performer4} on {$date4} at {$venue4}. ".random_pop(array("Currently","At this time","Right now","At the moment")).", the ".random_pop(array("most highly anticipated","most popular","hottest","biggest"))." concert ".random_pop(array("that will take place in","coming to","scheduled for"))." {$city} is {$top_performer} on {$top_event_date} at {$top_venue}. ".random_pop(array("We also carry","We also have","We sometimes carry","Often times we have","In certain instances we carry","In some cases we carry an inventory of"))." parking passes, VIP ".random_pop(array("passes","packages","tickets"))." and front row ".random_pop(array("passes","packages","tickets"))." for ".random_pop(array("certain","some","many"))." venues in {$city}. ".random_pop(array("Visit","Search","Check out","See"))." {$SScity} or follow {$sitename} for ".random_pop(array("more information about any","info about","news about any"))." ".random_pop(array("upcoming","new","newly added","newly scheduled"))." concerts in {$city} or any ".random_pop(array("updates","changes","modifications"))." to ".random_pop(array("any of these currently scheduled concerts","these scheduled concerts","any of the concerts already scheduled","the current list of concerts")).".
		                          "," {$city} ".random_pop(array("has quite an exciting","is ready for a robust","has a very busy","will be experiencing a full"))." concert schedule ".random_pop(array("coming up","ahead","planned"))." in the ".random_pop(array("near future","next few months","upcoming weeks")).". ".random_pop(array("To begin with","For starters","To start", "First")).", look to see {$performer1} ".random_pop(array("playing","performing","visiting"))." {$venue1} on {$date1}. ".random_pop(array("Then","After that","Following this","Next")).", ".random_pop(array("check out","look for","see"))." ".random_pop(array("many other exciting concerts","other great concerts","plenty of other entertaining concerts"))." that will be ".random_pop(array("visiting","coming to","stopping by","rolling into"))." {$city}, including {$performer2} on {$date2} in {$venue2}, {$name3} on {$date3} at {$venue3}, and {$performer4} on {$date4} at {$venue4}. {$top_performer} ".random_pop(array("will be","is expected to be","will probably end up being"))." ".random_pop(array("the most anticipated","one of the most popular","a very highly anticipated","a highly popular"))." concert, which ".random_pop(array("will take place","will be held","is scheduled"))." on {$top_event_date} at {$top_venue}. ".random_pop(array("For everything you need","For more information","To get more information"))." about these ".random_pop(array("current concerts","currently scheduled concerts","upcoming concerts"))." in {$city}, ".random_pop(array("visit","search","check out","see"))." {$SScity} or follow {$sitename}. ".random_pop(array("We also carry","We also have","We sometimes carry","Often times we have","In certain instances we carry","In some cases there are"))." parking passes, VIP ".random_pop(array("passes","packages","tickets"))." and front row ".random_pop(array("passes","packages","tickets"))." for ".random_pop(array("certain","some","many"))." venues in {$city}.
		                          "," {$city} is ".random_pop(array("a great","a perfect","an amazing and ideal"))." ".random_pop(array("location","place","city",""))." to ".random_pop(array("attend","go to","see","be at"))." a concert, and with ".count($events)." concerts ".random_pop(array("currently scheduled","that are upcoming","that will be happening","that will be rocking"))." in the {$city}, you will not be ".random_pop(array("disappointed","bored")).". ".random_pop(array("Beginning on","Starting on","Starting with","First, on"))." {$date1}, {$performer1} ".random_pop(array("will be making an appearance","can be seen at","will be visiting"))." {$venue1} on {$date1}. ".random_pop(array("After that","Following this one","Additionally")).", there will be ".random_pop(array("tons of","plenty of","many more"))." ".random_pop(array("exciting","popular","saught after"))." concerts ".random_pop(array("visiting","coming to","making stops in"))." {$city} in the ".random_pop(array("coming months","next few months","following weeks ahead")).", ".random_pop(array("like","including","especially"))." the ".random_pop(array("highly-anticipated","very popular","most saught after"))." concert at {$top_venue} on {$top_event_date} that ".random_pop(array("will feature","features","will include","will see"))." {$top_performer}. Other concerts ".random_pop(array("visiting","coming to","making stops in","performing in"))." {$city} include {$performer2} on {$date2} in {$venue2}, {$name3} on {$date3} at {$venue3}, and {$performer4} on {$date4} at {$venue4}. Look for our selection of parking passes, VIP ".random_pop(array("passes","packages","tickets"))." and front row ".random_pop(array("passes","packages","tickets"))." for ".random_pop(array("certain","some","many"))." venues in {$city}. ".random_pop(array("For everything you need","For more information","To get more information"))." about these ".random_pop(array("current concerts","currently scheduled concerts","upcoming concerts"))." in {$city}, ".random_pop(array("visit","search","check out","see"))." {$SScity} or ".random_pop(array("make sure to follow","follow us","be sure to follow us on Twitter","follow us on Twitter"))." {$sitename}."));

				break;
		endswitch;

		// var_dump($events);
/*		$rand1 = rand(1, count($events) - 2);
		$rand2 = rand(1, count($events) - 2);
		$rand3 = rand(1, count($events) - 2);
		$rand4 = rand(1, count($events) - 2);

*/







	return $text;
	}

	/**
	 * generates text for god knows what.... THE FUCK IS THIS??
	 * @param  array/bool $events
	 * @param  string $section
	 * @return string
	 */
	private function _generate_text($events, $section = 'city')
	{
		$a = array('currently penciled in so far', 'at this moment', 'currently scheduled', 'listed right now', 'happening this season', 'for this season');
		$b = array('starting', 'begining', 'kicking off');
		$c = array('Other popular concerts scheduled', 'Other hot concerts', 'List of other concerts booked', 'Many other events');
		$d = array('At this time', 'Currently', 'At the present moment', 'Today', 'Clearly', "Right now");
		$e = array('the hottest concert set to take place', 'the most exciting show', 'the most anticipated performance', ' the long awaited concert', 'the hottest concert');
		switch ($section) :
			case 'city':
				if(count($events) > 1):
					return "{event_city} has {event_count} concerts ".random_pop($a).", ".random_pop($b)." with {event_start} on {event_date_start} at {event_venue_start}. {other_events_insert} ".random_pop($d).", ".random_pop($e)." in {event_city} is {event_hottest} on {event_date_hottest} at {event_venue_hottest}. We also carry parking passes, VIP packages and front row tickets for concerts in all venues in {event_city}. Check out {ss_twitter_hash} or follow {ss_twitter_page} for everything you need for new concerts in {event_city} or any changes to these currently scheduled concerts.";
				else:
					return "{event_city} has only one concert scheduled, with {event_start} on {event_date_start} at {event_venue_start}. We also carry parking passes, VIP packages and front row tickets for concerts in all venues in {event_city}. Check out {ss_twitter_hash} or follow {ss_twitter_page} for everything you need for new concerts in {event_city} or any changes to these currently scheduled concerts.";
				endif;
			break;

			case 'city_venue':
				if(count($events) > 1):
					return "{event_venue_start} has {event_count} concerts ".random_pop($a).", ".random_pop($b)." with {event_start} on {event_date_start}. {other_events_insert}. ".random_pop($d).", ".random_pop($e)." in {event_city} is {event_hottest} on {event_date_hottest} at {event_venue_hottest}. We also carry parking passes, VIP packages and front row tickets for concerts in all venues in {event_city}. Check out {ss_twitter_hash} or follow {ss_twitter_page} for everything you need for new concerts in {event_city} or any changes to these currently scheduled concerts.";
				else:
					return "{event_venue_start} has only one concert scheduled, with {event_start} on {event_date_start}. We also carry parking passes, VIP packages and front row tickets for concerts in all venues in {event_city}. Check out {ss_twitter_hash} or follow {ss_twitter_page} for everything you need for new concerts in {event_city} or any changes to these currently scheduled concerts.";
				endif;
			break;

			case 'city_date':
				if(count($events) > 1):
					return "In {chosen_month}, {event_city} has {event_count} concerts ".random_pop($a).", ".random_pop($b)." with {event_start} on {event_date_start}. {other_events_insert}. ".random_pop($d).", ".random_pop($e)." in {event_city} is {event_hottest} on {event_date_hottest} at {event_venue_hottest}. We also carry parking passes, VIP packages and front row tickets for concerts in all venues in {event_city}. Check out {ss_twitter_hash} or follow {ss_twitter_page} for everything you need for new concerts in {event_city} or any changes to these currently scheduled concerts.";
				else:
					return "{event_venue_start} has only one concert scheduled, with {event_start} on {event_date_start}. We also carry parking passes, VIP packages and front row tickets for concerts in all venues in {event_city}. Check out {ss_twitter_hash} or follow {ss_twitter_page} for everything you need for new concerts in {event_city} or any changes to these currently scheduled concerts.";
				endif;

			break;
			case 'category':
				if(count($events) > 1):
			     return "{event_city} has {event_count} {chosen_genre} concerts ".random_pop($a).", ".random_pop($b)." with {event_start} on {event_date_start}. {other_events_insert}. ".random_pop($d).", ".random_pop($e)." in {event_city} is {event_hottest} on {event_date_hottest} at {event_venue_hottest}. We also carry parking passes, VIP packages and front row tickets for concerts in all venues in {event_city}. Check out {ss_twitter_hash} or follow {ss_twitter_page} for everything you need for new {chosen_genre} concerts in {event_city} or any changes to these currently scheduled {chosen_genre} concerts.";
			    else:
			     return "{event_venue_start} has only one concert scheduled, with {event_start} on {event_date_start}. We also carry parking passes, VIP packages and front row tickets for {chosen_genre} concerts in all venues in {event_city}. Check out {ss_twitter_hash} or follow {ss_twitter_page} for everything you need for new concerts in {event_city} or any changes to these currently scheduled {chosen_genre} concerts.";
			    endif;
			break;
			default:
				# code...
				break;
		endswitch;
	}

	/**
	 * gets text
	 * @param  string $slug
	 * @param  string $type
	 * @return obj
	 */
	private function _fetch_text($slug, $type = 'main')
	{
		$slug = $this->uri->segment(2);
		return $this->db->select('*')->from('page_text')->where(array('slug'=> $slug, 'type' => $type))->get()->row();
	}

}

/* End of file spinner_m.php */
/* Location: ./application/models/spinner_m.php */