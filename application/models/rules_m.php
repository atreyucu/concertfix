<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rules_m extends CI_Model {

	private $mongo;
	private $mdb;

	public function __construct()
	{
		parent::__construct();
		$this->mongo = new MongoClient("localhost:27017"); // connect
		// $this->mdb = $his->mongo->selectDB("concertfix");
	}

	public function fetch_rules($rule, $userId = false)
	{
		if(!$this->cfUser['loggedin']) return false;

		switch ($rule) {
			case 'performer':
				$rules = $this->db->select('urp.id rId, pf.Name performerName, pf.PerformerID pId, pf.PerformerSlug pSlug, pi.thumb img, lfmImagePath limg')
									->from('user_rules_performer urp')
									->join('performer_full pf', 'pf.PerformerID = urp.performerId')
									->join('performer_images pi', 'urp.performerId = pi.PerformerID', 'left')
									->join('lfm_performer_images li', 'pf.PerformerID = li.PerformerID','left')
									->where(array('urp.cfUserId' => $userId))
									->order_by('pf.Name','asc')
									->get()->result();
				# code...
			break;
			case 'city':
				$rules = $this->db->select('urc.id rId, urc.cityName, urc.citySlug cSlug')
									->from('user_rules_city urc')
									->where(array('urc.cfUserId' => $userId))
									->order_by('urc.cityName','asc')
									->get()->result();
			break;
			case 'genre':
				$rules = $this->db->select('urg.genreName, urg.genreId gId')
									->from('user_rules_genre urg')
									->where(array('urg.cfUserId' => $userId))
									->order_by('urg.genreName','asc')
									->get()->result();
				break;
			case 'genereids':
					$tmp = $this->db->select('urg.genreId gId')
									->from('user_rules_genre urg')
									->where(array('urg.cfUserId' => $userId))
									->order_by('urg.genreName','asc')
									->get()->result_array();
					$rules = array();
					foreach ($tmp as $r):
						array_push($rules, $r['gId']);
					endforeach;
				break;
			default:
				# code...
				break;
		}
		return $rules;

	}
	/*
	select *
from cached_events ce
join event_performers ep ON ep.EventID = ce.EventID
where
	(ce.LocationSlug = 'chicago-il' OR ce.LocationSlug = 'hallandale-fl')
AND
ce.Date > NOW()
AND
ep.PerformerID = '53909'
GROUP BY ce.EventID
ORDER BY ce.Date Asc;
*/
	public function get_feed_data($userId = false)
	{

		$rules = $this->fetch_tracking_rules($userId);
		if($rules['cities'] && $rules['performers']):
			$whereCities = implode('" OR ce.LocationSlug ="', $rules['cities']);
			$this->db->where("(ce.LocationSlug = \"{$whereCities}\" )");
			$wherePerformers = implode(' OR ep.PerformerID =', $rules['performers']);
			$this->db->where("(ep.PerformerID = {$wherePerformers} )");
		else:
			return false;
		endif;
        //$q['performers'] = $this->db->select('ce.EventID, ce.VenueID, ce.EventSlug, ce.Name, ce.City, ce.Date, ce.StateProvince, ce.Venue, ce.LocationSlug, tn_v.VenueSlug')

		$q['performers'] = $this->db->select('*')
					->from('cached_events ce')
					->join('event_performers ep', "ep.EventID = ce.EventID")
					->where('ce.Date > NOW()')
					->group_by('ce.EventID')
					->order_by('ce.Date','asc')
					->get()
					->result();

		if($rules['genres']):
			$whereGenre = implode(' OR pf.ChildCategoryID =', $rules['genres']);
			$this->db->where("(pf.ChildCategoryID = {$whereGenre} )");
			$whereCities = implode('" OR ce.LocationSlug ="', $rules['cities']);
			$this->db->where("(ce.LocationSlug = \"{$whereCities}\" )");
		endif;
		$q['genres'] = $this->db->select('*')
					->from('cached_events ce')
					->join('event_performers ep', "ep.EventID = ce.EventID")
					->join('performer_full pf', 'ep.PerformerID = pf.PerformerID')
					->join('category_filters cf','cf.ChildCategoryID = pf.ChildCategoryID')
					->where('ce.Date > NOW() AND ce.Date < ADDDATE(NOW(), 180)')
					->group_by('ce.EventID')
					->order_by('ce.ChildCategoryID')
					->order_by('ce.StateProvince')
					->order_by('ce.Date','asc')
					// ->limit(25)
					->get()
					->result();
					// echo $this->db->last_query();
		return $q;
	}

	public function fetch_tracking_rules($userId = false)
	{
		//first get list of track cities
		$cities = $this->fetch_rules('city', $userId);
		$data['cities'] = array();
		foreach ($cities as $c):
			array_push($data['cities'], $c->cSlug);
		endforeach;
		//now the performers
		$pers = $this->fetch_rules('performer', $userId);
		$data['performers'] = array();
		foreach ($pers as $p):
			array_push($data['performers'], $p->pId);
		endforeach;
		//now the genre ids
		$data['genres'] = $this->fetch_rules('genereids', $userId);
		return $data;
	}

	public function remove_rule($id, $uId = false, $type=false)
	{
		if($type == 'genre'): //remove rule that's set to 0
			$this->db->where(array('genreId' => $id, 'cfUserId' => $uId ));
			return $this->db->delete("user_rules_{$type}");
		endif;
		if($uId === $this->cfUser['cf']['id']): //just in case ddouble check user
			$this->db->where(array('id' => $id, 'cfUserId' => $uId ));
			return $this->db->delete("user_rules_{$type}");
		else:
			return false;
		endif;

	}

	public function get_user_rules($userId, $sort = 'createdDate')
	{
		$collection = $this->mongo->selectCollection('concertfix', 'user_rules');


	/*	$results = $collection->aggregate(
					array(
							array( 	'$match' 	=> array('cfUserId' => $userId)),
							array(	'$group'	=> array(
										'_id' =>  '$groupId',
										'cfUserId' => array('$push' => '$cfUserId'),
										'performerId' => array('$push' => '$performerId'),
														),
								),
							array(	'$sort' 	=> array('createdDate' => 1)),
						)
										);
		var_dump($results);*/


 		$query = array('cfUserId' => $userId);
 		$cursor =  $collection->find($query)->sort(array($sort => -1));
		return $cursor;
		/*
		$keys = array("groupId" => 1, 'performerId' => 1);
		$initial = array("items" => array());
		$reduce = "function (obj, prev) { prev.items.push(obj); }";
		$cond = array('condition' => array('cfUserId' => $userId));
		$g = $collection->group($keys, $initial, $reduce, $cond);
 		return (($g['retval']));
*/
		//
	}

	public function add_rule($rules = array())
	{
		if(empty($rules)) return false;
		$collection = $this->mongo->selectCollection('concertfix', 'user_rules');
		return $collection->batchInsert($rules);
	}

	public function insert_rule($rule, $ruleType)
	{
		return $this->db->insert('user_rules_'.$ruleType, $rule);
	}

	public function already_tracking($id, $type = false)
	{
		switch ($type) :
			case 'performer':
				return $this->db->select('count(id) cnt')
					->from('user_rules_performer')
					->where(array('performerId' => $id, 'cfUserId' => $this->cfUser['cf']['id']))
					->get()
					->row()
					->cnt;
			break;
			case 'city':
				return $this->db->select('count(id) cnt')
					->from('user_rules_city')
					->where(array('citySlug' => $id, 'cfUserId' => $this->cfUser['cf']['id']))
					->get()
					->row()
					->cnt;
			break;
			case 'genere':
				return $this->db->select('count(id) cnt')
					->from('user_rules_genre')
					->where(array('genreId' => $id, 'cfUserId' => $this->cfUser['cf']['id']))
					->get()
					->row()
					->cnt;
			break;
			default:
				# code...
				break;
		endswitch;
	}

	public function rule_exists($rule = array())
	{
		$collection = $this->mongo->selectCollection('concertfix', 'user_rules');
		switch ($rule['ruleType']):
			case 'performer-city':
					$find = array(
						'performerId' => $rule['performerId'],
						'citySlug' => $rule['citySlug'],
						'ruleType' => $rule['ruleType'],
						'cfUserId' => $rule['cfUserId'],
						'notifyEmail' => $rule['notifyEmail'],
						'notifyByFeed' => $rule['notifyByFeed'],
						'notifyByEmail' => $rule['notifyByEmail'],
						'notifyDays' => $rule['notifyDays'],
						'notifyDaysNum' => $rule['notifyDaysNum'],
						'notifyAsOccurs' => $rule['notifyAsOccurs'],
						);
			break;

			default:
				$find = array();
			break;
		endswitch;
		$result = $collection->count($find);
		return $result;
	}


	function docities()
	{
		return false;
		$cities = $this->db->select('*')->from('geo_cities')->group_by('slug')->get()->result();
		$collection = $this->mongo->selectCollection('concertfix', 'geo_table');

		foreach ($cities as $c) {
			# code...
			$longState = toggle_state($c->region);
			$insert = array(
					'country'		=>	$c->country,
					'short_state'	=> 	$c->region,
					'state'			=> 	$longState,
					'zip'			=> 	$c->postalCode,
					'city'			=> 	$c->city,
					'slug'			=> 	$c->slug,
					'search_terms'	=>	seoUrl($c->slug, ' ')." ".strtolower($c->city)." ".strtolower($longState)
				);
			$collection->insert($insert);
			echo $c->slug."<br>";
		}
	}

	function search_city($query)
	{
		$query = trim(seoUrl($query, ' '));
		$collection = $this->mongo->selectCollection('concertfix', 'geo_table');
		$regex = new MongoRegex("/\b{$query}\b/i");
		$results = $collection->find(array('search_terms'=> $regex));
		$return = array();
		foreach ($results as $val) {
			$temp = array('value' => $val['city'].", ".$val['short_state']." - ".$val['country'], 'slug' => $val['slug']);
			array_push($return, $temp);
		}
		return $return;
	}

	function search_city_wc($query)
	{
		$query = trim(seoUrl($query, ' '));
		$collection = $this->mongo->selectCollection('concertfix', 'geo_table');
		$regex = new MongoRegex("/\b{$query}\b/i");
		$results = $collection->find(array('search_terms'=> $regex));
		$return = array();
		foreach ($results as $val) {
			$temp = array('value' => $val['city'].", ".$val['short_state'], 'slug' => $val['slug']);
			array_push($return, $temp);
		}
		return $return;
	}

	public function get_performer_name($performer_id){
		$temp = $this->db
			->select('pf.Name PerformerName')
			->from('performer_full pf')
			->where('pf.PerformerID', $performer_id)
			->get()->row();
		return $temp;
	}


    public function get_upcoming_events($performers, $locations, $days = false, $user_id = false)
    {
        if(empty($performers)) return false;
        switch ($days):
            case 14:
            case 18:
            case 60:
                $whereCities = implode('" OR ce.LocationSlug ="', $locations);
                $this->db->where("(ce.LocationSlug = \"{$whereCities}\" )");
                $wherePerformers = implode(' OR ep.PerformerID =', $performers);
                $this->db->where("(ep.PerformerID = {$wherePerformers} )");

                $events = $this->db->select('*')
                    ->from('cached_events ce')
                    ->join('event_performers ep', "ep.EventID = ce.EventID")
                    ->where('DATE(ce.Date) = DATE(DATE_ADD( NOW(), INTERVAL '.$days.' DAY ))')
                    ->group_by('ce.EventID')
                    ->order_by('ce.Date','asc')
                    ->get()
                    ->result();
                break;

            default:
                $whereCities = implode('" OR ce.LocationSlug ="', $locations);
                $this->db->where("(ce.LocationSlug = \"{$whereCities}\" )");
                $wherePerformers = implode(' OR ep.PerformerID =', $performers);
                $this->db->where("(ep.PerformerID = {$wherePerformers} )");
                $events = $this->db->select("*, (SELECT count(nl.id) ann FROM notification_log nl WHERE nl.user_id = {$user_id} AND nl.event_id = ce.EventID) ann")
                    ->from('cached_events ce')
                    ->join('event_performers ep', "ep.EventID = ce.EventID")
                    ->where('ce.Date > NOW()')
                    ->where('DATE(ce.Date) <> DATE(DATE_ADD( NOW(), INTERVAL 60 DAY ))')
                    ->where('DATE(ce.Date) <> DATE(DATE_ADD( NOW(), INTERVAL 14 DAY ))')
                    ->having('ann = 0')
                    ->group_by('ce.EventID')
                    ->order_by('ce.Date','asc')
                    ->get()
                    ->result();
                    // echo $this->db->last_query()."\n\r<br><br>";
                break;
        endswitch;
        $events = $this->global_m->get_event_performers($events);
        return $events;
    }

	public function get_upcoming_events_by_genres($genres, $locations, $user_id = false)
	{
		if(empty($genres)) return false;

		$events = $this->db->select("*, (SELECT count(nl.id) ann FROM notification_log nl WHERE nl.user_id = {$user_id} AND nl.event_id = ce.EventID) ann")
			->from('cached_events ce')
			->join('event_performers ep', "ep.EventID = ce.EventID")
			->join('performer_full pf', 'ep.PerformerID = pf.PerformerID')
			->join('category_filters cf','cf.ChildCategoryID = pf.ChildCategoryID')
			->where('ce.Date >= NOW()')
			//->where('DATE(ce.Date) >= DATE(NOW())')
			->where('DATE(ce.Date) < DATE(DATE_ADD( NOW(), INTERVAL 7 DAY ))')
			->where_in('cf.id', $genres)
			->where_in('ce.LocationSlug', $locations)
			->having('ann = 0')
			->group_by('ce.EventID')
			->order_by('ce.Date','asc')
			->get()
			->result();
			//echo $this->db->last_query()."\n\r";

		$events = $this->global_m->get_event_performers($events);
		return $events;
	}

	public function get_new_events_by_genres($genres, $locations, $days = false, $user_id = false)
	{
		if(empty($genres)) return false;

		$events = $this->db->select("*, (SELECT count(nl.id) ann FROM notification_log nl WHERE nl.user_id = {$user_id} AND nl.event_id = ce.EventID) ann")
			->from('cached_events ce')
			->join('event_performers ep', "ep.EventID = ce.EventID")
			->join('performer_full pf', 'ep.PerformerID = pf.PerformerID')
			->join('category_filters cf','cf.ChildCategoryID = pf.ChildCategoryID')
			->where('ce.AddedDate <= NOW()')
			->where('ce.AddedDate > DATE(DATE_SUB( NOW(), INTERVAL '.$days.' DAY ))')
			->where_in('cf.id', $genres)
			->where_in('ce.LocationSlug', $locations)
			->having('ann = 0')
			->group_by('ce.EventID')
			->order_by('ce.Date','asc')
			->get()
			->result();
		//echo $this->db->last_query()."\n\r";

		$events = $this->global_m->get_event_performers($events);
		return $events;
	}

	public function add_to_notify_log($uId, $event, $days)
	{
		// echo "ADDED UPDATED";
	}

    public function get_rule_concert_by_user($userid)
    {
        return $this->db->select('*')->from('user_rules_concerts')->where(array('userid' => $userid))->get()->row();
    }

    #new_concert_freq => default = 2 (Once a week)
    #upcoming_concert_freq => default = 1 (Once a week)
    public function insert_rule_concert($userId, $new_concert_freq = 2, $upcoming_concert_freq = 1)
    {
        if($this->exists_rule_concert() == 0) {
            $data = array('userid' => $userId, 'new_concert' => $new_concert_freq, 'upcoming_concert' => $upcoming_concert_freq);
            return true;
        }
        return false;

    }
    public function exists_rule_concert($user_id = null){

        if($user_id == null)
        {
            $user_id = $this->cfUser['cf']['id'];
        }
        $data = $this->db->select('count(*) as cnt')->from('user_rules_concerts')->where(array('userid' => $user_id));
        $cnt = $data->get()->row()->cnt;
        return $cnt;

    }
    public function change_rule_new_concert($userId, $frequency)
    {
        if($frequency >= 0 and $frequency < 4)
        {
            $this->db->where('userid', $userId);
            $this->db->update('user_rules_concerts', array('new_concert'=> $frequency));
            return true;
        }
        else{
            return false;
        }


    }
    public function change_rule_upcoming_concert($userId, $frequency)
    {
        if ($frequency == 0 or $frequency == 1) {
            $this->db->where('userid', $userId);
            $this->db->update('user_rules_concerts', array('upcoming_concert' => $frequency));
            return true;
        } else {
            return false;
        }
    }

	public function change_miles($userId, $miles = 10){
		if ($miles == 10 or $miles == 25 or $miles == 50) {
			$this->db->where('userid', $userId);
			$this->db->update('user_rules_concerts', array('miles' => $miles));
			return true;
		} else {
			return false;
		}
	}

	public function change_home_city($userId, $citySlug, $cityName){
		if ($citySlug != null and $cityName != null) {
			$this->db->where('userid', $userId);
			$this->db->update('user_rules_concerts', array('citySlug' => $citySlug, 'cityName' => $cityName));
			return true;
		} else {
			return false;
		}
	}

	public function change_last_email($userId, $date){
		try{
			$this->db->where('userid', $userId);
			$this->db->update('user_rules_concerts', array('last_email' => $date));
			return true;
		} catch (Exception $e){
			return false;
		}
	}

    public function add_genre($rule_concert_id, $genre_id)
    {
        if(!$this->exists_genre($rule_concert_id, $genre_id))
        {
            $data = array('rule_concert_id' => $rule_concert_id, 'category_id' => $genre_id);
            $this->db->insert('user_rules_concerts_genre',$data);
        }
        return true;
    }

	public function add_rule_concert_userid($user_id, $new_concert = 2, $upcoming_concert = 1, $miles = 10)
	{
		if($this->exists_rule_concert($user_id) == 0) {
			$data = array('new_concert' => $new_concert, 'upcoming_concert' => $upcoming_concert, 'userid' => $user_id, 'miles' => $miles, 'last_email' => date('Y-m-d'));
			$this->db->insert('user_rules_concerts', $data);

			$concert_rule = $this->get_rule_concert_by_user($user_id);
			$this->load->model('concerts_m');
			$categories = $this->concerts_m->get_categories_user_settings();
			foreach($categories as $category){
				$data = array('rule_concert_id' => $concert_rule->id, 'category_id' => $category->id);
				$this->db->insert('user_rules_concerts_genre', $data);
			}

			return true;
		}
		return false;

	}
	public function exists_rules_concert_by_userid($user_id)
	{
		$data = $this->db->select('count(*) as cnt')
			->from('user_rules_concerts rc')
			->where(array('userid' => $user_id));
		$cnt = $data->get()->row()->cnt;
		if($cnt == 0)
			return false;
		else
			return true;
	}
    public function exists_genre($rule_concert_id, $genre_id)
    {
        $data = $this->db->select('count(*) as cnt')->from('user_rules_concerts_genre')->where(array('rule_concert_id' => $rule_concert_id, 'category_id' => $genre_id));
        $cnt = $data->get()->row()->cnt;
        if($cnt == 0)
            return false;
        else
            return true;
    }
    public function exists_genre_by_userid($userid, $genre_id)
    {
        $data = $this->db->select('count(*) as cnt')
            ->from('user_rules_concerts_genre rcg')
            ->join('user_rules_concerts rc', 'rcg.rule_concert_id = rc.id')
            ->where(array('userid' => $userid, 'category_id' => $genre_id));
        $cnt = $data->get()->row()->cnt;
        if($cnt == 0)
            return false;
        else
            return true;
    }
	public function get_genres_by_userid($userid)
	{
		$data = $this->db->select('rcg.category_id')
			->from('user_rules_concerts_genre rcg')
			->join('user_rules_concerts rc', 'rcg.rule_concert_id = rc.id')
			->where(array('rc.userid' => $userid))
			->get()
			->result();
		return $data;
	}
    public function remove_genre($rule_concert_id, $genre_id)
    {
        if ($genre_id == 'all')
			$this->db->where(array('rule_concert_id' => $rule_concert_id));
		else
			$this->db->where(array('rule_concert_id' => $rule_concert_id, 'category_id' => $genre_id));
        $this->db->delete('user_rules_concerts_genre');
        return true;
    }

	public function get_cron_date($cron_name)
	{
		$data = $this->db->select('*')
			->from('cron_run_dates crd')
			->where(array('crd.cron_name' => $cron_name))
			->get()
			->row();
		return $data;
	}

	public function change_cron_start_date($cron_name, $date){
		try{
			$this->db->where('cron_name', $cron_name);
			$this->db->update('cron_run_dates', array('start_date' => $date));
			return true;
		} catch (Exception $e){
			return false;
		}
	}

	public function change_cron_last_date($cron_name, $date){
		try{
			$this->db->where('cron_name', $cron_name);
			$this->db->update('cron_run_dates', array('final_date' => $date));
			return true;
		} catch (Exception $e){
			return false;
		}
	}
}

/* End of file rules_m.php */
/* Location: ./application/models/rules_m.php */