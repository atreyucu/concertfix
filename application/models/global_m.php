<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Global_m extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function where_am_i()
	{

		$ip = ($this->input->get('ip')) ? $this->input->get('ip') :	$this->input->ip_address() ;
		// echo $ip;
		// $ip = "108.92.157.190";
		$location = $this->db->select('c.city, c.region state, c.slug')
							->from('geo_ip i')
							->join('geo_cities c','c.locId = i.locId')
							->where("INET_ATON('".$ip."') BETWEEN startIpNum AND endIpNum")
							->limit(1)->get()->row_array();
		//echo "<p>".$$this->db->last_query()."</p>";
		return $location;

	}

	public function get_fetured_performers($where = 'both', $getArray = false, $location = false)
	{
	switch ($where) :
		case 'front':
			$result = $this->db
				->select('p.*, i.lfmImagePath img, pm.wide, pm.profile, pm.thumb, p.fbImagePath fbImage,
				         	(SELECT count(ep.id) event_count FROM event_performers ep WHERE ep.PerformerID = p.PerformerID) event_count
				         	')
				->from('performer_featured f')
				->join('performer_full p','p.PerformerID = f.PerformerID')
				->join('performer_images pm', 'pm.PerformerID = p.PerformerID', 'left')
				->join('lfm_performer_images i','p.PerformerID = i.PerformerID','left')
				->where('f.front','1')
				->group_by('p.PerformerID');
				if($getArray):

					$q = $this->db->get()->result_array();
					foreach ($q as $key => $p):
						$q[$key]['event'] = $this->get_performer_event($p->PerformerID, $location);
					endforeach;
					return $q;
				else:
					$q = $this->db->get()->result();
					foreach ($q as $key => $p):
						$q[$key]->event = $this->get_performer_event($p->PerformerID, $location);
					endforeach;
					return $q;
				endif;
			break;
		case 'sidebar':
			$this->db->where('f.side','1');
			break;
		case 'geo':
			$this->db->where('f.geo','1');
			break;
		case 'both':
		default:
			$this->db->where('f.front','1');
			$this->db->where('f.side','1');
			break;
	endswitch;
	$result = $this->db
				->select('p.*, i.lfmImagePath img, pm.wide, pm.profile, pm.thumb, p.fbImagePath fbImage')
				->from('performer_featured f')
				->join('performer_full p','p.PerformerID = f.PerformerID')
				->join('performer_images pm', 'pm.PerformerID = p.PerformerID', 'left')
				->join('lfm_performer_images i','p.PerformerID = i.PerformerID','left')
				->group_by('p.PerformerID');
				if($getArray):
					return $this->db->get()->result_array();
				else:
					return $this->db->get()->result();
				endif;
	}

	public function get_performer_event($id, $location = false)
	{
		if($location):
			$this->db->where(array('e.City' => $location['city'], 'e.StateProvince' => $location['state']));
		endif;
		return $this->db
						->select('e.Date, e.Venue, e.City, e.Date, v.VenueSlug, v.RegionSlug, e.VenueID, e.Name')
						->from('cached_events e')
						->join('tn_venues v','v.ID = e.VenueID')
						->join('event_performers ep', 'e.EventID = ep.EventID')
						->where("e.Date > NOW()")
						->where('ep.PerformerID', $id)
						->order_by('e.Date', 'asc')
						->limit(1)
						->get()->row();
	}

	public function get_event_performers_simple($eventID) {
		if(!$eventID) return false;
		$performers = $this->db
			->query("SELECT GROUP_CONCAT(PerformerName SEPARATOR ', ') as performers
			FROM event_performers
			WHERE EventID = $eventID")
			->row();
		return $performers ? $performers->performers : false;
	}
	// SELECT GROUP_CONCAT(`PerformerName` SEPARATOR ', ') FROM event_performers WHERE EventID = 2466057;

	public function get_upcoming_tour_annoucement($limit = 5, $offset = 0)
	{
		/*
		if($offset):
			$this->db->limit($offset, $limit);
		else:
			$this->db->limit($limit);
		endif;
	*/
		$q = $this->db->select('pf.Name PerformerName, pf.PerformerID, pf.PerformerSlug, i.lfmImagePath img, pm.wide, pm.profile, pm.thumb, ta.announce_text text, ta.title, ta.announce_date announced_on')
					->from('tour_announcements ta')
					->join('performer_full pf', 'pf.PerformerID = ta.performerID')
					->join('performer_images pm', 'pm.PerformerID = pf.PerformerID', 'left')
					->join('lfm_performer_images i','pf.PerformerID = i.PerformerID','left')
					->where(array('ta.active' => 1))
					->order_by('ta.announce_date','Desc')
					->limit($limit, $offset)
					->get()
					->result();
					return $q;
	}

	public function get_upcoming_tour_annoucement_without_geo()
	{

		$q = $this->db->select('pf.Name Name, i.lfmImagePath img')
			->from('tour_announcements ta')
			->join('performer_full pf', 'pf.PerformerID = ta.performerID')
			->join('performer_featured feat', 'feat.PerformerID = ta.PerformerID', 'left')
			->join('lfm_performer_images i','ta.PerformerID = i.PerformerID','left')
			->where(array('ta.active' => 1, 'feat.geo' => 0))
			//->order_by('ta.announce_date','Desc')
			//->limit($limit, $offset)
			->distinct()
			->get()
			->result();
		return $q;
	}

	public function fetch_categorized_announcements()
	{
		$cats = $this->db->select()->from('announcement_categories')->get()->result();
		$ret = array();
		foreach ($cats as $cat) :
			$ret[$cat->Category] = $this->db->select('ac.*, pf.Name PerformerName, pf.PerformerSlug, i.lfmImagePath img, pm.wide, pm.profile, pm.thumb, ta.announce_text text, ta.title, ta.announce_date announced_on')
											->from('announcement_category ac')
											->join('tour_announcements ta', 'ta.id = ac.a_id')
											->join('performer_full pf', 'pf.PerformerID = ta.performerID')
											->join('performer_images pm', 'pm.PerformerID = pf.PerformerID', 'left')
											->join('lfm_performer_images i','pf.PerformerID = i.PerformerID','left')
											->where('ac.a_category_id',$cat->id)
											->where('ta.active',1)
											->order_by('ta.announce_date','desc')
											->get()->result();
		endforeach;
		return $ret;

	}

	public function get_top_cities()
	{
		$areas = $this->db->select('area')->from('top_cities')->where('area <> ""')->group_by('area')->get()->result();
		$ret = array();
		foreach ($areas as $a) :
			$ret[$a->area] = $this->db->select('city, state_short, slug')->from('top_cities')->where('area', $a->area)->get()->result();
		endforeach;
		return $ret;
	}

	public function count_posts()
	{
		return $this->db->select('count(a.id) cnt')
					->from('tour_announcements a')
					->where(array('a.active' => 1))
					->get()->row()->cnt;
	}


	public function get_front_page_events($location = false, $limit = 8)
	{
		// var_dump($location);
		if(!$location) return false;
		$allInCity = $this->db->select('e.EventID ID, e.Name,  e.City, e.StateProvince, e.Venue, e.Date, v.VenueSlug, v.RegionSlug')
							->from('event_performers ep')
							->join('cached_events e', 'e.EventID = ep.EventID')
							->join('tn_venues v','v.ID = e.VenueID','left')
							->join('performer_featured f','ep.PerformerID = f.PerformerID')
							->where(
							        array(
							              //'f.geo' => 1,
							              'e.City' => strtoupper($location['city']),
							              'e.StateProvince' => ucwords($location['state']),
							              )
							        )
							->where('e.Date > NOW()')
							->group_by('e.Name')
							->order_by('e.Date', 'desc')
							//->limit(25)
							->get()->result();
				//			echo $this->db->last_query();
		//if (count($allInCity) < 100) {}
		$nearByCities = $this->get_nearby_cities($location, 50, 30);
		$uniqueCities = array();
		$otherEvents = array();
		foreach ($nearByCities as $city) :
			$uniqueCities[$city['city']]['city'] = $city['city'];
			$uniqueCities[$city['city']]['state'] = $city['state'];
		endforeach;
		$uniqueCities = array_values($uniqueCities);
		foreach ($uniqueCities as $loc) :
			$evs = $this->db->select('e.EventID ID, e.Name,  e.City, e.StateProvince, e.Venue, e.Date, v.VenueSlug, v.RegionSlug')
							->from('event_performers ep')
							->join('cached_events e', 'e.EventID = ep.EventID')
							->join('tn_venues v','v.ID = e.VenueID','left')
							->join('performer_featured f','ep.PerformerID = f.PerformerID')
							->where(
							        array(
							              //'f.geo' => 1,
							              'e.City' => strtoupper($loc['city']),
							              'e.StateProvince' => ucwords($loc['state']),
							              )
							        )
							->where('e.Date > NOW()')
							->where("e.City <> '$location[city]'")
							->group_by('e.Name')
							->order_by('e.Date', 'desc')
							//->limit(10)
							->get()->result();
			$otherEvents = array_merge($otherEvents, $evs);
			// array_push($otherEvents, $evs);
		endforeach;
		$allEvents = array_merge($allInCity, $otherEvents);
		// usort($allEvents, function (array $a, array $b) { return $a['Date'] - $b['Date']; });
		usort($allEvents, function($obja, $objb){
			$a = $obja->Date;
		    $b = $objb->Date;
		    if ($a == $b) {
		        return 0;
		    }
		    return ($a < $b) ? -1 : 1;
		});

		$allEvents = $this->get_event_performers($allEvents);
		return $allEvents;
		//return array_slice($allEvents, 0, 20);
	}

	public function get_front_page_events_in_city($location = false, $limit = 8)
	{
		// var_dump($location);
		if(!$location) return false;
		$allInCity = $this->db->select('e.EventID ID, e.Name,  e.City, e.StateProvince, e.Venue, e.Date, v.VenueSlug, v.RegionSlug')
			->from('event_performers ep')
			->join('cached_events e', 'e.EventID = ep.EventID')
			->join('tn_venues v','v.ID = e.VenueID','left')
			->join('performer_featured f','ep.PerformerID = f.PerformerID')
			->where(
				array(
					'f.geo' => 1,
					'e.City' => strtoupper($location['city']),
					'e.StateProvince' => ucwords($location['state']),
				)
			)
			->where('e.Date > NOW()')
			->group_by('e.Name')
			->order_by('e.Date', 'asc')
			->limit(25)
			->get()->result();
		//			echo $this->db->last_query();
		//if (count($allInCity) < 100) {}
		/*usort($allInCity, function($obja, $objb){
			$a = $obja->Date;
			$b = $objb->Date;
			if ($a == $b) {
				return 0;
			}
			return ($a < $b) ? -1 : 1;
		});*/

		$allEvents = $this->get_event_performers($allInCity);
		return array_slice($allEvents, 0, 20);
	}

	public function get_front_page_events_near_city($location = false, $limit = 8)
	{
		// var_dump($location);
		if(!$location) return false;

		$nearByCities = $this->get_nearby_cities($location, 50, 30);
		$uniqueCities = array();
		$otherEvents = array();
		foreach ($nearByCities as $city) :
			$uniqueCities[$city['city']]['city'] = $city['city'];
			$uniqueCities[$city['city']]['state'] = $city['state'];
		endforeach;
		$uniqueCities = array_values($uniqueCities);
		foreach ($uniqueCities as $loc) :
			$evs = $this->db->select('e.EventID ID, e.Name,  e.City, e.StateProvince, e.Venue, e.Date, v.VenueSlug, v.RegionSlug')
				->from('event_performers ep')
				->join('cached_events e', 'e.EventID = ep.EventID')
				->join('tn_venues v','v.ID = e.VenueID','left')
				->join('performer_featured f','ep.PerformerID = f.PerformerID')
				->where(
					array(
						'f.geo' => 1,
						'e.City' => strtoupper($loc['city']),
						'e.StateProvince' => ucwords($loc['state']),
					)
				)
				->where('e.Date > NOW()')
				->where("e.City <> '$location[city]'")
				->group_by('e.Name')
				->order_by('e.Date', 'asc')
				->limit(10)
				->get()->result();
			$otherEvents = array_merge($otherEvents, $evs);
			// array_push($otherEvents, $evs);
		endforeach;
		usort($otherEvents, function($obja, $objb){
			$a = $obja->Date;
			$b = $objb->Date;
			if ($a == $b) {
				return 0;
			}
			return ($a < $b) ? -1 : 1;
		});

		$allEvents = $this->get_event_performers($otherEvents);
		return array_slice($allEvents, 0, $limit);
	}

	public function get_childcategoryid_by_performer($events)
	{
		$array = array();
		if ($events){
			foreach ($events as $event){
				$result = $this->db
					->select('p.PerformerID, p.ChildCategoryID')
					->from('performer_full p')
					->where('p.PerformerID', $event->performers[0]->PerformerID)
					->get()->result();
				$array[$result[0]->PerformerID] = $result[0]->ChildCategoryID;
			}
		}
		return $array;
	}


	function cmp($obja, $objb)
	{
	    $a = $obja->Date;
	    $b = $objb->Date;
	    if ($a == $b) {
	        return 0;
	    }
	    return ($a < $b) ? -1 : 1;
	}


	public function get_nearby_cities($location, $radius = 70, $limit = 10, $eventsOnly = true)
	{

		$evCount = ($eventsOnly) ? 0 : -1;
		$slug = (isset($location['slug'])) ? $location['slug'] : slug_location($location);

		$q = $this->db->select('city, slug, state, distance, eventCount')
							->from('geo_nearby')
							->where(array('centerSlug' => $slug))
							->where("distance < $radius AND eventCount > $evCount")
							->limit($limit)
							->order_by('distance')
							->get()->result_array();

			//echo $this->db->last_query();
			return $q;
		$where = ($eventsOnly) ? "g.EventCount > 0 AND " : '';
		$coords = $this->db
						->select('latitude lat, longitude lon')
						->from('geo_cities g')
						->where(array('g.slug'=> $slug))
						->get()
						->row();
 		if(!$coords) return false;
		$query = "SELECT g.city, g.region, g.slug, g.eventCount, ( 3959 * acos( cos( radians({$coords->lat}) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians({$coords->lon}) ) + sin( radians({$coords->lat}) ) * sin( radians( latitude ) ) ) ) AS distance FROM geo_cities g WHERE {$where} g.slug <> '$slug' GROUP BY g.slug HAVING distance < {$radius} ORDER BY g.eventCount DESC LIMIT 0 , {$limit};";
		$result =  $this->db->query($query)->result_array();

 		$cleanList = array();
		foreach ($result as $res) :
			$holder = array();
			$holder['city'] = $res['city'];
			$holder['state'] = $res['region'];
			$holder['slug']	= $res['slug'];
			$holder['distance']	= $res['distance'];
			$holder['eventCount']	= $res['eventCount'];
			// $holder['slug']	= $res['slug'];
			array_push($cleanList, $holder);
		endforeach;
		// var_dump($cleanList);

		$r = array_map("unserialize", array_unique(array_map("serialize", $cleanList)));

		return $r;
	}

	private function _get_front_page_events($performers, $location)
	{
		// var_dump($location);
		$otherCities = $this->get_nearby_cities($location, 50, 10);
		$longOr = array();
		// var_dump($otherCities);
		foreach ($otherCities as $city) :
			array_push($longOr, " (ce.City = '{$city['city']}' AND ce.StateProvince  = '{$city['state']}' ) ");
		endforeach;
		// var_dump($longOr);
		$stringQ = implode(' OR ', $longOr);
		$limit = 0;
		$events = array();
		foreach ($performers as $p):
			if($limit == 25) break;
			$result = $this->db
							->select('ce.EventID, ce.City, ce.Date, ce.Name EventName, ce.StateProvince, ce.Venue, ce.VenueID, v.VenueSlug, v.RegionSlug')
							->from('event_performers ep')
							->join('cached_events ce', 'ce.EventID = ep.EventID')
							->join('tn_venues v', 'v.ID = ce.VenueID')
							// ->join('event_prices p','p.EventID = ce.EventID','left')
							->where('ce.Date > NOW()')
							->where('ep.PerformerID',$p['PerformerID'])
							->where($stringQ, false, false)
							// ->order_by('ce.EventID','RANDOM')
							->order_by('ce.Date','ASC')
							// ->limit(1)
							->get()->result_array();
							// echo $this->db->last_query()."<br>";
						//	echo "<br>{$p['PerformerID']} <br>";

			if(!empty($result)):
				$result['performer'] = $p;
				$limit++;
				// var_dump($p);
				// var_dump($result);
				// $tmp = 	array_merge($p, $result);
				array_push($events, $result);
			endif;
		endforeach;
	return $events;
	}

	private function _get_front_page_events_LEGACY($performers, $location, $notIn = false)
	{
 		$frontPageData = array();
		if($location):
			foreach ($performers as $p):
				if($notIn):
					$this->db->where("ce.City <> '$location[city]' AND ce.StateProvince <> '$location[state]'");
				else:
					$this->db->where(array('ce.City' => $location['city'], 'ce.StateProvince' => $location['state']));
				endif;
				$result = $this->db
								->select('ce.EventID, ce.City, ce.Date, ce.Name EventName, ce.StateProvince, ce.Venue, ce.VenueID, v.VenueSlug, v.RegionSlug, p.*')
								->from('event_performers ep')
								->join('cached_events ce', 'ce.EventID = ep.EventID')
								->join('tn_venues v', 'v.ID = ce.VenueID')
								->join('event_prices p','p.EventID = ce.EventID','left')
								->where('ce.Date > NOW()')
								->where('ep.PerformerID',$p['PerformerID'])
								->order_by('ce.Date','ASC')
								->limit(1)->get()->row_array();
								// echo $this->db->last_query()."<br>";
				if(!empty($result)):
					$tmp = 	array_merge($p, $result);
				/*
					$p->EventID = $result->EventID;
					$p->City = $result->City;
					$p->Date = $result->Date;
					$p->EventName = $result->EventName;
					$p->StateProvince = $result->StateProvince;
					$p->Venue = $result->Venue;
					$p->VenueID = $result->VenueID;
					array_push($frontPageData, $p);
	/*				$tmp['performer'] = $p;
					$tmp['event'] = $result;*/
					array_push($frontPageData, $tmp);
				endif;
			endforeach;

		else:
			foreach ($performers as $p):
				if($notIn):
					$this->db->where("ce.City <> '$notIn[city]' AND ce.StateProvince <> '$notIn[state]'");
				endif;
				$result = $this->db
								->select('ce.EventID, ce.City, ce.Date, ce.Name EventName, ce.StateProvince, ce.Venue, ce.VenueID, v.VenueSlug, v.RegionSlug')
								->from('event_performers ep')
								->join('cached_events ce', 'ce.EventID = ep.EventID')
								->join('tn_venues v', 'v.ID = ce.VenueID')
								->where('ce.Date > NOW()')
								->where('ep.PerformerID',$p['PerformerID'])
								->order_by('ce.Date','ASC')
								->limit(1)->get()->row_array();
				if(!empty($result)):
					$tmp = 	array_merge($p, $result);
				/*
					$p->EventID = $result->EventID;
					$p->City = $result->City;
					$p->Date = $result->Date;
					$p->EventName = $result->EventName;
					$p->StateProvince = $result->StateProvince;
					$p->Venue = $result->Venue;
					$p->VenueID = $result->VenueID;
				*/
					array_push($frontPageData, $tmp);
				endif;
			endforeach;
		endif;
		// pdump($frontPageData);
		usort($frontPageData, function (array $a, array $b) { return $a["Date"] - $b["Date"]; });
		return $frontPageData;
	}

	public function get_venue($id = false, $slug = false, $location = false, $orderby = 'EventCount', $order = "desc", $all = false)
	{
		if($id):
			return $this->db->select('*')->from('tn_venues v')->where('v.ID', $id)->get()->row();
		endif;
		if(!$all):
			$this->db->where("EventCount > 0");
		endif;
		if($slug && $location):
			return $this->db
						->select('*')
						->from('tn_venues')
						->where(array('VenueSlug' => $slug, 'RegionSlug' => $location['slug']))
                        //->where(array('VenueSlug' => $slug, 'RegionSlug' => substr($location['slug'], 0, -2))) // IN stagingdev DB RegionSlug = xxxx- not xxxx-xx
						->get()
						->row();

		elseif($location):
			return $this->db
						->select('*')
						->from('tn_venues')
						->where(array('RegionSlug' => $location['slug']))
                        //->where(array('RegionSlug' => substr($location['slug'], 0, -2))) // IN stagingdev DB RegionSlug = xxxx- not xxxx-xx
						->order_by($orderby, $order)
						->get()
						->result();
			return false;
		endif;
	}

	public function verify_location($loc)
	{
		// return true;
		return $this->db->select('count(locId) cnt')->from('geo_cities')->where(array('slug' => $loc['slug']))->get()->row()->cnt;

	}
	public function get_twitter_results($param = false, $count = 10, $filter = '')
	{
		$filter .= ' -tickets -tix'; // "AND -tickets AND -tix"
		$query = implode('+OR+', $param).$filter;
		$url = 'https://api.twitter.com/1.1/search/tweets.json';
		$query_data = array('q' => $query,'lang' => 'en', 'result_type' => 'recent', 'count' => $count, );

		$getfield = '?'.http_build_query($query_data);
		$requestMethod = 'GET';
		$twitter = new TwitterAPIExchange();
		$jsonContent = $twitter->setGetfield($getfield)
		             			->buildOauth($url, $requestMethod)
		             			->performRequest();
		return json_decode($jsonContent);
	}

	public function get_prices($events)
	{
		if($events):
			$pricedEvents = array();
			foreach ($events as $k => $e):
				$eid = (property_exists($e, 'EventID')) ? $e->EventID : $e->ID; // local vs TN data.
				$events[$k]->prices = $this->db->select("*")->from('event_prices')->where('EventID', $eid)->get()->row();
			endforeach;
		endif;
		return $events;
	}

 	public function event_performers($eid = false)
 	{
		$result = $this->db->select('pf.Name PerformerName, pf.PerformerSlug, i.lfmImagePath img, pm.wide, pm.profile, pm.thumb')
					->from('event_performers ep')
					->join('performer_full pf','pf.PerformerID = ep.PerformerID')
					->join('performer_images pm', 'pm.PerformerID = pf.PerformerID', 'left')
					->join('lfm_performer_images i','pf.PerformerID = i.PerformerID','left')
					->where('ep.EventID', $eid)
					->group_by('pf.PerformerID')
					->get()->result();
		return $result;
 	}

 	public function get_event_performers($events)
 	{
 		if($events):
 		foreach ($events as $key => $concert):

 				$eId = (isset($concert->ID)) ? $concert->ID : $concert->EventID; //second case is local cached
                #$eId = $concert->EventID; //second case is local cached

 				$temp = $this->db
						->select('pf.PerformerID, pf.Name PerformerName, pf.PerformerSlug, i.lfmImagePath img, pm.wide, pm.profile, pm.thumb, pf.fbImagePath fbImage')
						->from('event_performers ep')
						->join('performer_full pf','ep.PerformerID = pf.PerformerID')
						->join('performer_images pm', 'pm.PerformerID = pf.PerformerID', 'left')
						->join('performer_featured feat', 'feat.PerformerID = pf.PerformerID', 'left')
						->join('lfm_performer_images i','pf.PerformerID = i.PerformerID','left')
						->where('ep.EventID', $eId)
						->order_by('feat.geo','desc')
						->get()->result();
				$events[$key]->performers = (count($temp)) ? $temp : false;

	 	endforeach;
	 	endif;
	 	return $events;
 	}

 	public function get_menu($parentId = 0, $level = 1)
 	{
 		if($level < 4): // recursively till your ass drops
 			$menu = $this->db->select('*')->from('menus')->where('parent_id', $parentId)->order_by('order', 'asc')->get()->result();
 			$level++; //deepen
 			foreach ($menu as $key => $m) :
 				$menu[$key]->children = $this->get_menu($m->id, $level);
 			endforeach;
	 		return $menu;
 		endif;
 		return false;
 	}

 	public function signup($data)
 	{
 		return $this->db->insert('mailing_list', $data);
 	}

 	public function get_genre_by_slug($s)
 	{
 		$q = $this->db->select('ChildCategoryID, Filter')->from('category_filters')->where('Slug', $s)->get()->result();
 		return (isset($q[0]->ChildCategoryID)) ? $q : false;
 	}

 	public function search_performer($name = false)
 	{
 		$q = $this->db
 				->select('pf.Name value, pf.PerformerID pId, pi.thumb img, pf.PerformerSlug slug, lfmImagePath limg ')
 				->from('performer_full pf')
 				->join('performer_images pi','pi.PerformerID = pf.PerformerID','left')
				->join('lfm_performer_images li', 'pf.PerformerID = li.PerformerID','left')
 				->like('Name',ucwords($name))
 				->limit(20)
 				->get()
 				->result();
 		return $q;
 	}

	public function update_perfomer_image($performerID, $path) {
		$this->db->where('PerformerID', $performerID);
		$data = array('fbImagePath' => $path);
		return $this->db->update('performer_full', $data);
	}

	public function update_perfomer_image_by_name($name, $path) {
		$this->db->where('Name', $name);
		$data = array('fbImagePath' => $path);
		return $this->db->update('performer_full', $data);
	}

	public function get_performer_full($limit=100, $offset=100)
	{
		$q = $this->db
			->select('pf.Name Name, pf.PerformerID pId, pf.PerformerSlug slug, fe.geo as Geo')
			->from('performer_full pf')
			->join('performer_featured fe','fe.PerformerID = pf.PerformerID', 'left')
			->limit($limit, $offset)
			->get()
			->result();
		return $q;
	}

	public function get_performer_full_dimage($fb_image, $limit, $offset)
	{
		$q = $this->db
			->select('pf.Name Name, pf.PerformerID pId, pf.PerformerSlug slug')
			->from('performer_full pf')
			->where('fbImagePath', $fb_image)
			->limit($limit, $offset)
			->get()
			->result();
		return $q;
	}

	public function get_performer_full_by_name($names)
	{
		$q = $this->db
			->select('pf.Name Name, pf.PerformerID pId, pf.PerformerSlug slug')
			->from('performer_full pf')
			->where_in('Name', $names)
			->get()
			->result();
		return $q;
	}

 	public function search_city($name = false)
 	{
 		$cityState = explode(',', $name);
  		if(isset($cityState[1])):
 			$this->db->where('region', strtoupper(trim($cityState[1])));
 			$name = $cityState[0];
 		endif;

 		$q = $this->db
 				->select("CONCAT(city, ', ', region) value, slug", false)
 				->from('geo_cities')
 				->like('city',trim(ucwords($name)))
 				->limit(10)
 				->group_by('slug')
 				->get()
 				->result();
 		return $q;
 	}

	#Search Performers, Cities and Venues
	public function search_pcv($type, $criteria){
		switch($type){
			case 'performer':
				$query = $this->db
					->select('Name, PerformerID, PerformerSlug')
					->from('performer_full')
					->like('Lower(Name)',strtolower($criteria), 'both')
					->limit(10)
					->get()
					->result();
				return $query;
				break;
			case 'city':
				return $this->search_city($criteria);
				break;
			case 'venue':
				$query = $this->db
					->select('Name, URL, VenueSlug, RegionSlug')
					->from('tn_venues')
					->like('Lower(Name)',strtolower($criteria), 'both')
					->limit(10)
					->get()
					->result();
				return $query;
				break;
			default:
				break;
		}
	}

 	public function get_similar_artists($pId = false, $limit = 20)
 	{
 		//lets get name
 		$pName = $this->db->select('Name')->from('performer_full')->where('PerformerID', $pId)->get()->row()->Name;
 		$this->load->library('lastfmapi/lastfmapi');

		// Pass the array to the auth class to return a valid auth

		$this->auth = new lastfmApiAuth('setsession');
		// pdump($this->auth);
		$this->apiClass = new lastfmApi();
		// pdump($this->apiClass);
		$artistClass = $this->apiClass->getPackage($this->auth, 'artist');
		$methodVars = array('artist' => $pName, 'limit' => $limit);

		$similar = $artistClass->getSimilar($methodVars);
		$localSimilar = array();
		if($similar):
			foreach ($similar as $a) :
				$current = $this->db
								->select('pf.Name performerName, pf.PerformerID pId, pf.PerformerSlug pSlug, pi.thumb img, lfmImagePath limg')
								->from('performer_full pf')
								->join('performer_images pi', 'pi.PerformerID = pf.PerformerID','left')
								->join('lfm_performer_images li', 'pf.PerformerID = li.PerformerID','left')
								->where('pf.PerformerSlug', seoUrl($a['name']))
								->get()->row_array();
				if($current):
					array_push($localSimilar, $current);
				endif;
			endforeach;
		endif;
		return $localSimilar;
 	}

 	public function get_genres()
	{
		return $this->db->select('*')->from('category_filters')->get()->result();
	}

}

/* End of file global_m.php */
/* Location: ./application/models/global_m.php */
