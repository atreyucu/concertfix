<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron_m extends CI_Model {

	public function performer_exists($data)
	{
		return $this->db->select('count(id) count')->from('performer_full')->where('PerformerID', $data)->get()->row()->count;
	}
	public function count_performers()
	{
		return $this->db->select('count(id) count')->from('performer_full')->get()->row()->count;
	}
	public function slug_exists($data)
	{
		return $this->db->select('count(id) count')->from('performer_full')->where('PerformerSlug', $data)->get()->row()->count;
	}
	public function image_exists($data)
	{
		return $this->db->select('count(id) count')->from('lfm_performer_images')->where('PerformerID', $data)->get()->row()->count;
	}
	public function about_text_exists($data)
	{
		return $this->db->select('count(id) count')->from('performer_about')->where('PerformerID', $data)->get()->row()->count;
	}
	public function insert_performer($data)
	{
		$this->db->insert('performer_full', $data);
		return true;
	}


    public function get_all_performers2(){
        return $this
            ->db
            ->select('p.*')
            ->from('performer_full p')
            ->join('lfm_performer_images pi', 'p.PerformerID = pi.PerformerID')
            ->where('pi.lfmImagePath like "http://userserve-ak.last.fm%"')
            ->order_by('Name', 'asc')
            ->get()
            ->result();
    }
	public function get_all_performers($parentCat = 2)
	{
		return $this
				->db
				->select('p.*,c.Filter genre')
				->from('performer_full p')
				->join('category_filters c','c.ChildCategoryID = p.ChildCategoryID','left')
				->where('ParentCategoryID', $parentCat)
				->get()
				->result();
	}

	public function get_incorrect_about_performers()
	{
		return $this->db
			->select('p.PerformerID, p.Name')
			->from('performer_full p')
			->join('performer_album a', 'a.tnPerformerID = p.PerformerID')
			->join('performer_about ab', 'ab.PerformerID = p.PerformerID')
			->like('ab.text', 'does not have any albums')
			->group_by('p.PerformerID')
			->get()
			->result();
	}

    public function get_albums_to_update($parentCat = 2){
        return $this->db->select('p.Name, pa.*')
            ->from('performer_album pa')
            ->join('performer_full p', 'p.PerformerID = pa.tnPerformerID')
            ->where('pa.lfmImagePath not like "http://img2-ak.lst.fm/%" ')
            ->where('ParentCategoryID', $parentCat)
//            ->order_by('p.Name', 'asc')
            ->get()->result();
    }

    public function get_performer_albums($performer_id)
    {
        return $this->db->select('*')
            ->from('performer_album')
//            ->join('performes', 'PerformerId = tnPerformerId')
            ->where('tnPerformerId', $performer_id)->get()->result();
    }

	public function album_exist($data)
	{
		return $this->db->select('count(id) count')->from('performer_album')->where('AlbumSlug', $data)->get()->row()->count;
	}

    public function insert_album_2($data)
    {
//        $data['AlbumName'] = str_replace('"', '\"',$data['AlbumName']);
//        $values = implode('", "', $data);
//        $this->db->query("INSERT IGNORE INTO performer_album VALUES (NULL ,\"$values\");");
        $insert_query = $this->db->insert_string('performer_album', $data);
        $insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
        $this->db->query($insert_query);
        $affected_rows = $this->db->affected_rows();
//        echo 'Affected rows: '.$affected_rows."\n\r";
        return $affected_rows;
    }

	public function insert_album($data)
	{
		$this->db->insert('performer_album', $data);
		return true;
	}
	public function get_top_songs($aId)
	{
		return $this->db->select('*')->from('album_tracks')->where('album_id', $aId)->order_by('play_count','desc')->get()->result();
	}

	public function get_top_song($aId)
	{
		return $this->db->select('*')->from('album_tracks')->where('album_id', $aId)->order_by('play_count','desc')->limit(1)->get()->row();
	}

	public function insert_lfmpic($data)
	{
		$this->db->insert('lfm_performer_images', $data);
		return true;
	}

	public function get_lfmpic($performerID)
	{
		return $this->db->select('lfmImagePath')
			->from('lfm_performer_images')
			->where(array('PerformerID' => $performerID))
			->get()->row();
	}

	public function get_all_lfmpic()
	{
		return $this->db->select('p.Name, lfm.PerformerID, lfm.lfmImagePath')
			->from('lfm_performer_images lfm')
			->join('performer_full p', 'p.PerformerID = lfm.PerformerID')
			->get()
			->result();
	}

	public function get_performer($performerID)
	{
		return $this->db->select('p.Name')
			->from('performer_full p')
		    ->where(array('p.PerformerID' => $performerID))
			->get()->row();
	}

	public function update_lfmpic($performerID, $lfmImagePath)
	{
		$this->db->where('performerID', $performerID);
		$update = array('lfmImagePath' => $lfmImagePath);
		$this->db->update('lfm_performer_images', $update);
	}

	public function insert_about_pending($object)
	{
		$this->db->insert('about_pending', $object);
	}

	public function get_about_pending($parentCat = 2)
	{
		return $this
				->db
				->select('p.*,c.Filter genre')
				->from('performer_full p')
				->join('category_filters c','c.ChildCategoryID = p.ChildCategoryID','left')
				->join('about_pending ap','ap.PerformerID = p.PerformerID')
				->where('ParentCategoryID', $parentCat)
				->get()
				->result();
	}
	public function purge_about_pending()
	{
		$this->db->truncate('about_pending');
	}

	public function insert_about($object)
	{
		//clean up just in case
		$this->db->where('PerformerID', $object['PerformerID']);
		$this->db->delete('performer_about');
		//insert
		$this->db->insert('performer_about', $object);
	}

	public function insert_high_sales($batch)
	{
		$this->db->truncate('performer_hotsales');
		$this->db->insert_batch('performer_hotsales', $batch);
	}

	public function insert_inventory($batch)
	{
		$this->db->truncate('performer_inventory');
		$this->db->insert_batch('performer_inventory', $batch);
	}

	public function venue_exists($data, $slug = false, $locationSlug = false)
	{
		if($slug):
			$this->db->where(array('VenueSlug'=> $slug, 'RegionSlug' => $locationSlug));
		endif;
		return $this->db->select('count(ID) count')->from('tn_venues')->where(array('ID' => $data))->get()->row()->count;
	}
	public function insert_venue($data)
	{
		if($this->venue_exists($data->ID)): // second check to update
			//get legacy slug full venue
			$slugs = $this->db->select('VenueSlug, RegionSlug')->from('tn_venues v')->where('ID', $data->ID)->limit(1)->get()->row();
			$redirects = array();
			$redirects['old_slug'] = strtolower($slugs->VenueSlug."+".$slugs->RegionSlug);
			$redirects['redirect_slug'] = strtolower($data->VenueSlug."+".$data->RegionSlug);
			$this->db->insert('slug_redirect', $redirects);
			$this->db->where('ID', $data->ID);
			$this->db->update('tn_venues', $data); //update venue
		else:
			$this->db->insert('tn_venues', $data); //insert
		endif;
		return true;
	}

	public function short_region($data)
	{
		return strtolower($this->db->select('RegionShort st')->from('regions')->where('Region', $data)->get()->row()->st);
	}

	public function get_all_albums()
	{
		return $this->db->select('performer.Name as PerformerName, album.AlbumName, album.id')
			->from('performer_album album')
			->join('performer_full performer', 'performer.PerformerID = album.tnPerformerID')
			->get()
			->result();
	}

	public function update_album_pic($performerID, $lfmImagePath)
	{
		$this->db->where('performerID', $performerID);
		$update = array('lfmImagePath' => $lfmImagePath);
		$this->db->update('lfm_performer_images', $update);
	}

	public function get_albums($all = false, $limit = 100, $pId = false, $orderby = 'id', $ordertype = "RANDOM")
	{

		if(!$all) $this->db->where('a.ReleaseDate', '0000-00-00');
		if($limit) $this->db->limit($limit);
		if($pId):
			$this->db->where('a.tnPerformerID', $pId);
			$this->db->where("a.ReleaseDate <> '1970-01-01'");
			$this->db->where("a.ReleaseDate <> '1969-12-31'");
		endif;
		$q = $this->db
				->select('a.*, p.Name')
				->from('performer_album a')
				->join('performer_full p','p.PerformerID = a.tnPerformerID')
				->where("a.AlbumName <> '(null)'")
				->where("a.AlbumName <> ''")
				->order_by($orderby, $ordertype)
				->get()->result();

		return $q;
	}

	public function get_album_by_performer($performerID, $albumName)
	{
		$q = $this->db
			->select('a.*')
			->from('performer_album a')
			->where('a.tnPerformerID', $performerID)
			->where('a.AlbumName', $albumName)
			->get()->row();

		return $q;
	}

	public function get_first_album($performerID)
	{
		$q = $this->db
			->select('a.*, p.Name')
			->from('performer_album a')
			->where('a.tnPerformerID', $performerID)
			->join('performer_full p','p.PerformerID = a.tnPerformerID')
			->order_by('ReleaseDate', 'asc')
			->limit(1)
			->get()->row();

		return $q;
	}

    public function get_albums_without_tracks($limit = null, $offset = 0)
    {
//        $q = $this->db->select('pa.*')
//            ->from('performer_album pa')
////            ->join('performer_full p','p.PerformerID = pa.tnPerformerID')
//            ->where('0 = (select count(*) from album_tracks aa where aa.album_id = pa.id)')
//            ->get()->result();
        $str_query = 'select pa.*, p.Name from performer_album pa inner JOIN performer_full p on (p.PerformerID = pa.tnPerformerID) where 0 = (select count(*) from album_tracks aa where aa.album_id = pa.id)';
        if($limit != null)
        {
            $str_query .= ' limit '. $limit;
            $str_query .= ' offset '. $offset;
        }

        $q = $this->db->query($str_query)
        ->result();

        return $q;
    }
	public function has_tracks($aid, $pid)
	{
		return $this->db->select('count(id) count')->from('album_tracks')->where(array('tn_PerformerID' => $pid, 'album_id' => $aid))->get()->row()->count;
	}

	public function get_albums_average($pId)
	{
		return $this->db->select('SUM(PlayCount)/count(id) cnt')->from('performer_album')->where("ReleaseDate <> '1969-12-31'")->where('tnPerformerID', $pId)->get()->row()->cnt;
	}

	public function get_tracks($limit= false)
	{
		if($limit):
			$this->db->limit($limit);
			$this->db->where("play_count IS NULL");
		endif;
		return $this->db->select('t.id, t.name, p.Name PerformerName')->from('album_tracks t')->join('performer_full p', 'p.PerformerID = t.tn_PerformerID')->get()->result();
	}
	public function update_track_info($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('album_tracks', $data);
	}

	public function update_release_date($aid, $date)
	{
		$date = date('Y-m-d', $date);
		$this->db->where('id', $aid);
		$data = array('ReleaseDate' => $date);
		$this->db->update('performer_album', $data);
	}

	public function insert_tracks($tracks = false)
	{
		if($tracks):
			$this->db->insert_batch('album_tracks', $tracks);
		endif;
	}

	public function event_exists($id)
	{
		return $this->db->select('count(id) count')->from('cached_events')->where('EventID', $id)->get()->row()->count;
	}

	public function insert_event($data)
	{
		return $this->db->insert('cached_events', $data);
	}

	public function update_event($data)
	{
		$this->db->where('EventID', $data['EventID']);
		$this->db->update('cached_events', $data);
		return true;
	}

	public function update_seating_chart($vid, $chart)
	{
		$data = array('SeatChart' => $chart);
		$this->db->where('ID', $vid);
		$this->db->update('tn_venues', $data);
	}

	public function insert_event_performers($items = false)
	{
		// var_dump($items);
		// die();
		if($items):
			//truncate
			$this->db->truncate('event_performers');
			//insert batch
			// $this->db->insert_batch('event_performers', $items);

			foreach ($items as $item):
				$this->db->insert('event_performers', $item);

			endforeach;

			return "updated ".count($items). " items " ;
		else:
			return 0 . " items " ;
		endif;
	}
	/*public function do_the_dew()
	{
		// $this->db->truncate('performer_featured_copy');
		// $ojd = array('front', 'geo','side');
		// foreach ($ojd as $o):
			// echo "\n\r $o";
			# code...
			$perfs = $this->db->select('PerformerID')->from('performer_featured')->where('front', 1)->get()->result();
			var_dump($perfs);
			die();
			// echo $o."\n\r";
			foreach ($perfs as $p) {
				// var_dump($p);
				if($this->db->select('count(id) cnt')->from('performer_featured_copy')->where('PerformerID', $p->PerformerID)->get()->row()->cnt):
					$object = array('front' => 1);
					$this->db->where('PerformerID', $p->PerformerID);
					$this->db->update('performer_featured_copy', $object);
					// echo $this->db->last_query();
				else:
					$object = array('PerformerID' => $p->PerformerID, 'front' => 1);
					$this->db->insert('performer_featured_copy', $object);
				endif;
				# code...
			}
		// endforeach;
	}*/

	public function slug_locations()
	{
		$unslugged = $this->db->select('id, City, StateProvince')->from('cached_events')->where('LocationSlug IS NULL')->get()->result();
		foreach ($unslugged as $e) :
			$updata = array('LocationSlug' => seoUrl($e->City." ".$e->StateProvince));
			$this->db->where('id', $e->id);
			$this->db->update('cached_events', $updata);
		endforeach;
		return count($unslugged);
	}


	public function fix_city_slugs()
	{
		$cities = $this->db->select('locId, region, city')->from('geo_cities')->where("`slug` IS NULL")->get()->result();
		$count = count($cities);
		$i = 0;
		mail("mkovalch@gmail.com", "STARTED WITH $count "," more to go...");
		foreach ($cities as $c) {
			$object = array('slug' => seoUrl($c->city." ".$c->region));
			$this->db->where('locId', $c->locId);
			$this->db->update('geo_cities', $object);
			//echo $object['slug']."\n\r";
			$i++;
			if(!($i % 10000)):
				mail("mkovalch@gmail.com", "fixed $i of $count "," more to go...");
			endif;
		}
		mail("mkovalch@gmail.com", "BEAST IS DONE "," finally...!");
	}

	public function clean_featured()
	{
		$this->db->where(array('side' => 0, 'front' => 0, 'geo' => 0));
		$this->db->delete('performer_featured');
		return true;
	}

	public function update_counts()
	{
		echo "\n\r UPDATING event counts...";
		$query = "UPDATE tn_venues v SET v.EventCount = (SELECT count(e.id) cnt from cached_events e WHERE e.Date > NOW() AND e.VenueID = v.ID);";
		$this->db->query($query);
	}

	public function update_insert_prices($data)
	{
		if($this->db->select('count(id) cnt')->from('event_prices')->where('EventID', $data->EventID)->get()->row()->cnt):
			$this->db->where('EventID', $data->EventID);
			$this->db->update('event_prices', $data);
		else:
			$this->db->insert('event_prices', $data);
		endif;
	}

	public function remove_stale_events()
	{
		echo "\n\r REMOVING event_prices";
		$query = "DELETE FROM event_prices WHERE EventID IN (SELECT ce.EventID FROM cached_events ce WHERE ce.Date < DATE_SUB(NOW(), INTERVAL 1 DAY));";
		$this->db->query($query);
		echo "\n\r REMOVING event_performers";
		$query = "DELETE FROM event_performers WHERE EventID IN (SELECT ce.EventID FROM cached_events ce WHERE ce.Date < DATE_SUB(NOW(), INTERVAL 1 DAY));";
		$this->db->query($query);
		echo "\n\r REMOVING cached_events";
		$query = "DELETE FROM cached_events WHERE Date < DATE_SUB(NOW(), INTERVAL 1 DAY);";
		$this->db->query($query);
	}

	public function remove_old_text()
	{
		echo "\n\r REMOVING delete Ps withoout text";
		$query = "delete from performer_text where text LIKE '%did not release tour schedule yet.';";
		$this->db->query($query);
	}

	public function geo_event_count()
	{
		echo "\n\r UPDATING event counts...setting to 0.";

		$query = "UPDATE geo_cities SET eventCount = 0;";
		$this->db->query($query);

		echo "\n\r UPDATING event counts...populating....";

		$query = 'UPDATE geo_cities g SET g.eventCount = (SELECT SUM(v.EventCount) FROM tn_venues v WHERE v.RegionSlug = g.slug)';
		$this->db->query($query);

		echo "\n\r UPDATING event counts...setting to 0.";
		$query = "UPDATE geo_nearby SET eventCount = 0;";
		$this->db->query($query);

		echo "\n\r UPDATING event counts...Populating...";
		$query = 'UPDATE geo_nearby g SET g.eventCount = (SELECT SUM(v.EventCount) FROM tn_venues v WHERE v.RegionSlug = g.slug)';
		$this->db->query($query);
	}

	public function get_all_cities()
	{
		$all = $this->db->select('slug')->from('geo_cities')->where("city IS NOT NULL")->group_by('slug')->get()->result_array();
		return $all;
	}

	public function nearby_truncate()
	{
		$this->db->truncate('geo_nearby');
		return true;

	}

	public function insert_nearby($batch)
	{
		if($batch):
			$this->db->insert_batch('geo_nearby', $batch);
		endif;
	}

	public function get_perfs_imgs()
	{
		return $this->db->select('*')->from('performer_images')->get()->result();
	}
/*	public function get_event($id = false)
	{
		if($id):
			return $this->db->select('*')->from('cached_events')->where('EventID', $id)->get()->row();
		endif;

		return $this->db->select('*')->from('cached_events')->get()->resut();
	}*/

	public function get_nearby_cities($location, $radius = 70, $limit = 10, $eventsOnly = true)
	{

		$evCount = ($eventsOnly) ? 0 : -1;
		$slug = (isset($location['slug'])) ? $location['slug'] : slug_location($location);

/*		$q = $this->db->select('city, slug, state, distance, eventCount')
							->from('geo_nearby')
							->where(array('centerSlug' => $slug))
							->where("distance < $radius AND eventCount > $evCount")
							->limit($limit)
							->get()->result_array();

			echo $this->db->last_query();
			return $q;*/
		$where = ($eventsOnly) ? "g.EventCount > 0 AND " : '';
		$coords = $this->db
						->select('latitude lat, longitude lon')
						->from('geo_cities g')
						->where(array('g.slug'=> $slug))
						->get()
						->row();
 		if(!$coords) return false;
		$query = "SELECT g.city, g.region, g.slug, g.eventCount, ( 3959 * acos( cos( radians({$coords->lat}) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians({$coords->lon}) ) + sin( radians({$coords->lat}) ) * sin( radians( latitude ) ) ) ) AS distance FROM geo_cities g WHERE {$where} g.slug <> '$slug' GROUP BY g.slug HAVING distance < {$radius} ORDER BY g.eventCount DESC LIMIT 0 , {$limit};";
		$result =  $this->db->query($query)->result_array();

 		$cleanList = array();
		foreach ($result as $res) :
			$holder = array();
			$holder['city'] = $res['city'];
			$holder['state'] = $res['region'];
			$holder['slug']	= $res['slug'];
			$holder['distance']	= $res['distance'];
			$holder['eventCount']	= $res['eventCount'];
			// $holder['slug']	= $res['slug'];
			array_push($cleanList, $holder);
		endforeach;
		// var_dump($cleanList);

		$r = array_map("unserialize", array_unique(array_map("serialize", $cleanList)));

		return $r;
	}

	public function has_nearby($centerSlug)
	{
		return $this->db->select('count(id) cnt')->from('geo_nearby')->where('centerSlug', $centerSlug)->get()->row()->cnt;
	}

	public function fix_nearby_missing($debug = true)
	{
		$total = 0;
		$inserted = "<p>Getting missing cities: ";
		$missing = $this->db
						->select('e.City, e.StateProvince, e.LocationSlug')
						->from('cached_events e')
						->join('geo_cities g','g.slug = e.LocationSlug','left')
						->where('g.slug IS NULL')
						->group_by('e.City')
						->get()
						->result();

		foreach ($missing as $loc) :

			$geocode=file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($loc->City.", ".$loc->StateProvince)."&sensor=false");
			$output= json_decode($geocode);
			// var_dump($output);
			// die();
			$data['country']  = $output->results[0]->address_components[3]->short_name;
			$data['latitude']  = $output->results[0]->geometry->location->lat;
			$data['longitude'] = $output->results[0]->geometry->location->lng;
			$data['city']	= $loc->City;
			$data['region'] = $loc->StateProvince;
			$data['slug'] = $loc->LocationSlug;
			$inserted .= "<br>".$loc->LocationSlug;
			if(!$this->db->select('count(locId) cnt')->from('geo_cities')->where('slug', $data['slug'])->get()->row()->cnt):
				$this->db->insert('geo_cities', $data);
			endif;

			$nearby = $this->get_nearby_cities($data, 100, $limit = 50, false);
//			echo ($debug) ? "\n\r $i is $data[slug] with ".count($nearby). " nearby cities " : '';
			echo ($debug) ? "\n\r $loc->City is $data[slug] with ".count($nearby). " nearby cities " : '';
			foreach ($nearby as $key => $nb) :
				$nearby[$key]['centerSlug'] = $data['slug'];
			endforeach;
			if(!$this->has_nearby($data['slug'])):
				$this->insert_nearby($nearby);
			endif;
			$total++;
		endforeach;
		$inserted .= " <br> WITH TOTAL: $total </p>";
		return $inserted;
	}

	public function catchup_about()
	{
		return $this->db
		->select('p.*, (SELECT count(pa.id) cnt FROM performer_about pa WHERE p.PerformerID = pa.PerformerID) cnt, c.Filter genre')
		->from('performer_full p')
		->join('category_filters c','c.ChildCategoryID = p.ChildCategoryID','left')
		->having('cnt = 0')
		->get()->result();
	}
}

/* End of file cron_m.php */
/* Location: ./application/models/cron_m.php */