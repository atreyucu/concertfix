<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tours_m extends CI_Model {


	public function get_performer_by_slug($slug = false, $hideEmpty = false)
	{

		if($slug):
			$this->db->where(array('p.PerformerSlug' => $slug));
		endif;
		$result = $this->db
							->select('p.Name PerformerName, p.fbImagePath fbImage, a.text aboutText, p.PerformerID, p.PerformerSlug, i.lfmImagePath img, pm.wide, pm.profile, pm.thumb, cf.Slug cslug, cf.filter cat, p.ChildCategoryID,
											 (SELECT count(ep.id) FROM event_performers ep WHERE ep.PerformerID = p.PerformerID) event_count
											 ')
							->from('performer_full p')
							->join('lfm_performer_images i','i.PerformerID = p.PerformerID','left')
							->join('performer_about a','a.PerformerID = p.PerformerID','left')
							->join('performer_images pm', 'pm.PerformerID = p.PerformerID', 'left')
							->join('category_filters cf','cf.ChildCategoryID = p.ChildCategoryID','left')
							->order_by('UPPER(p.Name)', 'ASC');
							if($hideEmpty):
								return $this->db->having("event_count > 0")->get()->result();
							else:
								return $this->db->get()->result();
							endif;
	}

	public function get_upcoming_concerts($location, $exceptId = false)
	{
		$q =  $this->db->select('p.PerformerID, p.PerformerSlug, p.Name PerformerName, i.lfmImagePath img, pm.wide, pm.profile, pm.thumb, e.Date')
					->from('cached_events e')
					->join('event_performers ep','e.EventID = ep.EventID')
					->join('performer_full p','p.PerformerID = ep.PerformerID')
					->join('lfm_performer_images i','i.PerformerID = p.PerformerID','left')
					->join('performer_images pm', 'pm.PerformerID = p.PerformerID', 'left')
					->where('e.Date > NOW()')
					->where(array('e.City' => $location['city'], 'e.StateProvince' => $location['state']))
					->group_by('ep.PerformerID')
					->order_by('e.Date','asc')
					->limit(6)
					->get()->result();
					foreach ($q as $key => $p):
						$q[$key]->event = $this->get_performer_event($p->PerformerID, $location);
					endforeach;
					// echo $this->db->last_query();
					return $q;
	}

	public function get_performer_event($id, $location = false)
	{
		if($location):
			$this->db->where(array('e.City' => $location['city'], 'e.StateProvince' => $location['state']));
		endif;
		return $this->db
						->select('e.Date, e.Venue, e.City, e.Date, v.VenueSlug, v.RegionSlug')
						->from('cached_events e')
						->join('tn_venues v','v.ID = e.VenueID')
						->join('event_performers ep', 'e.EventID = ep.EventID')
						->where("e.Date > NOW()")
						->where('ep.PerformerID', $id)
						->order_by('e.Date', 'asc')
						->limit(1)
						->get()->row();
	}

	public function get_all_performers($hideEmpty = false)
	{
		$result = $this->db
							->select('p.Name PerformerName, p.PerformerID, p.PerformerSlug, (SELECT count(ep.id) FROM event_performers ep WHERE ep.PerformerID = p.PerformerID) event_count
											 ')
							->from('performer_full p')
							->order_by('p.PerformerSlug', 'ASC');
							if($hideEmpty):
								return $this->db->having("event_count > 0")->get()->result();
							else:
								return $this->db->get()->result();
							endif;
	}

	public function get_performer_albums($pId = false, $limit = false)
	{
		if($limit) $this->db->limit($limit);
		$album = $this->db->select('*')
						->from('performer_album')
						->where('tnPerformerID', $pId)
						->where("ReleaseDate <> '1969-12-31'")
						->order_by('ReleaseDate', 'Desc')
						->get()
						->result();
		foreach ($album as $key => $a):
			$album[$key]->tracks = $this->db->select('*')->from('album_tracks')->where('album_id', $a->id)->get()->result();
		endforeach;
		return $album;
	}
	public function get_venue_by_slug($slug = false)
	{
		return true;
	}
	public function get_location_by_slug($slug = false)
	{
		return true;
	}

	//DEPRECIATED FOR NOW
	public function get_last_album($performerId = false)
	{
		$result = $this->db
						->select('*')
						->from('last_album')
						->where(array('PerformerID' => $performerId))
						->where('last_updated > (NOW() - INTERVAL 1 WEEK)')
						->limit(1)
						->get()->result();
		if(!$result):
			//clean out if there is old data
			$this->db->where(array('PerformerID' => $performerId));
			$this->db->delete('last_album');
			return false;
		endif;
		return $result;

	}

	public function insert_last_album($data = false)
	{
		$this->db->insert('last_album', $data);
		return null;
	}

	public function get_venue($vSlug, $lSlug)
	{
		$result = $this->db
						->select('*')
						->from('tn_venues')
						->where(array('VenueSlug' => $vSlug, 'RegionSlug' => $lSlug ))
						->limit(1)
						->get()
						->row();
		//return $result;
		if($result):
			return $result;
		else: //last ditch effort to get something...
			$result = $this->db
						->select('*')
						->from('tn_venues')
						->where(array('VenueSlug' => $vSlug))
						->limit(1)
						->get()
						->row();
			return $result;
		endif;
	}

	public function get_venue_by_city($vSlug, $lSlug)
	{
		$result = $this->db
			->select('*')
			->from('tn_venues')
			->where(array('VenueSlug' => $vSlug, 'RegionSlug' => $lSlug ))
			->limit(1)
			->get()
			->row();
		//return $result;
		if($result):
			return $result;
		else: //last ditch effort to get something...
			return false;
		endif;
	}



	public function get_venue_events($vid)
	{
		// $this->db->select('e.');
	}

	public function get_li_text($performer, $page = 'performer', $venue = false, $location = false)
		{

		$refresh = $this->input->get('refresh');
		$slug = $this->uri->segment(2);
		$textData = $this->db->select("*")->from('page_text')->where(array('slug' => $slug, 'type' => 'li'))->get()->row();
		// if(falseisset($textData->text)):
		if(isset($textData->text) && !$refresh):
			return $textData->text;
		else:
			$newText=$this->_create_li_text($performer, $page, $venue, $location);
			$this->db->where(array('slug' => $slug, 'type' => 'li'));
			$this->db->delete('page_text'); //remove old text
			$data = array('text' => $newText, 'type' => 'li', 'var_data' => '', 'created' => date('Y-m-d H:i:s'),'slug' => $slug);
			$this->db->insert('page_text', $data);
			return $newText;
		endif;

	}

	public function get_qna_text($performer, $page = 'performer', $venue = false, $location = false, $tour_date, $force = false)
	{
		$slug = $this->uri->segment(2);
		$whereClause = array('slug' => $slug, 'type' => 'qna');
		$textData = $this->db->select("*")
			->from('page_text')
			->where($whereClause)
			->get()
			->row();
		if($tour_date === NULL):
			$this->db->where($whereClause);
			$this->db->delete('page_text');
			return 'false';
        elseif(isset($textData->text) && !$force && !$this->input->get('refresh')):
            return $textData->text;
		else:
			$newText = $this->_create_qna_text($performer, 'venue', $venue, $location, $tour_date);
			$this->db->where($whereClause);
			$this->db->delete('page_text');
			$query_params = array_merge(
				$whereClause,
				array('text' => $newText, 'var_data' => '', 'created' => date('Y-m-d H:i:s'))
			);
			$this->db->insert('page_text', $query_params);
			return $newText;
		endif;
	}

	public function get_all_qna_text(){
	    return $this->db->select("*")->from('page_text')->get()->result();
    }

	public function get_short_text($performer, $page = 'performer', $venue = false, $location = false)
	{
		$refresh = $this->input->get('refresh');
		$slug = $this->uri->segment(2);
		$textData = $this->db->select("*")->from('page_text')->where(array('slug' => $slug, 'type' => 'short'))->get()->row();
		// if(falseisset($textData->text)):
		if(isset($textData->text) && !$refresh):
			return $textData->text;
		else:
			$newText=$this->_create_short_text($performer, $page, $venue, $location);
			$this->db->where(array('slug' => $slug, 'type' => 'short'));
			$this->db->delete('page_text'); //remove old text
			$data = array('text' => $newText, 'type' => 'short', 'var_data' => '', 'created' => date('Y-m-d H:i:s'),'slug' => $slug);
			$this->db->insert('page_text', $data);
			return $newText;
		endif;

	}




	public function get_text($text_type, $events, $performer, $section = 'main', $slug = false, $location = false)
	{
		$eventsBackup = $events;
		if(!$slug):
			$slug = $this->uri->segment(2);
		endif;
		$totalCount = ($events) ? count($events) : 0;
		//FirePHP::getInstance(true)->fb('$totalCount='.print_r($totalCount,true));
		$cityEvents = array();
		if($events): // merge events
			// foreach ($events as $e):
			// 	$cityEvents[$e->VenueID] = $e;
			// endforeach;
			// $events = array_merge($cityEvents); // reindex
		endif;
		// var_dump($events);


		$eventCount = ($totalCount > 5) ? 5 : $totalCount;
		switch ($text_type) :
			case 'performer':
				if($customText = $this->_get_custom_text($performer)):
					return $customText;
				endif;
				// Check if performer text exists and has yet to expire
				// If not expired, return
				if($text = $this->_fetch_performer_text($performer->PerformerID, $eventCount)):
					return $text->text;
				else:
					// If expired, gen new
					return $this->_create_performer_text($section, $events, $performer);
				endif;
				break;
			case 'performer_city':
				if($textData = $this->_fetch_performer_page_text($slug, $section)):
				//if text comes back
					$vars = unserialize($textData->var_data);

					$eventCount = count($eventsBackup);
					// if event count stored > event count AND event count == 1
					if( $vars['event_count'] > $eventCount AND $eventCount == 1):
						//update text
						$text = $this->_create_performer_city_text($eventsBackup, $section, $location);
						//update vars
						$vars = $this->_create_performer_city_vars($eventsBackup);
						//update db
						$this->_create_update_performer_city_text_vars($slug, $text, serialize($vars), $section);
						//fetch text
						$textData = $this->_fetch_performer_page_text($slug, $section);
						return $textData->text;

					else:
						if(strtotime($textData->created) < strtotime(date('u')." + 6 hours")):
							//if expired
							$vars = $this->_create_performer_city_vars($eventsBackup);
							//update vars
							$this->_create_update_performer_city_text_vars($slug, false, serialize($vars), $section);
							//fetch text
							$textData = $this->_fetch_performer_page_text($slug, $section);
						endif;
						return $textData->text;
					endif;

				else:
					//update text
					$text = $this->_create_performer_city_text($eventsBackup, $section, $location);
					//update vars
					$vars = $this->_create_performer_city_vars($eventsBackup);
					//update db
					$this->_create_update_performer_city_text_vars($slug, $text, serialize($vars), $section);
					//fetch text
					$textData = $this->_fetch_performer_page_text($slug, $section);
					return $this->_replace_performer_venue_text($textData->text, $vars);
				//endif;
				endif;
				break;
			case 'performer_venue':
				//FirePHP::getInstance(true)->fb('performer_venue');
				//FirePHP::getInstance(true)->fb('$slug='.$slug);
				//FirePHP::getInstance(true)->fb('$section='.$section);

				if($textData = $this->_fetch_performer_page_text($slug, $section)):
				//if text comes back
					$vars = unserialize($textData->var_data);
					//FirePHP::getInstance(true)->fb('$vars='.print_r($vars,true));
					$eventCount = !empty($events) && is_array($events) ? count($events) : 0;
					//FirePHP::getInstance(true)->fb('$eventCount='.print_r($eventCount,true));
					// echo $eventCount."CC";
					// if event count stored > event count AND event count == 1
					if($totalCount == 0):
						// var_dump($location);
						return "Currently this venue has no events for {$performer[0]->PerformerName}. Please check back soon. We update ticket information and tour dates often.";

					elseif(
						($vars['event_count'] != $eventCount && $eventCount == 1)
						|| (
							$vars['event_count'] == $eventCount
							&& $eventCount == 1
							&& stristr($textData->text, 'Unfortunately there are')
						)
					):
						// echo "1";
						//update text
						//FirePHP::getInstance(true)->fb('_create_performer_venue_text 1');
						$text = $this->_create_performer_venue_text($eventCount, $performer[0]->PerformerName);
						//update vars
						$vars = $this->_create_performer_venue_vars($events);
						//update db
						$this->_create_update_performer_venue_text_vars($slug, $text, serialize($vars), $section);
						//fetch text
						$textData = $this->_fetch_performer_page_text($slug, $section);
						return $this->_replace_performer_venue_text($textData->text, $vars);

					else:
						if(strtotime($textData->created) < strtotime(date('u')." + 6 hours")):
							// echo "3";
							//if expired
							$vars = $this->_create_performer_venue_vars($events);
							//update vars
							$this->_create_update_performer_venue_text_vars($slug, false, serialize($vars), $section);
							//fetch text
							$textData = $this->_fetch_performer_page_text($slug, $section);
						endif;
						return $this->_replace_performer_venue_text($textData->text, $vars);
					endif;

				else:
					// echo "4";
					//update text
					// tours_pv
					//FirePHP::getInstance(true)->fb('_create_performer_venue_text 2');
					$text = $this->_create_performer_venue_text($eventCount, $performer[0]->PerformerName);
					//update vars
					$vars = $this->_create_performer_venue_vars($events);
					//update db
					$this->_create_update_performer_venue_text_vars($slug, $text, serialize($vars), $section);
					//fetch text
					$textData = $this->_fetch_performer_page_text($slug, $section);
					return $this->_replace_performer_venue_text($textData->text, $vars);
				//endif;
				endif;

				break;
			case 'performer_venue2':
				$slug = $this->uri->segment(2);
				$whereClause = array('slug' => $slug, 'type' => 'tours_pv2');
				$textData = $this->db->select("*")
					->from('page_text')
					->where($whereClause)
					->get()
					->row();
				if($eventCount == 0):
					$this->db->where($whereClause);
					$this->db->delete('page_text');
					return false;
				elseif(isset($textData->text) && !$this->input->get('refresh')):
					return $textData->text;
				else:
					$newText = $this->_create_performer_venue2_text($performer, $events[0]->Venue);
					$this->db->where($whereClause);
					$this->db->delete('page_text');
					$query_params = array_merge(
						$whereClause,
						array('text' => $newText, 'var_data' => '', 'created' => date('Y-m-d H:i:s'))
					);
					$this->db->insert('page_text', $query_params);
					return $newText;
				endif;

				break;
			default:
				# code...
				break;
		endswitch;
	}

	private function _get_performer_text($performer)
	{
		return '';
	}

	private function _get_custom_text($performer)
	{
		$req = $this->db->select('text')
			->from('performer_text')
			->where('custom = 1')
			->where('PerformerID = ' . $performer->PerformerID)
			->limit(1)
			->get()->result();
		// var_dump($performer->PerformerID);
		return !empty($req) ? $req[0]->text : false;
	}

	private function _create_performer_venue2_text($performer, $venue)
	{
		$performer = $performer[0];
		$alt = Spinner::prepare();
		return join(" ", array(
			$performer->PerformerName, "will be", $alt->visiting(), $venue, "for a", $alt->memorable(), $alt->concert(), $alt->that_you_cant_miss() . "!", "Tickets are", $alt->on_sale(), "at", $alt->great(), "prices here at", $alt->concert_fix(), "where", $alt->our_goal_is(), "to get you to the", $alt->concert(), "even if it's", $alt->sold_out(), $alt->at_the_box_office() . ".", "We want you to", $alt->get(), "up close, which is why we are a", $alt->leading_seller(), "for", $alt->front_row(), "and premium seats. If you need them last-minute,", $alt->look_for(), "eTickets for the", $alt->quickest(), "delivery so you", $alt->can(), $alt->print_anywhere() . ". All", $alt->purchases(), "are made through our safe and secure checkout and covered with a", $alt->money_back() . "."
		));
	}
	private function _create_short_text($performer, $page = 'performer', $venue = false, $location = false)
	{
		switch ($page) :
			case 'venue':
				$performer = $performer[0]->PerformerName;
				$venue = clean_venue($venue->Name);
				$city = $location['city'];
				$text = random_pop(array("When","Once","After"))." you ".random_pop(array("locate","find","view"))." the {$performer} {$venue} tickets you ".random_pop(array("desire","want","need","would like")).", you will be ".random_pop(array("buying them through","purchasing them through","using"))." our safe and secure checkout ".random_pop(array("system","page","form")).". All tickets for {$performer} at {$venue} in {$city} are ".random_pop(array("protected","sold","purchased","backed"))." with a 100% moneyback guarantee. Unless otherwise ".random_pop(array("directed","specified")).", your {$performer} {$venue} tickets are shipped using FedEx and ".random_pop(array("during","in","within"))." the same day if ".random_pop(array("taken","placed","ordered"))." ".random_pop(array("prior to","sooner than"))." 5pm EST. For the ".random_pop(array("fastest","quickest","most timely"))." delivery, ".random_pop(array("purchase","get","buy"))." {$performer} {$venue} eTickets so you can download and print them yourself.";
				return $text;
				break;
			case 'city':
				$performer = $performer[0]->PerformerName;
				$city = $location['city'];
				$text = random_pop(array("When","Once","After"))." you ".random_pop(array("locate","find","view"))." the {$performer} {$city} tickets you ".random_pop(array("desire","want","need","would like")).", you will be ".random_pop(array("buying them through","purchasing them through","using"))." our safe and secure checkout ".random_pop(array("system","page","form")).". All tickets for {$performer} in {$city} are ".random_pop(array("protected","sold","purchased","backed"))." with a 100% moneyback guarantee. Unless otherwise ".random_pop(array("directed","specified")).", your {$performer} {$city} tickets are shipped using FedEx and ".random_pop(array("during","in","within"))." the same day if ".random_pop(array("taken","placed","ordered"))." ".random_pop(array("prior to","sooner than"))." 5pm EST. For the ".random_pop(array("fastest","quickest","most timely"))." delivery, ".random_pop(array("purchase","get","buy"))." eTickets for {$performer} in {$city} so you can download and print them ".random_pop(array("whenever you want","at your leisure","from home","from anywhere")).".";
				return $text;
			break;

			case 'performer';
			default:
				return '';
				break;
		endswitch;
	}

	private function _create_li_text($performer, $page = 'performer', $venue = false, $location = false)
	{
		switch ($page) :
			case 'performer':
				$performer = $performer[0]->PerformerName;
				$text = "<ul class='pageUL'>";
				$middle = array(
						"<li>{$performer} ".random_pop(array("tour dates","concert schedule"))." has ".random_pop(array("just","recently","finally"))." been ".random_pop(array("announced","released","reported","publicized","revealed","made public")).".</li>",
						"<li>Tickets for the ".random_pop(array("approaching","forthcoming","upcoming","future"))." {$performer} concert ".random_pop(array("are on sale","are in stock","are now available","can be found here","can now be purchased")).".</li>",
						"<li>".random_pop(array("Tour schedule","Tour dates","Concert schedule","Concert dates"))." for {$performer} ".random_pop(array("can be found","is located","is available for viewing"))." ".random_pop(array("above","on top","on this page")).".</li>",
						"<li>".random_pop(array("Sold Out concerts","Front row tickets"))." will not be ".random_pop(array("a problem","an issue","any concern")).", we always have ".random_pop(array("great","top quality","terrific"))." ".random_pop(array("seats","tickets")).".</li>",
						"<li>".random_pop(array("Tour schedules","Tour dates","Concert schedules"))." for all {$performer} concerts are ".random_pop(array("revised","updated","refreshed"))." ".random_pop(array("daily","up to the minute","constantly","on a regular basis")).".</li>"
						);
				shuffle($middle);
				$text .= implode(' ', $middle);
				$text .=	"</ul><p>{$performer} ".random_pop(array("could be coming","may come","might soon come"))." to a city near you. ".random_pop(array("Check out","View","Browse"))." the {$performer} schedule ".random_pop(array("above","on this page","just above"))." and ".random_pop(array("click","press","push"))." the ticket ".random_pop(array("button","link","icon"))." to ".random_pop(array("see","view","checkout"))." our ".random_pop(array("huge inventory","big selection"))." of tickets. ".random_pop(array("Check out","View","Browse","Look through"))." our selection of {$performer} front row tickets, luxury boxes and VIP tickets. ".random_pop(array("Once you","As soon as you"," After you"))." ".random_pop(array("find","locate","track down"))." the {$performer} tickets you ".random_pop(array("want","need","desire")).", you can ".random_pop(array("buy","purchase"))." your ".random_pop(array("tickets","seats"))." from our safe and secure checkout. Orders taken before 5pm are ".random_pop(array("usually","generally","normally"))." shipped within the same business day. To ".random_pop(array("buy","purchase"))." last minute {$performer} tickets, ".random_pop(array("check out","browse through","look for"))." the eTickets that can be downloaded instantly.</p>";
				return $text;
			break;

			case 'city' :
				$performer = $performer[0]->PerformerName;
				$city = $location['city'];
				$text = "<ul class='pageUL'>";
				$middle = array(
						"<li>{$city} ".random_pop(array("will soon be","is about to be","is planning to be","is looking forward to being"))." ".random_pop(array("visited by","the city to find","the place to catch","where to see"))." {$performer}.</li>",
						"<li>Tickets to ".random_pop(array("catch","see","view","go see"))." {$performer} concert in {$city} ".random_pop(array("are now on sale","are in stock","are now available","can be found here","can now be purchased")).".</li>",
						"<li>{$performer} {$city} tickets are ".random_pop(array("protected","covered","sold","purchased"))." with a 100% moneyback guarantee.</li>",
						"<li>".random_pop(array("Sold out? No problem, tickets","Front row tickets"))." for the concert in {$city} ".random_pop(array("are now on sale","are in stock","are now available","can be found here","can now be purchased")).".</li>",
						"<li>ETickets and last minute tickets for {$city} ".random_pop(array("are now on sale","are in stock","are now available","can be found here","can now be purchased")).".</li>"
						);
				shuffle($middle);
				$text .= implode(' ', $middle);
				$text .= "</ul><p>{$performer} ".random_pop(array("will be coming","is planning on coming","is scheduled to come","is soon coming"))." to {$city} for a ".random_pop(array("much anticipated","very special","memorable"))." concert that you ".random_pop(array("cannot afford to","cannot","should not","better not"))." miss. ".random_pop(array("We provide","We offer","We supply"))." you with ".random_pop(array("a huge inventory","a big selection","a ton","plenty"))." of {$performer} {$city} tickets at cheap prices so you can ".random_pop(array("make it to the","attend this","go to this","get over to the"))." ".random_pop(array("show","concert","event")).". ".random_pop(array("View","Check out","Take a peek at"))." any available {$performer} front row tickets, luxury suites, meet and greet passes, VIP seats and Front row tickets for the {$city} concert.</p>";
				return $text;
			break;

			case 'venue':
				$performer = $performer[0]->PerformerName;
				$venue = clean_venue($venue->Name);
				$city = $location['city'];
				$text = "<ul class='pageUL'>";
				$middle = array(
						"<li>{$venue} ".random_pop(array("will soon be","is about to be","is planning to be","is looking forward to being"))." ".random_pop(array("visited by","the venue to find","the place to catch","where to see"))." {$performer}.</li>",
						"<li>Tickets ".random_pop(array("to see","for","to attend"))." {$performer} concert in {$venue} ".random_pop(array("are on sale","are in stock","are now available","can be found here","can now be purchased")).".</li>",
						"<li>ETickets and last minute tickets for {$venue} ".random_pop(array("are now on sale","are in stock","are now available","can be found here","can now be purchased")).".</li>",
						"<li>{$city} has ".random_pop(array("just","recently","finally"))." ".random_pop(array("announced"," released"," reported"," publicized","revealed","made public"))." the concert schedule for {$performer} at {$venue}.</li>",
						"<li>{$performer} {$venue} tickets are ".random_pop(array("protected","covered","sold","purchased"))." with a 100% moneyback guarantee.</li>",
						"<li>".random_pop(array("Sold out? No problem, tickets","Front row tickets"))." for {$performer} at {$venue} ".random_pop(array("are on sale","are in stock","are now available","can be found here","can now be purchased")).".</li>"
						);
				shuffle($middle);
				$text .= implode(' ', $middle);
				$text .=	"</ul><p>{$performer} ".random_pop(array("will be coming","is planning on coming","is scheduled to come","is soon coming"))." to {$venue} in {$city} for a ".random_pop(array("much anticipated","very special","memorable"))." concert that you ".random_pop(array("cannot afford to","cannot","should not","better not"))." miss. We provide you with ".random_pop(array("a huge inventory","a big selection","a ton","plenty"))." of {$performer} {$venue} tickets at cheap prices so you can ".random_pop(array("make it to the","attend this","go to this","get over to the"))." ".random_pop(array("show","concert","event")).". ".random_pop(array("View","Check out","Take a peek at"))." any available {$performer} front row tickets, luxury suites, meet and greet passes, VIP seats and Front row tickets for the {$venue} concert.</p>";
				return $text;
			break;

			default:
				# code...
			return '';
				break;
		endswitch;


	}

	public function get_upcoming_concerts_in_state($state, $venue_id, $performer_id)
	{
		// p.PerformerID, p.PerformerSlug, p.Name PerformerName, i.lfmImagePath img,
		$q =  $this->db->select('e.City, e.Date')
			->from('cached_events e')
			->join('event_performers ep','e.EventID = ep.EventID')
			->join('performer_full p','p.PerformerID = ep.PerformerID')
			->join('lfm_performer_images i','i.PerformerID = p.PerformerID','left')
			->join('performer_images pm', 'pm.PerformerID = p.PerformerID', 'left')
			->where('e.Date > NOW()')
			->where(array(
				'e.StateProvince' => $state,
				'ep.PerformerID' => $performer_id
			))
			->where("e.VenueID != $venue_id")
			->order_by('e.Date','asc')
			->limit(6)
			->get()->result();
			// foreach ($q as $key => $p):
			// 	$q[$key]->event = $this->get_performer_event($p->PerformerID, $location);
			// endforeach;
			// echo $this->db->last_query();
			// var_dump($q);
			return $q;
	}

	public function get_upcoming_concerts_in_city($city, $tour_date, $performer_id)
	{
		$tour_date = date('Y-m-d H:i:s', strtotime($tour_date));
		// var_dump($tour_date);
		$q =  $this->db->select('e.Date')
			->from('cached_events e')
			->join('event_performers ep','e.EventID = ep.EventID')
			->where('e.Date > NOW()')
			->where(array(
				'e.City' => $city,
				'ep.PerformerID' => $performer_id
			))
			->where("e.Date != '$tour_date'")
			->order_by('e.Date','asc')
			->limit(6)
			->get()->result();
			return $q;
	}

	public function get_related_performers_in_venue($venue_id, $tour_date, $performer_id)
	{
		$format = 'Y-m-d H:i:s';
		// $tour_date = date('Y-m-d H:i:s', strtotime($tour_date));
		// Start / END
		$event_start = new DateTime($tour_date);
		$event_start = date($format, $event_start->modify('today')->format('U'));

		$event_end = new DateTime($tour_date);
		$event_end = date($format, $event_end->modify('today')->modify('+1 day')->format('U'));
		// var_dump($event_end);
		$q =  $this->db->select('ep.PerformerName')
			->from('cached_events e')
			->join('event_performers ep','e.EventID = ep.EventID')
			->where("e.Date >= '$event_start'")
			->where("e.Date <= '$event_end'")
			->where(array(
				'e.VenueID' => $venue_id,
			))
			->where("ep.PerformerID != $performer_id")
			->order_by('e.Date','asc')
			->get()->result();
			// var_dump($q);
			return $q;
	}
	public function get_performer_top_songs($performer_id = false, $limit = false)
	{
		if($limit) $this->db->limit($limit);
		$songs = $this->db->select('name')
			->from('album_tracks')
			->where('tn_PerformerID', $performer_id)
			->order_by('play_count','desc')
			->group_by('name')
			->get()
			->result();
			// var_dump($songs);
		return $songs;
	}

	public function _create_qna_text($performer, $page = 'performer', $venue = false, $location = false, $tour_date = false)
	{
		switch ($page) :
			case 'venue':
				$alt = Spinner::prepare();
				$performer_name = $performer[0]->PerformerName;
				$performer_name_tr = preg_replace("/[t|T]he\ /", "", $performer_name);
				$top_songs      = $this->get_performer_top_songs($performer[0]->PerformerID, 2);
				$tour_date_full = $tour_date;
				$tour_datetime  = date('F j g:i A', strtotime($tour_date));
				$tour_date = date('F j', strtotime($tour_date));
				$city = $location['city'];
				$state = toggle_state($location['state']);
				$state_full = $location['state'];
				$venue_name = clean_venue($venue->Name);
				$nearby_concerts = $this->get_upcoming_concerts_in_state($state_full,$venue->ID,$performer[0]->PerformerID);
				$nearby_concerts_count = count($nearby_concerts);
				$city_concerts = $this->get_upcoming_concerts_in_city($city,$tour_date_full,$performer[0]->PerformerID);
				$city_concerts_count = count($city_concerts);
				$tour_start_time = date('g:i A', strtotime($tour_date_full));
				$tour_early_start_time = date('g:i A', strtotime('-30 minutes',  strtotime($tour_date_full)));
				$text = '';
				$qnaBrancher = array( // Q/A's
					array(
						"group_condition_index" => mt_rand(0,2),
						"questions" => array(
							array(
								"question_condition" 	=> true,
								"answer_index_condition"   	=> ($nearby_concerts_count > 1) ? 2 : $nearby_concerts_count,
								"question"  				 	=> random_pop(array(
									join(" ", array(
										"Is $performer_name", $alt->performing(), "in any other", $alt->cities(), $alt->near(), "$city, $state_full?"
									)),
									join(" ", array(
										"Are there $performer_name", $alt->concerts(), "in any other", $alt->cities(), "near $city, $state_full?"
									)),
									join(" ", array(
										"Does $performer_name have", $alt->concerts(), "in any other", $alt->cities(), "near $city, $state_full?"
									))
								)),
								"answer"						 	=> array(
									join(" ", array(
										"No,", $alt->unfortunately(), $performer_name, $alt->has_no_other(), $alt->concerts(), $alt->scheduled(), $alt->near(), $city, $alt->right_now() . ".", $alt->check_back(), "again", $alt->soon(), "to", $alt->see(), "if", $performer_name, $alt->adds(), "any other", $alt->concerts(), $alt->near(), $city, "or", $alt->anywhere_else(), "in the state of $state."
									)),
									join(" ", array( // Count == 1
										$alt->Yes() . ", there is another", $alt->concert(), "you could", $alt->attend(), "if you", $alt->are_not(), $alt->catch(), "$performer_name perform in", $city, "on", "$tour_date.", "$performer_name also has a", $alt->concert(), $alt->scheduled(), "in the state of $state", $alt->loop($nearby_concerts, "in %s on %s") . "."
									)),
									join(" ", array( // Count > 1
										$alt->Yes() . ",", "there are", $alt->several(), "other", $alt->concerts(), "you could", $alt->attend(), "if you", $alt->are_not(), $alt->catch(), "$performer_name perform in $city on $tour_date. $performer_name also has $nearby_concerts_count", $alt->concerts(), $alt->scheduled(), "in the state of $state", $alt->loop($nearby_concerts, "in %s on %s") . "."
									))
								)
							),
							array(
								"question_condition" 	=> true,
								"question"  				 	=> join(" ", array(
									"When", $alt->can_i(), $performer_name, $alt->perform(), "in $city?"
								)),
								"answer"						 	=> join(" ", array(
									"$performer_name is", $alt->currently(), $alt->scheduled(), "to", $alt->perform(),
									"in $city on $tour_datetime at $venue_name" . (($city_concerts_count > 0)
									? $alt->loopCities($city_concerts, ", and " . $alt->again() . " on ")
									: ".")
								))
							),
							array(
								"question_condition" 	=> true,
								"answer_index_condition"   	=> ($nearby_concerts_count > 1) ? 2 : $nearby_concerts_count,
								"question"  				 	=> join(" ", array(
									"Is $performer_name", $alt->performing(false, " in"), "$city, $state?"
								)),
								"answer_prefix"				=> join(" ", array(
									$alt->Yes() . ", $performer_name is", $alt->scheduled(), "to have a", $alt->concert(), "in $city at $venue_name on $tour_date. "
								)),
								"answer"						 	=> array(
									join(" ", array(
										"This is the only $performer_name", $alt->concert(), $alt->scheduled(), "in $state."
									)),
									join(" ", array( // Count == 1
										"There is another", $alt->concert(), "you could", $alt->attend(), "in the state of $state if you are not able to catch $performer_name in $city on $tour_date.",
										"$performer_name has another", $alt->concert(), $alt->scheduled(), $alt->loop($nearby_concerts, "in %s on %s") . "."
									)),
									join(" ", array( // Count > 1
										"There are also", $alt->some_other(), $alt->concerts(), "you could", $alt->attend(), "in the state of $state if you are not able to catch $performer_name in $city on $tour_date.",
										"$performer_name has $nearby_concerts_count", $alt->additional(), $alt->concerts(), $alt->scheduled(), $alt->loop($nearby_concerts, "in %s on %s") . "."
									))
								)
							)
						)
					),
					array(
						"group_condition_index" => mt_rand(0,2),
						"questions" => array(
							array(
								"question_condition" 	=> true,
								"question"  				 	=> join(" ", array(
									$alt->What(), $alt->time(), "does the $performer_name_tr", $alt->concert(), "in $city", $alt->start() . "?"
								)),
								"answer"						 	=> join(" ", array(
									"The", $alt->concert(), "is", $alt->currently(), $alt->scheduled(), "to", $alt->start(), "at $tour_start_time on $tour_date.", "$performer_name", $alt->is_infamous_for(), $alt->starting(), $alt->much(), "later than the", $alt->scheduled(), $alt->start_time(), "but", $alt->we_would_suggest(), $alt->getting_there(), $alt->around(), $tour_early_start_time, "to", $alt->make_sure(), "you don't miss", $alt->a_minute(), "of the", $alt->concert()
								))
							),
							array(
								"question_condition" 	=> true,
								"question"  				 	=> join(" ", array(
									$alt->Who(), $alt->performing(), "with $performer_name at the", $alt->concert(), "in $city at $venue_name on $tour_date?"
								)),
								"answer"						 	=> join(" ", array(
									$alt->currently(true) . ", ",
									(($related_performers = $this->get_related_performers_in_venue($venue->ID, $tour_date, $performer[0]->PerformerID)) && count($related_performers) > 0)
									?  join(" ", array(
										"$performer_name is", $alt->scheduled(), "to", $alt->perform(), "with",
										(count($related_performers) > 1)
										? $alt->loopPerformers($related_performers) . " in $city at $venue_name."
										: join(" ", array(
											$related_performers[0]->PerformerName, "in $city at $venue_name."
										))
									))
									: join(" ", array(
										$alt->there_are_no(),  $alt->performers(), $alt->scheduled(), "to", $alt->perform(), "at $city in $venue_name"
									))
								))
							),
							array(
								"question_condition" 	=> true,
								"question"  				 	=> join(" ", array(
									$alt->are_there_any(true), $performer_name, $alt->vip(), $alt->tickets(), "for the", "$city $venue_name", $alt->concert(), "?"
								)),
								"answer"						 	=> join(" ", array(
									"To", $alt->see(), "if there", $alt->are_any(), $performer_name, $alt->vip(), $alt->tickets(), "for the", $alt->concert(), "at $venue_name in $city,", $alt->see(), $alt->our(), "ticket", $alt->selection(), "and", $alt->scroll_down(), "the", $alt->bottom(), "of the ticket list."
								))
							)
						)
					),
					array(
						"group_condition_index" => mt_rand(0,2),
						"questions" => array(
							array(
								"question_condition" 	=> true,
								"question"  				 	=> join(" ", array(
									$alt->What(), $alt->venue(), "in $city is $performer_name", $alt->performing(), "in?"
								)),
								"answer"						 	=> join(" ", array(
									"$performer_name is", $alt->currently(), $alt->scheduled(), "to", $alt->perform(), "at $venue_name.",
									"The", $alt->entire(), $alt->city(), "of $city is", $alt->very_excited(), "to", $alt->catch(), $performer_name, $alt->perform(), "at", $venue_name, "on", $tour_date
								))
							),
							array(
								"question_condition" 	=> true,
								"question"  				 	=> join(" ", array(
									"Is the $performer_name_tr", $alt->concert(), "in $city sold out?"
								)),
								"answer"						 	=> join(" ", array(
									"The $performer_name_tr $city", $alt->concert(), "may be sold out but", $alt->other(), "people are", $alt->constantly(), "looking to", $alt->dump(), "tickets they already", $alt->possess() . ".", "So even if the $performer_name_tr at $venue_name", $alt->concert(), "is sold out, there is a", $alt->great(), "chance there are still tickets", $alt->available() . "."
								))
							),
							array(
								"question_condition" 	=> true,
								"question"  				 	=> join(" ", array(
									$alt->what_options(true), "$performer_name $city $venue_name tickets", $alt->sent(), "to me?"
								)),
								"answer"						 	=> join(" ", array(
									"E-tickets can", $alt->often(), "be", $alt->purchased(), "to", $alt->catch(), "$performer_name in $city, which allows", $alt->customers(), "to print your tickets from your", $alt->computer() . ".", "If not, your $performer_name $city tickets can either be", $alt->delivered(), $alt->in_the(), "mail, or", $alt->picked_up(), "locally or at $venue_name will call"
								))
							)
						)
					),
					array(
						"group_condition_index" => (count($top_songs) > 1) ? 1 : 0,
						"questions" => array(
							array(
								"question_condition" 	=> true,
								"question"  				 	=> join(" ", array(
									$alt->when(true), "is the", $alt->day(), "for the $performer_name_tr", $alt->concert(), "in $city?"
								)),
								"answer"						 	=> join(" ", array(
									(($city_concerts_count > 0)
									? join(" ", array( // Adding 1 to concerts since it doesn't include the current concert. Could be implemented cleaner
											"$performer_name is", $alt->scheduled(), "to", $alt->perform(), $city_concerts_count+1, $alt->concerts(), "in $city,", $alt->the_first_being(), "on $tour_date.",
											$alt->some_other(true), $alt->appearances(), "for $performer_name in $city will", $alt->be(), $alt->loopCities($city_concerts)
										))
									: join(" ", array("$performer_name only has one", $alt->concert(), $alt->scheduled(), "on $tour_date at $venue_name in $city."))
									)
								))
							),
							array(
								"question_condition" 	=> true,
								"question"  				 	=> join(" ", array(
									"What can", $alt->fans(), $alt->expect(), "to hear at the $performer_name_tr", $alt->concert(), "in $city?"
								)),
								"answer"						 	=> join(" ", array(
									(isset($top_songs[0]) ? '"' . $top_songs[0]->name . '"' : false), "is", $alt->constantly(), "a fan favorite and will", $alt->probably(), "be", $alt->performed(), "at the $performer_name_tr $city", $alt->concert() . ".", $alt->additionally(true) . ",", $alt->fans_you(), $alt->could(), $alt->hear(), $performer_name, $alt->play(), (isset($top_songs[1]) ? '"' . $top_songs[1]->name . ',"' : false), " which is also", $alt->very(), $alt->popular() . "."
								))
							)
						)
					)
				);
				$newBrancher = array();
				if($qnaBrancher[3]["group_condition_index"] == 1) {
					array_push($newBrancher, $qnaBrancher[mt_rand(0,2)]);
					array_push($newBrancher, $qnaBrancher[3]);
				} else {
					array_push($newBrancher, array_pop($qnaBrancher));
					array_push($newBrancher, $qnaBrancher[mt_rand(0,1)]);
				}
				foreach($newBrancher as $idx => $qnaGroupBranch) {
					if($qnaGroupBranch["group_condition_index"] === null) continue;
					$qnaBranch = $qnaGroupBranch["questions"][$qnaGroupBranch["group_condition_index"]];
					if($qnaBranch["question_condition"]) {
						$text .= "<b>" . $qnaBranch["question"] . "</b>\n";
						if(array_key_exists("answer_index_condition", $qnaBranch)) {
							$prefix = (array_key_exists("answer_prefix", $qnaBranch)) ? $qnaBranch["answer_prefix"] : "";
							$text .= "<p>" . $prefix . $qnaBranch["answer"][$qnaBranch["answer_index_condition"]] . "</p>\n";
						} else {
							$text .= "<p>" . $qnaBranch["answer"] . "</p>\n";
						}
					}
				}
				return $text;
			break;

			default:
				# code...
			return '';
		endswitch;
	}

	private function _create_performer_text($section, $events, $performer)
	{
		$now = date('Y-m-d H:i:s');
		$genre = ucwords(strtolower($performer->cat));
		$performer_id = $performer->PerformerID;
		$genre_id = $performer->ChildCategoryID;
		if($genre == "OTHER") { $genre = null; }
		$performer = $performer->PerformerName;
		$params = array(
				"events_count" => count($events),
				"performer" => $performer,
				"genre" => $genre
		);
		// Check if
		// $query = $this->db
		// 	->select('text')
		// 	->from('performer_text')
		// 	->where('PerformerID', $pid)
		// 	->where('refresh_date >', $now);

		// $obj = array('slug' => $slug, 'type' => $type, 'text' => $text, 'var_data' => $vars, 'created' => date('Y-m-d H:i:s'));
		// return $this->db->insert('page_text', $obj);
		// $res = $query->get()->row();
		$alt = Spinner::prepare();
		if($events) {
			// TextA
			// Get date of next event and set expiration
			$last_performance_date = max(array_map(function($event) {
				return $event->Date;
			}, $events));
			$new_text = join(" ", array(
				$performer, "recently", $alt->announced(), "a concert schedule", $alt->visiting(), "select", $alt->cities(), "in North America.",
					"As one of the", $alt->top(), $genre, "live performers", $alt->to_date() . ",", $performer, "will", $alt->soon(), $alt->be_appearing(), $alt->on_stage(), $alt->once_again(), "for", $alt->fans_to_enjoy() . ".",
					$alt->check_out(true), "the", $alt->schedule(), $alt->below(), $alt->select_show() . ".",
					$alt->then(true) . ",", "you can", $alt->check_out(), "the", $alt->concert(), $alt->details() . ",", $alt->rsvp(), $alt->through(), "Facebook and", $alt->check_out(), "a", $alt->great_selection(), "concert tickets.",
					"If you", $alt->cant(), $alt->make(), "any of the current", $alt->concerts() . ", sign up for our", $alt->concertTrackerLink(), $alt->to_get_notified(), $alt->when(), $performer, $alt->concerts(), "are", $alt->announced(), $alt->in_your_city() . ".",
					$performer, $alt->could(), $alt->announce(), $alt->additional(), "North American", $alt->concerts() . ",", "so", $alt->come_back_often(), "for", $alt->updates() . "."
			));
			$query_params = array(
				'PerformerID' => $performer_id,
				'text' => $new_text,
				'custom' => 2,
				'announced' => 0,
				'created_on' => date('Y-m-d H:i:s'),
				'refresh_date' => $last_performance_date
			);
			$this->db->insert('performer_text', $query_params);
		} else {
			// GET 3 other performers for genre!
			$related_performers = $this->db
				->query('
					SELECT pf.Name, pf.PerformerSlug, ce.Date
					 FROM performer_full AS pf
					 JOIN event_performers ep ON ep.PerformerID = pf.PerformerID
					 JOIN cached_events ce ON ce.EventID = ep.EventID
					 WHERE ce.Date > NOW()
					 AND pf.ChildCategoryID = ?
					 GROUP BY pf.Name
					 ORDER BY RAND()
					 LIMIT 3
				 ', array($genre_id, $genre_id)
				)->result();
			$last_related_performance_date = max(array_map(function($related_performer) {
				return $related_performer->Date;
			}, $related_performers));
			$new_text = join(" ", array(
				$alt->as_of_right_now(), "there haven't been any", $alt->upcoming(), $performer, "concerts or tours", $alt->announced(), "for", $alt->north_america() . ".", "Sign up for our", $alt->concertTrackerLink(), "to get", $alt->notified(), "when", $performer, $alt->concerts(), "have been", $alt->announced(), "to the", $alt->schedule() . ".",
				$alt->randomSentence2($params),
				"In the meantime,", $alt->check_out(), "other", $genre, "performances coming up by", $alt->otherGenrePerformers($related_performers) . "."
			));
			$query_params = array(
				'PerformerID' => $performer_id,
				'text' => $new_text,
				'custom' => 0,
				'announced' => 0,
				'created_on' => date('Y-m-d H:i:s'),
				'refresh_date' => $last_related_performance_date
			);
			$this->db->insert('performer_text', $query_params);
		}
		return $new_text;
	}

	private function _fetch_performer_text($pid, $eventsCount)
	{
		// Check for custom text '2', then '0'
		$now = date('Y-m-d H:i:s');
		if($eventsCount > 0) {
			$query = $this->db
				->select('text')
				->from('performer_text')
				->where('PerformerID', $pid)
				->where('refresh_date >', $now)
				->where('custom', 2);
			$res = $query->get()->row();
		} else {
			$query = $this->db
				->select('text')
				->from('performer_text')
				->where('PerformerID', $pid)
				->where('refresh_date >', $now)
				->where('custom', 0);
			$res = $query->get()->row();
		}
		if(count($res)) {
			return $res;
		}
		return;
	}

	private function _create_performer_city_text($events, $section = false, $location = false)
	{
		//$section is location in this event (city)
				$randomEvent = false;

				//set up a switch for event counts

				if(!$events) return "Unfortunately there are no events scheduled for your city. Please check back in few days as we are adding events every day!";
				$eventCount = count($events);

				if($eventCount > 1):
					foreach ($events as $key => $event) :
						if($event->City == $location['city']):
							$cityEvent = $event;
							array_splice($events, $key);
							break;
						endif;
					endforeach;
					$rand_keys = array_rand($events, 1);
					$randomEvent = $events[$rand_keys];
					// $cityEvent = $events[0];
					// $randomEvent = $events[1];
				else:
					$cityEvent = $events[0];
					$randomEvent = false;
				endif;

				if($randomEvent):
					$insertAlso = random_pop(array("If you are unable","If you're too busy","If you are not going to be able"))." to ".random_pop(array("catch","see","attend","watch"))." the {$cityEvent->City}, {$cityEvent->StateProvince} ".random_pop(array("concert","show"))." on ".date('F j', strtotime($cityEvent->Date)).", ".random_pop(array("you might be able to","you could still","you still have a chance to"))." ".random_pop(array("go to","attend"))." one of the other concerts in {$randomEvent->StateProvince}. {$cityEvent->Name} has other concerts coming to {$randomEvent->City} ({$randomEvent->StateProvince}), including a ".random_pop(array("show in","concert in","event in"))." ".clean_venue($randomEvent->Venue)." on ".date('F j, Y', strtotime($randomEvent->Date)).".";
				else:
					$insertAlso = '';
				endif;
				$text = random_pop(array("If you live in","If you are a resident of","If you live near"))." or ".random_pop(array("plan on visiting","are going to be visiting","might pass through"))." {$cityEvent->City}, ". toggle_state($cityEvent->StateProvince)." and ".random_pop(array("are looking to see","want to attend","want to catch"))." {$cityEvent->Name} live in concert ".random_pop(array("you are in luck","you came to the right place","you might just get your wish")).". {$cityEvent->Name} ".random_pop(array("will be having a concert","has a concert scheduled","will be performing","has a show scheduled"))." in {$cityEvent->City}, {$cityEvent->StateProvince} at ".clean_venue($cityEvent->Venue)." on ".date('F j', strtotime($cityEvent->Date)).". {$insertAlso} ".random_pop(array("So far","Up to this point","As of now"))." all ".random_pop(array("reviews","feedback","news"))." for {$cityEvent->Name} live in concert ".random_pop(array("seem to be extremely positive","have been positive","have been very good")).". ".random_pop(array("No doubt","Without a doubt","Without question","Definitely","Undoubtedly"))." all the {$cityEvent->Name} fans from {$cityEvent->City}, {$cityEvent->StateProvince} have been ".random_pop(array("waiting","wanting","hoping"))." to see {$cityEvent->Name} live in concert for ".random_pop(array("quite some time","a long time"))." and it ".random_pop(array("should be a night to remember","should be one heck of a concert","will be a night they will not soon forget")).". You can browse through all of the {$cityEvent->City}, ". toggle_state($cityEvent->StateProvince)." tickets and seating charts by viewing the {$cityEvent->City}, {$cityEvent->StateProvince} schedule and the schdule for the rest of the state of ". toggle_state($cityEvent->StateProvince).".";
				return $text;
	}

	private function _create_performer_venue_text($count, $performer= '')
	{
		//FirePHP::getInstance(true)->fb('_create_performer_venue_text $count='.$count);
		// TODO: Figure out what is going on with this and why it fails
		// pdump($tickets);
		if(!$count) return "Unfortunately there are no {$performer} events scheduled for this venue. Please check back in few days as we are adding events every day!";

		$a1 = array("will host","will be where you can see","is going to be hosting","will be rocking with a concert by");
		$a2 = array("has a great selection of seats starting at","has many ticket options that start at","provides a wide variety of tickets that start at","gives you plenty of available tickets that start at just");
		$a3 = array("all the way up to","up to","up to the most expensive at");
		$a4 = array("has been extremely active","has been very busy","recently has been getting action","is filled with action");
		$a5 = array("cant wait to see","are excited to see","are anxious to see","are planning on seeing");
		$a6  = array("concert","event","show");
		$a6m  = array("last concert","final event","last show", "final show", "final concert", "last event");
		$a7 = array("will be held on","will take place on","can be seen on","will be happening on");
		$a8 = array("at this time","currently","for the moment","as of right now","as of today");
		$a8m = array("for this year","for this season","for this tour","for remaining of the tour","for a while");
		$a9 = array("secure yours ASAP","secure yours today","get them as soon as you can");
		$a10 = array("completely sell out","are all gone","are history","get sold out");

		$text= "{event_venue} ".random_pop($a1)." $performer. ConcertFix ".random_pop($a2)." \${low_price} for the {low_seat} section and range ".random_pop($a3)." \${high_price} for the {high_seat} section.";
		if($count > 1):
			$text .= " The ".random_pop($a6m)." ".random_pop($a7)." {last_date_long} and is the last remaining $performer ".random_pop($a6)." scheduled at {event_venue} ".random_pop($a8).".";
		else:
			$text .= " The ".random_pop($a6)." ".random_pop($a7)." {date_long} and is the only $performer concert scheduled at {event_venue} ".random_pop($a8).".";
		endif;

		$text .= " We only have {ticket_count} tickets left in stock, so ".random_pop($a9)." before they ".random_pop($a10).". Purchased tickets come with a 125% moneyback guarantee so you know your order is safe. Give us a shout at ".$this->config->item('phone')." if you have any questions about this exciting event in {event_city} on {date_short}.";
		return $text;
	}

	private function _create_performer_venue_vars($events)
	{
		if(!$events):
			 return false;
		endif;

		$params = array(
				'eventId'	=> $events[0]->ID,
					);
		$tickets = $this->soapy->run_soap('GetEventTickets', $params);
		$sorted_array = array();
		$vars['ticket_count'] = 0;
		foreach ($tickets as $ticket):
			$vars['ticket_count'] += $ticket->TicketQuantity;
			array_push($sorted_array, (array)$ticket);
		endforeach;
		// var_dump($sorted_array);
		$resorted = usort($sorted_array, function($a, $b) {
				return $a['ActualPrice'] - $b['ActualPrice'];
		});
		$firstEvent = $events[0];
		$last = array_pop($sorted_array);

		$lastEvent = end($events);

		$vars['event_venue'] 	= clean_venue($firstEvent->Venue);
		$vars['event_name'] 	= clean_venue($firstEvent->Name);
		$vars['event_city'] 	= clean_venue($firstEvent->City);
		$vars['last_date_long'] = date('F j, Y', strtotime($lastEvent->Date));
		$vars['date_long'] = date('F j, Y', strtotime($firstEvent->Date));
		$vars['date_short'] = date('F j', strtotime($firstEvent->Date));
		$vars['high_price'] = $last['ActualPrice'];
		$vars['low_price'] = (isset($sorted_array[0]['ActualPrice'])) ? $sorted_array[0]['ActualPrice'] : $last['ActualPrice'];
		$vars['high_seat'] = $last['Section'];
		$vars['low_seat'] = (isset($sorted_array[0]['Section'])) ? $sorted_array[0]['Section'] : $last['Section'];
		// $vars['twitter_hash'] = twHash($firstEvent->Venue);
		$vars['event_count'] = !empty($events) && is_array($events) ? count($events) : 0;
		//FirePHP::getInstance(true)->fb('_create_performer_venue_vars $vars='.print_r($vars,true));
		return $vars;
	}

	private function _create_performer_city_vars($events)
	{
		if(!$events) :
			$vars = array();
			$vars['event_count'] = 0;
			return $vars;
		endif;
		$params = array(
				'eventId'	=> $events[0]->ID,
					);
		$tickets = $this->soapy->run_soap('GetEventTickets', $params);
		$sorted_array = array();
		$vars['event_count'] = count($events);

		return $vars;
	}

	private function _replace_performer_venue_text($text, $replaceArray)
	{
		if(!is_array($replaceArray)) {
			return $text;
		}
		foreach ($replaceArray as $find => $replace) :
			$text = str_replace("{".$find."}", $replace, $text);
		endforeach;
		return $text;
	}

	private function _replace_performer_text($text, $events, $performer, $eventCount)
	{

		//replace performer first
		$text = str_replace('{performer}', ucwords($performer), $text);
		switch ($eventCount) :
			case 0:
				return $text;
				break;
			case 1:
				$replaceArray = array(
					'tour_city_F' => link_city($events[0]->City, $events[0]->StateProvince),
					'tour_state_F' => $events[0]->StateProvince,
					'tour_venue_F' => clean_venue($events[0]->Venue),
					'tour_date_F' => date('M j, Y', strtotime($events[0]->Date))
				);
				break;
			case 2:
				$replaceArray = array(
					'tour_city_F' => link_city($events[0]->City, $events[0]->StateProvince),
					'tour_state_F' => $events[0]->StateProvince,
					'tour_venue_F' => clean_venue($events[0]->Venue),
					'tour_date_F' => date('M j, Y', strtotime($events[0]->Date)),
					'tour_city_L' => link_city($events[1]->City, $events[1]->StateProvince),
					'tour_state_L' => $events[1]->StateProvince,
					'tour_venue_L' => clean_venue($events[1]->Venue),
					'tour_date_L' => date('M j, Y', strtotime($events[1]->Date)),
				);
				break;
			case 3:
				$replaceArray = array(
					'tour_city_F' => link_city($events[0]->City, $events[0]->StateProvince),
					'tour_state_F' => $events[0]->StateProvince,
					'tour_venue_F' => clean_venue($events[0]->Venue),
					'tour_date_F' => date('M j, Y', strtotime($events[0]->Date)),
					'tour_city_1' => link_city($events[1]->City, $events[1]->StateProvince),
					'tour_state_1' => $events[1]->StateProvince,
					'tour_venue_1' => clean_venue($events[1]->Venue),
					'tour_date_1' => date('M j, Y', strtotime($events[1]->Date)),
					'tour_city_L' => link_city($events[2]->City, $events[2]->StateProvince),
					'tour_state_L' => $events[2]->StateProvince,
					'tour_venue_L' => clean_venue($events[2]->Venue),
					'tour_date_L' => date('M j, Y', strtotime($events[2]->Date)),
				);
				break;
			case 4:
				$replaceArray = array(
					'tour_city_F' => link_city($events[0]->City, $events[0]->StateProvince),
					'tour_state_F' => $events[0]->StateProvince,
					'tour_venue_F' => clean_venue($events[0]->Venue),
					'tour_date_F' => date('M j, Y', strtotime($events[0]->Date)),
					'tour_city_1' => link_city($events[1]->City, $events[1]->StateProvince),
					'tour_state_1' => $events[1]->StateProvince,
					'tour_venue_1' => clean_venue($events[1]->Venue),
					'tour_date_1' => date('M j, Y', strtotime($events[1]->Date)),
					'tour_city_2' => link_city($events[2]->City, $events[2]->StateProvince),
					'tour_state_2' => $events[2]->StateProvince,
					'tour_venue_2' => clean_venue($events[2]->Venue),
					'tour_date_2' => date('M j, Y', strtotime($events[2]->Date)),
					'tour_city_L' => link_city($events[3]->City, $events[3]->StateProvince),
					'tour_state_L' => $events[3]->StateProvince,
					'tour_venue_L' => clean_venue($events[3]->Venue),
					'tour_date_L' => date('M j, Y', strtotime($events[3]->Date)),
				);
				break;

			case 5:
			default:
				$replaceArray = array(
					'tour_city_F' => link_city($events[0]->City, $events[0]->StateProvince),
					'tour_state_F' => $events[0]->StateProvince,
					'tour_venue_F' => clean_venue($events[0]->Venue),
					'tour_date_F' => date('M j, Y', strtotime($events[0]->Date)),
					'tour_city_1' => link_city($events[1]->City, $events[1]->StateProvince),
					'tour_state_1' => $events[1]->StateProvince,
					'tour_venue_1' => clean_venue($events[1]->Venue),
					'tour_date_1' => date('M j, Y', strtotime($events[1]->Date)),
					'tour_city_2' => link_city($events[2]->City, $events[2]->StateProvince),
					'tour_state_2' => $events[2]->StateProvince,
					'tour_venue_2' => clean_venue($events[2]->Venue),
					'tour_date_2' => date('M j, Y', strtotime($events[2]->Date)),
					'tour_city_3' => link_city($events[3]->City, $events[3]->StateProvince),
					'tour_state_3' => $events[3]->StateProvince,
					'tour_venue_3' => clean_venue($events[3]->Venue),
					'tour_date_3' => date('M j, Y', strtotime($events[3]->Date))
					);

					$last = end($events);

					$replaceArray['tour_city_L'] = link_city($last->City, $last->StateProvince);
					$replaceArray['tour_state_L'] = $last->StateProvince;
					$replaceArray['tour_venue_L'] = clean_venue($last->Venue);
					$replaceArray['tour_date_L'] = date('M j, Y', strtotime($last->Date));

				break;
		endswitch;
		$replaceArray['tour_event_count'] = $eventCount;

		// $replaceArray['tour_season'] =  "for ".($events) ? date('M j, Y', strtotime($events[0]->Date))." - " : '';
		// $replaceArray['tour_season'] .= ($events) ? date('M j, Y', strtotime(end($events)->Date)) : '';

		$replaceArray['tour_season'] = get_season($events[0]->Date, end($events)->Date);
		foreach ($replaceArray as $find => $replace) :
			$text = str_replace("{".$find."}", $replace, $text);
		endforeach;
		return $text;
	}


	private function _fetch_performer_page_text($slug, $type = "main")
	{

		$q = $this->db
			->select('*')
			->from('page_text')
			->where('slug', $slug)
			->where('type', $type)
			->get()
			->row();
			// echo $this->db->last_query();
			return $q;
	}

	private function _create_update_performer_venue_text_vars($slug, $text, $vars, $type = "main")
	{
		if($this->_fetch_performer_page_text($slug, $type)):

			$obj = array('type' => $type, 'created' => date('Y-m-d H:i:s'));
			if($text) $obj['text'] = $text;
			if($vars) $obj['var_data'] = $vars;

			$this->db->where('slug', $slug);
			$this->db->where('type', $type);

			return $this->db->update('page_text', $obj);
		else:
			//insert

			$obj = array('slug' => $slug, 'type' => $type, 'text' => $text, 'var_data' => $vars, 'created' => date('Y-m-d H:i:s'));
			return $this->db->insert('page_text', $obj);
		endif;
	}

	private function _create_update_performer_city_text_vars($slug, $text, $vars, $type = "main")
	{
		if($this->_fetch_performer_page_text($slug, $type)):
			$obj = array('type' => $type, 'created' => date('Y-m-d H:i:s'));
			if($text) $obj['text'] = $text;
			if($vars) $obj['var_data'] = $vars;
			$this->db->where('slug', $slug);
			$this->db->where('type', $type);
			return $this->db->update('page_text', $obj);
		else:
			//insert
			$obj = array('slug' => $slug, 'type' => $type, 'text' => $text, 'var_data' => $vars, 'created' => date('Y-m-d H:i:s'));
			return $this->db->insert('page_text', $obj);
		endif;
	}
}
/* End of file tours_m.php */
/* Location: ./application/models/tours_m.php */
