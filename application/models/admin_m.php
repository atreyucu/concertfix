<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_m extends CI_Model {


	function login($username, $password)
	{
		$query = $this->db->select('role, username')
				->from('users')
				->where(array('username' => $username, 'password' => md5($password), 'status' => 1))
				->limit(1)->get();
		if($query->num_rows() == 1):
			return $query->result();
		else:
			return false;
		endif;
	}

	function get_top_cities()
	{
		return $this->db->select('*')->from('top_cities')->order_by('area','desc')->get()->result();
	}

	function update_top_city($id, $area)
	{
		$this->db->where('id', $id);
		$data = array('area'	=> $area);
		$this->db->update('top_cities', $data);
	}

	function insert_city($data)
	{
		$data['slug'] = seoUrl($data['city'].' '.$data['state_short']);

		$data['city'] = trim(ucwords($data['city']));
		$data['state_short'] = trim(strtoupper($data['state_short']));
		return $this->db->insert('top_cities', $data);
	}

	function delete_city($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('top_cities');
	}

	function get_featured_performers()
	{
		return $this->db->select('pf.front, pf.geo, pf.side, p.Name, p.PerformerID,lfm.lfmImagePath img, pm.wide, pm.profile, pm.thumb')
				->from('performer_featured pf')
				->join('performer_full p', 'p.PerformerID = pf.PerformerID')
				->join('lfm_performer_images lfm', 'lfm.PerformerID = p.PerformerID', 'left')
				->join('performer_images pm', 'pm.PerformerID = p.PerformerID', 'left')
				->order_by('wide','asc')
				->order_by('profile','asc')
				->order_by('thumb','asc')
				->get()->result();
	}

	function get_performer_profile($id = false)
	{
		return $this->db
				->select('p.Name PerformerName, p.PerformerSlug, p.PerformerID, lfm.lfmImagePath img, pf.front, pf.side, pm.wide, pm.profile, pm.thumb,
				         txt.text, txt.custom, txt.created_on, txt.announced, txt.title, txt.announced_on, txt.refresh_date')
				->from('performer_full p')
				->join('performer_featured pf', 'pf.PerformerID = p.PerformerID','left')
				->join('lfm_performer_images lfm', 'lfm.PerformerID = p.PerformerID', 'left')
				->join('performer_images pm', 'pm.PerformerID = p.PerformerID', 'left')
				->join('performer_text txt', 'txt.PerformerID = p.PerformerID', 'left')
				->join('performer_album al','al.tnPerformerID = p.PerformerID','left')
				->where('p.PerformerID',$id)
				->get()->row();
	}

	function search_performer($string = false)
	{
		return $this->db
				->select('p.Name, p.PerformerSlug, p.PerformerID, lfm.lfmImagePath img, pf.front, pf.side, pf.geo, pm.wide, pm.profile, pm.thumb, txt.text, txt.custom, txt.created_on, txt.announced')
				->from('performer_full p')
				->join('performer_featured pf', 'pf.PerformerID = p.PerformerID','left')
				->join('lfm_performer_images lfm', 'lfm.PerformerID = p.PerformerID', 'left')
				->join('performer_images pm', 'pm.PerformerID = p.PerformerID', 'left')
				->join('performer_text txt', 'txt.PerformerID = p.PerformerID', 'left')
				->like('LOWER(p.Name)',strtolower($string), 'both')
				->limit(15)
				->get()->result();
	}

	function search_performers_with_announcements($string = false)
	{
		$result = $this->search_performer($string);
		foreach ($result as $key => $p):
			$result[$key]->announcements = $this->db->select('*')->from('tour_announcements')->where('PerformerID', $p->PerformerID)->order_by('announce_date', 'desc')->get()->result();
		endforeach;
		return $result;
	}

	function update_text($PerformerID, $saveData)
	{
		$this->db->where('PerformerID', $PerformerID);
		$this->db->update('performer_text', $saveData);
		return true;
	}

	function create_announcement($data)
	{
		$this->db->insert('tour_announcements', $data);
		return $this->db->insert_id();

	}

	function update_announcement($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('tour_announcements', $data);
		return true;
	}

	function get_cats()
	{
		return $this->db->select()->from('announcement_categories')->get()->result();
	}

	function update_cats($aId, $cats)
	{
		$this->db->where('a_id', $aId);
		$this->db->delete('announcement_category');
		foreach ($cats as $k => $c) :
				$data = array('a_id' => $aId, 'a_category_id' => $k);
				$this->db->insert('announcement_category', $data);
			# code...
		endforeach;
	}
	function get_performer_by_id($id = false)
	{
		return $this->db
				->select('p.Name, p.PerformerSlug, p.PerformerID, p.fbImagePath, lfm.lfmImagePath, pf.front, pf.side, pm.wide, pm.profile, pm.thumb')
				->from('performer_full p')
				->join('performer_featured pf', 'pf.PerformerID = p.PerformerID','left')
				->join('lfm_performer_images lfm', 'lfm.PerformerID = p.PerformerID', 'left')
				->join('performer_images pm', 'pm.PerformerID = p.PerformerID', 'left')
				->where('p.PerformerID', $id)
				->limit(1)
				->get()->row();
	}

	function get_announcment_with_performer($aId = false)
	{

			$this->db
				->select('p.Name PerformerName, p.PerformerSlug, p.PerformerID, pt.text pText, lfm.lfmImagePath, pf.front, pf.side, pm.wide, pm.profile, pm.thumb, a.announce_text, a.title, a.announce_date, a.end_date, a.active, a.id aId')
				->from('tour_announcements a')
				->join('performer_full p', 'p.PerformerID = a.performerID')
				->join('performer_text pt', 'pt.PerformerID = a.performerID','left')
				->join('performer_featured pf', 'pf.PerformerID = p.PerformerID','left')
				->join('lfm_performer_images lfm', 'lfm.PerformerID = p.PerformerID', 'left')
				->join('performer_images pm', 'pm.PerformerID = p.PerformerID', 'left')
				->group_by('aId')
				->order_by('a.announce_date','desc');
		if($aId):
			return $this->db->where('a.id', $aId)
						->limit(1)
						->get()->row();
		else:
			$q = $this->db->get()->result();
			return $q;
		endif;
	}

	private function _performer_images_exist($pid)
	{
		return $this->db->select('count(id) count')->from('performer_images')->where('PerformerID', $pid)->get()->row()->count;
	}

	public function get_announcements($pId)
	{

	}
	public function get_announced($all = false)
	{
		return $this->db->select('pt.*, pf.Name PerformerName')
					->from('performer_text pt')
					->join('performer_full pf', 'pf.PerformerID = pt.PerformerID')
					// ->join('performer_images pm', 'pm.PerformerID = pf.PerformerID', 'left')
					// ->join('lfm_performer_images i','pf.PerformerID = i.PerformerID','left')
					->where(array('pt.announced' => 1))
					->order_by('pt.created_on','Desc')
					// ->limit($limit, $offset)
					->get()
					->result();
	}

	public function get_announcement_cat($aid = false)
	{
		$a = $this->db->select('ac.id, ac.Category')
						->from('announcement_category acy')
						->join('announcement_categories ac', 'ac.id = acy.a_category_id','left')
						->where('acy.a_id', $aid)
						->get()
						->result();
		$ret = array();
		foreach ($a as $f) {
			$ret[$f->Category] = $f->id;
		}
		return $ret;
	}

	private function _is_featured($pid)
	{
		return $this->db->select('count(id) count')->from('performer_featured')->where('PerformerID', $pid)->get()->row()->count;
	}

	function toggle_featured($pid, $status, $type)
	{

		switch ($type) :
			case 'nofeat':
				$data = array('geo' => 0, 'front' => 0);
				break;
			case 'geo':
				$data = array('geo' => 1, 'front' => 0);
				break;
			case 'both':
				$data = array('geo' => 1, 'front' => 1);
				break;
			case 'side':
				$data = array('side' => $status);
				break;
			default:
				$data = array('geo' => 0, 'side' => 0, 'front' => 0);
				break;
		endswitch;

		if($this->_is_featured($pid)):
			$this->db->where('PerformerID', $pid);
			return $this->db->update('performer_featured', $data);
		else:
			$data['PerformerID'] = $pid;
			return $this->db->insert('performer_featured', $data);
		endif;
	}

	function update_performer_image($pid, $imgPath, $type)
	{
		if($this->_performer_images_exist($pid)):
			$this->db->where('PerformerID', $pid);
			$data = array($type => $imgPath);
			return $this->db->update('performer_images', $data);
		else:
			$data = array($type => $imgPath, 'PerformerID' => $pid);
			return $this->db->insert('performer_images', $data);
		endif;
	}

	function update_lastfm_fb_image($pid, $imgPath, $type)
	{
		if ($type == 'fb') {

			$this->db->where('PerformerID', $pid);
			$data = array('fbImagePath' => $imgPath);
			return $this->db->update('performer_full', $data);

		} elseif ($type == 'last.fm') {

			$this->db->where('PerformerID', $pid);
			$data = array('lfmImagePath' => $imgPath);
			return $this->db->update('lfm_performer_images', $data);
		}

	}

	public function get_menu_list()
	{
		return $this->db->select('*')->from('menus')->where('level < 3')->get()->result();
	}

	public function get_menu($parentId = 0, $level = 1)
 	{
 		if($level < 4): // recursively till your ass drops
 			$menu = $this->db->select('*')->from('menus')->where('parent_id', $parentId)->order_by('order', 'asc')->get()->result();
 			$level++; //deepen
 			foreach ($menu as $key => $m) :
 				$menu[$key]->children = $this->get_menu($m->id, $level);
 			endforeach;
	 		return $menu;
 		endif;
 		return false;
 	}

 	private function _get_kids_menu($id, $level = 3)
 	{
 		return $this->db->select('id, menu_title')->from('menus')->where('parent_id', $id)->get()->result();
 	}

 	public function add_menu($data)
 	{
 		$parentLevel = $this->db->select('level, menu_title')->from('menus')->where('id', $data['parent_id'])->get()->row()->level;
 		$data['level'] = $parentLevel + 1;
 		$data['menu_link'] = (preg_match('/http/i', $data['menu_link'])) ? $data['menu_link'] : "/".$data['menu_link'];
 		$this->db->insert('menus', $data);
 		return true;
 	}

 	public function delete_menu($id)
 	{
 		$removed = array();
 		$thisMenu = $this->db->select('id, level, menu_title')->from('menus')->where('id', $id)->get()->row();
 		if(isset($thisMenu->id)):
	  		switch ($thisMenu->level) :
	 			case 1:
	 				$children = $this->_get_kids_menu($id);
	 				foreach ($children as $kid) :
	 					$grands = $this->_get_kids_menu($kid->id);
	 					foreach ($grands as $grand):
	 						$this->db->where('id', $grand->id);
	 						$this->db->delete('menus');
	 						// echo $this->db->last_query();
	 						array_push($removed, $grand->menu_title);
	 					endforeach;
	 					$this->db->where('id', $kid->id);
						$this->db->delete('menus');
						// echo $this->db->last_query();
	 					array_push($removed, $kid->menu_title);
	 				endforeach;
	 				$this->db->where('id', $id);
					$this->db->delete('menus');
	 				array_push($removed, $thisMenu->menu_title);

	 				break;
	 			case 2:
	 				$children = $this->_get_kids_menu($id);
	 				foreach ($children as $kid) :
	 					$this->db->where('id', $kid->id);
						$this->db->delete('menus');
						// echo $this->db->last_query();
	 					array_push($removed, $kid->menu_title);
	 				endforeach;
	 				$this->db->where('id', $id);
					$this->db->delete('menus');
	 				array_push($removed, $thisMenu->menu_title);
					break;
	 			case 3:
	 			default:
	 				$this->db->where('id', $id);
					$this->db->delete('menus');
					// echo $this->db->last_query();
	 				array_push($removed, $thisMenu->menu_title);
	 				break;
	 		endswitch;
	 		return $removed;
	 	endif;
	 	return false;
 	}

 	public function remove_picture($pId, $type)
 	{
 		$path = $this->db->select("$type itm")->from('performer_images')->where('PerformerID', $pId)->get()->row()->itm;

 		$data = array($type => '');
 		$this->db->where('PerformerID', $pId);
 		$this->db->update('performer_images', $data);
 		return $path;
 	}

 	public function what_cat_dragged_in($date = false)
 	{
 		if($date):
 			return $this->db->select('*')->from('cached_events')->where('AddedDate', $date)->order_by('Name','asc')->order_by('Date','asc')->get()->result();
 		else:
			return $this->db->select('*')->from('cached_events')->where('AddedDate', date('Y-m-d'))->order_by('Name','asc')->order_by('Date','asc')->get()->result();
 		endif;
 	}

}

/* End of file admin_m.php */
/* Location: ./application/models/admin_m.php */
