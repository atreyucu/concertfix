<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Concerts_m extends CI_Model {


	public function get_event_performers($eId)
	{
		return $this->db
				->select('ep.PerformerName, p.PerformerSlug, i.lfmImagePath img')
				->from('event_performers ep')
				->join('performer_full p','ep.PerformerID = p.PerformerID')
				->join('lfm_performer_images i','p.PerformerID = i.PerformerID','left')
				->where('ep.EventID', $eId)
				->get()->result();
	}

	public function get_categories($id = false, $slug = false)
	{
		if($slug):
			$this->db->where('Slug', $slug);
		endif;
		if($id):
			$this->db->where('ChildCategoryID', $id);
		endif;
		$query = $this->db->select('*')->from('category_filters')->where("id <> 5")->get()->result();
		// var_dump($query);
		return $query;
	}

    public function get_categories_user_settings()
    {
        $query = $this->db->select('*')->from('category_filters')->where("id <> 5 and Slug <> 'other' and Slug <> 'performance-series'")->get()->result();
        // var_dump($query);
        return $query;
    }

	public function get_venues_by_location_slug($slug)
	{
		$q = $this->db
					->select('v.RegionSlug, v.VenueSlug, v.Name, (SELECT count(e.id) cnt FROM cached_events e WHERE e.VenueID = v.ID) cnt')
					->from('tn_venues v')
                    ->where('RegionSlug', $slug)
                    //->where('RegionSlug', substr($slug, 0, -2)) // IN stagingdev DB RegionSlug = xxxx- not xxxx-xx
					->get()
					->result();
		return $q;
	}

	public function get_performers_by_genre_id($id, $limit = false, $active = true)
	{
		if($active):
			$this->db->having('event_count > 1');
		endif;
		$result = $this->db
				->select('p.*, i.lfmImagePath img, pm.wide, pm.profile, pm.thumb, h.Percent,
				         	(SELECT count(ep.id) event_count FROM event_performers ep WHERE ep.PerformerID = p.PerformerID) event_count
				         	')
				->from('performer_full p')
				->join('performer_images pm', 'pm.PerformerID = p.PerformerID', 'left')
				->join('lfm_performer_images i','p.PerformerID = i.PerformerID','left')
				->join('performer_hotsales h','h.PerformerID = p.PerformerID','left')
				->where('p.ChildCategoryID', $id)
				->group_by('p.PerformerID')
				->order_by('h.Percent','desc'	)
				// ->limit($limit)
				->get()->result();
				// echo $this->db->last_query();
				foreach ($result as $key => $p):
						$result[$key]->event = $this->global_m->get_performer_event($p->PerformerID);
				endforeach;
				array_slice($result, $limit);
				return $result;


	}

	private function _do_city_text_var_magic($events, $text)
	{

	}

	public function get_category_events($cats = array())
	{
		$where = "(";
		$count = count($cats);
		foreach ($cats as $key => $cat):
			$where .= "e.ChildCategoryID = {$cat->ChildCategoryID}";

			$where .= (($count -1) > ($key)) ? " OR " : ' ';
		endforeach;
		$where .= ")";
		$result  = $this->db->select('e.EventID ID, e.Date DisplayDate, e.Date, e.Name, e.Venue, e.City')
							->from('cached_events e')
							// ->join('performer_featured f', 'f.PerformerID = ', 'left')
							->where('e.Date > NOW()')
							->where($where)
							->get()
							->result();
					return $result;
	}

	public function get_cities($countryId = 217)
	{
		$country = array(217 => 'US', 38 => 'CA' );

		$original = $this->input->get('legacy');
		// var_dump($original);
		if(!$original):
		$result = $this->db->select('g.city City, g.region StateProvince, g.slug')
						->from('geo_cities g')
						->where("g.EventCount > 0")
						->where('g.country', $country[$countryId])
						->group_by('g.slug')
						->order_by('g.region','asc')
						->order_by('g.slug','asc')
						->get()
						->result();
		else:
		$result = $this->db->select('e.City, e.StateProvince, LOWER(REPLACE(CONCAT(`City`, "-", `StateProvince`)," ", "-")) slug', false)
						->from('cached_events e')
						->where('CountryID', $countryId)
						->group_by('slug')
						->order_by('StateProvince', 'Asc')
						->get()
						->result();
						// echo $this->db->last_query();
		endif;

		return $result;
	}

	public function get_main_text($params)
	{
		$slug = $this->uri->segment(2);
		$whereClause = array('slug' => $slug, 'type' => 'top');
		$textData = $this->db->select("*")
			->from('page_text')
			->where($whereClause)
			->get()
			->row();
		// die(var_dump($params));
		if(array_key_exists('events_count', $params) && $params['events_count'] == 0):
			return $this->_create_main_text($params);
		elseif(isset($textData->text) && !$this->input->get('refresh')):
			return $textData->text;
		else:
			$newText = $this->_create_main_text($params);
			$this->db->where($whereClause);
			$this->db->delete('page_text');
			$query_params = array_merge(
				$whereClause,
				array('text' => $newText, 'var_data' => '', 'created' => date('Y-m-d H:i:s'))
			);
			$this->db->insert('page_text', $query_params);
			return $newText;
		endif;
	}

	private function _create_main_text($params)
	{
		switch ($params['type']) :
			case 'genre':
				extract($params);
				$params["rand_key"] = $genre;
				$alt = Spinner::prepare();
				if($params['events_count'] == 0) {
					return "$city currently has no $genre events scheduled. Stay tuned for any updates to the schedule below.";
				}
				return join(" ", array(
					$city, $alt->will_be_hosting(), "a", $alt->list(), "of", $genre, $alt->concerts(), $alt->that_you_can(), $alt->view(), "in the", $alt->schedule(), "below.",
					$alt->you_can(true), "RSVP", $alt->through(), "Facebook,", $alt->view(), $alt->event(), $alt->details(), "and", $alt->purchase(), "tickets for the following", $genre, $alt->events(), $alt->coming_to(), $city . ".",
					$alt->concert_fix(), $alt->always_offers(), "a", $alt->money_back(), "for all tickets to the", $genre, $alt->events(), "in", $city . ".",
					$alt->randomSentence($params),
					"The", $genre, $alt->concert(), $alt->schedule(), "for", $city, $alt->is_always(), $alt->changing() . ",", "so", $alt->check_back_with_us(), "for", $alt->any_updates_to(), "the", $genre, $alt->concerts(), $alt->coming_to(), "the city of", $city . "."
				));
				break;
			case 'venue':
				extract($params);
				$params["rand_key"] = $venue;
				$alt = Spinner::prepare();
				if($params['events_count'] == 0) {
					return "$venue currently has no events scheduled. Stay tuned for any updates to the schedule below.";
				}
				return join(" ", array(
					$venue, $alt->will_be_hosting(), "a", $alt->list(), "of", $alt->concerts(), $alt->that_you_can(), $alt->view(), "in the", $alt->schedule(), "below.",
					$alt->you_can(true), "RSVP", $alt->through(), "Facebook,", $alt->view(), $alt->event(), $alt->details(), "and", $alt->purchase(), "tickets for the following", $alt->events(), $alt->coming_to(), $venue . ".",
					$alt->concert_fix(), $alt->always_offers(), "a", $alt->money_back(), "for all tickets to the", $alt->events(), "in", $venue . ".",
					$alt->randomSentence($params),
					"The", $venue, $alt->concert(), $alt->schedule(), $alt->is_always(), $alt->changing() . ",", "so", $alt->check_back_with_us(), "for", $alt->any_updates_to(), "the", $alt->concerts(), $alt->coming_to(), $venue . "."
				));
				break;
			case 'date':
				extract($params);
				$params["rand_key"] = $month;
				$alt = Spinner::prepare();
				if($params['events_count'] == 0) {
					return "$city currently has no $month, $year events scheduled. Stay tuned for any updates to the schedule below.";
				}
				return join(" ", array(
					"In", $month, $year, $city, $alt->will_be_hosting(), "a", $alt->list(), "of", $alt->concerts(), $alt->that_you_can(), $alt->view(), "in the", $alt->schedule(), "below.",
					$alt->you_can(true), "RSVP", $alt->through(), "Facebook,", $alt->view(), $alt->event(), $alt->details(), "and", $alt->purchase(), "tickets for the following", $alt->events(), $alt->coming_to(), $city, $alt->during(), "the month of", $month . ".",
					$alt->concert_fix(), $alt->always_offers(), "a", $alt->money_back(), "for all tickets to the", $city, $alt->events(), "in", $month, $year . ".",
					$alt->randomSentence($params),
					"The", $city, $alt->concert(), $alt->schedule(), $alt->during(), $month, $alt->is_always(), $alt->changing() . ",", "so", $alt->check_back_with_us(), "for", $alt->any_updates_to(), "the", $month, $year, $alt->concerts(), $alt->coming_to(), "the city of", $city . "."
				));
				break;

			default:
				# code...
			return '';
		endswitch;
	}

}

/* End of file concerts_m.php */
/* Location: ./application/models/concerts_m.php */
