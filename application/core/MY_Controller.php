<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_SiteController extends CI_Controller {
	//Class-wide variable to store user object in.
	public $caching;
	public $cfUser;
	public $menuLinks;
	public function __construct() {
		parent::__construct();
		$this->caching = $this->config->item('enable_caching');
		//DEBUGGING

		$this->output->enable_profiler(false);
		$this->load->helper('url');
		date_default_timezone_set('America/New_York');
			// Needed to disable canonical on ticket pages.
			$canonical = strrpos(uri_string(), 'ticket') === 0
			? ''
			: "<link rel='canonical' href='".strtr(current_url(),array('http:'=>'https:'))."'/>";

			if (strstr(current_url(),'/concerts/') and strstr(current_url(),'+')){
				$current_url = explode('+', current_url());
				if ($this->check_date($current_url[1]))
					$canonical = "<link rel='canonical' href='".strtr($current_url[0],array('http:'=>'https:'))."'/>";
			}

			$jqbt = "
				<!-- global settings -->
				<meta property='og:site_name' content='ConcertFix'>
				<meta property='fb:app_id' content='376715072474405'>
				<meta property='og:email' content='info@concertfix.com'>
				<meta property='og:phone_number' content='".$this->config->item('phone')."'>
				<meta property='og:type' content='company'>
				<meta property='og:url' content='".strtr(current_url(),array('http:'=>'https:'))."'>

				<meta name='viewport' content='width=device-width, initial-scale=1.0'>
				<meta name='author' content='ConcertFix'>
				<meta name='rating' content='general'>
				<meta name='page-type' content='document'>

				<!-- Fav and touch icons -->

				$canonical
				<link rel='shortcut icon' href='/public/img/ico/favicon.ico'>
				<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Cabin:400,400italic,700,700italic'>
				<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=PT+Sans:regular,italic,bold'>

				<link rel='apple-touch-icon-precomposed' sizes='114x114' href='/public/img/ico/apple-touch-icon-114-precomposed.png'>
				<link rel='apple-touch-icon-precomposed' sizes='72x72' href='/public/img/ico/apple-touch-icon-72-precomposed.png'>
				<link rel='apple-touch-icon-precomposed' href='/public/img/ico/apple-touch-icon-57-precomposed.png'>

				<link rel='stylesheet' type='text/css' href='https://code.jquery.com/ui/1.10.0/themes/smoothness/jquery-ui.css'>
				<link rel='stylesheet' type='text/css' href='/public/css/concertfix.css'>
				<link rel='stylesheet' type='text/css' href='/public/css/grid.css'>
				<link rel='stylesheet' type='text/css' href='/public/css/temp.css'>


				";
		$this->template->prepend_metadata($jqbt);
 		$this->load->library('twitterapi/TwitterAPIExchange');
 		$this->load->library('userLib');
		if (isset($_SESSION['cfUser']))
			$this->cfUser = $_SESSION['cfUser'];
		else
 			$this->cfUser = $this->session->all_userdata();
 		$this->menuLinks = $this->global_m->get_menu();
		// $this->load->vars(array('menuLinks' => ));

	//	parse_str( $_SERVER['QUERY_STRING'], $_REQUEST );
/*        $CI = & get_instance();
*/
		setlocale(LC_TIME, 'ru_CA.UTF-8');
	}

	private function check_date($param)
	{
		$param = explode('-', $param);
		switch($param[0]){
			case 'january':
			case 'february':
			case 'march':
			case 'april':
			case 'may':
			case 'june':
			case 'july':
			case 'august':
			case 'september':
			case 'october':
			case 'november':
			case 'december':
				$result = true;
				break;
			default:
				$result = false;
		}
		return $result;
	}

}


class TN_SiteController extends CI_Controller {
	//Class-wide variable to store user object in.
	public $caching;
	public $cfUser;
	public $menuLinks;
	public function __construct() {
		parent::__construct();
		$this->caching = $this->config->item('enable_caching');
		//DEBUGGING

		$this->output->enable_profiler(false);
		$this->load->helper('url');
		date_default_timezone_set('America/New_York');
		// Needed to disable canonical on ticket pages.
		$canonical = strrpos(uri_string(), 'ticket') === 0
			? ''
			: "<link rel='canonical' href='".strtr(current_url(),array('http:'=>'https:'))."'/>";

		if (strstr(current_url(),'/concerts/') and strstr(current_url(),'+')){
			$current_url = explode('+', current_url());
			if ($this->check_date($current_url[1]))
				$canonical = "<link rel='canonical' href='".strtr($current_url[0],array('http:'=>'https:'))."'/>";
		}

		$jqbt = "
				<!-- global settings -->
				<!--<meta http-equiv='X-UA-Compatible' content='IE=8' />-->
				<meta property='og:site_name' content='ConcertFix'>
				<meta property='fb:app_id' content='376715072474405'>
				<meta property='og:email' content='info@concertfix.com'>
				<meta property='og:phone_number' content='".$this->config->item('phone')."'>
				<meta property='og:type' content='company'>
				<meta property='og:url' content='".strtr(current_url(),array('http:'=>'https:'))."'>

				<meta name='viewport' content='width=device-width, initial-scale=1.0'>
				<meta name='author' content='ConcertFix'>
				<meta name='rating' content='general'>
				<meta name='page-type' content='document'>

				<!-- Fav and touch icons -->

				$canonical
				<link rel='shortcut icon' href='/public/img/ico/favicon.ico'>
				<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Cabin:400,400italic,700,700italic'>
				<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=PT+Sans:regular,italic,bold'>

				<link rel='apple-touch-icon-precomposed' sizes='114x114' href='/public/img/ico/apple-touch-icon-114-precomposed.png'>
				<link rel='apple-touch-icon-precomposed' sizes='72x72' href='/public/img/ico/apple-touch-icon-72-precomposed.png'>
				<link rel='apple-touch-icon-precomposed' href='/public/img/ico/apple-touch-icon-57-precomposed.png'>
				<link rel='stylesheet' type='text/css' href='https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css'>
				<link rel='stylesheet' type='text/css' href='/public/css/concertfix_tickets.css'>
				<link rel='stylesheet' type='text/css' href='/public/css/grid.css'>
				<link rel='stylesheet' type='text/css' href='/public/css/temp.css'>


				";
		$this->template->prepend_metadata($jqbt);
		$this->load->library('twitterapi/TwitterAPIExchange');
		$this->load->library('userLib');
		$this->cfUser = $this->session->all_userdata();
		$this->menuLinks = $this->global_m->get_menu();
		// $this->load->vars(array('menuLinks' => ));

		//	parse_str( $_SERVER['QUERY_STRING'], $_REQUEST );
		/*        $CI = & get_instance();
        */
		setlocale(LC_TIME, 'ru_CA.UTF-8');
	}

	private function check_date($param)
	{
		$param = explode('-', $param);
		switch($param[0]){
			case 'january':
			case 'february':
			case 'march':
			case 'april':
			case 'may':
			case 'june':
			case 'july':
			case 'august':
			case 'september':
			case 'october':
			case 'november':
			case 'december':
				$result = true;
				break;
			default:
				$result = false;
		}
		return $result;
	}

}


class MY_AdminController extends CI_Controller
{

		public function __construct()
		{
		parent::__construct();
		$this->output->enable_profiler(false);
		// error_reporting(E_ALL);		

		date_default_timezone_set('America/New_York');

		$jqbt = "
				<!-- Fav and touch icons -->
				<link rel='shortcut icon' href='/public/img/ico/favicon.ico'>
				<link rel='apple-touch-icon-precomposed' sizes='114x114' href='/public/img/ico/apple-touch-icon-114-precomposed.png'>
				<link rel='apple-touch-icon-precomposed' sizes='72x72' href='/public/img/ico/apple-touch-icon-72-precomposed.png'>
				<link rel='apple-touch-icon-precomposed' href='/public/img/ico/apple-touch-icon-57-precomposed.png'>
				<link rel='stylesheet' type='text/css' href='/public/css/concertfix.css'>
				<link rel='stylesheet' type='text/css' href='/public/css/grid.css'>
				<link rel='stylesheet' type='text/css' href='/public/css/jquery.wysiwyg.css'>
				<link rel='stylesheet' type='text/css' href='//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css'>

				<!-- JS Libs -->
				<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js'></script>
				<script src='https://code.jquery.com/ui/1.10.0/jquery-ui.js'></script>
				<script src='/public/js/bootstrap/bootstrap.js'></script>
				<script src='/public/js/libs/jquery.uploadifive.min.js'></script>
				<script src='/public/js/plugins/jquery.wysiwyg.js'></script>
				<script src='/public/js/admin.js'></script>
				";
		$this->template->prepend_metadata($jqbt);

		setlocale(LC_TIME, 'ru_CA.UTF-8');
}

}

 ?>
