<?php
// application/core/MY_Exceptions.php
class MY_Exceptions extends CI_Exceptions {

    public function show_404($page = '', $log_error = TRUE)
    {
     include APPPATH . 'config/routes.php';

     // By default we log this, but allow a dev to skip it
  if ($log_error)
  {
   log_message('error', '404 Page Not Found --> '.$page);
  }

  if(!empty($route['404_override']) ){
    header("HTTP/1.0 404 Not Found");

   $CI =& get_instance();

   $ogMeta = array(
                'description'   => "Concert Fix, find all the concerts you need",
                'keywords'    => "concerts, tickets, performers, tour announcements, fix",
                'copyright'   => "Copyright 2013, Concert Fix",
                'image'     => base_url()."public/img/assets/concert-fix-logo.png",
                );
    $metaTags = array(
                  'title'   =>"Concert Fix - NOT FOUND! 404",
                  'description' => "concerts, tickets, performers, tour announcements, fix",
                  );
    echo $CI->template
        ->prepend_metatags($metaTags)
        ->prepend_metatags($ogMeta, true)
        ->title("404: ConcertFix | Not Found!")
        ->set_layout('fullwidth')
        ->set_partial('header', 'frontend/global/header')
        ->set_partial('search', 'frontend/global/404search')
        // ->set_partial('popular', 'frontend/home/popular')
        // ->set_partial('search', 'frontend/home/search')
        // ->set_partial('sidebar', 'frontend/home/home_sidebar')
        ->set_partial('footer', 'frontend/global/footer')
        ->set_breadcrumb('Home','/')
        ->set_breadcrumb('NOT FOUND!')
        ->build('frontend/pages/404', false);


   // $CI->load->view('my_view');
         // echo $CI->output->get_output();
         exit;
  } else {

   $heading = "404 Page Not Found";
   $message = "The page you requested was not found.";



   echo $this->show_error($heading, $message, 'error_404', 404);
   exit;
  }
    }

}