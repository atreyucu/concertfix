<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "site";
$route['404_override'] = 'site/not_found';

//tour announcements and ticket specials
$route['tour-announcements'] = "site/announcements";
$route['great-deals-on-tickets'] = "site/specials";

//random pages
$route['track-your-favorite-artists'] = "site/pages/concert-tracker";
$route['find-your-local-concerts'] = "site/pages/all-cities";
$route['policies']	= "site/pages/privacy";
$route['about-us']	= "site/pages/about";
$route['faq']	= "site/pages/faq";
$route['contact-us']	= "site/pages/contact";
$route['maintenance']	= "site/pages/maintenance";
$route['ticket-guarantee']	= "site/pages/guarantee";
$route['top-tours']	= "site/pages/toptours";


//grab single performer
$route['search'] = "site/search";

//search with $_GET - not used in this release
$route['search/[:any]'] = "site/search/$1";


/* End of file routes.php */
/* Location: ./application/config/routes.php */