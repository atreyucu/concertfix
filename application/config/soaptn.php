<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// $config['wsd'] = "http://tnwebservices-test.ticketnetwork.com/tnwebservice/v3.1/WSDL/tnwebservice.xml";
$config['wsd'] = "http://tnwebservices.ticketnetwork.com/tnwebservice/v3.2/WSDL/tnwebservice.xml";
// Alternative
//$config['wsd'] = "https://tnwebservices.ticketnetwork.com/TNWebservice/v3.2/TNWebserviceStringinputs.asmx?wsdl";
//THE LINES ABOVE sets TO TEST OR PROD on 3.2

$config['bid'] = "3282";
$config['webconfigID'] = "14132";
$config['soapMode'] = "";
$config['soap_exec_time'] = 0;
$config['phone']	= "855-428-3860";
