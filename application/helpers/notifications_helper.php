<?php


function buildMessage($user, $subject, $html, $txt = '', $params = false)
{
	$ci =& get_instance();
	$conf = $ci->config->item('mandrill');
	$key = $conf['api_key'];
    $to = $user['email'];
    $name = $user['name'];
    $message2  = array(
       "html"      => $html,
        "text"      => $txt,
        "subject"   => $subject,
        "from_email"=>"no-reply@concertfix.com",
        "from_name"=> "ConcertFix",
        'to' => array(
            array(
                "email" => $to,
                "name" => $name,
                "type" => "to"
            )
        ),
        "headers"   => array('Reply-To' => "notifications@concertfix.com"),
        "important" => false,
        "track_opens" => null,
        "track_clicks" => null,
        "auto_text" => null,
        "auto_html" => null,
        "inline_css" => true,
        "url_strip_qs" => null,
        "preserve_recipients" => null,
        "view_content_link" => null,
        "bcc_address" => "theconcertfix+signups@gmail.com",
        "tracking_domain" => "concertfix.com",
        "signing_domain" => null,
        "return_path_domain" => null,
        "merge" => false,
        "tags"  => array("cf-signup"),
        'global_merge_vars' => array(
            array(
                'name' => 'merge1',
                'content' => 'merge1 content'
            )
        ),
        'merge_vars' => array(
            array(
                'rcpt' => $to,
                'vars' => array(
                    array(
                        'name' => 'merge2',
                        'content' => 'merge2 content'
                    )
                )
            )
        ),
        "tags"  => array("cf-signup"),
        'subaccount' => 'stagingdev-1',
        'google_analytics_domains' => array('concertfix.com'),
        'google_analytics_campaign' => 'message.from_email@concertfix.com',
        'metadata' => array('website' => 'www.concertfix.com'),
        'recipient_metadata' => array(
            array(
                'rcpt' => $to,
                'values' => array('user_id' => 123456)
            )
        ),
        'attachments' => array(),
        'images' => array()
    );
    return $message2;
}


function sendEmailSparkpost($user, $subject, $html, $params = false){
    try{
        $this->load->library('email');

        $this->email->from('no-reply@concertfix.com', 'ConcertFix');
        $this->email->to($user);

        $this->email->reply_to('info@concertfix.com', 'ConcertFix');

        $this->email->subject($subject);
        $this->email->message($html);

        if ($this->email->send())
            echo "Email Sent Successfuly"."\n\r";
        else
            echo $this->email->print_debugger();
        return $this->email->send();
    }
    catch(Exception $e){
        echo $e->getMessage();
    }

}

 ?>