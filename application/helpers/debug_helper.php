<?php 

function pdump($var, $format = true)
{

     $_ci = get_instance();
    if($format):
    $string = print_r($var,true);
    $string = str_replace('stdClass','', $string);
    $string = str_replace('[', "</li><li style='list-style:none;margin-left:15px;'>[<span style='font-weight:bold;'>", $string);
    $string = str_replace(']', "</span>]", $string);
    $string = str_replace('Array', "ARRAY<div style='border-left:2px dotted #184692;display:block;background:#fff;'>", $string);
    $string = str_replace('Object', "OBJECT<div style='border-left:2px dotted #6D3030;display:block; background:#E0EEFE;'>", $string);
    $string = str_replace('(', "<strong>(</strong><ul style='list-style:none;margin-left:10px;'>", $string);
 //   $string = str_replace('Array', "<div style='color:#184692'>ARRAY => ", $string);
    $string = str_replace(')', "</ul><strong>)</strong></div>", $string);

   $debugOut = "<div style='display:block;width:100%;font-size:12px;'><div>".$string."</div><br style='clear:both;'></div>";
   else:

        $debugOut = print_r($var,true);

    endif;
   $_ci->load->vars(array('debug-output' => $debugOut));
}

 ?>