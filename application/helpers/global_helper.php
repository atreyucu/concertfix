<?php
function seoUrl($string, $separator = "-") {
    //Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
    $string = strtolower($string);
    //Strip any unwanted characters
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", $separator, $string);
    return $string;
}

function get_cfg($itm = false)
{
    $ci =& get_instance();
    return $ci->config->item($itm);
}

function dateRange($posts)
{

}

function clean_venue($venue)

{
    //I feel sooo dirty writing this split, but.... Code Gods forgive for I have sinned
    $fixCase = str_replace('(Formerly', '(formerly', $venue);// if some smartass put (Formerly, then we convert that to lover case just that
    $vArray = explode('(formerly', $fixCase);
    return trim($vArray[0]); //return not dignity
}

function clean_song($string)
{
    $removeEdits = explode('(', $string);
    return trim($removeEdits[0]);
}

function tour_link($tour, $performer_name) {
  return '/tickets/'.$tour->ID.tour_slug($tour, $performer_name);
}

function tour_link_full($tour, $performer_name) {
    return 'https://'.$_SERVER['HTTP_HOST'].'/tickets/'.$tour->ID.tour_slug($tour, $performer_name);
}

function tour_slug($tour, $performer_name) {
    return '?' . http_build_query(
      array_filter(
        array(
          'p' => seoUrl($performer_name),
          'v' => slug_venue($tour->Venue),
          'c' => slug_location(array(
            'city' => $tour->City,
            'state' => $tour->StateProvince
          ))
        )
      )
    );
}

function event_link($event, $trueLocation) {
    return '/tickets/'.$event->ID.event_slug($event, $trueLocation);
}

function event_link_full($event, $trueLocation) {
    return 'https://'.$_SERVER['HTTP_HOST'].'/tickets/'.$event->ID.event_slug($event, $trueLocation);
}

function event_slug($event, $trueLocation) {
    return '?' . http_build_query(
        array_filter(
            array(
                'p' => $event->performers ? $event->performers[0]->PerformerSlug : '',
                'v' => slug_venue($event->Venue),
                'c' => slug_location($trueLocation)
            )
        )
    );
}

function slug_venue($name)
{
	$fixCase = str_replace('(Formerly', '(formerly', $name);// if some smartass put (Formerly, then we convert that to lover case just that
    $vArray = explode('(formerly', $fixCase);
    $string = trim($vArray[0]); //return not dignity
    //Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
    $string = strtolower($string);
    //Strip any unwanted characters
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "-", $string);
    return $string;
}

function twitter_clean($status,$targetBlank=true,$linkMaxLen=250){

  // The target
  $target=$targetBlank ?  " rel=\"nofollow\" target=\"_blank\" " : "";

    // convert link to url
    $status = preg_replace("/((http:\/\/|https:\/\/)[^ )
]+)/e", "'<a href=\"$1\" title=\"$1\"  $target >'. ((strlen('$1')>=$linkMaxLen ? substr('$1',0,$linkMaxLen).'...':'$1')).'</a>'", $status);

    // convert @ to follow
    $status = preg_replace("/(@([_a-z0-9\-]+))/i","<a href=\"http://twitter.com/$2\" title=\"Follow $2\" $target >$1</a>",$status);

    // convert # to search
    $status = preg_replace("/(#([_a-z0-9\-]+))/i","<a href=\"http://search.twitter.com/search?q=%23$2\" title=\"Search $1\" $target >$1</a>",$status);

    // return the status
    return $status;
}

function deslug_location($slug)
{
    $tmpArray = explode('-', $slug);
    $loc = array();
    $loc['state'] = strtoupper(array_pop($tmpArray));
    $loc['city']  = ucwords(implode(' ', $tmpArray));
    $loc['slug']  = $slug;
    return $loc;
}

function slug_location($array)
{
    return strtolower(seoUrl($array['city']." ".$array['state']));
}

function isAjax() {
    return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
    ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
}

function get_season($start, $end)
{
    $startMonth = date('n', strtotime($start));
    $endMonth   = date('n', strtotime($end));
    $startYear = date('Y', strtotime($start));
    $endYear    = date('Y', strtotime($end));
    $seasons = array(1 => 'winter', 2 => 'winter', 12 => 'winter',
                    3 => 'spring', 4 => 'spring', 5 => 'spring',
                    6 => 'summer', 7 => 'summer', 8 => 'summer',
                    9 => 'fall', 10 => 'fall', 11 => 'fall'
                    );

    $seasonStart = ucwords($seasons[$startMonth]);
    $seasonEnd = ucwords($seasons[$endMonth]);
    if($startYear < $endYear): //over year
        $overYear = true;
    else:
        $overYear = false;
    endif;

    if($overYear):
        $tour = $seasonStart." of ".date('Y', strtotime($start))." to $seasonEnd in ". date('Y', strtotime($end));
    else:
        if($seasonStart == $seasonEnd): //avoids Spring to Spring 2014
            $tour = $seasonEnd. " ".date('Y', strtotime($start));
        else:
            $tour = "  $seasonStart to $seasonEnd ".date('Y', strtotime($start));
        endif;
    endif;
    return $tour;

}

function performerImage($performer, $type = "profile")
{
    if(count($performer) == 0):
        return "/public/img/artist_{$type}.jpg";
    endif;
    // var_dump($performer);
    $performer = (array)$performer;
    if(!isset($performer[0])):
        $tmp = $performer;
        $performer = array();
        $performer[0] = $tmp;
    endif;

    if(is_array($performer[0])):
        $img = ($performer[0][$type]) ? "https://s3.amazonaws.com/ssglobalcdn/".$performer[0][$type] : $performer[0]['img'];
    else:
        $img = ($performer[0]->$type) ? "https://s3.amazonaws.com/ssglobalcdn/".$performer[0]->$type : $performer[0]->img;
    endif;
    //last fall back
    return ($img) ? $img : "/public/img/artist_{$type}.jpg";
}

function performerOgImage($performer, $type = "profile")
{
    if(count($performer) == 0):
        return "/public/img/artist_{$type}.jpg";
    endif;
    $base_url = strtr(base_url(), array('http:'=>'https:'));
    // var_dump($performer);
    $performer = (array)$performer;
    if(!isset($performer[0])):
        $tmp = $performer;
        $performer = array();
        $performer[0] = $tmp;
    endif;

    if(is_array($performer[0])):
        $img = ($performer[0][$type]) ? "https://s3.amazonaws.com/ssglobalcdn/".$performer[0][$type] : $base_url."public/img/assets/concert-fix-logo.png";
    else:
        $img = ($performer[0]->$type) ? "https://s3.amazonaws.com/ssglobalcdn/".$performer[0]->$type : $base_url."public/img/assets/concert-fix-logo.png";
    endif;
    //last fall back

    return ($img) ? $img : $base_url."public/img/assets/concert-fix-logo.png";
}

function random_pop($array = array())
{
    shuffle($array);
    return array_pop($array);
}

function random_pop2($array = array()){
    $key = array_rand($array);
    return $array[$key];
}

function twHash($string)
{
    $hash = str_replace(' ', '', ucwords(seoUrl(clean_venue($string), ' ')));
    $link = "<a target='_blank' href='https://twitter.com/search?q=%23{$hash}&src=hash'>#{$hash}</a>";
    return $link;
}

function twFollow($string)
{
    $link = "<a target='_blank' href='https://twitter.com/{$string}'>@{$string}</a>";
    return $link;
}

function toggle_state($st)
{
    $states = array("QC" => 'Quebec',"AB" => 'Alberta',"ON" => 'Ontario',"MB" => 'Manitoba',"NS" => 'Nova Scotia',"SK" => 'Saskatchewan',"NB" => 'New Brunswick',"BC" => 'British Columbia', "NL" => 'NewFoundLand', 'PE' => 'Prince Edward Island',"AL" => 'Alabama',"AK" => 'Alaska',"AZ" => 'Arizona',"AR" => 'Arkansas',"CA" => 'California',"CO" => 'Colorado',"CT" => 'Connecticut',"DE" => 'Delaware',"DC" => 'District Of Columbia',"FL" => 'Florida',"GA" => 'Georgia',"HI" => 'Hawaii',"ID" => 'Idaho',"IL" => 'Illinois',"IN" => 'Indiana',"IA" => 'Iowa',"KS" => 'Kansas',"KY" => 'Kentucky',"LA" => 'Louisiana',"ME" => 'Maine',"MD" => 'Maryland',"MA" => 'Massachusetts',"MI" => 'Michigan',"MN" => 'Minnesota',"MS" => 'Mississippi',"MO" => 'Missouri',"MT" => 'Montana',"NE" => 'Nebraska',"NV" => 'Nevada',"NH" => 'New Hampshire',"NJ" => 'New Jersey',"NM" => 'New Mexico',"NY" => 'New York',"NC" => 'North Carolina',"ND" => 'North Dakota',"OH" => 'Ohio',"OK" => 'Oklahoma',"OR" => 'Oregon',"PA" => 'Pennsylvania',"RI" => 'Rhode Island',"SC" => 'South Carolina',"SD" => 'South Dakota',"TN" => 'Tennessee',"TX" => 'Texas',"UT" => 'Utah',"VA" => 'Virginia',"WA" => 'Washington',"WV" => 'West Virginia',"WI" => 'Wisconsin',"WY" => 'Wyoming',"VT" => 'Vermont');
    if(strlen($st) === 2):
        return $states[strtoupper($st)];
    else:
        // var_dump($st);
        $rev = array_flip($states);
        return $rev[ucwords($st)];
    endif;
}

function link_city($city, $st)
{
    $st = (strlen($st) === 2) ? $st : strtolower(toggle_state($st));
    return "<a href='/concerts/".seoUrl($city." ".$st)."'>".ucwords($city)."</a>";
}

function shortenText($string, $limit, $break=" ", $pad="...")
{
  // return with no change if string is shorter than $limit
  if(strlen($string) <= $limit) return $string;

  $string = substr($string, 0, $limit);
  if(false !== ($breakpoint = strrpos($string, $break))) {
    $string = substr($string, 0, $breakpoint);
  }

  return restoreTags($string) . $pad;
}

/**
 * error nums:
 0 - success
 1 - warning
 2- critical
 */
function throwMessage($error = 1, $message = "Error Occurred", $json = true, $flashdata = false)
{
    $errs = array("alert-success", "alert-warn", "alert-error");
    $output = array(
                    'error' => $error,
                    'type'  => $errs[$error],
                    'message'   => $message
                    );
    if($flashdata):
        $ci =& get_instance();

        $ci->session->set_flashdata($output);
    endif;
    return ($json) ? json_encode($output) : $output;
}

function restoreTags($input)
  {
    $opened = array();

    // loop through opened and closed tags in order
    if(preg_match_all("/<(\/?[a-z]+)>?/i", $input, $matches)) {
      foreach($matches[1] as $tag) {
        if(preg_match("/^[a-z]+$/i", $tag, $regs)) {
          // a tag has been opened
          if(strtolower($regs[0]) != 'br') $opened[] = $regs[0];
        } elseif(preg_match("/^\/([a-z]+)$/i", $tag, $regs)) {
          // a tag has been closed
          unset($opened[array_pop(array_keys($opened, $regs[1]))]);
        }
      }
    }

    // close tags that are still open
    if($opened) {
      $tagstoclose = array_reverse($opened);
      foreach($tagstoclose as $tag) $input .= "</$tag>";
    }

    return $input;
  }
  class Spinner extends stdClass
  {
    public function __call($method, $args)
    {
      if (isset($this->$method)) {
        $func = $this->$method;
        return call_user_func_array($func, $args);
      }
      return "FAIL";
    }
    public function populate($array) {
      foreach ($array as $key => $value) {
        // $this->$key = $value;
        $this->$key = function($capitalize = false, $suffix = '') use ($value) {
          // Doesn't enforce array
          if($capitalize) { return ucfirst(random_pop($value)); }
          return random_pop($value) . $suffix;
        };
      }
    }
    public function loop($array, $format) {
      $loop_text = array();
      foreach ($array as $key => $value) {
        array_push($loop_text, sprintf($format, $value->City, date('F j', strtotime($value->Date))));
      }
      if(count($array) > 1) {
        $last = array_pop($loop_text);
        return join(", ", $loop_text) . " and " . $last;
      } else {
        return join(" ", $loop_text);
      }
    }
    public function loopCities($cities, $prefix = '') {
      // $loop_text = array(", and", $this->again(), "on ");
      $dates = array();
      foreach($cities as $key => $city) {
        array_push($dates, date('F j h:i A', strtotime($city->Date)));
      }
      if(count($cities) > 1) {
        $last = array_pop($dates);
        return $prefix . join(", ", $dates) . " and " . $last . ".";
      } else {
        return $prefix . join(", ", $dates) . ".";
      }
    }
    public function loopPerformers($performers) {
      $enum_performers = array();
      foreach($performers as $key => $performer) {
        array_push($enum_performers, $performer->PerformerName);
      }
      if(count($performers) > 1) {
        $last = array_pop($enum_performers);
        return join(", ", $enum_performers) . " and " . $last;
      } else {
        return join(", ", $enum_performers);
      }
    }

    public function performerAnchor($performer) {
      return "<a href='{$performer->PerformerSlug}'>{$performer->Name}</a>";
    }

    public function otherGenrePerformers($relatedPerformers) {
      $totalRelated = count($relatedPerformers);
      $output = "";

      for ($idx=0; $idx < $totalRelated; $idx++) {
        $performer = $relatedPerformers[$idx];
        if($idx == $totalRelated - 1) {
          $output .= "and " . $this->performerAnchor($performer);
        } else {
          $output .= $this->performerAnchor($performer) . ", ";
        }
      }
      return $output;
    }

    public function twitterLink() {
      return "<a target='_blank' href='https://twitter.com/concertfix'>@ConcertFix</a>";
    }
    public function facebookLink() {
      return "<a target='_blank' href='https://www.facebook.com/concertfix'>Facebook page</a>";
    }
    public function googlePlusLink() {
      return "<a target='_blank' href='https://plus.google.com/+Concertfix'>Google+ page</a>";
    }
    public function announcementsLink() {
      return "<a href='/tour-announcements'>Tour announcements page</a>";
    }
    public function concertTrackerLink() {
      return "<a href='/user/login'>Concert Tracker</a>";
    }
    /*
     * The $params['rand_key'] ascribes itself depending on the page (date/venue/genre)
     *
     *
     *
     */
    public function randomSentence($params) {
      $alt = self::prepare();
      return random_pop(array(
        join(" ", array(
          $alt->follow(true), "us on Twitter", $alt->twitterLink(), "for announcements", $alt->regarding(), $params['rand_key'], $alt->events(), "in", $params['city'] . "."
        )),
        join(" ", array(
          "For announcements", $alt->regarding(), $params['rand_key'], $alt->events(), "in", $params['city'] . ",", $alt->follow(), "us on Twitter", $alt->twitterLink() . "."
        )),
        join(" ", array(
          "For", $alt->up_to_date(), "announcements of", $params['rand_key'], $alt->events(), "in", $params['city'] . ",", $alt->visit(), "our", $alt->facebookLink() . "."
        )),
        join(" ", array(
          $alt->visit(true), "our", $alt->facebookLink(), "for", $alt->up_to_date(), "announcements for", $params['rand_key'], $alt->events(), "in", $params['city'] . "."
        )),
        join(" ", array(
          "Get the latest tour announcements for", $params['rand_key'], $alt->events(), "in", $params['city'], "by visiting our", $alt->announcementsLink() . "."
        )),
        join(" ", array(
          $alt->visit(true), "our", $alt->announcementsLink(), "to get the latest tour announcements for", $params['rand_key'], $alt->events(), "in", $params['city'] . "."
        )),
        join(" ", array(
          "For", $alt->up_to_date(), "announcements", $alt->regarding(), $params['rand_key'], $alt->events(), "in", $params['city'] . ",", $alt->visit(), "our", $alt->googlePlusLink() . "."
        )),
        join(" ", array(
          $alt->visit(true), "our", $alt->googlePlusLink(), "for", $alt->up_to_date(), "announcements", $alt->regarding(), $params['rand_key'], $alt->events(), "in", $params['city'] . "."
        )),
        join(" ", array(
          $alt->sign_up_for(true), "our", $alt->concertTrackerLink(), "to follow", $params['rand_key'], $alt->events(), "in", $params['city'] . "."
        )),
        join(" ", array(
          $alt->track(true), $params['rand_key'], $alt->events(), "in", $params['city'], "by", $alt->signing_up_for(), "our", $alt->concertTrackerLink() . "."
        )),
      ));
    }

    /*
     * The $params['rand_key'] ascribes itself depending on the page (date/venue/genre)
     *
     *
     *
     */
    public function randomSentence2($params) {
      $alt = self::prepare();
      return random_pop(array(
        join(" ", array(
          $alt->follow(true), "us on Twitter", $alt->twitterLink(), "for announcements", $alt->regarding(), $params['performer'] . "."
        )),
        join(" ", array(
          "For announcements", $alt->regarding(), $params['performer'], $alt->events() . ",", $alt->follow(), "us on Twitter", $alt->twitterLink() . "."
        )),
        join(" ", array(
          "For", $alt->up_to_date(), "announcements of", $params['performer'], $alt->events() . ",", $alt->visit(), "our", $alt->facebookLink() . "."
        )),
        join(" ", array(
          $alt->visit(true), "our", $alt->facebookLink(), "for", $alt->up_to_date(), "announcements for", $params['performer'] . "."
        )),
        join(" ", array(
          "Get the latest tour announcements for", $params['performer'], $alt->events(), "by visiting our", $alt->announcementsLink() . "."
        )),
        join(" ", array(
          $alt->visit(true), "our", $alt->announcementsLink(), "to get the latest tour announcements for", $params['performer']
        )),
        join(" ", array(
          "For", $alt->up_to_date(), "announcements", $alt->regarding(), $params['performer'], $alt->events() . ",", $alt->visit(), "our", $alt->announcementsLink() . "."
        )),
        join(" ", array(
          $alt->visit(true), "our", $alt->announcementsLink(), "for", $alt->up_to_date(), "announcements", $alt->regarding(), $params['performer'] . "."
        ))
      ));
    }
    public static function prepare() {
      $alt = new Spinner;
      $alt->populate(array(
        "will_be_hosting"   => array("will be hosting", "currently has", "has", "will be having"),
        "list"              => array("list", "bunch", "handful"),
        "that_you_can"      => array("that you can", "available to"),
        "can"               => array("can", "are able to", "simply"),
        "print_anywhere"    => array("download and print them from anywhere", "get them sent to your email", "have them sent to your email", "print them from your email"),
        "view"              => array("view", "see", "check out", "take a look at"),
        "schedule"          => array("schedule", "calendar", "lineup", "itinerary"),
        "concert"           => array("concert", "event", "show"),
        "purchase"          => array("purchase", "buy", "get your"),
        "you_can"           => array("You can", "Make sure to"),
        "event"             => array("event", "show"),
        "check_back_with_us" => array("check back with us", "visit this page often", "stay tuned"),
        "any_updates_to"    => array("any updates to", "the latest list of", "the most up-to-date list of", "an up-to-date list of"),
        "details"           => array("details", "specifics"),
        "events"            => array("events", "shows", "concerts"),
        "coming_to"         => array("coming to", "planned in", "that will be held in"),
        "visiting"          => array("coming to", "performing at", "visiting", "stopping by"),
        "memorable"         => array("memorable", "big", "huge"),
        "during"            => array("during", "in", "for"),
        "concert_fix"       => array("ConcertFix", "our site", "Concert Fix", "Concertfix"),
        "through"           => array("through", "by using", "in"),
        "always_offers"     => array("always offers", "makes sure to offer you", "offers all customers", "offers"),
        "money_back"        => array("125% money-back guarantee", "125% guarantee", "125 percent money-back guarantee", "125 percent guarantee"),
        "is_always"         => array("is always", "will be constantly", "will constantly be"),
        "changing"          => array("changing", "updating", "revised"),
        "regarding"         => array("regarding", "pertaining to", "for"),
        "follow"            => array("follow", "visit", "see"),
        "visit"             => array("visit", "check out", "take a look at"),
        "up_to_date"        => array("up-to-date", "new", "recent"),
        "track"             => array("Track", "Follow"),
        "purchases"         => array("purchases", "sales", "transactions"),
        "sign_up_for"       => array("Sign up for", "Join", "Subscribe for"),
        "signing_up_for"    => array("signing up for", "joining", "subscribing for"),
        "concert"           => array("concert", "show", "performance", "event"),
        "concerts"          => array("concerts", "shows", "performances", "events"),
        "perform"           => array("perform", "play", "have a concert"),
        "play"              => array("perform", "play"),
        "performed"         => array("performed", "played", "heard"),
        "performers"        => array("performers", "artists"),
        "performing"        => array("performing", "playing", "having a concert", "having a show", "having a performance", "having an event"),
        "cities"            => array("cities", "areas", "locations"),
        "city"              => array("city", "area", "location"),
        "near"              => array("near", "around", "close to"),
        "Yes"               => array("Yes", "Absolutely", "Yep"),
        "several"           => array("several", "a few", "quite a few"),
        "attend"            => array("attend", "go to", "make it to", "catch", "be at"),
        "are_not"           => array("are not able to", "won't be able to", "can't"),
        "catch"             => array("catch", "watch", "make it to see", "witness", "experience"),
        "scheduled"         => array("scheduled", "booked", "planned", "expected"),
        "unfortunately"     => array("unfortunately", "sadly", "regrettably"),
        "has_no_other"      => array("has no other", "doesn't have any other"),
        "right_now"         => array("right now", "at the moment", "at thit time"),
        "check_back"        => array("Check back", "Visit this page", "Return"),
        "soon"              => array("soon", "in the near future", "eventually", "finally"),
        "see"               => array("see", "find out", "check", "view"),
        "check_out"         => array("check out", "take a look at", "view", "have a look at"),
        "adds"              => array("adds", "has added", "scheduled any other"),
        "anywhere_else"     => array("anywhere else", "any other location", "any other place"),
        "What"              => array("What", "At exactly what", "At what exact", "At which", "At what"),
        "time"              => array("time", "moment"),
        "start"             => array("start", "begin", "kick off"),
        "is_infamous_for"   => array("is infamous for", "has been known for", "does have a reputation for"),
        "starting"          => array("starting", "beginning", "kicking off"),
        "the_first_being"   => array("starting", "beginning", "kicking off", "the first being"),
        "start_time"        => array("start time", "event time", "exact start time"),
        "currently"         => array("currently", "presently"),
        "much"              => array("much", "20 mins - 30 mins", "quite a few minutes", "up to a half hour"),
        "we_would_suggest"  => array("we would suggest", "we do suggest", "it might be smart", "it might be wise"),
        "getting_there"     => array("getting there", "arriving", "making it there"),
        "that_you_cant_miss" => array("that you better not miss", "that you won't want to miss", "that will bring fans from all over", "that you can't miss", "that's expected to sell out"),
        "around"            => array("around", "at approximately", "closer to"),
        "make_sure"         => array("make sure", "ensure", "assure", "confirm"),
        "on_sale"           => array("on sale now", "currently on sale", "now on sale", "available", "now available"),
        "sold_out"          => array("sold out", "unavailable", "not available", "completely sold out"),
        "a_minute"          => array("a minute", "a second", "any", "a moment"),
        "can_i"             => array("can I catch", "can I watch", "can I make it to see", "can I witness", "can I experience", "will I be able to catch", "will I be able to watch", "will I be able to make it to see", "will I be able to witness", "will I be able to experience"),
        "venue"             => array("venue", "place", "location"),
        "entire"            => array("entire", "whole"),
        "very_excited"      => array("very excited", "stoked", "pumped up", "thrilled"),
        "Who"               => array("Who will be", "Who else is", "Which other performers are", "Which other artists are"),
        "there_are_no"      => array("there are no other", "there aren't any other"),
        "are_there_any"     => array("are there any", "is there any", "will there be", "can I get", "can I buy", "can I purchase"),
        "are_any"           => array("are", "is any"),
        "vip"               => array("VIP", "backstage", "meet and greet", "front row"),
        "tickets"           => array("tickets", "passes"),
        "available"         => array("available", "in stock", "on hand", "up for grabs"),
        "our"               => array("our", "the"),
        "selection"         => array("selection", "choices", "options"),
        "what_options"      => array("what are the options for getting", "what are the ways for getting", "what are the methods for getting", "how are the"),
        "scroll_down"       => array("scroll down to", "check", "look at", "review"),
        "bottom"            => array("bottom", "end"),
        "at_the_box_office" => array("at the box office", "through the venue", "at the venue", "through the box office"),
        "some_other"        => array("some other", "more", "additional", "other"),
        "get"               => array("get", "see the show from", "sit"),
        "top"               => array("top", "best", "most popular", "most followed", "popular"),
        "to_date"           => array("to date", "currently", "at the moment", "right now"),
        "leading_seller"    => array("leading seller", "top seller", "major seller", "trusted source", "top source"),
        "front_row"         => array("front row", "first row"),
        "look_for"          => array("look for", "find", "check out", "make sure to get"),
        "quickest"          => array("quickest", "easiest", "fastest"),
        "other"             => array("other", "many", "plenty of"),
        "often"             => array("often", "many times", "usually", "generally"),
        "constantly"        => array("constantly", "always", "continuously", "definitely", "usually", "absolutely"),
        "dump"              => array("dump", "sell", "exchange", "unload", "get rid of"),
        "our_goal_is"       => array("our goal is", "it's our goal", "we strive", "it's our job", "we make it our priority", "it's our priority"),
        "possess"           => array("possess", "own", "carry"),
        "great"             => array("great", "good", "healthy"),
        "great_low"         => array("great", "good", "low", "cheap", "honest"),
        "purchased"         => array("purchased", "bought", "acquired"),
        "customers"         => array("customers", "you", "fans"),
        "computer"          => array("computer", "email", "home"),
        "delivered"         => array("delivered", "shipped", "brought to you"),
        "in_the"            => array("in the", "through the", "by"),
        "picked_up"         => array("picked up", "collected", "secured"),
        "again"             => array("again", "also"),
        "sent"              => array("brought", "delivered", "sent"),
        "when"              => array("when", "what"),
        "day"               => array("day", "date"),
        "be"                => array("be", "include"),
        "below"             => array("below", "that follows", "here"),
        "then"              => array("then", "from there"),
        "on_stage"          => array("on stage", "live"),
        "once_again"        => array("once again", "yet again", "again"),
        "be_appearing"      => array("be appearing", "be showing up", "be performing", "make appearances", "show up", "perform"),
        "appearances"       => array("appearances", "dates", "performances"),
        "fans"              => array("fans", "I", "we", "the crowd"),
        "fans_you"          => array("fans", "the crowd", "you"),
        "fans_to_enjoy"     => array("fans to enjoy", "fans to attend", "fans"),
        "expect"            => array("expect", "look forward", "hope"),
        "probably"          => array("probably", "likely", "most likely"),
        "additional"        => array("additional", "other", "more", "some other", "extra"),
        "additionally"      => array("additionally", "plus", "secondly"),
        "could"             => array("could", "might", "may", "possibly might", "possibly could"),
        "hear"              => array("hear", "see", "catch"),
        "very"              => array("very", "quite"),
        "popular"           => array("popular", "beloved by fans", "famous", "crowd-pleasing", "saught-after", "in demand"),
        "rsvp"              => array("RSVP", "say you're going", "post it"),
        "through"           => array("through", "on", "in"),
        "great_selection"   => array("great selection of", "selection of great", "huge selection of", "big inventory of", "inventory of"),
        "details"           => array("details", "specifics"),
        "make"              => array("make", "attend", "go to", "get to"),
        "to_get_notified"   => array("to get notified", "for notifications", "to get alerts", "for alerts"),
        "cant"              => array("can't", "are unable to", "won't be able to"),
        "when"              => array("when", "as soon as", "right when", "immediately when"),
        "announce"          => array("announce", "reveal", "add", "book", "unveil", "plan", "release"),
        "updates"           => array("the latest updates", "up-to-date information", "the most recent information", "any updates", "more updates"),
        "come_back_often"   => array("come back often", "visit us often", "keep coming back", "check back with us", "keep checking back with us"),
        "select_show"       => array("and select a show of your choice", "to find a show for you", "to select a show that works for you", "to find a show that interests you", "and locate an event to attend", "to find a specific show"),
        "as_of_right_now"   => array("As of right now", "So far", "To date", "Currently"),
        "upcoming"          => array("upcoming", "big", "notable"),
        "north_america"     => array("North America", "cities in North America", "the U.S. or Canada", "America", "the United States", "the States", "cities in the US"),
        "notified"          => array("notified", "told", "informed", "alerts"),
        "in_your_city"      => array("in your city", "in your area", "in your town", "near you", "in any city", "in a city near you"),
        "to_see_if"         => array("to see if", "to view if"),
        "any_new"           => array("any new", "any additional", "additional", "new"),
        "announced"         => array("announced", "revealed", "added", "booked", "unveiled", "planned", "released")
      ));
      return $alt;
    }
  }

 ?>
