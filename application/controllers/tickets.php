<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tickets extends TN_SiteController {

	public function __construct()
	{
		parent::__construct();
/*		if ($_SERVER['REMOTE_ADDR'] != '109.87.99.55') {
  			// restrict
  			redirect('/');
		}	*/
		$this->load->model('global_m');

		$this->soapy = new TN_soap();
		// $this->soapy->initialize();

	}
	public function index()
	{
		redirect('/');
	}

	public function _remap($eId = false, $params = array())
	{
		// var_dump($slug);
		// $eventId = $params[0];
		$_SESSION['uri_string'] = $this->uri->uri_string();

		if(isset($eId)):
			$this->_tickets($eId);
		else:
		//nothing was passed to url after /buy/
	//	redirect('/');
		endif;
	}

	private function _tickets($id)
	{
		$this->load->library('user_agent');

		$paramsTix = array(
 						'eventId' => (int)$id,
 						// 'numberOfRecords' => 100
 						);
		$params = array(
 						'eventID' => (int)$id,
 						// 'numberOfRecords' => 100
 						);
	/*	$appendMeta = "<script type='text/javascript' src='/public/js/tickets.js'></script>
		<script type='text/javascript' src='http://seatics.tickettransaction.com/swfobject_tn.js'></script>
		<script type='text/javascript' src='http://seatics.tickettransaction.com/maincontrol_tnservice_tn.js'></script>
		<link rel='stylesheet' type='text/css' href='/public/css/ticketmap.css'>";*/


		$event = $this->soapy->run_soap('GetEvents', $params);
		if (!isset($event[0])){
			show_404();
			/*$title = "Tickets Currently Unavailable";
			$description = "Tickets Currently Unavailable";
			$pageData['noTickets'] = true;
			//header("HTTP/1.1 404 Not Found");
			set_status_header(404);*/
		}
		//$tickets = $this->soapy->run_soap('GetEventTickets', $paramsTix);

 		$pageData = array('event' => $event, 'noTickets' => false, 'ios' => false);

 		// Load performers for event:
		$this->load->model('global_m');
		$ticketsFor = $this->global_m->get_event_performers_simple($event[0]->ID);
		$appendMeta = "<link rel='stylesheet' type='text/css' href='/public/css/ticketmap.css'>";
		$ticketsFor = is_null($ticketsFor) ? $event[0]->Name : $ticketsFor;
		$title = "Tickets for {$ticketsFor} at {$event[0]->Venue} in {$event[0]->City} on {$event[0]->DisplayDate} ";
		$description = "Tickets for {$ticketsFor} at {$event[0]->Venue} in {$event[0]->City} on {$event[0]->DisplayDate}. All tickets come with a 100% money back gaurantee.";
		$nrobot = "noindex, follow";


 		$ogMeta = array(
			'title' 		=> $title,
			'description' 	=> $description,
			'copyright'		=> "Copyright ".Date('Y').", Concert Fix",
			'image'			=> base_url()."public/img/assets/concert-fix-logo.png",
    );
		$metaTags = array(
			'description'	=> $description,
			'robots' => $nrobot,
			'googlebot' => $nrobot,
			'msnbot' => $nrobot
		);
		/*$footerExtra = '
			<script type="text/javascript">

				var $ = jQuery.noConflict();


				$(window).load(function(){ ssc.criteriaChanged("showPlus1=" + (this.checked?1:0)); });

			</script>';*/

		$footerExtra = '';
		//if($this->agent->platform() == 'iOS' || $this->agent->platform() == 'Mac OS X'){
			$footerExtra = "<script>
                jQuery(document).ready(function(){
					$('div#sea-mobile-header > div#event-info-cnt > div.event-info-col > h1.event-info-name').css({'text-overflow':'initial'})
                });
            </script>";
		//}

		$this->template->prepend_metatags($metaTags)
					   ->prepend_metatags($ogMeta, true);

 		$this->template
 				->title($title)
				->set_layout('ticketlay')
				//->set_layout('default');
				->set_partial('header', 'frontend/global/header')
				->set_partial('sidebar', 'frontend/tickets/tickets_sidebar')
				->set_partial('footer', 'frontend/global/footer-tix');
				// ->set_partial('footer', 'frontend/global/footer', array('extra' => $jqbt))
				// ->append_metadata($appendMeta)
				//->build('frontend/tickets/tickets_index', $pageData);

		//Esto hay que comentarlo en production, solo es para probar
		//$this->template->build('frontend/tickets/tickets_m_index', $pageData);

		//Esto hay que descomentarlo en production, solo es para probar
		if($this->agent->is_mobile()):
			if($this->agent->platform() == 'iOS' || $this->agent->platform() == 'Mac OS X') {
				$pageData['ios'] = true;
			}
			$this->template->build('frontend/tickets/tickets_m_index', $pageData);
		else:
			$this->template->build('frontend/tickets/tickets_index', $pageData);
		endif;

	}
}

/* End of file tickets.php */
/* Location: ./application/controllers/tickets.php */
