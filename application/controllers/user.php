<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MY_SiteController {


	public function __construct()
	{
		parent::__construct();
        $this->load->model('concerts_m');

	}
	public function index()
	{
		if($this->cfUser['loggedin']):
			redirect('/user/dashboard');
		else:
			redirect('/user/login');
		endif;
	}

	public function docities()
	{
		return false;
		$this->load->model('rules_m');
		$this->rules_m->docities();
	}

	public function rules()
	{
		if(!$this->cfUser['loggedin']) redirect('/user/login');
		//pdump($this->cfUser['facebook']);
		if($this->_needs_email()) redirect('/user/verify/twitter');

		$pageData = array('genres' => $this->global_m->get_genres(), 'userGenres' => $this->rules_m->fetch_rules('genereids',$this->cfUser["cf"]['id']));
		$title = "Rules for Tracking Favorite Artists with Concertfix.com";
		$description = "Create Rules | ConcertFix";
		$keywords = "ConcertFix, members, tracking, rules";
		$jqbt = "<script src='/public/js/rules.js'></script>";
		//array of open graph
 		$ogMeta = array(
 		                'title'			=> $title,
		                'description' 	=> $description,
		                'keywords'		=> $keywords,
		                'copyright'		=> "Copyright 2013, Concert Fix",
		                'image' 		=> base_url()."public/img/assets/concert-fix-logo.png",
		                // 'url'			=> "this is where the page URL goes",
		                );

 		//array of meta
        $yrobot = "noindex, nofollow";
        $metaTags = array(
            'description' => $description,
            'keywords'    => $keywords,
            'robots'      => $yrobot,
            'googlebot'   => $yrobot,
            'msnbot'      => $yrobot,
        );

		//set title
		$this->template->title($title)
						->prepend_metatags($metaTags) //set meta tags
						->prepend_metatags($ogMeta, true); // $og = true

		$this->template
				->set_layout('user')
				->set_partial('header', 'frontend/global/header')
				->set_breadcrumb('Home','/')
				->set_breadcrumb('Dashboard', '/user/dashboard')
				->set_breadcrumb('Rules')
				->set_partial('sidebar_twitter', 'frontend/user/user_sidebar')
				->set_partial('footer', 'frontend/global/footer', array('extra' => $jqbt))
				// ->append_metadata($jqbt)
				->build('frontend/user/rules_main', $pageData);

	}

	public function settings()
	{
		if(!$this->cfUser['loggedin']) redirect('/user/login'); // redirect('/user/login');
		if($this->_needs_email()) redirect('/user/verify/twitter');
        $userId  =	$this->cfUser['cf']['id'];

		if($_POST):

			if($_POST['op'] == 'remove'):
				$network = $this->input->post('network');
				if($network == 'cf'):

					//DO THE DANGER
					$this->user_m->remove_user($userId);
					$this->_drop_userdata();
					throwMessage(0, "<p>You have been removed! Bye!</p>",false, true);

					redirect('/user/login');
				endif;
				if($this->user_m->unlink($network, $userId)):
					throwMessage(0, "<p> <strong>{$network}</strong> was removed!</p>",false, true);
					$data[$network] = false;
					$this->session->set_userdata($data);
					redirect('/user/settings');
				else:
					throwMessage(1, "<p>Some error occurred removing <strong>{$network}</strong>!</p>",false, true);
					redirect('/user/settings');
				endif;
			endif;
			// $pass0 = $this->input->post('old_password');
			$pass1 = $this->input->post('new_password1');
			$pass2 = $this->input->post('new_password2');
			if($pass1 == $pass2):
				if(strlen($pass1) < 4):
					throwMessage(2, "Passwords should be longer than 3 charaters!.",false, true);
					redirect('user/settings');
				endif;
				$this->user_m->update_password($pass1);
				$email = $this->cfUser['cf']['primary_email'];
				$data['cf'] = $this->user_m->get_user_by_email($email, false, true);
				$this->session->set_userdata($data);
				throwMessage(0, "Password Changed!.",false, true);
				redirect('user/settings');
			else:
				throwMessage(2, "Passwords Did NOT Match! Try Again!.",false, true);
				redirect('user/settings');
			endif;
		endif;

		$jqbt = "<script src='/public/js/settings.js'></script>";
		$jqbt .= "<script src='/public/js/rules.js'></script>";
		$css = "<link rel='stylesheet' type='text/css' href='/public/css/black-tie/jqui.min.css'>";

		if($this->rules_m->exists_rule_concert($userId) == 0)
		{
			$this->rules_m->add_rule_concert_userid($userId);
		}

		$settings = $this->user_m->get_user_settings();
        $categories = $this->concerts_m->get_categories_user_settings();
        $categories_temp = array();
        foreach($categories as $category)
        {
            $temp = array('id' => $category->id, 'Filter' => $category->Filter);
            if($this->rules_m->exists_genre_by_userid($userId, $category->id))
            {
                $temp['checked'] = true;
            }
            else{
                $temp['checked'] = false;
            }
            $categories_temp[] = $temp;
        }

        $this->load->model('rules_m');
        $rule_concert = $this->rules_m->get_rule_concert_by_user($userId);
		$pageData = array('settings' => $settings, 'filters'=> $categories_temp, 'rule_concert'=> $rule_concert);
		//title for title of the page
		$title = "User Settings | ConcertFix";
		$description = "User Settings | ConcertFix";
		$keywords = "ConcertFix, members, login";

		//array of open graph
 		$ogMeta = array(
 		                'title'			=> $title,
		                'description' 	=> $description,
		                'keywords'		=> $keywords,
		                'copyright'		=> "Copyright 2013, Concert Fix",
		                'image' 		=> base_url()."public/img/assets/concert-fix-logo.png",
		                // 'url'			=> "this is where the page URL goes",
		                );

 		//array of meta
        $yrobot = "noindex, nofollow";
        $metaTags = array(
            'description' => $description,
            'keywords'    => $keywords,
            'robots'      => $yrobot,
            'googlebot'   => $yrobot,
            'msnbot'      => $yrobot,
        );

		//set title
		$this->template->title($title)
						->prepend_metatags($metaTags) //set meta tags
						->prepend_metatags($ogMeta, true); // $og = true

		$this->template
				->set_layout('user')
				->set_partial('header', 'frontend/global/header')
				->set_breadcrumb('Home','/')
				->set_breadcrumb('Account Settings')
				->set_partial('sidebar_user', 'frontend/user/user_sidebar')
				->set_partial('footer', 'frontend/global/footer', array('extra' => $jqbt))
				//->set_partial('footer', 'frontend/global/footer', array('extra2' => $jqbt2))
				//->prepend_metadata($css)
				// ->append_metadata($jqbt)
				->build('frontend/user/settings', $pageData);
	}

	public function passreset()
	{

		$resetLink = $this->input->get('rl');
		$email = $this->input->post('email');

		$pageData = false;

		if($resetLink):
			if($resetUser = $this->user_m->get_reset_user($resetLink)):
				$pageData = array('user' => $resetUser);
				$pass1 = $this->input->post('new_password1');
				$pass2 = $this->input->post('new_password2');
				if($pass1):
					if($pass1 == $pass2 && strlen($pass1) > 3):
						if($this->user_m->update_password($pass1, $resetUser['id'])):
							$this->user_m->remove_reset($resetUser['id']);
							throwMessage(0, "Password was reset. Login please!", false, true);
							redirect('/user/login');
						else:
							throwMessage(2, "Something went wrong, try again?!", false, true);
							redirect('/user/passreset?rl='.$resetLink);
						endif;
					else:
						throwMessage(2, "Passwords didn't match or too short (3 or more charaters)!", false, true);
						redirect('/user/passreset?rl='.$resetLink);
					endif;
				endif;
			else:
			    throwMessage(2,"You have followed bad reset link or your reset timer has  expired! Try again!", false, true);
				redirect('/user/passreset');
			endif;
		else:
			if($email):
				if (filter_var($email, FILTER_VALIDATE_EMAIL)):
					$user = $this->user_m->get_user_by_email(trim(strtolower($email)));
					if($user && $user['active'] == 1):
						//confirm
						if($hash = $this->user_m->create_reset_link($user['id'])):
							$confirmUrl = base_url()."/user/passreset?rl={$hash}";
							$mandrillconfig = $this->config->load("mandrill",TRUE);
							$this->load->library('Mandrill', $mandrillconfig);
							$this->load->helper('notifications');
							$templateData = array('name' => $user['name'], 'url' => $confirmUrl, 'email' => $email);
							$htmlView = $this->load->view('email_templates/reset_password', $templateData, true);
							//$message = buildMessage($templateData, "Password Reset", $htmlView);

							try {
								/*$mdrConnection = $this->mandrill->create();
								$async = false;
							    $ip_pool = 'Main Pool';
							    $send_at = NULL; // '2001-12-12 12:12:12';
							    $result = $mdrConnection->messages->send($message, $async);*/

								$this->load->library('email');

								$this->email->from('no-reply@concertfix.com', 'ConcertFix');
								$this->email->to($email);

								$this->email->reply_to('info@concertfix.com', 'ConcertFix');

								$this->email->bcc('theconcertfix@gmail.com');

								$this->email->subject("Password Reset");
								$this->email->message($htmlView);
								if ($this->email->send()){
									$this->_drop_userdata();
									throwMessage(0, "Link was sent to {$email} to reset your password", false, true);
								}
								else
									throwMessage(2,"There was an error sending notification message... try again in a bit.", false, true);

								redirect('/user/login');
							} catch(Mandrill_Error $e) {
							    throwMessage(2,"There was an error sending notification message... try again in a bit.", false, true);
								redirect('/user/passreset');
							}
						else:
							throwMessage(1, 'Sorry, error occurred. Try again later!', false, true);
							redirect('/user/passreset');
						endif;
					else:
						throwMessage(1, "Sorry but user $email either  does not exist or inactive", false, true);
						redirect('/user/passreset');
					endif;
				else:
					throwMessage(1, "Sorry $email does not look like a valid email", false, true);
					redirect('/user/passreset');
				endif;
			endif;
		endif;
			$title = "Reset Password | ConcertFix";
			$description = "Password forgotten";
			$keywords = "ConcertFix, members, password, twitter";
			//array of open graph
	 		$ogMeta = array(
	 		                'title'			=> $title,
			                'description' 	=> $description,
			                'keywords'		=> $keywords,
			                'copyright'		=> "Copyright 2013, Concert Fix",
			                'image' 		=> base_url()."public/img/assets/concert-fix-logo.png",
			                // 'url'			=> "this is where the page URL goes",
			                );

	 		//array of meta
            $yrobot = "noindex, nofollow";
            $metaTags = array(
                'description' => $description,
                'keywords'    => $keywords,
                'robots'      => $yrobot,
                'googlebot'   => $yrobot,
                'msnbot'      => $yrobot,
            );

			//set title
			$this->template->title($title)
							->prepend_metatags($metaTags) //set meta tags
							->prepend_metatags($ogMeta, true); // $og = true

			$this->template
					->set_layout('fullwidth')
					->set_partial('header', 'frontend/global/header')
					->set_breadcrumb('Home','/')
					->set_breadcrumb('Password Reset')
					->set_partial('footer', 'frontend/global/footer')
					->build('frontend/user/reset_password', $pageData);

	}

	public function verify($network = 'twitter')
	{
		if(isset($_GET['user']) && $network == 'cf'):
			if($this->user_m->verify_email($_GET['user'])):
				$this->_drop_userdata();
				throwMessage(0, "Perfect! You're all set! Please login now.",false, true);
			else:
				$this->_drop_userdata();
				throwMessage(2, "Account already confirmed, please login below.",false, true);
			endif;
			redirect('user/login?next=settings');
		elseif(isset($_GET['twuser']) && isset($_GET['token']) && $network == 'twitter'): //confirm ownership of the twitter email
		       //get twitter pending email by hash and email
		       if($pendingUser = $this->user_m->pending_exists($_GET['twuser'], $_GET['token'])):

			       $insertUser = array('email' => $pendingUser->email,
					                    'id'	=> $pendingUser->twitter_user_id,
					                    );
					$this->user_m->insert_user($insertUser, 'twitter');
					//clear old data
                   $this->cfUser = false;
                   $this->userlib->logoff();
                   if ($this->session->all_userdata()):
                       $this->session->sess_destroy();  // Assuming you have session helper loaded
                   endif;
                   #$this->_drop_userdata();
                   throwMessage(0, "Successfully confirmed ({$pendingUser->email}) please click Twitter button to login!!", false, true);
                   redirect('/user/login?next=settings&media=twitter');
				else:
					throwMessage(2, "Email email does not look like  valid email address...c'mon!",false, true);
					redirect('/user/verify/twitter');
				endif;
		elseif($this->_needs_email()):
			if($email = trim($this->input->post('tw_email'))):
				if (filter_var($email, FILTER_VALIDATE_EMAIL)):
						if($pendingConfirm = $this->user_m->get_pending_tw_user($email)):
							$confirmUrl = "user/verify/twitter?twuser={$pendingConfirm->confirm_hash}&token={$pendingConfirm->email_hash}";
						else:
							$confirmHash =  md5("RandomGarbageGoesHere".date('U').$email);
							$emailHash = md5($email);
							$confirmUrl = "user/verify/twitter/?twuser={$confirmHash}&token={$emailHash}";
							$pendingConfirm = array(
							                        'twitter_user_id'	=> $_SESSION['cfUser']['twitter']['id'],
													'confirm_hash'		=> $confirmHash,
													'email_hash'		=> $emailHash,
													'email'				=> trim(strtolower($email)),
							                        );
							$this->user_m->insert_tw_pending($pendingConfirm);
						endif;
						/*$mandrillconfig = $this->config->load("mandrill",TRUE);
						$this->load->library('Mandrill', $mandrillconfig);*/
						$this->load->helper('notifications');
						$templateData = array('name' => $this->cfUser['twitter']['name'], 'url' => $confirmUrl, 'email' => $email);
						$htmlView = $this->load->view('email_templates/confirm_email_html', $templateData, true);
						/*$textView = $this->load->view('email_templates/confirm_email_text', $templateData, true);
						$message = buildMessage($templateData, "Please Confirm Email", $htmlView, $textView);*/

						try {
							/*$mdrConnection = $this->mandrill->create();
							$async = false;
						    $ip_pool = 'Main Pool';
						    $send_at = NULL; // '2001-12-12 12:12:12';
						    $result = $mdrConnection->messages->send($message, $async);*/
							$this->load->library('email');

							$this->email->from('no-reply@concertfix.com', 'ConcertFix');
							$this->email->to($email);

							$this->email->reply_to('info@concertfix.com', 'ConcertFix');

							$this->email->bcc('theconcertfix@gmail.com');

							$this->email->subject("Please Confirm Email");
							$this->email->message($htmlView);
							if ($this->email->send()){
								$this->_drop_userdata();
								throwMessage(0, "To avoid spam, we sent a confirmation link to ({$email}) to make sure you own this email :)",false, true);
							}else
								throwMessage(2,"There was an error sending notification message... try again in a bit.", false, true);

							// redirect('/user/login?next=settings');
                            redirect('/user/verify/twitter');

						} catch(Mandrill_Error $e) {
						    throwMessage(2,"There was an error sending notification message... try again in a bit.", false, true);
						//	redirect('/user/verify/twitter');
						}
				else:
					$err = throwMessage(2, "Email ({$email}) does not look like  valid email address...c'mon!",false, true);
			//		redirect('/user/verify/twitter');
				endif;

			//	redirect('/user/verify/twitter');
			endif;
			$pageData = false;
			$title = "Verify Account | ConcertFix";
			$description = "Add Email";
			$keywords = "ConcertFix, members, email, twitter";
			//array of open graph
	 		$ogMeta = array(
	 		                'title'			=> $title,
			                'description' 	=> $description,
			                'keywords'		=> $keywords,
			                'copyright'		=> "Copyright 2013, Concert Fix",
			                'image' 		=> base_url()."public/img/assets/concert-fix-logo.png",
			                // 'url'			=> "this is where the page URL goes",
			                );

	 		//array of meta
            $yrobot = "noindex, nofollow";
            $metaTags = array(
                'description' => $description,
                'keywords'    => $keywords,
                'robots'      => $yrobot,
                'googlebot'   => $yrobot,
                'msnbot'      => $yrobot,
            );

			//set title
			$this->template->title($title)
							->prepend_metatags($metaTags) //set meta tags
							->prepend_metatags($ogMeta, true); // $og = true

			$this->template
					->set_layout('fullwidth')
					->set_partial('header', 'frontend/global/header')
					->set_breadcrumb('Home','/')
					->set_breadcrumb('Verify')
					->set_breadcrumb('Add Email For Twitter Sign-In')
					->set_partial('footer', 'frontend/global/footer')
					->build('frontend/user/twitter_email', $pageData);
		else:
			$this->_drop_userdata();
			redirect('/user/login');
		endif;
	}

	public function resend()
	{
		if($email = strtolower(trim($this->input->post('cf_email')))):
			$cfU = $this->user_m->get_user_by_email($email, false, true);
			if(!$cfU):
			     throwMessage(2,"This email is not found!.", false, true);
			else:
				$mandrillconfig = $this->config->load("mandrill",TRUE);
				$this->load->library('Mandrill', $mandrillconfig);
				$this->load->helper('notifications');
				$confirmUrl = "user/verify/cf?user={$cfU['confirm_hash']}";
				$templateData = array('name' => $cfU['name'],  'url' => $confirmUrl, 'email' => $cfU['primary_email']);
				$htmlView = $this->load->view('email_templates/confirm_email_html', $templateData, true);
				$textView = $this->load->view('email_templates/confirm_email_text', $templateData, true);
				$message = buildMessage($templateData, "Please Confirm Email", $htmlView, $textView);

				try {
					/*$mdrConnection = $this->mandrill->create();
					$async = false;
				    $ip_pool = 'Main Pool';
				    $send_at = NULL; // '2001-12-12 12:12:12';
				    $result = $mdrConnection->messages->send($message, $async);*/

					$this->load->library('email');

					$this->email->from('no-reply@concertfix.com', 'ConcertFix');
					$this->email->to($cfU['primary_email']);

					$this->email->reply_to('info@concertfix.com', 'ConcertFix');

					$this->email->bcc('theconcertfix@gmail.com');

					$this->email->subject("Please Confirm Email");
					$this->email->message($htmlView);
					if ($this->email->send())
						throwMessage(0,"Email Sent Successfully! Please check {$templateData['email']} for verification message!", false, true);
					else
						throwMessage(2,"There was an error sending notification message... try again in a bit.", false, true);

				    redirect('/user/login');
				} catch(Exception $e) {
				    // Mandrill errors are thrown as exceptions
				     throwMessage(2,"There was an error sending notification message... try again in a bit.", false, true);
				}
			endif;
		endif;

		$pageData = array('cEmail' => $this->input->get('email'));
		$title = "Request Verification Email | ConcertFix";
		$description = "Request Verification Email";
		$keywords = "ConcertFix, members, email";
		//array of open graph
 		$ogMeta = array(
 		                'title'			=> $title,
		                'description' 	=> $description,
		                'keywords'		=> $keywords,
		                'copyright'		=> "Copyright 2013, Concert Fix",
		                'image' 		=> base_url()."public/img/assets/concert-fix-logo.png",
		                // 'url'			=> "this is where the page URL goes",
		                );

 		//array of meta
        $yrobot = "noindex, nofollow";
        $metaTags = array(
            'description' => $description,
            'keywords'    => $keywords,
            'robots'      => $yrobot,
            'googlebot'   => $yrobot,
            'msnbot'      => $yrobot,
        );

		//set title
		$this->template->title($title)
						->prepend_metatags($metaTags) //set meta tags
						->prepend_metatags($ogMeta, true); // $og = true

		$this->template
				->set_layout('fullwidth')
				->set_partial('header', 'frontend/global/header')
				->set_breadcrumb('Home','/')
				->set_breadcrumb('Resend Verification Email')
				->set_breadcrumb('Add Email For Twitter Sign-In')
				->set_partial('footer', 'frontend/global/footer')
				->build('frontend/user/request_email', $pageData);
	}

	public function process_login($network = false)
	{
		if($_POST):
			$params = $_POST;
		elseif($_GET):
			$params = $_GET;
		else:
			/*throwMessage(2,"Login data not received...",false, true);
			redirect('/user/login');*/
			$params = false;
		endif;

		$this->cfUser = $this->userlib->getUser($network, $params);
		$_SESSION['cfUser'] = $this->cfUser;

		/*This is just a code to test*/
		/*pdump($this->cfUser);
		$this->template
			->set_layout('fullwidth')
			->set_partial('header', 'frontend/global/header')
			->set_breadcrumb('Home','/')
			->set_breadcrumb('Login')
			// ->set_partial('sidebar_twitter', 'frontend/global/twitter')
			// ->append_metadata($jqbt)
			->build('frontend/user/login');*/

		if ($this->cfUser['no_email']){
			$_SESSION['contact_email'] = true;
			redirect('/user/contactemail');
		}

		if($this->cfUser['loggedin']):
			if(isset($_POST['next']) && $_POST['next'] == 'settings')
				redirect('/user/settings');
			else
				redirect('/user/dashboard');
		elseif($this->cfUser['cf']['active'] == 2): //not active yet
			$redirectUrl = base_url().'/user/resend?email='.$this->cfUser['cf']['primary_email'];
			throwMessage(2,"Account Innactive. Please follow activation link in {$this->cfUser['cf']['primary_email']} or <a href='{$redirectUrl}'>Request Another Email</a>",false, true);
		elseif(isset($this->cfUser['cf']['active']) && $this->cfUser['cf']['active'] == 0):
			throwMessage(2,"Account Was Deleted!! contact us.",false, true);
		else:
			throwMessage(2,"FAILED TO LOGIN! Please check your credentials!",false, true);
		endif;
		redirect('/user/login');

	}

	public function contactemail()
	{
		if (!isset($_SESSION['contact_email']))
			redirect('/user/login');

		$email = $this->input->post('email');
		/*if (!filter_var($email, FILTER_VALIDATE_EMAIL))
			throwMessage(2,"Wrong email! Please enter a valid email address!",false, true);*/
		$pageData = false;
		//pdump($this->cfUser);


		if($email):
			$this->load->model('user_m');
			$this->user_m->update_facebook_email($email);
			unset($_SESSION['contact_email']);
			if ($this->cfUser['f_time'] == 1)
				redirect('/user/settings');
			else
				redirect('/user/dashboard');
		endif;
		$title = "Contact Email | ConcertFix";
		$description = "Contact Email";
		$keywords = "ConcertFix, contact, email, facebook";
		//array of open graph
		$ogMeta = array(
			'title'			=> $title,
			'description' 	=> $description,
			'keywords'		=> $keywords,
			'copyright'		=> "Copyright 2013, Concert Fix",
			'image' 		=> base_url()."public/img/assets/concert-fix-logo.png",
			// 'url'			=> "this is where the page URL goes",
		);

		//array of meta
        $yrobot = "noindex, nofollow";
        $metaTags = array(
            'description' => $description,
            'keywords'    => $keywords,
            'robots'      => $yrobot,
            'googlebot'   => $yrobot,
            'msnbot'      => $yrobot,
        );

		//set title
		$this->template->title($title)
			->prepend_metatags($metaTags) //set meta tags
			->prepend_metatags($ogMeta, true); // $og = true

		$this->template
			->set_layout('fullwidth')
			->set_partial('header', 'frontend/global/header')
			->set_breadcrumb('Home','/')
			->set_breadcrumb('Set Contact Email')
			->set_partial('footer', 'frontend/global/footer')
			->build('frontend/user/contact_email', $pageData);

	}

	public function login()
	{
		//error_reporting(E_ALL);
		if(isset($this->cfUser['loggedin']) && $this->cfUser['loggedin']) redirect('/user/rules');
	//	$this->_drop_userdata(); //justin case
		$next = 'rules';
		if(isset($_GET['next']) && $_GET['next'] == 'settings')
			$next = 'settings';

        $listItem = array();
        $listItem[0] = ['name' => 'Login', 'url'=> base_url().'user/login'];

		$pageData = array('urls' => $this->userlib->getLoginUrls(), 'next' => $next, 'listItem' => $listItem);

        if (isset($_GET['media']) && $_GET['media'] == 'twitter') {
            $pageData['media'] = "Perfect! You're all set! Please login with Twitter now.";
        }

		$title = "Login Or Register | ConcertFix";
		$description = "Login Or Register | ConcertFix";
		$keywords = "ConcertFix, members, login";

		//array of open graph
 		$ogMeta = array(
 		                'title'			=> $title,
		                'description' 	=> $description,
		                'keywords'		=> $keywords,
		                'copyright'		=> "Copyright 2013, Concert Fix",
		                'image' 		=> base_url()."public/img/assets/concert-fix-logo.png",
		                // 'url'			=> "this is where the page URL goes",
		                );

 		//array of meta
        $yrobot = "index, follow";
        $metaTags = array(
            'description' => $description,
            'keywords'    => $keywords,
            'robots'      => $yrobot,
            'googlebot'   => $yrobot,
            'msnbot'      => $yrobot,
        );

		$jqbt = "<script src='/public/js/login.js'></script>";

		//set title
		$this->template->title($title)
						->prepend_metatags($metaTags) //set meta tags
						->prepend_metatags($ogMeta, true); // $og = true

		$this->template
				->set_layout('fullwidth')
                ->set_partial('ldjson', 'frontend/global/breadcrumb_ldjson')
				->set_partial('header', 'frontend/global/header')
				->set_breadcrumb('Home','/')
				->set_breadcrumb('Login')
				// ->set_partial('sidebar_twitter', 'frontend/global/twitter')
				// ->append_metadata($jqbt)
				->set_partial('footer', 'frontend/global/footer', array('extra' => $jqbt))
				->build('frontend/user/login', $pageData);
	}

	public function logoff($redirect = true)
	{
		$this->cfUser = false;
		$this->userlib->logoff();
		if($this->session->all_userdata()):
	        $this->session->sess_destroy();  // Assuming you have session helper loaded
	    endif;
        if($redirect)redirect('/');
        return true;
	}

	public function dashboard()
	{
		if(!$this->cfUser['loggedin']) redirect('/user/login'); // redirect('/user/login');

		if($this->_needs_email()) redirect('/user/verify/twitter');

		$events = $this->rules_m->get_feed_data($this->cfUser['cf']['id']);
		if($events):
			$events['performers'] = $this->global_m->get_event_performers($events['performers']);
			$events['performers'] = $this->global_m->get_prices($events['performers']);
			$events['genres'] = false;  // $this->global_m->get_event_performers($events['genres']);
		else:
			$events['performers'] = false;
			$events['genres'] = false;
		endif;
        #var_dump($events);
		$pageData = $events;
		$pageData['user_performers'] = $this->rules_m->fetch_rules("performer", $this->cfUser['cf']['id']);
		$rindex = rand(0, count($pageData['user_performers'])-1);

		if($rindex):
			$rId = $pageData['user_performers'][$rindex]->pId;
			$pageData['similar'] = $this->global_m->get_similar_artists($rId, 50);
		else:
			$pageData['similar'] = array();
		endif;
		//title for title of the page
		$title = "Dashboard And Tracker | ConcertFix";
		$description = "Dashboard And Tracker | ConcertFix";
		$keywords = "ConcertFix, members, login";

		//array of open graph
 		$ogMeta = array(
 		                'title'			=> $title,
		                'description' 	=> $description,
		                'keywords'		=> $keywords,
		                'copyright'		=> "Copyright 2013, Concert Fix",
		                'image' 		=> base_url()."public/img/assets/concert-fix-logo.png",
		                // 'url'			=> "this is where the page URL goes",
		                );
		$jqbt = "<script src='/public/js/dashboard.js?".rand(1,100)."'></script>";

		$css = "<link rel='stylesheet' type='text/css' href='/public/css/black-tie/jqui.min.css'>";
 		//array of meta
        $yrobot = "noindex, nofollow";
        $metaTags = array(
            'description' => $description,
            'keywords'    => $keywords,
            'robots'      => $yrobot,
            'googlebot'   => $yrobot,
            'msnbot'      => $yrobot,
        );

		//set title
		$this->template->title($title)
						->prepend_metatags($metaTags) //set meta tags
						->prepend_metatags($ogMeta, true); // $og = true

		$this->template
				->set_layout('user')
				->set_partial('header', 'frontend/global/header', array('cfUser' => $this->cfUser))
				->set_breadcrumb('Home','/')
				->set_breadcrumb('Dashboard')
				->set_partial('sidebar_user', 'frontend/user/user_sidebar')
				->set_partial('footer', 'frontend/global/footer', array('extra' => $jqbt))
				->prepend_metadata($css)
				// ->append_metadata($jqbt)
				->build('frontend/user/dashboard', $pageData);
	}

	public function information(){
		$email = $this->input->get('email');
		$pageData = array();

		$u = $this->user_m->get_user($email);
		if ($u) {
			$performers = $this->user_m->get_user_rules($u->id, 'performer');
			$pageData['performers'] = $this->user_m->get_performers_names($performers);
			$pageData['cities'] = $this->user_m->get_user_rules($u->id, 'city');
			$pageData['user'] = $u;

			$categories = $this->concerts_m->get_categories_user_settings();
			$categories_temp = array();
			foreach($categories as $category)
			{
				if($this->rules_m->exists_genre_by_userid($u->id, $category->id))
				{
					$categories_temp[] = $category->Filter;
				}
			}

			$pageData['categories'] = $categories_temp;
		}

		//title for title of the page
		$title = "User Information | ConcertFix";
		$description = "User Information | ConcertFix";
		$keywords = "User Information";

		//array of open graph
		$ogMeta = array(
			'title'			=> $title,
			'description' 	=> $description,
			'keywords'		=> $keywords,
			'copyright'		=> "Copyright 2013, Concert Fix",
			'image' 		=> base_url()."public/img/assets/concert-fix-logo.png",
			// 'url'			=> "this is where the page URL goes",
		);

		$css = "<link rel='stylesheet' type='text/css' href='/public/css/black-tie/jqui.min.css'>";
		//array of meta
        $yrobot = "noindex, nofollow";
        $metaTags = array(
            'description' => $description,
            'keywords'    => $keywords,
            'robots'      => $yrobot,
            'googlebot'   => $yrobot,
            'msnbot'      => $yrobot,
        );

		//set title
		$this->template->title($title)
			->prepend_metatags($metaTags) //set meta tags
			->prepend_metatags($ogMeta, true); // $og = true

		$this->template
			->set_layout('user')
			->set_partial('header', 'frontend/global/header')
			->set_breadcrumb('Home','/')
			->set_breadcrumb('Information')
			->set_partial('sidebar_user', 'frontend/user/user_sidebar')
			->set_partial('footer', 'frontend/global/footer')
			->prepend_metadata($css)
			// ->append_metadata($jqbt)
			->build('frontend/user/information', $pageData);
	}

	private function _drop_userdata()
	{
		$this->session->unset_userdata('oldtwUrl');
		$this->session->unset_userdata('twitter');
		$this->session->unset_userdata('cf');
		$this->session->unset_userdata('facebook');
		$this->session->unset_userdata('network');
		$this->session->unset_userdata('loggedin');
		$this->cfUser = false;
	}

	private function _needs_email()
	{
		return ($this->cfUser['loggedin'] && $this->cfUser['network'] == 'twitter' && $this->cfUser['cf'] == "pending") ? true : false;
	}
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */
