<?php ini_set('memory_limit', '-1'); ?>
<?php //error_reporting(E_ALL); ?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/***********

CREATE DROP FEATURED that do not have anything featured

**********/
class Cron extends MY_SiteController {

	private $apiClass;
	private $auth;
	private $soapy;

	public function __construct()
	{
		parent::__construct();
		$this->output->enable_profiler(false);
		$this->load->model('cron_m');
		$this->load->model('user_m');
		$this->load->model('notifier_m');
		$mandrillconfig = $this->config->load("mandrill",TRUE);
		$this->load->library('Mandrill', $mandrillconfig);
		$this->load->helper('notifications');
	}


	public function rundaily($whichRun = 'all', $mailto = "john@lightningmarketinggroup.com, ebergolla@gmail.com", $debug = true)
	{
		echo ($debug)  ? "process started" : '';
		if(!$this->input->is_cli_request()) die('Not allowed from URL');
		switch ($whichRun) :
			case 'all':
				$this->_runtn($mailto, $debug);
				$this->_runNotices($mailto, $debug);
                $this->_runLFM($mailto, $debug);
				# code...
				break;
			case 'tn':
				$this->_runtn($mailto, $debug);
				break;
			case 'lfm':
				$this->_runLFM($mailto = 'ebergolla@gmail.com', $debug);
				break;
			case 'track':
				$this->_runNotices($mailto, $debug);
				break;
            case 'tn_track':
				$this->rules_m->change_cron_start_date('rundaily_tn_track',date('Y-m-d'));
                $this->_runtn($mailto, $debug);
				$this->rules_m->change_cron_last_date('rundaily_tn_track',date('Y-m-d'));
				$this->_runNotices($mailto, $debug);
				break;
			default:
				echo "\n\r NOTHING TO RUN";
				break;
		endswitch;
		//run TN
		//run
	}

	private function _runtn($mailto = "ebergolla@gmail.com", $debug = true)
	{
		if(!$this->input->is_cli_request()) die('Not allowed from URL');
		$startTime = date('U');
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'To: Ernesto <ebergolla@gmail.com>, John <john@lightningmarketinggroup.com>' . "\r\n";

		$subject = 'CF CRON PROD | TicketNetwork Run Started at: '.date('F j Y H:i:s');
		$message = "<html><body><p>Ticket Network Nightly Cron Started at: <strong>".date('F j Y, H:i:s', $startTime). "</strong>  with following updates</p><ul><li>Get Artists</li><li>Get Venues</li><li>Get Events</li><li>Get Event Performers</li><li>Clean up (update event and price counts)</li>;<ul></body></html>";

		mail($mailto, $subject, $message, $headers);
		$startTimeReset = date('U');
		$message ="<html><body><h3>Runtimes for TN update:</h3><ul>";
		// $message .= "RUNNING ONLY TN FEEDS";
		$message .= "<li>Running Get Artists: ";
        echo "Running Get Artists \n\r";
		$startTimeReset = date('U');
		$message .= $this->getartists($mailto, $debug);
		$runtime = number_format((date('U') - $startTimeReset), 0, '.', ',');
		$message .="<strong> $runtime </strong> seconds";
		$message .= "</li><li>Running Get Venues: ";
        echo "Running Get Venues \n\r";
		$startTimeReset = date('U');
		$message .= $this->getvenues($mailto, $debug);
		$runtime = number_format((date('U') - $startTimeReset), 0, '.', ',');
		$message .="<strong> $runtime </strong> seconds";
		$message .= "</li><li>Running Get Events: ";
        echo "Running Get Events \n\r";
		$startTimeReset = date('U');
		$message .= $this->getevents($mailto, $debug);
		//because TN is soo awesome
		$message .= $this->_fixmissing();
		$runtime = number_format((date('U') - $startTimeReset), 0, '.', ',');
		$message .="<strong> $runtime </strong> seconds";
		$message .= "</li><li>Running Get Event Performers: ";
        echo "Running Get Event Performers \n\r";
		$startTimeReset = date('U');
		$this->geteventperformers($mailto, $debug);
		$runtime = number_format((date('U') - $startTimeReset), 0, '.', ',');
		$message .="<strong> $runtime </strong> seconds";
		$message .= "</li><li>Running Get Hot Sales: ";
        echo "Running Get Hot Sales \n\r";
		$startTimeReset = date('U');
		//$message .= $this->gethotsales($mailto, $debug);
		$runtime = number_format((date('U') - $startTimeReset), 0, '.', ',');
		$message .="<strong> $runtime </strong> seconds";
		$message .= "</li><li>Running High Inventory: ";
        echo "Running High Inventory \n\r";
		$startTimeReset = date('U');
		$message .= $this->getinventory($mailto, $debug);
		$runtime = number_format((date('U') - $startTimeReset), 0, '.', ',');
		$message .="<strong> $runtime </strong> seconds";
		$message .= "</li><li>cleaning up the mess: ";
        echo "Cleaning up the mess \n\r";
		$startTimeReset = date('U');
		$message .= $this->cleanup($mailto, $debug);
		$runtime = number_format((date('U') - $startTimeReset), 0, '.', ',');
		$message .="<strong> $runtime </strong> seconds</li></ul>";
		$message .= "<h3>TOTAL RUNTIME: ". number_format( (date('U') - $startTime )/60, 2, '.', ',')." minutes";
		$message .= "</h3></body></html>";
		$subject = "CF CRONT PROD FINISHED | TicketNetwork at ".date('F j Y H:i');
		mail($mailto, $subject, $message, $headers);
        echo "Finished RUNTN \n\r";

	}

	private function _runLFM($mailto = 'ebergolla@gmail.com', $debug = true)
	{
		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		$startTime = date('U');

		$subject = "CF CRON PROD |  Last.FM Started at: ".date('F j Y H:i:s');
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'To: Ernesto <ebergolla@gmail.com>, John <john@lightningmarketinggroup.com>' . "\r\n";

		$message = "<html><body><p>Daily run of LastFM data started at: <strong>".date('F j Y, H:i:s', $startTime). "</strong>  with following updates</p><ul><li>Get Artists from LFM</li><li>Get Albums that don't exist</li><li>Get LFM Images</li><li>Get Album tracks</li><li>Update new Artist album paragraph</li><ul></body></html>";
		mail($mailto, $subject, $message, $headers);

		$startTimeReset = date('U');
		$message ="<html><body><h3>Runtimes for LFM update:</h3><ul>";
        echo "Runtimes for LFM update \n\r";

		$startTimeReset = date('U');
		// $message .= "RUNNING ONLY TN FEEDS";
		$message .= "</li><li>Running Get Albums: ";
        echo "Running Get Albums \n\r";
		$startTimeReset = date('U');
		$message .= $this->getalbums($mailto, $debug);
		$runtime = number_format((date('U') - $startTimeReset), 0, '.', ',');
		$message .="<strong> $runtime </strong> seconds";
		$startTimeReset = date('U');
		// $message .= "RUNNING ONLY TN FEEDS";
		$message .= "</li><li>Running Get Images: ";
        echo "Running Get Images \n\r";
		$startTimeReset = date('U');
		$message .= $this->getlfmimages($mailto, $debug);
		$runtime = number_format((date('U') - $startTimeReset), 0, '.', ',');
		$message .="<strong> $runtime </strong> seconds";
		$startTimeReset = date('U');
		// $message .= "RUNNING ONLY TN FEEDS";
		$message .= "</li><li>Running Get Tracks: ";
        echo "Running Get Tracks \n\r";
		$startTimeReset = date('U');
		$message .= $this->gettracks($mailto, $debug);
		$runtime = number_format((date('U') - $startTimeReset), 0, '.', ',');
		$message .="<strong> $runtime </strong> seconds";

        $date = getdate();
        $wday = $date['wday'];
        if($wday == 3) {

            $message .= "</li><li>Running To Fix Artists about text: ";
            echo "Running To Fix Artists about text \n\r";
            $startTimeReset = date('U');
            $message .= $this->fixabout();
            $runtime = number_format((date('U') - $startTimeReset), 0, '.', ',');
            $message .= "<strong> $runtime </strong> seconds</li></ul>";

            $startTimeReset = date('U');
            // $message .= "RUNNING ONLY TN FEEDS";
            $message .= "</li><li>Running To Updated About Albums: ";
            echo "Running To Updated About Albums \n\r";
            $startTimeReset = date('U');
            $message .= $this->createaboutnew($mailto, $debug);
            $runtime = number_format((date('U') - $startTimeReset), 0, '.', ',');
            $message .= "<strong> $runtime </strong> seconds</li></ul>";
        }

		$startTimeReset = date('U');
		// $message .= "RUNNING ONLY TN FEEDS";
		$message .= "</li><li>Running To Updated STILL MISSING About Albums: ";
        echo "Running To Updated STILL MISSING About Albums \n\r";
		$startTimeReset = date('U');
		$message .= $this->catchup_about($mailto, $debug);
		$runtime = number_format((date('U') - $startTimeReset), 0, '.', ',');
		$message .="<strong> $runtime </strong> seconds</li></ul>";

		//DONE
		$runtime = number_format(((date('U') - $startTime)/60/60), 2, '.', ',');

		$message .= "<h3>TOTAL RUNTIME: ". $runtime." hours ";
		$message .= "</h3></body></html>";
		$subject = "CF CRON PROD FINISHED | Last.FM Finished at ".date('F j Y H:i');
		mail($mailto, $subject, $message, $headers);
        echo "Finished RUN LFM \n\r";

	}

	private function _runNotices($mailto="john@lightningmarketinggroup.com, concertfix@gmail.com", $debug = true)
	{
		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		$startTime = date('U');

        echo 'Intro runNotices'."\n\r";

		$subject = "CF CRON PROD | Generation of Track Emails Started at: ".date('F j Y H:i:s');
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'To: Ernesto <ebergolla@gmail.com>, John <john@lightningmarketinggroup.com>' . "\r\n";

		$message = "<html><body>";
		$message .= "<h1>Notifications Statustics:</h1> <ul>";
		$message .= $this->create_queue();
//		$message .= $this->send_notifications();
		$message .= "</ul>";
		$runtime = number_format(((date('U') - $startTime)/60/60), 2, '.', ',');
		$message .= "<h4>Total Run: ".$runtime." Hours</h4></body></html>";
		mail($mailto, $subject, $message, $headers);
	}


	public function wordpress()
	{
		error_reporting(E_ALL);
		if(!$this->input->is_cli_request()) die('Not allowed from URL');
		$this->load->model('tours_m');
		$this->load->model('cron_m');
		$this->load->model('global_m');

		$artists = $this->cron_m->get_all_performers();
		$header = array("URL","Title","Wiki","Keywords","Description");
		$fp = fopen('/home/sites/stubstock/html/artists.csv', 'w');
		fputcsv($fp, $header);
		foreach ($artists as $a) :
			$events = $this->global_m->get_performer_event($a->PerformerID);
			if(count($events)):
				$performer = $this->tours_m->get_performer_by_slug($a->PerformerSlug);


				$line = array("http://www.concertfix.com/tours/".$a->PerformerSlug,
				              "{$a->Name}",
				              "NA",
				              "{$a->Name} tours, {$a->Name} concerts, {$a->Name} schedule, {$a->Name} tickets",
				              "{$performer[0]->aboutText}"
				              );
				echo "\n\r".implode(" -- ", $line);
				fputcsv($fp, $line);
			endif;
		endforeach;
	}


/*	// DO NOT RUN
	public function insert_geos($mailto = 'ebergolla@gmail.com', $debug = true)
	{
	//	$perfs = $this->cron_m->do_the_dew();
	}

	//DO NOT RUN
	public function fix_city_slugs($mailto = 'ebergolla@gmail.com', $debug = true)
	{
		// $this->cron_m->fix_city_slugs();
	}
*/
	//run this daily gets all performers from TN
	public function getartists($mailto = 'john@lightningmarketinggroup.com', $debug = true)
	{
		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		$newList = array();
		//load soap
		$this->soapy = new TN_soap();
		echo ($debug) ? "intialized soapy \n\r" : '';
		//call GetEventPerformers
		$params = array('parentCategoryID' => 2);
		$artists = $this->soapy->run_soap('GetPerformerByCategory', $params);
		echo ($debug) ? "got the list ... \n\r" : '';

		/*Block added by R@mdy*/
		$this->load->library('lastfmapi/lastfmapi');
		$this->auth = new lastfmApiAuth('setsession');
		$this->apiClass = new lastfmApi();
		$artistClass = $this->apiClass->getPackage($this->auth, 'artist');
		/*Endblock*/

		foreach ($artists->Performer as $artist) {
			echo "$artist->Description \n\r ";

			if(!$this->cron_m->performer_exists($artist->ID)):
				//echo " - NEW ";
				echo "{$artist->Description} - NEW \n\r";
				$slug = ($this->cron_m->slug_exists(seoUrl($artist->Description))) ? seoUrl($artist->Description." ".$artist->ID) :  seoUrl($artist->Description);
				$perf = array(
								'ChildCategoryID'					=> $artist->Category->ChildCategoryID,
								'ChildCategoryDescription'			=> $artist->Category->ChildCategoryDescription,
								'GrandchildCategoryID'				=> $artist->Category->GrandchildCategoryID,
								'GrandchildCategoryDescription'		=> $artist->Category->GrandchildCategoryDescription,
								'ParentCategoryID'					=> $artist->Category->ParentCategoryID,
								'ParentCategoryDescription'			=> $artist->Category->ParentCategoryDescription,
								'Name'								=> $artist->Description,
								'PerformerID'						=> $artist->ID,
								'PerformerSlug'						=> $slug,
								'fbImagePath'						=> $this->getFBImage($artist->Description)
					);
				$pendPerf = array("PerformerID" => $artist->ID);
				$this->cron_m->insert_about_pending($pendPerf);
				$this->cron_m->insert_performer($perf);

				/*Block added by R@mdy*/
				if(!$this->cron_m->image_exists($perf['PerformerID'])):
					$methodVars = array('artist' => $perf['Name']);
					$aInfo = $artistClass->getInfo($methodVars);
					if ($aInfo['image']['extralarge'] != '')
						$lfmImagePath = $aInfo['image']['extralarge'];
					else
						$lfmImagePath = 'https://concertfix.com/public/img/artist_profile.jpg';
					$imgs = array('PerformerID' => $perf['PerformerID'] ,'lfmImagePath' => $lfmImagePath);
					$this->cron_m->insert_lfmpic($imgs);
					echo "{$perf['Name']} was inserted as LastFM Image \n\r";

				endif;
				/*Endblock*/

				array_push($newList, $perf['Name']."(".$perf['PerformerID'].")\n\r");
			endif;
		}
		$cnt = count($newList);
		$mess = implode('</li><li>', $newList);
		return "<span style='color:green;'>INSERTED: {$cnt} NEW ARTISTS</span><ul><li>{$mess}</li></ul>";
		// mail($mailto, "", $mess);
	}


	public function test_newartist($debug = true) {
        if(!$this->input->is_cli_request()) die('Not allowed from URL');

        $newList = array();
        //load soap
        $this->soapy = new TN_soap();
        echo ($debug) ? "intialized soapy \n\r" : '';
        //call GetEventPerformers
        $params = array('parentCategoryID' => 2);
        $artists = $this->soapy->run_soap('GetPerformerByCategory', $params);
        echo ($debug) ? "got the list ... \n\r" : '';

        $total = 0;

        try {
            $myfile = fopen("new_artists.txt", "w");
        }
        catch(Exception $e)
        {
            echo "File can't open";
            die;
        }
        foreach($artists->Performer as $artist)
        {
            fwrite($myfile, $artist->Description."\n\r");

        }
        fclose($myfile);

    }

	private function getFBImage($name){
		$name = str_replace(array("\r", "\n", "-", "'", " "), "", $name);
		$name = str_replace("&", "and", $name);

		$suffix = ['Official', 'Official_left', 'music', 'band', 'live', 'comedian', 'comedy', 'Oficial'];
		$url = "https://graph.facebook.com/".$name."/picture?type=large";
		$exist = $this->url_exists($url);
		if ($exist)
			return $url;
		else {
			$i = 0;
			while($i < count($suffix)){
				$nick = ($suffix[$i] == 'Official_left') ? 'Official'.$name : $name.$suffix[$i];
				$url = "https://graph.facebook.com/".$nick."/picture?type=large";
				$exist = $this->url_exists($url);
				if ($exist)
					return $url;
				else
					$i += 1;
			}

		}
		return 'https://concertfix.com/public/img/artist_profile.jpg';

	}

    public function album_data($album_name, $performer_name)
    {
        $this->load->library('lastfmapi/lastfmapi');
        $this->auth = new lastfmApiAuth('setsession');
        $this->apiClass = new lastfmApi();
        $albumClass = $this->apiClass->getPackage($this->auth, 'album');
        $param = array('artist' => $performer_name, 'album' => $album_name);
        $album_data = $albumClass->getInfo($param);
        return $album_data;
    }

    public function update_albums2(){
        $this->load->library('lastfmapi/lastfmapi');
        $this->auth = new lastfmApiAuth('setsession');
        $this->apiClass = new lastfmApi();
        $albumClass = $this->apiClass->getPackage($this->auth, 'album');
        $albums = $this->cron_m->get_albums_to_update();

        echo 'Artists count: '.count($albums)."<br> \n\r";
        $count = 1;
//        $this->cron_m->db->trans_start();
        $query = '';
        try {
            $myfile = fopen("query_update_albums.txt", "w");
        }
        catch(Exception $e)
        {
            echo "File can't open";
            die;
        }
        foreach($albums as $album)
        {
            echo 'Pass '. $count. ' of '. count($albums)."\n\r";
            $param = array('artist' => $album->Name, 'album' => $album->AlbumName);
            $album_data = $albumClass->getInfo($param);

            if($album_data['image']['large'] != '' and $album_data['image']['large'] != false and $album_data['image']['large'] != null) {
                $query = "update performer_album set lfmImagePath = \"{$album_data['image']['large']}\"  where id = {$album->id}; \n\r";
                fwrite($myfile, $query);
            }



            $update_data = array('lfmImagePath' => $album_data['image']['large']);
//            $this->cron_m->db->where('id',$album->id);
//            $this->cron_m->db->update('performer_album', $update_data);

//                $this->cron_m->db->set('lfmImagePath', $album_data['image']['large']);
            echo '  Artist Name: '.$album->Name."<br> \n\r";
            echo '  Updated album name: '.$album->AlbumName."<br> \n\r";
            echo '  Updated album Image: '.$album_data['image']['large']."<br> \n\r";

            $count += 1;
        }
        fclose($myfile);


//        $this->cron_m->db->trans_complete();
    }

    public function update_artist_image(){
        $this->load->library('lastfmapi/lastfmapi');
        $this->auth = new lastfmApiAuth('setsession');
        $this->apiClass = new lastfmApi();
        $albumClass = $this->apiClass->getPackage($this->auth, 'album');
        $artists = $this->cron_m->get_all_performers2();

        echo 'Artists count: '.count($artists)."<br> \n\r";
        $count = 0;
        $this->cron_m->db->trans_start();
        foreach ($artists as $artist) {

            $artist_param = array('artist' => $artist->Name);


            $artist_ws = $this->apiClass->getPackage($this->auth, 'artist');
            $artist_ws_data = $artist_ws->getInfo($artist_param);

//            $this->cron_m->db->trans_start();

            $this->cron_m->db->where('PerformerID', $artist->PerformerID);
            $this->cron_m->db->update('lfm_performer_images', array('lfmImagePath' => $artist_ws_data['image']['extralarge']));

//            $this->cron_m->db->trans_complete();

            $count += 1;
            echo 'Pass '. $count. ' of '. count($artists)."\n\r";
            echo 'Artist name: '.$artist->Name. " <br>\n\r";
            echo 'Artist Image: '.$artist_ws_data['image']['extralarge']. " <br>\n\r";

        }
        $this->cron_m->db->trans_complete();

    }

    public function getalbums_temp(){
        $this->load->library('lastfmapi/lastfmapi');
        $this->auth = new lastfmApiAuth('setsession');
        $this->apiClass = new lastfmApi();
        $artistClass = $this->apiClass->getPackage($this->auth, 'artist');
        $artists = $this->cron_m->get_all_performers2();
        $a = 0;
        $p = 0;
        $startTime = date('U');
        echo "Getting Artists for Albums: Counted ". count($artists).'</br>';
        foreach ($artists as $artist) {
            $p++;
            $methodVars = array('artist' => $artist->Name);

            //save artist image
            $artist_param = array('artist' => $artist->Name);
            $artistdata = $artistClass->getInfo($artist_param);


            echo 'Artist name: '.$artist->Name;
            $this->cron_m->db->where('PerformerID', $artist->PerformerID);
            $this->cron_m->db->update('lfm_performer_images', array('lfmImagePath' => $artistdata['image']['extralarge']));
            //end save artist image

            $lfmTopAlbums = $artistClass->getTopAlbums($methodVars);
            echo 'Artist name: ' . $artist->Name . '<br> Total albums: ' . count($lfmTopAlbums).'<br>';
            echo '<br> Performer Id: '.$artist->PerformerID.'</br>';
            foreach ($lfmTopAlbums as $album)
            {
                $slug = seoUrl($artist->Name . " " . $album['name']);
                if ($this->cron_m->album_exist($slug))
                {
					/*$albumClass = $this->apiClass->getPackage($this->auth, 'album');
					$params = array('artist' => $artist->Name, 'album' => $album['name']);
					$albumInfo = $albumClass->getInfo($params);
					echo $albumClass.'</br>';*/
                    $this->cron_m->db->where('AlbumSlug', $slug);
                    $data = array(
                                'tnPerformerID'		=> $artist->PerformerID,
                                'Rank'				=> $album['rank'],
                                'AlbumName'			=> $album['name'],
                                'PlayCount'			=> $album['playcount'],
                                'lfmPath'			=> $album['url'],
                                'lfmImagePath'		=> $album['image']['extralarge'],
                                //'ReleaseDate'		=> $albumInfo['releasedate'],
                                'AlbumSlug'			=> $slug
                    );

                    $this->cron_m->db->update('performer_album', $data);
                }
            }

        }
        $runtime = (date('U') - $startTime)/60;
        return "<span style='color:green;'>Added $a albums, for ". count($artists)." ran for $runtime minutes</span>";

    }
	//run this daily gets performers albums from LFM
	public function getalbums($mailto = 'ebergolla@gmail.com', $debug = true)
	{
		if(!$this->input->is_cli_request()) die('Not allowed from URL');
		$startTime = date('U');
		$this->load->library('lastfmapi/lastfmapi');

		// Pass the array to the auth class to return a valid auth

		$this->auth = new lastfmApiAuth('setsession');
		// pdump($this->auth);
		$this->apiClass = new lastfmApi();
		// pdump($this->apiClass);
		$artistClass = $this->apiClass->getPackage($this->auth, 'artist');
        $albumClass = $this->apiClass->getPackage($this->auth, 'album');
		$artists = $this->cron_m->get_all_performers();
		$a = 0;
		$p = 0;
		echo ($debug) ? "Getting Artists for Albums: Counted ". count($artists)."\n\r" : '';
        $this->cron_m->db->trans_start();
        foreach ($artists as $artist):
			$p++;
            echo ($debug) ? "Pass ".$p." of ".count($artists)."\n\r":'';
			$methodVars = array('artist' => $artist->Name, 'limit' => 10);
			$lfmTopAlbums = $artistClass->getTopAlbums($methodVars);

            echo ($debug) ? "Albums counted: ".count($lfmTopAlbums)."\n\r":"";
            if($p%100 == 0)
            {
                $this->cron_m->db->trans_complete();
                $this->cron_m->db->trans_start();
            }
            if($lfmTopAlbums)
            {

                foreach ($lfmTopAlbums as $album):
                    $slug = seoUrl($artist->Name . " " . $album['name']);
                    $album = array(
                        'tnPerformerID' => $artist->PerformerID,
                        'Rank' => $album['rank'],
                        'AlbumName' => $album['name'],
                        'PlayCount' => $album['playcount'],
                        'lfmPath' => $album['url'],
                        'lfmImagePath' => $album['image']['large'],
                        'ReleaseDate' => '0000-00-00',
                        'AlbumSlug' => $slug
                    );
                    $affected_rows = $this->cron_m->insert_album_2($album);

                    if($affected_rows != 0)
                    {
                        $methodVars_album = array('artist' => $artist->Name, 'album' => $album['name']);
                        $albumInfo = $albumClass->getInfo($methodVars_album);

                        if(isset($albumInfo['tracks'])):
                            foreach ($albumInfo['tracks'] as $key => $track) :
                                $albumInfo['tracks'][$key]['album_id'] = $a->id;
                                $albumInfo['tracks'][$key]['tn_PerformerID'] = $a->tnPerformerID;
                            endforeach;
                            $this->cron_m->insert_tracks($albumInfo['tracks']);
                            echo $debug ? "     Inserted ". count($albumInfo['tracks'])." track(s) to album $a->AlbumName of artist $a->Name \n\r" : '';
                        endif;

                        $a += $affected_rows;
                        echo ($debug) ? "added ($a) - albums \n\r" : '';
                    }
                    /*if (!$this->cron_m->album_exist($slug)):
                        //insert album
                        $album = array(
                            'tnPerformerID' => $artist->PerformerID,
                            'Rank' => $album['rank'],
                            'AlbumName' => $album['name'],
                            'PlayCount' => $album['playcount'],
                            'lfmPath' => $album['url'],
                            'lfmImagePath' => $album['image']['large'],
                            'ReleaseDate' => '0000-00-00',
                            'AlbumSlug' => $slug
                        );
                        $this->cron_m->insert_album($album);
                        $a++;

						$methodVars_album = array('artist' => $artist->Name, 'album' => $album['name']);
						$albumInfo = $albumClass->getInfo($methodVars_album);

						if(isset($albumInfo['tracks'])):
							foreach ($albumInfo['tracks'] as $key => $track) :
								$albumInfo['tracks'][$key]['album_id'] = $a->id;
								$albumInfo['tracks'][$key]['tn_PerformerID'] = $a->tnPerformerID;
							endforeach;
							$this->cron_m->insert_tracks($albumInfo['tracks']);
							echo $debug ? "     Inserted ". count($albumInfo['tracks'])." track(s) to album $a->AlbumName of artist $a->Name \n\r" : '';
						endif;

                        echo ($debug) ? "added ($a) - $p \n\r" : '';
                    else:
                        // echo ($debug) ? "skipped - $p \n\r" : '';
                    endif;*/

                endforeach; //end albums

            }
			// var_dump($lfmTopAlbums);

//			sleep(rand(1,3));
		endforeach; //end artist
        $this->cron_m->db->trans_complete();

		$runtime = (date('U') - $startTime)/60;
        echo ($debug) ? "Finished Update Artist's Albums":"";
		return "<span style='color:green;'>Added $a albums, for ". count($artists)." ran for $runtime minutes</span>";
		// mail($mailto, "Job Ran: Adding Albums", $mess);
	}

	public function updateAlbumImages($debug=true){
		if(!$this->input->is_cli_request()) die('Not allowed from URL');
		$startTime = date('U');
		$this->load->library('lastfmapi/lastfmapi');

		// Pass the array to the auth class to return a valid auth

		$this->auth = new lastfmApiAuth('setsession');
		// pdump($this->auth);
		$this->apiClass = new lastfmApi();

		$albumClass = $this->apiClass->getPackage($this->auth, 'album');

		$albums = $this->cron_m->get_all_albums();
		$i = 0;
		echo ($debug) ? "Getting Albums: Counted ". count($albums)."\n\r" : '';
		foreach ($albums as $album) {
			$methodVars_album = array('artist' => $album->PerformerName, 'album' => $album->AlbumName);
			$albumInfo = $albumClass->getInfo($methodVars_album);
			if ($this->url_exists($albumInfo['image']['large'])){
				echo $album->PerformerName."\n\r";
				echo $album->AlbumName."\n\r";
				$this->cron_m->update_album_pic($album->id, $albumInfo['image']['large']);
				echo $albumInfo['image']['large']."\n\r";
				$i++;
				echo $i." of ".count($albums);
			}
		}
		echo "Cantidad de albunes actualizados ".$i."\n\r";

	}

	private function inserttLastFMImage($performer) {
		#if(!$this->input->is_cli_request()) die('Not allowed from URL');

		$this->load->library('lastfmapi/lastfmapi');

		// Pass the array to the auth class to return a valid auth
		$this->auth = new lastfmApiAuth('setsession');
		$this->apiClass = new lastfmApi();
		$artistClass = $this->apiClass->getPackage($this->auth, 'artist');
		if(!$this->cron_m->image_exists($performer['PerformerID'])):
			$methodVars = array('artist' => $performer['Name']);
			$aInfo = $artistClass->getInfo($methodVars);
			$imgs = array('PerformerID' => $performer['PerformerID'] ,'lfmImagePath' => $aInfo['image']['extralarge']);
			$this->cron_m->insert_lfmpic($imgs);
			echo "{$performer['Name']} was inserted as LastFM Image";

		endif;

	}

	//fall back images from LFM run this daily (After getartists)
	public function getlfmimages($mailto = 'ebergolla@gmail.com', $debug = true)
	{
		if(!$this->input->is_cli_request()) die('Not allowed from URL');
		$startTime = date('U');
		$this->load->library('lastfmapi/lastfmapi');

		// Pass the array to the auth class to return a valid auth

		$this->auth = new lastfmApiAuth('setsession');
		// pdump($this->auth);
		$this->apiClass = new lastfmApi();
		// pdump($this->apiClass);
		$artistClass = $this->apiClass->getPackage($this->auth, 'artist');
		$artists = $this->cron_m->get_all_performers();
		$i = 0;
        $a = 1;
		foreach ($artists as $p) :
            echo ($debug) ? "\n\r Running GET IMAGES... Pass : $a - ".count($artists) : '';
            $a += 1;
			if(!$this->cron_m->image_exists($p->PerformerID)):
				$methodVars = array('artist' => $p->Name);
				$aInfo = $artistClass->getInfo($methodVars);
				$imgs = array('PerformerID' => $p->PerformerID ,'lfmImagePath' => $aInfo['image']['extralarge']);
				$this->cron_m->insert_lfmpic($imgs);
				echo ($debug) ? "\n\r INSERTED: $p->Name - ".$aInfo['image']['extralarge'] : '';
				$i++;
			endif;
//			sleep(rand(1,5));
		endforeach;
		$runtime = (date('U') - $startTime)/60;
		return "<span style='color:green;'>Added $i performer images, for ". count($artists)." ran for $runtime minutes</span>";
		// mail($mailto, "Job Ran: Adding LFM images", $mess);
	}

	public function updateImagesChUrls($debug=true){
		if(!$this->input->is_cli_request()) die('Not allowed from URL');
		$startTime = date('U');
		$this->load->library('lastfmapi/lastfmapi');

		// Pass the array to the auth class to return a valid auth

		$this->auth = new lastfmApiAuth('setsession');
		// pdump($this->auth);
		$this->apiClass = new lastfmApi();
		// pdump($this->apiClass);
		$artistClass = $this->apiClass->getPackage($this->auth, 'artist');
		$artists = $this->cron_m->get_all_lfmpic();
		$i = 0;
		foreach ($artists as $p) :
			//echo ($debug) ? "\n\r Running GET IMAGES... Pass : $a - ".count($artists) : '';
			if (preg_match('/^http:\/\/img2-ak.lst.fm/', $p->lfmImagePath)) {
				$methodVars = array('artist' => $p->Name);
				$aInfo = $artistClass->getInfo($methodVars);
				//if ($this->url_exists($aInfo['image']['large'])) {*/
				echo $p->Name."\n\r";
				echo $p->lfmImagePath."\n\r";
				$i++;
				$this->cron_m->update_lfmpic($p->PerformerID, $aInfo['image']['extralarge']);
				echo $aInfo['image']['extralarge']."\n\r";
					//echo ($debug) ? "\n\r INSERTED: $p->Name - ".$aInfo['image']['large'] : '';
				//}
			}
//			sleep(rand(1,5));
		endforeach;
		echo "Cantidad de Urls actualizadas ".$i."\n\r";

	}

	public function updateImagesChUrlsv2($debug=true){
		if(!$this->input->is_cli_request()) die('Not allowed from URL');
		$startTime = date('U');
		$this->load->library('lastfmapi/lastfmapi');

		// Pass the array to the auth class to return a valid auth

		$this->auth = new lastfmApiAuth('setsession');
		// pdump($this->auth);
		$this->apiClass = new lastfmApi();
		// pdump($this->apiClass);
		$artistClass = $this->apiClass->getPackage($this->auth, 'artist');
		$artists = $this->cron_m->get_all_lfmpic();
		$i = 0;
		foreach ($artists as $p) :
			//echo ($debug) ? "\n\r Running GET IMAGES... Pass : $a - ".count($artists) : '';
			$methodVars = array('artist' => $p->Name);
			$aInfo = $artistClass->getInfo($methodVars);
			if ($this->url_exists($aInfo['image']['extralarge'])){
				echo $p->Name."\n\r";
				echo $p->lfmImagePath."\n\r";
				$this->cron_m->update_lfmpic($p->PerformerID, $aInfo['image']['extralarge']);
				echo $aInfo['image']['extralarge']."\n\r";
				$i++;
			}

//			sleep(rand(1,5));
		endforeach;
		echo "Cantidad de Urls actualizadas ".$i."\n\r";

	}

	//update frontend artist Images
	public function updateFrontendArtistImages($debug=true){
		if(!$this->input->is_cli_request()) die('Not allowed from URL');
		//$startTime = date('U');
		$this->load->library('lastfmapi/lastfmapi');

		// Pass the array to the auth class to return a valid auth

		$this->auth = new lastfmApiAuth('setsession');
		// pdump($this->auth);
		$this->apiClass = new lastfmApi();
		// pdump($this->apiClass);
		$artistClass = $this->apiClass->getPackage($this->auth, 'artist');
		$artists = $this->global_m->get_fetured_performers('front');
		$i = 0;
		foreach ($artists as $p) :
			//echo ($debug) ? "\n\r Running GET IMAGES... Pass : $a - ".count($artists) : '';
			$methodVars = array('artist' => $p->Name);
			$aInfo = $artistClass->getInfo($methodVars);
			if ($this->url_exists($aInfo['image']['extralarge'])){
				echo $p->Name."\n\r";
				//echo $p->lfmImagePath."\n\r";
				$this->cron_m->update_lfmpic($p->PerformerID, $aInfo['image']['extralarge']);
				echo $aInfo['image']['extralarge']."\n\r";
				$i++;
			}

//			sleep(rand(1,5));
		endforeach;
		echo "Cantidad de Urls actualizadas ".$i."\n\r";

	}

	public function getPerformersGeo($debug=true){
		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		$artists = $this->global_m->get_fetured_performers('geo');
		$i = 0;
		foreach ($artists as $p) :
			echo $p->Name."\n\r";
			echo $p->img."\n\r";
			$i++;
//			sleep(rand(1,5));
		endforeach;
		echo "Cantidad de Urls actualizadas ".$i."\n\r";

	}

	public function getPerformersFull($debug=true){
		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		$no = 0;

		//Read the file
		$file = fopen('/home/ubuntu/sites/concertfix/performers_offset.txt', 'r');
		$offset = fgets($file);
		fclose($file);

		$limit = 20000;
		$performers = $performers = $this->global_m->get_performer_full($limit, $offset);
		foreach ($performers as $performer) {
			$name = str_replace(array("\r", "\n", "-", "'", " "), "", $performer->Name);
			$name = str_replace("&", "and", $name);

			$url = "https://graph.facebook.com/".$name."/picture?type=large";
			$exist = $this->url_exists($url);
			if ($exist){
				echo $name.' => '.$url."\n\r";
				$this->global_m->update_perfomer_image($performer->pId, $url);
			}
			else {
				$name1 = $name.'Official';
				$url = "https://graph.facebook.com/".$name1."/picture?type=large";
				$exist = $this->url_exists($url);
				if ($exist){
					echo $name1.' => '.$url."\n\r";
					$this->global_m->update_perfomer_image($performer->pId, $url);
				}
				else {
					$name2 = 'Official'.$name;
					$url = "https://graph.facebook.com/".$name2."/picture?type=large";
					$exist = $this->url_exists($url);
					if ($exist){
						echo $name2.' => '.$url."\n\r";
						$this->global_m->update_perfomer_image($performer->pId, $url);
					}
					else {
						$name3 = $name.'music';
						$url = "https://graph.facebook.com/".$name3."/picture?type=large";
						$exist = $this->url_exists($url);
						if ($exist) {
							echo $name3.' => '.$url."\n\r";
							$this->global_m->update_perfomer_image($performer->pId, $url);
						}
						else {
							$name4 = $name.'Oficial';
							$url = "https://graph.facebook.com/".$name4."/picture?type=large";
							$exist = $this->url_exists($url);
							if ($exist) {
								echo $name4.' => '.$url."\n\r";
								$this->global_m->update_perfomer_image($performer->pId, $url);
							}
							else {
								echo $name." don't has an image ".$url."\n\r";
								$no += 1;
							}
						}
					}
				}
			}
		}

		$fp = fopen('/home/ubuntu/sites/concertfix/performers_offset.txt', 'w');
		fwrite($fp, ($limit + $offset));
		fclose($fp);

		echo "\n\r";
		echo "Total of performers without an url ".$no."\n\r";
	}

	public function getPerformersFull_default($debug=true){
		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		//Read the file
		/*$file = fopen('/home/ubuntu/sites/stagingdev/performers_offset.txt', 'r');
		$offset = fgets($file);
		fclose($file);*/

		$limit = 10000;
		$offset = 27227;
		$counter = 0;
		$no = 0;

		$dimage = 'https://concertfix.com/public/img/artist_profile.jpg';
		$performers = $performers = $this->global_m->get_performer_full_dimage($dimage, $limit, $offset);


		$suffix = ['Official', 'Official_left', 'music', 'band', 'live', 'comedian', 'comedy', 'Oficial'];
		foreach ($performers as $performer) {
			$name = str_replace(array("\r", "\n", "-", "'", " "), "", $performer->Name);
			$name = str_replace("&", "and", $name);
			$found = false;

			$url = "https://graph.facebook.com/".$name."/picture?type=large";
			$exist = $this->url_exists($url);
			if ($exist){
				echo $name.' => '.$url."\n\r";
				$this->global_m->update_perfomer_image($performer->pId, $url);
				$counter += 1;
			}
			else {
				$i = 0;
				while($i < count($suffix)){
					$nick = ($suffix[$i] == 'Official_left') ? 'Official'.$name : $name.$suffix[$i];
					$url = "https://graph.facebook.com/".$nick."/picture?type=large";
					$exist = $this->url_exists($url);
					if ($exist){
						$this->global_m->update_perfomer_image($performer->pId, $url);
						echo $nick.' => '.$url."\n\r";
						$counter += 1;
						break;
					}
					else
						$i += 1;
				}

				if (!$found) {
					echo $name." don't has an image ".$url."\n\r";
					$no += 1;
				}
			}
		}

		$fp = fopen('/home/ubuntu/sites/stagingdev/performers_offset.txt', 'w');
		fwrite($fp, ($limit + $offset));
		fclose($fp);

		echo "\n\r";
		echo "Total of performers without an url ".$no."\n\r";
	}

	public function getPerformersFull_FB($debug=true){
		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		$no = 0;

		//Read the file
		$file = fopen('/home/ubuntu/sites/stagingdev/performers_fb.txt', 'r');
		$counter = 0;
		while(!feof($file)) {
			$line = fgets($file);
			list($name, $username) = explode('=', $line);
			$username = trim($username);
			$name = trim($name);
			$url = "https://graph.facebook.com/{$username}/picture?type=large";
			$this->global_m->update_perfomer_image_by_name($name, $url);

			$counter += 1;
			echo "{$counter} {$name} {$url}\n\r";
		}

	}

	public function getSpecificPerformers($debug=true){
		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		$no = 0;

		$names = ['Penny Engine', 'The Rackatees', 'Diamante Electrico', 'Madison Ryann Ward', 'Gravespawn', 'Cody Lovaas', 'New Hope Club', 'Hellbound Glory'];
		$performers = $performers = $this->global_m->get_performer_full_by_name($names);

		$suffix = ['Official', 'Official_left', 'music', 'band', 'live', 'comedian', 'comedy', 'Oficial'];
		foreach ($performers as $performer) {
			$name = str_replace(array("\r", "\n", "-", "'", " "), "", $performer->Name);
			$name = str_replace("&", "and", $name);
			$found = false;

			$url = "https://graph.facebook.com/".$name."/picture?type=large";
			$exist = $this->url_exists($url);
			if ($exist){
				echo $name.' => '.$url."\n\r";
				$this->global_m->update_perfomer_image($performer->pId, $url);
			}
			else {
				$i = 0;
				while($i < count($suffix)){
					$nick = ($suffix[$i] == 'Official_left') ? 'Official'.$name : $name.$suffix[$i];
					$url = "https://graph.facebook.com/".$nick."/picture?type=large";
					$exist = $this->url_exists($url);
					if ($exist){
						$this->global_m->update_perfomer_image($performer->pId, $url);
						echo $nick.' => '.$url."\n\r";
						$found = true;
						break;
					}
					else
						$i += 1;
				}

				if (!$found) {
					echo $name." don't has an image ".$url."\n\r";
					$no += 1;
				}
			}
		}

		echo "\n\r";
		echo "Total of performers without an url ".$no."\n\r";
	}

	//Update Page Text
    public function updatePageText($debug=true) {
        if(!$this->input->is_cli_request()) die('Not allowed from URL');

        $this->soapy = new TN_soap();
        echo ($debug) ? "intialized soapy \n\r" : '';
        $this->load->model('tours_m');

        $texts = $this->tours_m->get_all_qna_text();
        $total = count($texts);
        echo "Total of texts {$total}"."\n\r";
        foreach ($texts as $text) {
            //echo $text->slug."\n\r";
            $urlParams = explode('+', $text->slug);
            if (count($urlParams) == 3) {
                $performerSlug = $urlParams[0];
                $venueSlug = $urlParams[1];
                $cityStateSlug = $urlParams[2];

                $performer = $this->tours_m->get_performer_by_slug($performerSlug);
                $venue = $this->tours_m->get_venue_by_city($venueSlug, $cityStateSlug);
                $location = deslug_location($cityStateSlug);

                if ($performer and $venue) {
                    $params = array(
                        'beginDate' => date('c'),
                        'orderByClause' => 'Date',
                        'performerID' =>  $performer[0]->PerformerID,
                        'venueID'		=> $venue->ID,
                        'whereClause' => 'CountryID = 217 OR CountryID = 38'
                    );

                    $tourDatesVenue = $this->soapy->run_soap('GetEvents', $params);

                    if ($tourDatesVenue) {
                        $whereClause = array('slug' => $text->slug, 'type' => 'qna');
                        $newText = $this->tours_m->_create_qna_text($performer, 'venue', $venue, $location, $tourDatesVenue[0]->Date);
                        $this->db->where($whereClause);
                        $this->db->delete('page_text');
                        $query_params = array_merge(
                            $whereClause,
                            array('text' => $newText, 'var_data' => '', 'created' => date('Y-m-d H:i:s'))
                        );
                        $this->db->insert('page_text', $query_params);

                        echo "Inserted text of {$text->slug}"."\n\r";
                    }
                }
            }

        }
    }

	//update frontend artist Images
	public function updateGeoArtistImages($debug=true){
		if(!$this->input->is_cli_request()) die('Not allowed from URL');
		//$startTime = date('U');
		$this->load->library('lastfmapi/lastfmapi');

		// Pass the array to the auth class to return a valid auth

		$this->auth = new lastfmApiAuth('setsession');
		// pdump($this->auth);
		$this->apiClass = new lastfmApi();
		// pdump($this->apiClass);
		$artistClass = $this->apiClass->getPackage($this->auth, 'artist');
		$artists = $this->global_m->get_fetured_performers('geo');
		$i = 0;
		foreach ($artists as $p) :
			//echo ($debug) ? "\n\r Running GET IMAGES... Pass : $a - ".count($artists) : '';
			$methodVars = array('artist' => $p->Name);
			$aInfo = $artistClass->getInfo($methodVars);
			if ($this->url_exists($aInfo['image']['extralarge'])){
				echo $p->Name."\n\r";
				//echo $p->lfmImagePath."\n\r";
				$this->cron_m->update_lfmpic($p->PerformerID, $aInfo['image']['extralarge']);
				echo $aInfo['image']['extralarge']."\n\r";
				$i++;
			}

//			sleep(rand(1,5));
		endforeach;
		echo "Cantidad de Urls actualizadas ".$i."\n\r";

	}
	//update frontend artist Images

	//Show performers are not listed as "geo" buy appear in tour announcements
	public function getAnnouncementsPerformersNoGeo($debug=true){
		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		$artists = $this->global_m->get_upcoming_tour_annoucement_without_geo();
		$i = 0;
		foreach ($artists as $p) :
			echo $p->Name."\n\r";
			echo $p->img."\n\r";
			$i++;
//			sleep(rand(1,5));
		endforeach;
		echo "Number of updated url ".$i."\n\r";

	}
	//End

	public function updateImagesWrUrls($debug=true){
		if(!$this->input->is_cli_request()) die('Not allowed from URL');
		$startTime = date('U');
		$this->load->library('lastfmapi/lastfmapi');

		// Pass the array to the auth class to return a valid auth

		$this->auth = new lastfmApiAuth('setsession');
		// pdump($this->auth);
		$this->apiClass = new lastfmApi();
		// pdump($this->apiClass);
		$artistClass = $this->apiClass->getPackage($this->auth, 'artist');
		$artists = $this->cron_m->get_all_lfmpic();
		$i = 0;
		foreach ($artists as $p) :
			//echo ($debug) ? "\n\r Running GET IMAGES... Pass : $a - ".count($artists) : '';
			if (!$this->url_exists($p->lfmImagePath)) {
				$methodVars = array('artist' => $p->Name);
				$aInfo = $artistClass->getInfo($methodVars);
				if ($this->url_exists($aInfo['image']['extralarge'])){
					echo $p->Name."\n\r";
					echo $p->lfmImagePath."\n\r";
					$this->cron_m->update_lfmpic($p->PerformerID, $aInfo['image']['extralarge']);
					echo $aInfo['image']['extralarge']."\n\r";
					$i++;
				}
				//echo ($debug) ? "\n\r INSERTED: $p->Name - ".$aInfo['image']['large'] : '';
			}

//			sleep(rand(1,5));
		endforeach;
		echo "Cantidad de Urls actualizadas ".$i."\n\r";

	}

	//run this daily gets venues from TN
	public function getvenues($mailto = 'ebergolla@gmail.com', $debug = true)
	{
		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		$newList = array();
		//load soap
		$this->soapy = new TN_soap();
		echo ($debug) ? "intialized soapy \n\r" : '';
		//call GetVenue
		$params = array();
		$venues = $this->soapy->run_soap('GetVenue', $params);
		echo ($debug) ? "got the list ... \n\r" : '';
		$i = 0;
		foreach ($venues as $v):


			// 'merica!
			if($v->Country == "United States of America" || $v->Country == "Canada"):
				$slug  = slug_venue($v->Name);
				$regionSlug = seoUrl(strtolower($v->City."-".$this->cron_m->short_region($v->StateProvince)));
				// echo ($debug) ? "\n\r $v->Country" : '';
				if(!$this->cron_m->venue_exists($v->ID, $slug, $regionSlug)):
					$i++;
					$venue = $v;
					$venue->VenueSlug = strtolower($slug);
					$venue->LastUpdated = date('Y-m-d');
					$venue->RegionSlug = $regionSlug;
					$this->cron_m->insert_venue($venue);
					// echo ($debug) ? " -  ".$venue->VenueSlug."+".$venue->RegionSlug."\n\r" : '';
				endif;
			endif;

		endforeach;
		return "<span style='color:green;'> Inserted $i Venues </span>";
	}

	// RUN WHEN TN IS DONE
	public function gettracks($mailto = 'ebergolla@gmail.com', $debug = true)
	{
		if(!$this->input->is_cli_request()) die('Not allowed from URL');
		mail($mailto, "Get Tracks Started...", "-na-");
		echo ($debug) ? "STARTED...\n\r" : '';
		$startTime = date('U');
		$this->load->library('lastfmapi/lastfmapi');

		// Pass the array to the auth class to return a valid auth

		$this->auth = new lastfmApiAuth('setsession');
		echo ($debug) ? "LIB LOADED \n\r" : '';
		// pdump($this->auth);
		$this->apiClass = new lastfmApi();
		// pdump($this->apiClass);
		$albumClass = $this->apiClass->getPackage($this->auth, 'album');
		echo ($debug) ? "CLASS INITED ALBUM \n\r" : '';
//		$albums = $this->cron_m->get_albums(false, false);
        $date = getdate();
        $wday = $date['wday'];
        $limit = 100000;
        switch($wday)
        {
            case 6:
                $offset = 0;
				break;
            case 7:
                $offset = 100000;
				break;
            case 1:
                $offset = 200000;
				break;
            case 2:
                $offset = 300000;
				break;
            case 3:
                $offset = 400000;
				break;
            default:
                $limit = null;
                $offset = 0;
        }

        if($limit != null) {
            $albums = $this->cron_m->get_albums_without_tracks($limit, $offset);
        }
        else{
            $albums = array();
        }

		echo ($debug) ? "GOT ALBUMS ".count($albums)." \n\r" : '';
		echo ($debug) ? "OFFSET - $offset"." \n\r" : '';
		$i = 1;
		// var_dump($albums);
        $pass = 1;
		foreach ($albums as $a) :
            echo ($debug) ? "Get Tracks ... Pass:  $pass - ".count($albums)." \n\r" : '';
            echo ($debug) ? "     Artist: $a->Name". " \n\r" : '';
            echo ($debug) ? "     Album: $a->AlbumName". " \n\r" : '';
            $pass += 1;
            if(!($i % 1000)):
                echo ($debug) ? "Sleeping a sec at $i \n\r" : '';
                $sleepingFor = rand(1,3);
                sleep($sleepingFor);
            endif;
//			if(!$this->cron_m->has_tracks($a->id, $a->tnPerformerID)):
                $methodVars = array('artist' => $a->Name, 'album' => $a->AlbumName);
                $albumInfo = $albumClass->getInfo($methodVars);
                $this->cron_m->update_release_date($a->id, $albumInfo['releasedate']);
                if(isset($albumInfo['tracks'])):
                    foreach ($albumInfo['tracks'] as $key => $track) :
                        $albumInfo['tracks'][$key]['album_id'] = $a->id;
                        $albumInfo['tracks'][$key]['tn_PerformerID'] = $a->tnPerformerID;
                    endforeach;
                    $this->cron_m->insert_tracks($albumInfo['tracks']);
                    echo $debug ? "     Inserted ". count($albumInfo['tracks'])." track(s) to album $a->AlbumName of artist $a->Name \n\r" : '';
                endif;
//				sleep(rand(1, 2));
                $i++;
//				if(!($i % 5000)):
//					mail($mailto, "$i processed so far", "going and going and going...");
//				endif;
//			endif;
		endforeach;
		return "<span style='color:green;'> Processed $i tracks...</span>";
	}

	//daily run
	public function getevents($mailto = 'ebergolla@gmail.com', $debug = true)
	{
		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		//load soap
		$this->soapy = new TN_soap();
		echo ($debug) ? "intialized soapy \n\r" : '';
		//call GetEvents
		$params = array(
						'beginDate' => date('c'),
 						'orderByClause' => 'Date',
 						'parentCategoryID' => 2,
 						'whereClause' => 'CountryID = 217 OR CountryID = 38'
			);
		$events = $this->soapy->run_soap('GetEvents', $params);


		echo ($debug) ? "got the list ...".count($events)." \n\r" : '';
		$inserts = 0;
		$updates = 0;
        $count = 1;
		foreach ($events as $event) :
            $this->cron_m->update_seating_chart($event->VenueID, $event->MapURL);
            $tmp = '';
            $tmp2 = '';
			if($event->ParentCategoryID == 2): // just in case garbage comes back
				// var_dump($event); die();
				$data = array(
						'ChildCategoryID'=> $event->ChildCategoryID,
						'City'=> $event->City,
						'Date'=> $event->Date,
						'GrandchildCategoryID'=> $event->GrandchildCategoryID,
						'EventID'=> $event->ID,
						'Name'=> $event->Name,
						'ParentCategoryID'=> $event->ParentCategoryID,
						'StateProvince'=> $event->StateProvince,
						'StateProvinceID'=> $event->StateProvinceID,
						'Venue'=> $event->Venue,
						'VenueID'=> $event->VenueID,
						'CountryID'=> $event->CountryID,
						'EventSlug'=> seoUrl($event->Name),
						'LocationSlug'	=> seoUrl($event->City." ".$event->StateProvince)
					);

				if($this->cron_m->event_exists($data['EventID'])):
					$this->cron_m->update_event($data);
					$updates++;
                    $tmp = 'update_event';
				else:
					$data['AddedDate'] = date('Y-m-d');
					$this->cron_m->insert_event($data);
					$inserts++;
                    $tmp = 'insert_event';
				endif;
				$params = array('eventID' => $event->ID);
				$prices = $this->soapy->run_soap('GetPricingInfo', $params);
				if($prices):
					$prices->EventID = $event->ID;
					$this->cron_m->update_insert_prices($prices);
                    $tmp2 = 'update_insert_prices';
				else:
					$prices->EventID = $event->ID;
				endif;
				// die();

			endif;
            echo 'Get events: Pass '.$count.'of '.count($events)." ".$data['Name']." Action ".$tmp." ".$tmp2. " \n\r" ;
            $count++;
		endforeach;
		return "<span style='color:green;'> INSERTED $inserts events and UPDATED $updates events. Total count from SOAP: ".count($events)."</span>";
		// mail($mailto, "SOAP Insert Events Completed", $mess);
	}

    public function test_getevents($debug = true){
        if(!$this->input->is_cli_request()) die('Not allowed from URL');

        //load soap
        $this->soapy = new TN_soap();
        echo ($debug) ? "intialized soapy \n\r" : '';
        //call GetEvents
        $params = array(
            'beginDate' => '2020-05-21T00:19:21+00:00',
            'orderByClause' => 'Date',
            'parentCategoryID' => 2,
            'whereClause' => 'CountryID = 217 OR CountryID = 38'
        );
        $events = $this->soapy->run_soap('GetEvents', $params);

        echo ($debug) ? "got the list ...".count($events)." \n\r" : '';

        try {
            $myfile = fopen("public/img/get_events.txt", "w");
        }
        catch(Exception $e)
        {
            echo "File can't open";
            die;
        }

        foreach ($events as $event) :
            fwrite($myfile, "Event ID {$event->ID}, Event Name {$event->Name} \n\r");
        endforeach;

        fclose($myfile);
    }

	//run this after getevents
	public function geteventperformers($mailto = 'ebergolla@gmail.com', $debug = true)
	{
		//load soap
		$this->soapy = new TN_soap();
		echo ($debug) ? "intialized soapy \n\r" : '';
		//call GetEventPerformers
		$params = array();
		$events = $this->soapy->run_soap('GetEventPerformers', $params);
		echo ($debug) ? "got the list ... \n\r" : '';
		return $this->cron_m->insert_event_performers($events->EventPerformer);
		// return true;
	}

    public function test_geteventperformers($debug = true){
        if(!$this->input->is_cli_request()) die('Not allowed from URL');

        //load soap
        $this->soapy = new TN_soap();
        echo ($debug) ? "intialized soapy \n\r" : '';
        //call GetEventPerformers
        $params = array();
        $events = $this->soapy->run_soap('GetEventPerformers', $params);

        echo ($debug) ? "got the list ...".count($events->EventPerformer)." \n\r" : '';

        try {
            $myfile = fopen("public/img/get_eventsperformers.txt", "w");
        }
        catch(Exception $e)
        {
            echo "File can't open";
            die;
        }

        foreach ($events->EventPerformer as $event) :
            fwrite($myfile, "Event ID {$event->EventID}, PerformerName {$event->PerformerName}, Performer ID {$event->PerformerID} \n\r");
        endforeach;

        fclose($myfile);
    }

	//daily run
	public function gethotsales($mailto = 'ebergolla@gmail.com', $debug = true)
	{
		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		$count = $this->cron_m->count_performers();
		//load soap
		$this->soapy = new TN_soap();
		echo ($debug) ? "intialized soapy \n\r" : '';
		//call GetEvents
		$params = array(
 						'parentCategoryID' => 2,
 						'numReturned'	=> (int)$count
			);
		//performer_hotsales
		$highSales = $this->soapy->run_soap('GetHighSalesPerformers', $params);
		$batch = array();
		// var_dump($highSales);
		// die();
		foreach ($highSales->PerformerPercent as $h) :
			$data = array(
			            'Percent'	=> $h->Percent,
						'PerformerID'	=> $h->ID
			              );
			array_push($batch, $data);
		endforeach;
		$this->cron_m->insert_high_sales($batch);
		return "<span style='color:green;'> Hot sales refreshed. </span>";
	}

	// daily run
	public function getinventory($mailto = 'ebergolla@gmail.com', $debug = true)
	{
		if(!$this->input->is_cli_request()) die('Not allowed from URL');
		$count = $this->cron_m->count_performers();
		//load soap
		$this->soapy = new TN_soap();
		echo ($debug) ? "intialized soapy \n\r" : '';
		//call GetEvents
		$params = array(
 						'parentCategoryID' => 2,
 						'numReturned'	=> (int)$count
			);
		//performer_hotsales
		$inventory = $this->soapy->run_soap('GetHighInventoryPerformers', $params);
		$batch = array();
		// var_dump($inventory);
		// die();
		foreach ($inventory->PerformerPercent as $h) :
			echo ($debug) ? $h->ID."\n\r" : '';
			$data = array(
			            'Percent'	=> $h->Percent,
						'PerformerID'	=> $h->ID
			              );
			array_push($batch, $data);
		endforeach;
		$this->cron_m->insert_inventory($batch);

		return "<span style='color:green;'> High Sale Inventory Refreshed </span>";
/*		$cats = $this->soapy->run_soap('GetCategoriesMasterList', $params);
		$data = array();*/
	}

	//weekly - cleans up empty featured
	public function cleanup($mailto = 'ebergolla@gmail.com', $debug = true)
	{
		if(!$this->input->is_cli_request()) die('Not allowed from URL');
		//clean featured performers
		$this->cron_m->clean_featured();
		$this->cron_m->remove_stale_events();
		$this->cron_m->update_counts();
		$this->cron_m->geo_event_count();
		$this->cron_m->slug_locations();
		$this->cron_m->remove_old_text();

	}


	public function trackinfo($run = 1, $i = false)
	{

		if(!$this->input->is_cli_request()) die('Not allowed from URL');
		$this->load->library('lastfmapi/lastfmapi');

		// Pass the array to the auth class to return a valid auth

		$this->auth = new lastfmApiAuth('setsession');
		// pdump($this->auth);
		$this->apiClass = new lastfmApi();
		// pdump($this->apiClass);
		$trackClass = $this->apiClass->getPackage($this->auth, 'track');
		//echo ($debug) ? "STARTED..." : '';
		mail("ebergolla@gmail.com", "Started 50k", date('F d, Y H:i:s'));

		$batch = $this->cron_m->get_tracks(50000);
		$time_start = microtime(true);
		$i = ($i) ? $i : 1;
		foreach ($batch as $track):

			$methodVars = array('artist' => $track->PerformerName, 'track' => $track->name);
			$trackInfo = $trackClass->getInfo($methodVars);
			// echo ($debug) ? "\n\r".$i : '';
			// var_dump($trackInfo);
			// die();
			$data = array('play_count' => $trackInfo['playcount'], 'listeners' => $trackInfo['listeners']);
			$this->cron_m->update_track_info($track->id, $data);
			$i++;
			if(!($i % 1000)):
				sleep(2);
			//echo ($debug) ? "\n\r Sleep 2 seconds, RUN: $run at $i tracks processed... " : '';
			endif;
		endforeach;
		$message = "\n\r". $run.'Completed. Total execution time in seconds: ' . (microtime(true) - $time_start);
		//echo ($debug) ? $message : '';
		mail("ebergolla@gmail.com", "Finished 50k", $message);

		$run++;
		if($run < 18): //recurse
			sleep(5);
			$this->trackinfo($run, $i);
		endif;
		return "<span style='color:green;'> Rcursively ran $run times for Track Info $i total </span>";
		// echo ($debug) ? " RAN ".date('U') - $start : '';
	}


	public function createaboutnew($mailto = 'ebergolla@gmail.com', $debug = true)
	{
		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		error_reporting(E_ALL);

		$list = array();

		$artists = $this->cron_m->get_about_pending();
		foreach ($artists as $artist):

			array_push($list, $artist->Name);
			$this->_generate_about_text($artist, $debug);

		endforeach;
		$this->cron_m->purge_about_pending();
		$mess = "<span style='color:green;'> Updated about text for following performers: ".implode('<br>', $list)." </span>";
		echo $mess;
		return $mess;

	}

	/*************************** Generating new texts ***************************************/

	public function create_album_text($debug = true){
		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		error_reporting(E_ALL);

		$performers = $this->cron_m->get_all_performers();
		$count = 0;
		foreach ($performers as $performer){
			$text = $this->_generate_album_text($debug, $performer);
			$count += 1;
			//echo "Performer # ".$count." ".$performer->Name."\n\r";;
			$data = array('PerformerID' => $performer->PerformerID, 'text' => $text);
			$this->cron_m->insert_about($data);
		}
		echo "Total of performers updated ".$count."\n\r";

	}

	/************************************ End **************************************************/

	private function _generate_about_text($artist, $debug = true)
	{

		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		$pId = $artist->PerformerID;

		$albums = $this->cron_m->get_albums(true, false, $pId, 'ReleaseDate', 'asc');
		$averageListen = $this->cron_m->get_albums_average($pId);
		$totAlbums = count($albums);
		$genre = ucwords(strtolower($artist->genre));
		$performer = $artist->Name;
		$performerz = (substr($performer, -1) == 's') ? $performer."'" : $performer."'s";


		switch ($genre) :
			case 'Festival / Tour':
			case 'Performance Series':
				$genre = "music";
				break;

			default:
				# code...
				break;
		endswitch;

		if($totAlbums):
			$firstAlbum = $albums[0];
			$lastAlbum = end($albums);
			$albumYears = date('Y', strtotime($lastAlbum->ReleaseDate)) - date('Y', strtotime($firstAlbum->ReleaseDate));
			$topSongs = $this->cron_m->get_top_songs($firstAlbum->id);


			switch(count($topSongs)):

				case 1:
					$doTopSongs = clean_song($topSongs[0]->name);
				break;

				case 2:
					$doTopSongs = clean_song($topSongs[0]->name)." and ".clean_song($topSongs[1]->name);
				break;

				case 3:
				default:
					$doTopSongs = clean_song($topSongs[0]->name).", ".clean_song($topSongs[1]->name).", and ".clean_song($topSongs[2]->name);
				break;
			endswitch;




			$topAlbum = $this->cron_m->get_albums(true, false, $pId, 'PlayCount','desc');

			$topSongsT = $this->cron_m->get_top_songs($topAlbum[0]->id);

			switch(count($topSongsT)):

				case 1:
					$doTopSongsT = clean_song($topSongsT[0]->name);
				break;

				case 2:
					$doTopSongsT = clean_song($topSongsT[0]->name)." and ".clean_song($topSongsT[1]->name);
				break;

				case 3:
				default:
					$doTopSongsT = clean_song($topSongsT[0]->name).", ".clean_song($topSongsT[1]->name).", and ".clean_song($topSongsT[2]->name);
				break;
			endswitch;

			echo ($debug) ? $performer." WITH ".$totAlbums ." albums and " . count($topSongsT)." top songs ... \n\r" : '';

            $first_album_date = getdate($firstAlbum->ReleaseDate);
            $current_date = getdate();
            if($first_album_date['year'] == 1970 || $first_album_date['year'] > $current_date['year']) {
                $first_album_date = 'N/A';
            }
            else{
                $first_album_date = date('F j, Y', strtotime($firstAlbum->ReleaseDate));
            }

			$s1 = "{$performer} ".
			random_pop(array("arrived","came","showed up"))." on to the {$genre} scene with the ". random_pop(array("debut","release","appearance","introduction"))." of tour album \"{$firstAlbum->AlbumName}\", ". random_pop(array("released","published"))." on ".$first_album_date.". The song ".clean_song($topSongs[0]->name)." ". random_pop(array("was instantly","instantly became"," immediatley became"))." a ". random_pop(array("success","hit"))." and made {$performer} one of the ". random_pop(array("new","newest","top new","top emerging"))." great ". random_pop(array("concerts","shows"))." to ". random_pop(array("attend","witness","experience","go to")).".";
		endif;

		if($totAlbums > 2): // 3 or more
			$secondAlbum = $albums[1];
			$topSongs2 = $this->cron_m->get_top_songs($secondAlbum->id);

			switch(count($topSongs2)):

				case 1:
					$doTopSongs2 = clean_song($topSongs2[0]->name);
				break;

				case 2:
					$doTopSongs2 = clean_song($topSongs2[0]->name)." and ".clean_song($topSongs2[1]->name);
				break;

				case 3:
				default:
					$doTopSongs2 = clean_song($topSongs2[0]->name).", ".clean_song($topSongs2[1]->name).", and ".clean_song($topSongs2[2]->name);
				break;
			endswitch;

            $second_album_date = getdate($secondAlbum->ReleaseDate);
            $current_date = getdate();
            if($second_album_date['year'] == 1970 || $second_album_date['year'] > $current_date['year']) {
                $second_album_date = 'N/A';
            }
            else{
                $second_album_date = date('F j, Y', strtotime($secondAlbum->ReleaseDate));
            }

			$s1 .= " ".random_pop(array("After","Following","Subsequently following"))." the ".random_pop(array("debut","release","appearance","introduction"))." of \"{$firstAlbum->AlbumName}\", {$performer} ".random_pop(array("released","announced","published","revealed"))." \"{$secondAlbum->AlbumName}\" on ".$second_album_date.". ";
			if($secondAlbum->PlayCount <= $averageListen): //second album sucked donkey balls
			$s1 .= " ".random_pop(array("Its fair to say","Its reasonable to say"))." that \"{$secondAlbum->AlbumName}\" was not ".random_pop(array("one of the more","among the more","amongst the more"))." ".random_pop(array("popular","mainstream","well known"))." tour albums, although the ".random_pop(array("song","track","single","tune"))." ".clean_song($topSongs2[0]->name)." did get a ".random_pop(array("fair","decent","resonable","noteworthy"))." amount of ".random_pop(array("circulation","attention","distribution"))." and is a ".random_pop(array("big hit","hit","fan favorite","crowd pleaser","crowd favorite"))." at every ".random_pop(array("show","performance","concert")).". ";
			else: //second album did not suck donkey balls

			//

			$s1 .= " The album \"{$secondAlbum->AlbumName}\" ".random_pop(array("stayed as","remained","is still","continues to be"))." one of the ".random_pop(array("favorite","more popular","more beloved","more admired","higly regarded"))." tour albums from {$performer}. The Tour Albums three top ".random_pop(array("songs","tracks","singles","tunes"))." included ".$doTopSongs2." and are a ".random_pop(array("big hit","hit","fan favorite","crowd pleaser","crowd favorite"))." at every ".random_pop(array("show","performance","concert")).". ";
			endif;
			$s1 .= " {$performer} has ".random_pop(array("released","published"))." ".($totAlbums - 2)." more tour albums since \"{$secondAlbum->AlbumName}\". ".random_pop(array("After","With over"))." {$albumYears} years of albums, {$performer} ".random_pop(array("top","most popular","best"))." tour album has been \"{$topAlbum[0]->AlbumName}\" and some of the ".random_pop(array("top","most popular","best"))." concert songs ".random_pop(array("are","have been"))." ".$doTopSongsT.". ";
		elseif($totAlbums == 2): // only two
			$secondAlbum = $albums[1];
			$topSongs2 = $this->cron_m->get_top_songs($secondAlbum->id);

			switch(count($topSongs2)):

				case 1:
					$doTopSongs2 = clean_song($topSongs2[0]->name);
				break;

				case 2:
					$doTopSongs2 = clean_song($topSongs2[0]->name)." and ".clean_song($topSongs2[1]->name);
				break;

				case 3:
				default:
					$doTopSongs2 = clean_song($topSongs2[0]->name).", ".clean_song($topSongs2[1]->name).", and ".clean_song($topSongs2[2]->name);
				break;
			endswitch;

            $second_album_date = getdate($secondAlbum->ReleaseDate);
            $current_date = getdate();
            if($second_album_date['year'] == 1970 || $second_album_date['year'] > $current_date['year']) {
                $second_album_date = 'N/A';
            }
            else{
                $second_album_date = date('F j, Y', strtotime($secondAlbum->ReleaseDate));
            }

			$s1 .= " After the ".random_pop(array("debut","release","appearance","introduction"))." of \"{$firstAlbum->AlbumName}\", {$performer} ".random_pop(array("released","announced","published","revealed"))." \"{$secondAlbum->AlbumName}\" on ".$second_album_date.". ";

			if($secondAlbum->PlayCount <= $averageListen): //second album sucked donkey balls
				$s1 .= " \"{$secondAlbum->AlbumName}\" ".random_pop(array("did not have","was not","did not carry"))." as ".random_pop(array("much","heavy","great"))." of an impact on its ".random_pop(array("fans","followers","listeners"))." as \"{$firstAlbum->AlbumName}\" as of yet, although the song ".clean_song($topSongs2[0]->name)." did ".random_pop(array("get","receive"))." a ".random_pop(array("fair","decent","resonable","noteworthy"))." amount of ".random_pop(array("circulation","attention","distribution"))." and is a ".random_pop(array("big hit","hit","fan favorite","crowd pleaser","crowd favorite"))." at every ".random_pop(array("show","performance","concert")).". ";

			else:
				$s1 .= " The album \"{$secondAlbum->AlbumName}\" ".random_pop(array("exceeded","surpassed","eclipsed"))." the ".random_pop(array("popularity","sucess"))." of first album \"{$firstAlbum->AlbumName}\". Its three top songs included ".$doTopSongs2." and are a ".random_pop(array("big hit","hit","fan favorite","crowd pleaser","crowd favorite"))." at every ".random_pop(array("show","performance","concert")).".  ";

			endif;
			$s1 .= " \"{$secondAlbum->AlbumName}\" is the ".random_pop(array("newest","most recent","latest"))." tour album ".random_pop(array("released","published"))." by {$performer}. {$performerz} ".random_pop(array("top","most popular","biggest"))." ".random_pop(array("songs","tracks","hits"))." are ".$doTopSongsT.". ";
		elseif($totAlbums == 1): // only album
			$s1 .= " \"{$firstAlbum->AlbumName}\" is the only album ".random_pop(array("released","published"))." by {$performer}. The ".random_pop(array("top","most popular","biggest"))." ".random_pop(array("songs","tracks","hits"))." remain ".$doTopSongs." which are a ".random_pop(array("big hit","hit","fan favorite","crowd pleaser","crowd favorite"))." at every ".random_pop(array("show","performance","concert")).". ";
		else: // most likely 0 albums
			$s1 = "  Currently {$performer} does not have any albums ";
		endif;

		// var_dump($albums);
		$data = array('PerformerID' => $pId, 'text' => $s1);
		$this->cron_m->insert_about($data);
		// die();
		// echo ($debug) ? "\n\r".$performer : '';
	}
	//this should run after new perfs
	//

	public function fixabout() {
		$artists = $this->cron_m->get_incorrect_about_performers();

		$list = array();

		if (!empty($artists)) {
			foreach ($artists as $artist) {
				array_push($list, $artist->Name);
				$this->cron_m->insert_about_pending(array('PerformerID' => $artist->PerformerID));
			}
			// Run fixing
			//$this->createaboutnew();
		}

		return "<span style='color:green;'> Fixed about text for following performers: ".implode('<br>', $list)." </span>";
	}

	public function createabout($mailto = 'ebergolla@gmail.com', $debug = true, $overwrite = false)
	{

		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		$list = array();
		$artists = $this->cron_m->get_all_performers();
		foreach ($artists as $artist):
			$pId = $artist->PerformerID;
			//if text is not generated
			if(!$this->cron_m->about_text_exists($pId)):
				array_push($list, $artist->Name);
				echo "\n\r $artist->Name";
				$this->_generate_about_text($artist, $debug);
			endif;

		endforeach;
		return "<span style='color:green;'> Created about text for following performers: ".implode('<br>', $list)." </span>";
	}

	public function catchup_about($mailto = 'ebergolla@gmail.com', $debug = true, $overwrite = false)
	{
		if(!$this->input->is_cli_request()) die('Not allowed from URL');
		$list = array();
		$artists = $this->cron_m->catchup_about();
		foreach ($artists as $artist):
			$pId = $artist->PerformerID;
			//if text is not generated
			if(!$this->cron_m->about_text_exists($pId)):
				array_push($list, $artist->Name);
				echo "\n\r $artist->Name";
				$this->_generate_about_text($artist, $debug);
			endif;

		endforeach;
		if($debug):
			echo "<span style='color:green;'> Creating about album text that are missing for some fucking reason..: ".implode('<br>', $list)." </span>";
		endif;
		return "<span style='color:green;'> Creating about album text that are missing for some fucking reason..: ".implode('<br>', $list)." </span>";
	}

	//DO NOT RUN THIS
/*	public function getgenres($mailto = 'ebergolla@gmail.com', $debug = true)
	{
		return false;
		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		//load soap
		$this->soapy = new TN_soap();
		echo ($debug) ? "intialized soapy \n\r" : '';
		//call GetEvents
		$params = array(
 						'parentCategoryID' => 2,
			);
		$cats = $this->soapy->run_soap('GetCategoriesMasterList', $params);
		$data = array();

		foreach ($cats->Category as $c) {

			if($c->ParentCategoryID == 2):
				$item = array('ChildCategoryID' => $c->ChildCategoryID, 'Filter' => $c->ChildCategoryDescription, 'Slug' => seoUrl($c->ChildCategoryDescription));
				$this->db->insert('category_filters', $item);

				array_push($data, $item);
			endif;
		}
			// $this->db->insert_batch('category_filters', $data);
	}*/

	//DO NOT RUN THIS ONLY FOR UPDATES MANUALLY
	public function nearby($mailto = 'ebergolla@gmail.com', $debug = true)
	{
		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		return false;

		$allItems = $this->cron_m->get_all_cities();
		$this->cron_m->nearby_truncate();
		$i = 0;
		$countt = count($allItems);
		$startTime = date('U');
		echo ($debug) ? "\n\rTRUNCATED ...started ". $countt : '';
		mail("ebergolla@gmail.com,john@lightningmarketinggroup.com", "NEARBY STARTED  {$countt} this will run for few hrs");
		foreach ($allItems as $city):
			$i++;
			$nearby = $this->cron_m->get_nearby_cities($city, 100, $limit = 50, false);

			echo ($debug) ? "\n\r $i is $city[slug] with ".count($nearby). " nearby cities " : '';

			foreach ($nearby as $key => $nb) :
				$nearby[$key]['centerSlug'] = $city['slug'];
			endforeach;
			$this->cron_m->insert_nearby($nearby);
			if($i == 100):
				$runtime = (date('U') - $startTime)/60;
 				$message = "\n\r did $i of $countt nearby cities for $runtime minutes averaging  ". $i/$runtime. " per minute \n\r NEXT UPDATE every 10,000 records..";
				mail("ebergolla@gmail.com,john@lightningmarketinggroup.com", "Did 1k ", $message);
			endif;
			if(!$i % 10000):
				$runtime = (date('U') - $startTime)/60;
 				$message = "\n\r did $i of $countt nearby cities for $runtime minutes averaging  ". $i/$runtime. " per minute ";
				mail("ebergolla@gmail.com,john@lightningmarketinggroup.com", "Did 1k ", $message);
 			endif;
		endforeach;

	}

	//DO NOT RUN THIS
	//this should never run, was created for manual pic resizing
/*	public function getpicsforresize($action = 'get')
	{
		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		return false;

		if($action = 'get'):
			$perfs = $this->cron_m->get_perfs_imgs();
			foreach ($perfs as $p) :
				echo ($debug) ? ("https://s3.amazonaws.com/ssglobalcdn/{$p->profile} ". " /home/sites/stubstock/pix/{$p->profile}") : '';
				if(copy("https://s3.amazonaws.com/ssglobalcdn/{$p->profile}", "/home/sites/stubstock/pix/{$p->profile}")):
					echo ($debug) ? "\n\r copyin $p->profile " : '';
				else:
					echo ($debug) ? "failed" : '';
					die();
				endif;
				copy("https://s3.amazonaws.com/ssglobalcdn/{$p->thumb}", "/home/sites/stubstock/pix/{$p->thumb}");
				echo ($debug) ? "\n\r copyin $p->thumb " : '';
				copy("https://s3.amazonaws.com/ssglobalcdn/{$p->wide}", "/home/sites/stubstock/pix/{$p->wide}");
				echo ($debug) ? "\n\r copyin $p->wide " : '';
			endforeach;
		else:

		endif;
	}*/

	//DO NOT RUN THIS
	//was created for manual picture resizing
/*	public function requality($path = "/home/sites/stubstock/pix/performers/")
	{
		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		return false;

		$sets = array('profile', 'wide', 'thumb');
		$bucket = "ssglobalcdn";
		$manipulate = false;
		$upload = false;

		$static = "/home/sites/stubstock/pix";

		foreach ($sets as $set):
			$current = "performers/".$set;
			$fullPath = $static."/".$current;
			$arrays = glob($fullPath.'/*.jpg');
			// var_dump($images);
			//
			//
			//
			foreach($arrays as $img) :
				$aws = str_replace("/home/sites/stubstock/pix/", '', $img);
				$image = new Imagick($img);
				$sizekb = $image->getImageSize();
				$size = $sizekb / 1024;

			if($manipulate):
				switch ($set) :
				case 'profile':
					$h = 400;
					$w = 400;
					$image->scaleImage($w,$h);
					// echo ($debug) ? ($image->writeImage($img)) ? "\n\r written $img " : "\n\r failed on $img " : '';
					break;
				case 'wide':
					$h = 442;
					$w = 900;
					$image->scaleImage($w,$h);
					// echo ($debug) ? ($image->writeImage($img)) ? "\n\r written $img " : "\n\r failed on $img " : '';
				break;
				case 'thumb':
					$h = 100;
					$w = 100;
					$image->scaleImage($w,$h);
					// echo ($debug) ? ($image->writeImage($img)) ? "\n\r written $img " : "\n\r failed on $img " : '';
				break;
				default:

					break;
				endswitch;
			//
					$image->setImageCompressionQuality(80);
					echo ($debug) ? ($image->writeImage($img)) ? "\n\r written $img " : "\n\r failed on $img " : '';
					// $image->writeImage($img);
			endif;

			if($upload):
				$input = $this->s3->inputFile($img);
				if($this->s3->putObject($input, $bucket, $aws, "public-read", array(), $header)):
					echo ($debug) ? "\n UPLOADED $img to $aws " : '';
				endif;
			endif;

				 echo ($debug) ? "\n \"{$aws}\",\"".number_format($size, 2, '.', ','). "\",\"".number_format($sizekb, 2, '.', ',')."\"" : '';
			endforeach;

		//	$images->writeImages();
		endforeach;
	}*/

	private function _fixmissing()
	{
		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		return  $this->cron_m->fix_nearby_missing();
	}

	/***********************************EMAIL NOTIFICATIONS!!!*****************************/
	public function create_queue($mailto = 'ebergolla@gmail.com', $debug = true)
	{
		if(!$this->input->is_cli_request()) die('Not allowed from URL');

        echo 'Intro create_queue'."\n\r";

		//get all users w performers and locations
		$users = $this->user_m->get_all_users();
        echo "Users Count: ".count($users). "\n\r";
        $count = 1;
        $notices = 0;
		foreach ($users as $u):
            echo "Users... Pass ".$count." of ". count($users)."\n\r";
            echo 'Get users rules'."\n\r";
            echo "User: ".$u->name."\n\r";
            echo "User email: ".$u->primary_email."\n\r";
			$performers = $this->user_m->get_user_rules($u->id, 'performer');
			$locations = $this->user_m->get_user_rules($u->id, 'city');
            echo 'End users rules'."\n\r";
			$events14 = false;
			$events60 = false;
			$announced = false;
			if($u->remind14):
				$events14 = $this->rules_m->get_upcoming_events($performers,$locations, 14);
				if($events14) $this->notifier_m->add_notification_log($events14, $u->id, 'reminder_2');
			endif;
			if($u->remind60):// remind  60 days
				$events60 = $this->rules_m->get_upcoming_events($performers,$locations, 60);
				if($events60) $this->notifier_m->add_notification_log($events60, $u->id, 'reminder_1');
			endif;
			$announced = $this->rules_m->get_upcoming_events($performers, $locations, false, $u->id);
			if($announced) $this->notifier_m->add_notification_log($announced, $u->id);
			$data = array('events14' => $events14, 'events60' => $events60, 'announced' => $announced,
				'name' => $u->name, 'tracking' => '?utm_source=email&utm_medium=remindperformer&utm_campaign=tickets');

            echo " Event 14: ".( (is_array($events14)) ? count($events14) : "0")."\n\r";
            echo " Event 60: ".( (is_array($events60)) ? count($events60) : "0")."\n\r";
            echo " Announcements: ".( (is_array($announced)) ? count($announced) : "0")."\n\r";

            is_array($events14) ? $notices += count($events14) : $notices +=0;
            is_array($events60) ? $notices += count($events60) : $notices +=0;
            is_array($announced) ? $notices += count($announced) : $notices +=0;
			//if(count($performers) > 0):
			if($events14 || $events60 || $announced):
                echo "Load email template \n\r";

				$subject = "Upcoming Concert Announcements Near You!";
				if ($events14 && !$events60 && !$announced){
					$performers = $this->seeking_performers_in_events($events14, $performers);
					$performer = $this->rules_m->get_performer_name($performers[0]);
					if (count($performers) > 1)
						$subject = "{$performer->PerformerName} & Others Performing in Two Weeks!";
					elseif (count($performers) == 1)
						$subject = "{$performer->PerformerName} Performing in Two Weeks!";
				} elseif (!$events14 && $events60 && !$announced) {
					$performers = $this->seeking_performers_in_events($events60, $performers);
					$performer = $this->rules_m->get_performer_name($performers[0]);
					if (count($performers) > 1)
						$subject = "{$performer->PerformerName} & Others Performing in Two Months!";
					elseif (count($performers) == 1)
						$subject = "{$performer->PerformerName} Performing in Two Months!";
				} elseif (!$events14 && !$events60 && $announced) {
					$performers = $this->seeking_performers_in_events($announced, $performers);
					$performer = $this->rules_m->get_performer_name($performers[0]);
					$data['tracking'] = '?utm_source=email&utm_medium=newperformer&utm_campaign=tickets';
					if (count($performers) > 1)
						$subject = "{$performer->PerformerName} & Others Just Announced New Concerts!";
					elseif (count($performers) == 1)
						$subject = "{$performer->PerformerName} Just Announced New Concerts!";
				}

                //$subject = "Upcoming Concert Announcements Near You!";
				$body = $this->load->view('email_templates/event_notification', $data, true);

				$templateData = array('name' => $u->name, 'email' => $u->primary_email);
                //$message = buildMessage($templateData, $subject, $body);
                echo 'after message'."\n\r";
                try {
                    /*$mdrConnection = $this->mandrill->create();
                    $async = false;
                    $ip_pool = 'Main Pool';
                    $send_at = NULL; // '2001-12-12 12:12:12';
                    $result = $mdrConnection->messages->send($message, $async);*/
					$this->sendEmailSparkpost($u->primary_email, $subject, $body);

                } catch(Exception $e)
                {
//                    $this->notifier_m->change_status(2,$email->id);
//                    $failed++;
                }
                echo "Email finished". "\n\r";

				//$this->notifier_m->add_email_to_queue($subject, $body, $u->primary_email);

                echo "END Load email template \n\r";
			endif;
            $count ++;
		endforeach;

		return "<li>Created Queue: <br><strong>Users:</strong> ".count($users)."<br><strong>Notices:</strong> ".$notices." items</li>";
	}

	public function send_redhotcruiseplanners($debug = true){

		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		echo 'Intro create_queue'."\n\r";

		//get all users w performers and locations
		$users = $this->user_m->get_all_users();
		echo "Users Count: ".count($users). "\n\r";
		$count = 1;
		foreach($users as $u){
			echo "Users... Pass ".$count." of ". count($users)."\n\r";
			echo 'Get users rules'."\n\r";
			echo "User: ".$u->name."\n\r";
			echo "User email: ".$u->primary_email."\n\r";

			$data = array('name' => $u->name);
			echo "Load email template \n\r";
			$body = $this->load->view('email_templates/redhotcruiseplanners', $data, true);
			$subject = "CONCERTFIX Parent Company Acquires Travel Website, Red Hot Cruise Planners";

			$templateData = array('name' => $u->name, 'email' => $u->primary_email);
			$message = buildMessage($templateData, $subject, $body);
			echo 'after message'."\n\r";
			try {
				$mdrConnection = $this->mandrill->create();
				$async = false;
				$ip_pool = 'Main Pool';
				$send_at = NULL; // '2001-12-12 12:12:12';
				$result = $mdrConnection->messages->send($message, $async);
				echo 'Succeful'."\n\r";
			} catch(Mandrill_Error $e)
			{
				echo 'Wrong'."\n\r";
			}
			$count ++;
		}

	}

	public function send_concerttracker_press_release($debug = true){

		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		echo 'Intro create_queue'."\n\r";

		//get all users w performers and locations
		$users = $this->user_m->get_all_users();
		echo "Users Count: ".count($users). "\n\r";
		$count = 1;
		foreach($users as $u){
			echo "Users... Pass ".$count." of ". count($users)."\n\r";
			echo 'Get users rules'."\n\r";
			echo "User: ".$u->name."\n\r";
			echo "User email: ".$u->primary_email."\n\r";

			$data = array('name' => $u->name);
			echo "Load email template \n\r";
			$body = $this->load->view('email_templates/concerttracker', $data, true);
			$subject = "New Tracking Feature Added: Concerts by Home City!";

			$templateData = array('name' => $u->name, 'email' => $u->primary_email);
			$message = buildMessage($templateData, $subject, $body);
			echo 'after message'."\n\r";
			try {
				$mdrConnection = $this->mandrill->create();
				$async = false;
				$ip_pool = 'Main Pool';
				$send_at = NULL; // '2001-12-12 12:12:12';
				$result = $mdrConnection->messages->send($message, $async);
				echo 'Succeful'."\n\r";
			} catch(Mandrill_Error $e)
			{
				echo 'Wrong'."\n\r";
			}
			$count ++;
		}

	}

	public function send_notifications()
	{
		if(!$this->input->is_cli_request()) die('Not allowed from URL');
		$ok = 0;
		$failed = 0;
		$emails = $this->notifier_m->get_emails();
        echo 'Intro send_notifications'."\n\r";
		foreach ($emails as $email) :
			$templateData = array('name' => $email->name, 'email' => $email->email);
            echo 'intro for'."\n\r";
			$message = buildMessage($templateData, $email->subject, $email->body);
            echo 'after message'."\n\r";
			try {
				$mdrConnection = $this->mandrill->create();
				$async = false;
			    $ip_pool = 'Main Pool';
			    $send_at = NULL; // '2001-12-12 12:12:12';
			    $result = $mdrConnection->messages->send($message, $async);
				$this->notifier_m->change_status(1,$email->id);
				$ok++;
			} catch(Mandrill_Error $e)
			{
			    $this->notifier_m->change_status(2,$email->id);
			    $failed++;
			}
		endforeach;
		return "<li>Notifications Sent:<br><strong>OK: </strong>{$ok}<br><strong>Failed: </strong>{$failed}</li>";
	}

	public function send_upcoming_concerts($debug = true){
		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		echo 'Intro create_queue'."\n\r";

		//get all users w performers and locations
		$users = $this->user_m->get_upcoming_concerts_users();

		/*$ids = array('2134');
		$users = $this->user_m->get_upcoming_concerts_users_by_id($ids);*/

		echo "Users Count: ".count($users). "\n\r";
		$count = 1;
		$notices = 0;
		foreach ($users as $u):
			echo "Users... Pass ".$count." of ". count($users)."\n\r";
			echo 'Get users rules'."\n\r";
			echo "User: ".$u->name."\n\r";
			echo "User email: ".$u->primary_email."\n\r";
			echo "User home city: ".$u->cityName."\n\r";
			//$locations = $this->user_m->get_user_rules($u->id, 'city');
			$locations = $this->user_m->get_home_nearby_cities($u->citySlug, $u->miles);
			//print_r($locations)."\n\r";

			$announced  = false;
			$genres = $this->rules_m->get_genres_by_userid($u->id);
			$genres_values = array();
			foreach($genres as $genre){
				$genres_values[] = $genre->category_id;
			}
			echo " Genres: ".count($genres_values)."\n\r";
			//print_r($genres_values)."\n\r";
			$announced = $this->rules_m->get_upcoming_events_by_genres($genres_values, $locations, $u->id);
			$data = array('announced' => $announced, 'name' => $u->name);

			echo "Announces: ".count($announced)."\n\r";

			if($announced):
				echo "Load email template \n\r";
				$body = $this->load->view('email_templates/upcoming_concerts', $data, true);
				$range_beg = date('M d');
				$range_end = date('M d', strtotime($range_beg.' + 6 days'));
				$subject = "Upcoming Concerts near {$u->cityName} {$range_beg} - {$range_end}";


				$templateData = array('name' => $u->name, 'email' => $u->primary_email);
				//$message = buildMessage($templateData, $subject, $body);
				echo 'after message'."\n\r";
				try {
					/*$mdrConnection = $this->mandrill->create();
					$async = false;
					$ip_pool = 'Main Pool';
					$send_at = NULL; // '2001-12-12 12:12:12';
					$result = $mdrConnection->messages->send($message, $async);*/
					$this->sendEmailSparkpost($u->primary_email, $subject, $body);

				} catch(Exception $e)
				{
					echo "Problem with the email server, Sparkpost"."\n\r";
				}
				echo "Email finished". "\n\r";

				//$this->notifier_m->add_email_to_queue($subject, $body, $u->primary_email);

				echo "END Load email template \n\r";
			endif;
			$count ++;
		endforeach;

		echo "Total of Users: ".count($users)."\n\r";

	}

	public function send_new_concerts($debug = true){
		//if(!$this->input->is_cli_request()) die('Not allowed from URL');

		echo 'Intro create_queue'."\n\r";

		//Just to test
		//$this->rules_m->change_last_email($_GET['userid'], $_GET['date']);

		//get all users w performers and locations
		$users = $this->user_m->get_new_concerts_users();

		/*$ids = array('2134');
		$users = $this->user_m->get_upcoming_concerts_users_by_id($ids);*/

		echo "Users Count: ".count($users). "\n\r";
		$count = 1;
		$notices = 0;
		foreach ($users as $u):
			echo "Users... Pass ".$count." of ". count($users)."\n\r";
			echo 'Get users rules'."\n\r";
			echo "User: ".$u->name."\n\r";
			echo "User email: ".$u->primary_email."\n\r";
			$locations = $this->user_m->get_home_nearby_cities($u->citySlug, $u->miles);

			$genres = $this->rules_m->get_genres_by_userid($u->id);
			$genres_values = array();
			foreach($genres as $genre){
				$genres_values[] = $genre->category_id;
			}
			echo " Genres: ".count($genres_values)."\n\r";

			switch ($u->new_concert):
				case 1:
					$announced = $this->rules_m->get_new_events_by_genres($genres_values, $locations, 1, $u->id);
					echo 'Once a day'."\n\r";
					break;
				case 2:
					$announced = $this->rules_m->get_new_events_by_genres($genres_values, $locations, 7, $u->id);
					echo 'Once a week'."\n\r";
					break;
				case 3:
					$announced = $this->rules_m->get_new_events_by_genres($genres_values, $locations, 30, $u->id);
					echo 'Once a month'."\n\r";
					break;

				default:

					break;
			endswitch;

			$data = array('announced' => $announced, 'name' => $u->name);

			echo "Announces: ".count($announced)."\n\r";

			if($announced):
				echo "Load email template \n\r";
				$body = $this->load->view('email_templates/new_concerts', $data, true);
				$subject = "New Concerts Just Announced near " .$u->cityName."!";


				$templateData = array('name' => $u->name, 'email' => $u->primary_email);
				//$message = buildMessage($templateData, $subject, $body);
				echo 'after message'."\n\r";
				try {
					/*$mdrConnection = $this->mandrill->create();
					$async = false;
					$ip_pool = 'Main Pool';
					$send_at = NULL; // '2001-12-12 12:12:12';
					$result = $mdrConnection->messages->send($message, $async);*/
					$this->sendEmailSparkpost($u->primary_email, $subject, $body);

					$this->rules_m->change_last_email($u->id, date('Y-m-d'));

				} catch(Mandrill_Error $e)
				{
//                    $this->notifier_m->change_status(2,$email->id);
//                    $failed++;
				}
				echo "Email finished". "\n\r";

				//$this->notifier_m->add_email_to_queue($subject, $body, $u->primary_email);

				echo "END Load email template \n\r";
			endif;
			$count ++;
		endforeach;

		echo "Total of Users: ".count($users)."\n\r";

	}

	public function show_user_information(){

		$email = $this->input->get('email');

		$users = $this->user_m->get_user($email);
		echo "User count: ".count($users)."\n\r";
		$count = 1;
		$notices = 0;
		foreach ($users as $u):
			echo 'Get users rules'."\n\r";
			echo "User: ".$u->name."\n\r";
			echo "User performers: "."\n\r";
			$performers = $this->user_m->get_user_rules($u->id, 'performer');
			$performers = $this->user_m->get_performers_names($performers);

			foreach($performers as $performer){
				echo $performer->performerName."\n\r";
			}
			$locations = $this->user_m->get_user_rules($u->id, 'city');
			print_r($locations);
			echo 'End users rules'."\n\r";
		endforeach;

	}

	/*********************************** END ********************************************/

	/*********************************** Miselaneas ********************************************/

	function seeking_performers_in_events($events, $performers){
		$results = array();
		foreach($events as $event) {
			foreach ($event->performers as $performer) {
				if (in_array($performer->PerformerID, $performers))
					$results[] = $performer->PerformerID;
			}
		}
		return $results;
	}

	public function test_sparkpost($debug = true){
		$templateData = array('name' => 'Ramdy', 'url' => 'concertfix.com/user/login');
		$html = $this->load->view('email_templates/confirm_email_html', $templateData, true);
		$subject = "New Tracking Feature Added: Concerts by Home City!";
		//$user = 'john@lightningmarketinggroup.com';
		$user = 'ridosbeymi@gmail.com';
		$this->sendEmailSparkpost($user, $subject, $html);

	}

	public function check_cron_task($debug=true){
		echo "Begin the Check Cron Task ".date('Y-m-d'). "\n\r";
		$cron_task = $this->rules_m->get_cron_date('rundaily_tn_track');
		echo "Start Date ".$cron_task->start_date. "\n\r";
		echo "Final Date ".$cron_task->final_date. "\n\r";
		if ($cron_task){

			$star_date = strtotime($cron_task->start_date);
			$final_date = strtotime($cron_task->final_date);
			if ($star_date != $final_date){
				$this->rundaily('tn_track');
				echo "Executed the Cron Task \n\r";
			} else
				echo "Not executed the Cron Task \n\r";
		}

	}

	private function sendEmailSparkpost($user, $subject, $html, $params = false){
		try{
			$this->load->library('email');

			$this->email->from('no-reply@concertfix.com', 'ConcertFix');
			$this->email->to($user);

			$this->email->reply_to('info@concertfix.com', 'ConcertFix');

			$this->email->bcc('theconcertfix@gmail.com');

			$this->email->subject($subject);
			$this->email->message($html);

			if ($this->email->send())
				echo "Email Sent Successfuly"."\n\r";
			else
				echo $this->email->print_debugger();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}

	}

	private function url_exists( $url = NULL ) {

		if( empty( $url ) ){
			return false;
		}

		$ch = curl_init( $url );

		//Establecer un tiempo de espera
		curl_setopt( $ch, CURLOPT_TIMEOUT, 5 );
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 5 );

		//establecer NOBODY en true para hacer una solicitud tipo HEAD
		curl_setopt( $ch, CURLOPT_NOBODY, false );
		//Permitir seguir redireccionamientos
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
		//recibir la respuesta como string, no output
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

		$data = curl_exec( $ch );

		//Obtener el código de respuesta
		$httpcode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
		//cerrar conexión
		curl_close( $ch );

		//Aceptar solo respuesta 200 (Ok), 301 (redirección permanente) o 302 (redirección temporal)
		$accepted_response = array( 200, 301, 302 );
		return in_array($httpcode, $accepted_response);

	}

	public function remove_user($email, $debug=true) {
        echo ($debug)  ? "process started"."\n\r" : ''."\n\r";
        if(!$this->input->is_cli_request()) die('Not allowed from URL');
        #$email = 'ramdy.rl@gmail.com';
        $user = $this->user_m->get_cf_user_id($email);

        echo "Taking the user with {$email} and ID {$user->id}"."\n\r";
        $this->user_m->remove_user_all($user->id);


        echo "User with {$email} removed"."\n\r";

    }

    public function insert_new_user($debug=true) {
        echo ($debug)  ? "process started"."\n\r" : ''."\n\r";
        if(!$this->input->is_cli_request()) die('Not allowed from URL');

        $username = 'tester932';
        $password = '7vav6GE7jP';
        $data = array('username' => $username, 'password' => md5($password), 'status' => 1, 'role' => 1);
        #$this->db->insert('users', $data);


        echo "User {$username} added"."\n\r";

    }


    public function change_password_usser($debug=true) {
        echo ($debug)  ? "process started"."\n\r" : ''."\n\r";
        if(!$this->input->is_cli_request()) die('Not allowed from URL');

        $username = 'tester932';
        $password = 'TpG2xAHLQZ';

        $this->db->where('username', $username);
        $data = array('password' => md5($password));
        $this->db->update('users', $data);

        echo "User {$username} updated"."\n\r";

    }

	/*********************************** END ********************************************/

	/*********************************** New Text Logic ********************************************/

	private function _generate_album_text($debug = true, $performer){


		if(!$this->input->is_cli_request()) die('Not allowed from URL');

		$genre = ucwords(strtolower($performer->genre));
		switch ($genre) :
			case 'Festival / Tour':
			case 'Performance Series':
				$genre = "music";
				break;

			default:
				# code...
				break;
		endswitch;

		#First Album by Release Date
		$albums = $this->cron_m->get_albums(true, false, $performer->PerformerID, 'ReleaseDate', 'asc');
		if (count($albums) == 0)
			return "";
		$firstAlbum = $albums[0];

		#Top Albums according the webservice
		$topAlbums = $this->_getTopAlbums($performer->Name);
		$topAlbum = $this->cron_m->get_album_by_performer($performer->PerformerID, $topAlbums[0]['name']);
		$topAlbumName = $topAlbums[0]['name'];
		$topAlbum_info = $this->_getAlbumInfo($topAlbumName, $performer->Name);
		if (isset($topAlbum_info['tracks'][0]))
			$topAlbum_topSong = $topAlbum_info['tracks'][0]['name'];

		$date = date('M j, Y', strtotime($firstAlbum->ReleaseDate));
		echo "Performer ".$performer->Name." Albums ".count($albums)." First Album Song ".$date."\n\r";;

		if (count($albums) == 1 || $firstAlbum->AlbumName == '' || $firstAlbum->AlbumName == '(null)' || $firstAlbum->ReleaseDate == null) {
			//IF {FirstAlbumReleaseDate} does not exist OR IF {performer} has only 1 album
			if ($topAlbumName && isset($topAlbum_topSong))
				return $this->_text_no_release_date($performer->Name, $topAlbumName, $topAlbum_topSong);
			return "";
		}
		elseif ($firstAlbum){

			#Year between first album and now
			$Years = date('Y') - date('Y', strtotime($firstAlbum->ReleaseDate));

			$firstAlbum_topSong = $this->cron_m->get_top_song($firstAlbum->id);
			$firstAlbum_topSong = clean_song($firstAlbum_topSong->name);
			//echo "Performer ".$performer->Name." First Album First Song ".$firstAlbum_topSong."\n\r";;
			$date = date('F j, Y', strtotime($firstAlbum->ReleaseDate));

			if (!isset($firstAlbum_topSong)) {
				//IF {FirstAlbum} OR {PopularAlbum} does not have any tracks
				return $this->_text_release_date_no_songs($performer->Name, $date, $firstAlbum->AlbumName,
					$topAlbumName, $Years, $genre);
			} elseif (!isset($topAlbum_topSong) && $topAlbumName)
				return $this->_text_popularalbum_no_songs($performer->Name, $topAlbumName);

			//$topAlbum_topSong = clean_song($topAlbum_topSong->name);

			return $this->_text_release_date($performer->Name, $date, $firstAlbum->AlbumName, $firstAlbum_topSong,
				$topAlbumName, $topAlbum_topSong, $Years, $genre);
		}


		/*$performer = $performer->Name;
        $performerz = (substr($performer, -1) == 's') ? $performer."'" : $performer."'s";*/
	}

	private function _text_release_date($performer_name, $firstalbum_date, $firstalbum_name, $firstalbum_popularsong,
								$popularalbum, $popularalbum_popularsong, $years, $genre){
		$array1 = array("arrived","came","appeared");
		$array2 = array("release","appearance");
		$array3 = array("published","released");
		$array4 = array("song","track","single");
		$array5 = array("immediately became","was instantly","quickly became");
		$array6 = array("fan favorite","success","hit");
		$array7 = array("newest emerging","fastest growing","most popular");
		$array8 = array("artists","performers","acts","talents");
		$array9 = array("moment","time");
		$array10 = array("After that","Since then","Thereafter","Later on","Afterwards");
		$array11 = array("published","released", "came out with");
		$array12 = array("extremely","most","hugely");
		$array13 = array("popular","famous","beloved");
		$array14 = array("features","contains","includes","is comprised of");
		$array15 = array("listened to","popular","famous","beloved","well-known");
		$array16 = array("songs","tracks","music","work");
		$array17 = array("catalog","discography","collection");
		$array18 = array("features","contains","includes","hosts");
		$array19 = array("song","track","single");
		$array20 = array("been","become","proven to be","made itself");
		$array21 = array("recognized","sought-after","requested","well-known");
		$array22 = array("fans","music lovers","followers");
		$array23 = array("experience","hear","see","enjoy");
		$array24 = array("shows","performances","live events","gigs");
		$array25 = array("Apart from","Aside from","Besides","In addition to");
		$array26 = array("many","most","a host");
		$array27 = array("tracks","songs");
		$array28 = array("recognized","sought-after","requested","well-known");
		$array29 = array("A few","Some","A handful");
		$array30 = array("popular","famous","beloved");
		$array31 = array("seen","found","provided","displayed");
		$array32 = array("the release of","releasing");
		$array33 = array("having","making");
		$array34 = array("huge","massive","major","true");
		$array35 = array("effect","impact");
		$array36 = array("business","industry");
		$array37 = array("fans","music lovers","followers");
		$array38 = array("continue to","still","consistently");
		$array39 = array("flock","gather","unite","head");
		$array40 = array("experience","see","watch","hear");
		$array41 = array("on stage","appear live","in person");
		$array42 = array("play","perform");
		$array43 = array("hits","tracks","songs","favorites");
		$array44 = array("full","entire","complete");
		$array45 = array("catalog","discography","collection");

		$text = $performer_name." ".random_pop2($array1)." on the ".$genre." scene with the ".random_pop2($array2).
			" of the album <i>'".$firstalbum_name."'</i> ".random_pop2($array3)." on ".$firstalbum_date.
			". The ".random_pop2($array4)." <i>'".$firstalbum_popularsong."'</i> ".random_pop2($array5)." a ".
			random_pop2($array6)." and made ".$performer_name." one of the ".random_pop2($array7)." ".random_pop2($array8).
			" at that ".random_pop2($array9).". ".random_pop2($array10).", ".$performer_name." ".random_pop2($array11).
			" the ".random_pop2($array12)." ".random_pop2($array13)." album <i>'".$popularalbum."'</i> which ".
			random_pop2($array14)." some of the most ".random_pop2($array15)." ".random_pop2($array16)." from the ".
			$performer_name." ".random_pop2($array17).". <i>'".$popularalbum."'</i> ".random_pop2($array18)." the ".
			random_pop2($array19)." <i>'".$popularalbum_popularsong."'</i> which has ".random_pop2($array20)." the most ".
			random_pop2($array21)." for ".random_pop2($array22)." to ".random_pop2($array23)." during the ".random_pop2($array24).
			". ".random_pop2($array25)." <i>'".$popularalbum_popularsong."'</i>, ".random_pop2($array26)." of other ".
			random_pop2($array27)." from <i>'".$popularalbum."'</i> have also become ".random_pop2($array28)." as a result. ".
			random_pop2($array29)." of ".$performer_name."'s most ".random_pop2($array30)." tour albums and songs are ".
			random_pop2($array31)." below. After ".$years." years since ".random_pop2($array32)." <i>'".$firstalbum_name.
			"'</i> and ".random_pop2($array33)." a ".random_pop2($array34)." ".random_pop2($array35)." in the ".random_pop2($array36).
			", ".random_pop2($array37)." ".random_pop2($array38)." ".random_pop2($array39)." to ".random_pop2($array40)." ".$performer_name.
			" ".random_pop2($array41)." to ".random_pop2($array42)." ".random_pop2($array43)." from the ".random_pop2($array44).
			" ".random_pop2($array45).'.';

		return $text;
	}

	private function _text_release_date_no_songs($performer_name, $firstalbum_date, $firstalbum_name, $popularalbum, $years, $genre){
		$array01 = array("arrived","came","appeared");
		$array02 = array("release","appearance");
		$array03 = array("published","released");
		$array04 = array("immediately became","was instantly","quickly became");
		$array05 = array("fan favorite","success","hit");
		$array06 = array("newest emerging","fastest growing","most popular");
		$array07 = array("artists","performers","acts","talents");
		$array08 = array("moment","time");
		$array09 = array("After that","Since then","Thereafter","Later on","Afterwards");
		$array10 = array("published","released","came out with");
		$array11 = array("extremely","most","hugely");
		$array12 = array("popular","famous","beloved");
		$array13 = array("features","contains","includes","is comprised of");
		$array14 = array("listened to","popular","famous","beloved","well-known");
		$array15 = array("songs","tracks","music","work");
		$array16 = array("catalog","discography","collection");
		$array17 = array("been","become","proven to be","made itself");
		$array18 = array("recognized","sought-after","requested","well-known");
		$array19 = array("fans","music lovers","followers");
		$array20 = array("experience","hear","see","enjoy");
		$array21 = array("shows","performances","live events","gigs");
		$array22 = array("A few","Some","A handful");
		$array23 = array("popular","famous","beloved");
		$array24 = array("seen","found","provided","displayed");
		$array25 = array("the release of","releasing");
		$array26 = array("having","making");
		$array27 = array("huge","massive","major","true");
		$array28 = array("effect","impact");
		$array29 = array("business","industry");
		$array30 = array("fans","music lovers","followers");
		$array31 = array("continue to","still","consistently");
		$array32 = array("flock","gather","unite","head");
		$array33 = array("experience","see","watch","hear");
		$array34 = array("on stage","appear live","in person");
		$array35 = array("play","perform");
		$array36 = array("hits","tracks","songs","favorites");
		$array37 = array("full","entire","complete");
		$array38 = array("catalog","discography","collection");

		$text = $performer_name." ".random_pop2($array01)." on the ".$genre." scene with the ".random_pop2($array02).
			" of the album <i>'".$firstalbum_name."'</i> ".random_pop2($array03)." on ".$firstalbum_date."'</i> ".
			random_pop2($array04).' a '.random_pop2($array05)." and made ".$performer_name." one of the ".random_pop2($array06).
			" ".random_pop2($array07)." at that ".random_pop2($array08).". ".random_pop2($array09).", ".$performer_name." ".
			random_pop2($array10)." ".random_pop2($array11)." ".random_pop2($array12)." album <i>'".$popularalbum."'</i> which ".
			random_pop2($array13)." some of the most ".random_pop2($array14)." ".random_pop2($array15)." from the ".$performer_name.
			" ".random_pop2($array16).". <i>'".$popularalbum."'</i> has ".random_pop2($array17)." the most ".random_pop2($array18).
			" album for ".random_pop2($array19)." to ".random_pop2($array20)." during the ".random_pop2($array21).". ".
			random_pop2($array22)." of ".$performer_name."'s most ".random_pop2($array23)." tour albums and songs are ".
			random_pop2($array24)." below. After ".$years." years since ".random_pop2($array25)." <i>'".$firstalbum_name.
			"'</i> and ".random_pop2($array26)." a ".random_pop2($array27)." ".random_pop2($array28)." in the ".random_pop2($array29).
			", ".random_pop2($array30)." ".random_pop2($array31)." ".random_pop2($array32)." to ".random_pop2($array33)." ".
			$performer_name." ".random_pop2($array34)." to ".random_pop2($array35)." ".random_pop2($array36)." from the ".
			random_pop2($array37)." ".random_pop2($array38).".";

		return $text;
	}

	private function _text_no_release_date($performer_name, $popularalbum, $popularalbum_popularsong){
		$array01 = array("published","released","came out with");
		$array02 = array("extremely","most","hugely");
		$array03 = array("popular","famous","beloved");
		$array04 = array("features","contains","includes","is comprised of");
		$array05 = array("listened to","popular","famous","beloved","well-known");
		$array06 = array("songs","tracks","music","work");
		$array07 = array("catalog","discography","collection");
		$array08 = array("features","contains","includes","hosts");
		$array09 = array("song","track","single");
		$array10 = array("been","become","proven to be","made itself");
		$array11 = array("recognized","sought-after","requested","well-known");
		$array12 = array("fans","music lovers","followers");
		$array13 = array("experience","hear","see","enjoy");
		$array14 = array("shows","performances","live events","gigs");
		$array15 = array("Apart from","Aside from","Besides","In addition to");
		$array16 = array("many","most","a host");
		$array17 = array("tracks","songs");
		$array18 = array("recognized","sought-after","requested","well-known");
		$array19 = array("A few","Some","A handful");
		$array20 = array("popular","famous","beloved");
		$array21 = array("seen","found","provided","displayed");
		$array22 = array("having","making");
		$array23 = array("huge","massive","major","true");
		$array24 = array("effect","impact");
		$array25 = array("business","industry");
		$array26 = array("fans","music lovers","followers");
		$array27 = array("continue to","still","consistently");
		$array28 = array("flock","gather","unite","head");
		$array29 = array("experience","see","watch","hear");
		$array30 = array("on stage","appear live","in person");
		$array31 = array("play","perform");
		$array32 = array("hits","tracks","songs","favorites");
		$array33 = array("full","entire","complete");
		$array34 = array("catalog","discography","collection");


		$text = $performer_name." ".random_pop2($array01)." the ".random_pop2($array02)." ".random_pop2($array03)." album <i>'".
			$popularalbum."'</i> which ".random_pop2($array04)." some of the most ".random_pop2($array05)." ".random_pop2($array06).
			" from the ".$performer_name." ".random_pop2($array07).". <i>'".$popularalbum."'</i> ".random_pop2($array08)." the ".random_pop2($array09).
			" <i>'".$popularalbum_popularsong."'</i> which has ".random_pop2($array10)." the most ".random_pop2($array11)." for ".random_pop2($array12).
			" to ".random_pop2($array13)." during the ".random_pop2($array14).". ".random_pop2($array15)." <i>'".$popularalbum_popularsong.
			"'</i>, ".random_pop2($array16)." of the ".random_pop2($array17)." from <i>'".$popularalbum."'</i> have also become ".
			random_pop2($array18)." as a result. ".random_pop2($array19)." of ".$performer_name."'s most ".random_pop2($array20).
			" tour albums and songs are ".random_pop2($array21)." below. After ".random_pop2($array22)." a ".random_pop2($array23).
			" ".random_pop2($array24)." in the ".random_pop2($array25).", ".random_pop2($array26)." ".random_pop2($array27).
			" ".random_pop2($array28)." to ".random_pop2($array29)." ".$performer_name." ".random_pop2($array30)." to ".random_pop2($array31).
			" ".random_pop2($array32)." from the ".random_pop2($array33)." ".random_pop2($array34).'.';
		return $text;
	}

	private function _text_popularalbum_no_songs($performer_name, $popularalbum){
		$array1 = array("published","released","came out with");
		$array2 = array("extremely","most","hugely");
		$array3 = array("popular","famous","beloved");
		$array4 = array("features","contains","includes","is comprised of");
		$array5 = array("listened to","popular","famous","beloved","well-known");
		$array6 = array("songs","tracks","music","work");
		$array7 = array("catalog","discography","collection");
		$array8 = array("been","become","proven to be","made itself");
		$array9 = array("recognized","sought-after","requested","well-known");
		$array10 = array("fans","music lovers","followers");
		$array11 = array("experience","hear","see","enjoy");
		$array12 = array("shows","performances","live events","gigs");
		$array13 = array("A few","Some","A handful");
		$array14 = array("popular","famous","beloved");
		$array15 = array("seen","found","provided","displayed");
		$array16 = array("publishing","releasing");
		$array17 = array("having","making");
		$array18 = array("huge","massive","serious","true");
		$array19 = array("effect","impact","example");
		$array20 = array("business","industry");
		$array21 = array("fans","music lovers","followers");
		$array22 = array("continue to","still","consistently");
		$array23 = array("flock","gather","unite","head");
		$array24 = array("experience","see","watch","hear");
		$array25 = array("on stage","appear live","in person");
		$array26 = array("play","perform");
		$array27 = array("hits","tracks","songs","favorites");
		$array28 = array("full","entire","complete");
		$array29 = array("catalog","discography","collection");

		$text = $performer_name." ".random_pop2($array1)." the ".random_pop2($array2)." ".random_pop2($array3)." album
		    <i>'".$popularalbum."'</i> which ".random_pop($array4)." some of the most ".random_pop2($array5)." ".random_pop2($array6).
			" from the ".$performer_name." ".random_pop2($array7).". <i>'".$popularalbum."'</i> has ".random_pop2($array8).
			" the most ".random_pop2($array9)." album for ".random_pop2($array10)." to ".random_pop2($array11)." during the ".
			random_pop2($array12).". ".random_pop2($array13)." of ".$performer_name."'s most ".random_pop2($array14).
			" tour albums and songs are ".random_pop2($array15)." below. After ".random_pop2($array16)." <i>'".$popularalbum.
			"'</i> and ".random_pop2($array17)." a ".random_pop2($array18)." ".random_pop2($array19)." in the ".random_pop2($array20).
			", ".random_pop2($array21)." ".random_pop2($array22)." ".random_pop2($array23)." to ".random_pop2($array24).
			" ".$performer_name." ".random_pop2($array25)." to ".random_pop2($array26)." ".random_pop2($array27)." from the ".
			random_pop2($array28)." ".random_pop2($array29).".";

		return $text;
	}

	private function _getTopAlbums($performerName){
		$this->load->library('lastfmapi/lastfmapi');

		// Pass the array to the auth class to return a valid auth

		$this->auth = new lastfmApiAuth('setsession');
		// pdump($this->auth);
		$this->apiClass = new lastfmApi();

		$artistClass = $this->apiClass->getPackage($this->auth, 'artist');

		$methodVars_album = array('artist' => $performerName);

		return $artistClass->getTopAlbums($methodVars_album);
	}

	private function _getAlbumInfo($albumName, $performerName){
		$this->load->library('lastfmapi/lastfmapi');

		// Pass the array to the auth class to return a valid auth

		$this->auth = new lastfmApiAuth('setsession');
		// pdump($this->auth);
		$this->apiClass = new lastfmApi();

		$albumClass = $this->apiClass->getPackage($this->auth, 'album');

		$methodVars_album = array('artist' => $performerName, 'album' => $albumName);
		return $albumClass->getInfo($methodVars_album);
	}

	/*********************************** END ************************************************/

}

/* End of file cron.php */
/* Location: ./application/controllers/cron.php */
