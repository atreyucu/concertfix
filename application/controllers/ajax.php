
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//use Aws\S3\S3Client;
//use Aws\Common\Credentials\Credentials;
//use Aws\S3\Exception\S3Exception;

class Ajax extends MY_SiteController {

	public function __construct()
	{
		parent::__construct();
		$this->output->enable_profiler(false);
		//Do your magic here
        error_reporting(0);

	}
	public function index()
	{
		redirect('/'); //no ajax access
	}

	public function venue_image()
	{
		// die('asd');
		$baseUrl = 'http://tickettransaction.com/VenueInformation.aspx?';
		$venueID = $this->input->get('venue_id');
		$eventID = $this->input->get('event_id');
		$body = file_get_contents($baseUrl . "VenueID=$venueID&EventID=$eventID");
		preg_match_all("/src=\"(.*?)\"/", $body, $matches);
		$url = false;
		if(count($matches) == 2) {
			$url = $matches[1][0];
		}
		echo json_encode(array(
			'status' => !!$url,
			'image' => $url
		));
	}

	public function venue_search($query = false)
	{
		$viewData = array('genres' => $this->global_m->get_genres());

		$view = $this->load->view('frontend/user/ajax/venue_search_result', $viewData, true);
		echo $view;
	}

	public function fe_search_performer($query = false)
	{
		if(!isAjax()) redirect('/'); //no soup

		$query = $this->input->get('term');
		$this->load->model('global_m');
		$result = $this->global_m->search_performer($query);
		echo json_encode($result);
	}

	public function fe_search_city($query = false)
	{
		if(!isAjax()) redirect('/'); //no soup

		$query = $this->input->get('term');
		$this->load->model('rules_m');
		$result = $this->rules_m->search_city($query);
		echo json_encode($result);
	}

	public function search_city($query = false)
	{
		if(!isAjax()) redirect('/'); //no soup

		$query = $this->input->get('term');
		$this->load->model('rules_m');
		$result = $this->rules_m->search_city_wc($query);
		echo json_encode($result);
	}

	#Search Performers, Cities and Venues
	public function search_pcv($query=false){
		if(!isAjax()) redirect('/'); //no soup

		$query = $this->input->post('term');
		$this->load->model('global_m');
		$result = array();
		$result['performers'] = $this->global_m->search_pcv('performer', $query);
		$result['cities'] = $this->global_m->search_pcv('city', $query);
		$result['venues'] = $this->global_m->search_pcv('venue', $query);

		$view = $this->load->view('frontend/global/search_jax', $result, false);
		//echo "<h5>Suggestions for: $query</h5>";
		echo($view);

	}

	public function add_rule()
	{
		if(!isAjax()) redirect('/'); //no soup
		if(!$this->cfUser['loggedin']) redirect('/'); //no not-logged in adds

		$data = $this->input->post('rule');
		$userId = $this->cfUser['cf']['id'];

		switch ($data['ruleType']):
			case 'performer':
				$pId = $data['performerId'];
				if($this->rules_m->already_tracking($pId, $data['ruleType'])):
					echo throwMessage(1, "<p> <strong>{$data['performerName']}</strong> is already in your list!</p>");
				else: //not yet
					$insertData = array('cfUserId' => $userId, 'performerId' => $pId);
					$this->rules_m->insert_rule($insertData, 'performer');
					echo throwMessage(0, "<p> <strong>{$data['performerName']}</strong> was added to the list!</p>");
				endif;
			break;
			case 'city':
				$slug = $data['slug'];
				$name = $data['city'];
				if($this->rules_m->already_tracking($slug, $data['ruleType'])):
					echo throwMessage(1, "<p> <strong>{$data['city']}</strong> is already in your list!</p>");
				else: //not yet
					$insertData = array('cfUserId' => $userId, 'citySlug' => $slug, 'cityName' => $name);
					$this->rules_m->insert_rule($insertData, 'city');
					echo throwMessage(0, "<p> <strong>{$data['city']}</strong> was added to the list!</p>");
				endif;
			break;
			case 'genre':
				$gId = $data['gId'];
				$on = $data['on'];
				if($on === "true"):
					$insertData = array('cfUserId' => $userId, 'genreId' => $gId);
					$this->rules_m->insert_rule($insertData, 'genre');
					$result = array('added');
					echo json_encode($result);
				else: //not yet
					$this->rules_m->remove_rule($gId,$userId,'genre');
					$result = array('removed');
					echo json_encode($result);
				endif;
			break;
			default:
				# code...
				break;
		endswitch;

	}
	public function add_rule_LEGACY()
	{
		if(!isAjax()) redirect('/'); //no soup

 		$this->load->model('rules_m');
 		$data = $this->input->post('data');

		$notifyEmail = $this->cfUser['cf']['primary_email'];
		$cfUserId = $this->cfUser['cf']['id'];
		//generate unique group ID
		$groupId = md5(date('U').$notifyEmail);
		$createdDate = date('Y-m-d H:i:s');
		$notifyByFeed = $data['notifications']['feed'];
		$notifyByEmail = $data['notifications']['email'];
		$notifyDays = $data['notifications']['days-before'];
		$notifyDaysNum = $data['notifications']['days-num'];
		$notifyAsOccurs = $data['notifications']['as-occurs'];
		$ruleType = $data['rule'];
		$rules = array(

			);

		foreach ($data['performers'] as $p) :
			foreach ($data['cities'] as $c) :
				$rule = array(
						"ruleType"		=> $ruleType,
						"performerId"	=> $p['pId'],
						"performerName"	=> $p['name'],
						"citySlug"	=> $c['slug'],
						"cityName"	=> $c['prettyName'],
						"groupId"=> $groupId,
						"createdDate" => $createdDate,
						"cfUserId"	=> $cfUserId,
						"notifyEmail" => $notifyEmail,
						"groupId" => $groupId,
						"createdDate" => $createdDate,
						"notifyByFeed" => $notifyByFeed,
						"notifyByEmail" => $notifyByEmail,
						"notifyDays" => $notifyDays,
						"notifyDaysNum" => $notifyDaysNum,
						"notifyAsOccurs" => $notifyAsOccurs,
					);
			if(!$this->rules_m->rule_exists($rule)):
				array_push($rules, $rule);
			endif;
			endforeach;
		endforeach;

		if($this->rules_m->add_rule($rules)):
			$error = array("error" => 0, "message" => "Notification Rule Created Successfully!");
		else:
			$error = array("error" => 1, "message" => "Error Occurred. Please Try Again!");
		endif;
		echo json_encode($error);
	}

	public function fetch_rules()
	{
		if(!isAjax()) redirect('/'); //no soup
		if(!$this->cfUser['loggedin']):
			echo "UNAUTHORIZED!!! PLEASE LOGIN!";
			return false;
		endif;
 		$userId = $this->cfUser['cf']['id'];

		$rule = $this->input->post('rule');
		$data['rules'] = $this->rules_m->fetch_rules($rule, $userId);
		$output = $this->load->view("frontend/user/ajax/{$rule}_rule_list", $data, true);
		echo $output;
	}
	public function get_user_rules()
	{
		if(!isAjax()) redirect('/'); //no soup
 		$this->load->model('rules_m');
 		$userId = $this->cfUser['cf']['id'];
 		$rules = $this->rules_m->get_user_rules($userId);
 		$viewData = array('rules' => $rules);
 		$list = $this->load->view('frontend/user/rules_list_ajax', $viewData, true);
 		echo($list);

	}

	public function get_nearby_cities()
	{
		if(!isAjax()) redirect('/'); //no soup

		$loc['slug'] =	$this->input->post('slug');
		$this->load->model('global_m');
		$nearby = $this->global_m->get_nearby_cities($loc, 60, 10, false);
		echo json_encode($nearby);
	}

	public function get_similar_artists()
	{
		if(!isAjax()) redirect('/'); //no soup

		$pId = $this->input->post('pid');
		$similar = $this->global_m->get_similar_artists($pId);
		echo json_encode($similar);

	}

	public function unlink()
	{
		if(!isAjax()) redirect('/'); //no soup
		if(!$this->cfUser['loggedin']):
			echo "UNAUTHORIZED!!! PLEASE LOGIN!";
			return false;
		endif;
		$network = $this->input->post('network');
		$userId  =	$this->cfUser['cf']['id'];
		$this->load->model('user_m');
		if($this->user_m->unlink($network, $userId)):
			throwMessage(0, "<p> <strong>{$network}</strong> was removed!</p>",false, true);
			$data[$network] = false;
			$this->session->set_userdata($data);
		else:
			throwMessage(1, "<p>Some error occurred removing <strong>{$network}</strong>!</p>",false, true);
		endif;

	}
	public function remove_rule()
	{
		if(!isAjax()) redirect('/'); //no soup
		if(!$this->cfUser['loggedin']):
			echo "UNAUTHORIZED!!! PLEASE LOGIN!";
			return false;
		endif;
 		$userId = $this->cfUser['cf']['id'];
 		$ruleId = $this->input->post('ruleId');
 		$type = $this->input->post('type');
 		$title = $this->input->post('title');
		if($this->rules_m->remove_rule($ruleId, $userId, $type)):
			echo throwMessage(0, "<p> <strong>{$title}</strong> was removed!</p>");
		else:
			echo throwMessage(1, "<p>Some error occurred removing <strong>{$title}</strong>!</p>");
		endif;
	}

	public function update_name()
	{
		if(!isAjax()) redirect('/'); //no gsoup
		$this->load->model('user_m');
		$name = $this->input->post('name');
		if($this->user_m->update_name($name)):
			$email = $this->cfUser['cf']['primary_email'];
			$data['cf'] = $this->user_m->get_user_by_email($email, false, true);
			$this->session->set_userdata($data);
			echo throwMessage(0, "<p>Updated to $name</p>");
		else:
			echo throwMessage(1, "<p>Failed Try Again!</p>");
		endif;
	}

	public function change_notify_type()
	{
		if(!isAjax()) redirect('/'); //no gsoup
		$this->load->model('user_m');
		$notify = $this->input->post('notify');
		if($this->user_m->change_notify_type($notify)):
			echo throwMessage(0, "<p>Updated email preference </p>");
		else:
			echo throwMessage(1, "<p>Failed Try Again!</p>");
		endif;
	}

	public function change_remind_type()
	{
		if(!isAjax()) redirect('/'); //no gsoup
		$this->load->model('user_m');
		$remindtype = $this->input->post('remindtype');
		$status = $this->input->post('status');
		$data = array($remindtype => $status);
		if($this->user_m->change_remind_type($data)):
			echo throwMessage(0, "<p>Updated reminder preference </p>");
		else:
			echo throwMessage(1, "<p>Failed Try Again!</p>");
		endif;
	}

    public function change_new_concert_email_frequency()
    {
        if(!isAjax()) redirect('/'); //no gsoup
        $this->load->model('user_m');
        if(array_key_exists('loggedin', $this->cfUser))
        {
            $user_id = $this->cfUser['cf']['id'];
            $frequency = $this->input->post('frequency');

            if ($this->rules_m->exists_rule_concert($user_id))
                $was_changed = $this->rules_m->change_rule_new_concert($user_id, $frequency);
            else
                $was_changed = $this->rules_m->insert_rule_concert($user_id, $new_concert_freq = $frequency);

            if ($was_changed):
                echo throwMessage(0, "<p>Updated new concert email frequency </p>");
            else:
                echo throwMessage(1, "<p>Failed Try Again!</p>");
            endif;
        }
        else{
            echo throwMessage(2, "<p>Go to login </p>");
        }
    }

	public function change_home_city_miles()
	{
		if(!isAjax()) redirect('/'); //no gsoup
		$this->load->model('user_m');
        if(array_key_exists('loggedin', $this->cfUser)) {
            $user_id = $this->cfUser['cf']['id'];
            $miles = $this->input->post('miles');

            if($this->rules_m->exists_rule_concert($user_id))
                $was_changed = $this->rules_m->change_miles($user_id, $miles);
            else
                $was_changed = false;

            if($was_changed):
                echo throwMessage(0, "<p>Updated distance from your home city </p>");
            else:
                echo throwMessage(1, "<p>Failed Try Again!</p>");
            endif;
        }
        else{
            echo throwMessage(2, "<p>Go to login </p>");
        }
	}

	public function change_home_city_slug()
	{
		if(!isAjax())
			redirect('/'); //no gsoup
		$this->load->model('user_m');
        if(array_key_exists('loggedin', $this->cfUser)) {
            $user_id = $this->cfUser['cf']['id'];
            $slug = $this->input->post('slug');
            $name = $this->input->post('name');

            if($this->rules_m->exists_rule_concert($user_id))
                $was_changed = $this->rules_m->change_home_city($user_id, $slug, $name);
            else
                $was_changed = false;

            if($was_changed):
                echo throwMessage(0, "<p>Updated your home city </p>");
            else:
                echo throwMessage(1, "<p>Failed Try Again!</p>");
            endif;
        }
        else{
            echo throwMessage(2, "<p>Go to login </p>");
        }
	}

    public function change_upcoming_concert_email_frequency()
    {
        if(!isAjax()) redirect('/'); //no gsoup
        $this->load->model('user_m');
        if(array_key_exists('loggedin', $this->cfUser)) {
            $user_id = $this->cfUser['cf']['id'];
            $frequency = $this->input->post('frequency');

            if ($this->rules_m->exists_rule_concert($user_id))
                $was_changed = $this->rules_m->change_rule_upcoming_concert($user_id, $frequency);
            else
                $was_changed = $this->rules_m->insert_rule_concert($user_id, $upcoming_concert_freq = $frequency);

            if ($was_changed):
                echo throwMessage(0, "<p>Updated upcoming concert email frequency </p>");
            else:
                echo throwMessage(1, "<p>Failed Try Again!</p>");
            endif;
        }
        else{
            echo throwMessage(2, "<p>Go to login </p>");
        }
    }
    public function change_genre()
    {
        if(!isAjax()) redirect('/'); //no gsoup
        $this->load->model('user_m');
		if(array_key_exists('loggedin', $this->cfUser)):
			$user_id = $this->cfUser['cf']['id'];
			$rule_concert = $this->rules_m->get_rule_concert_by_user($user_id);
			if(count($rule_concert) == 0)
			{
				$this->rules_m->insert_rule_concert($user_id);
				$rule_concert = $this->rules_m->get_rule_concert_by_user($user_id);
			}
			$rule_concert_id = $rule_concert->id;
			$genre = $this->input->post('genre');
			$action = $this->input->post('action');

			if($action)
			{
				if($genre == 'all'){
					$this->load->model('concerts_m');
					$all_category = $this->concerts_m->get_categories_user_settings();

					foreach($all_category as $category){
						$this->rules_m->add_genre($rule_concert_id, $category->id);
					}
				}
				else {
					$this->rules_m->add_genre($rule_concert_id, $genre);
				}
			}
			else{
				$this->rules_m->remove_genre($rule_concert_id, $genre);
			}
			echo throwMessage(0, "<p>Updated Genre </p>");
		else :
			echo throwMessage(2, "<p>Go to login </p>");
		endif;

    }

	public function register()
	{
		if(!isAjax()) redirect('/'); //no gsoup
		$this->load->model('user_m');
		$data = $this->input->post('data');
		if($this->user_m->email_exists($data['email'], 'primary')):
			echo throwMessage(2, "{$data['email']} Already Exists!! Try another!", true, false);
		else:
			$data['hash'] = md5(rand(1,100).$data['email'].date('U'));
			$this->user_m->insert_user($data, 'register');
			$mandrillconfig = $this->config->load("mandrill",TRUE);
			$this->load->library('Mandrill', $mandrillconfig);
			$this->load->helper('notifications');
			$confirmUrl = "user/verify/cf?user={$data['hash']}";
			$templateData = array('name' => $data['name'], 'url' => $confirmUrl);
			$htmlView = $this->load->view('email_templates/confirm_email_html', $templateData, true);
			//$textView = $this->load->view('email_templates/confirm_email_text', $templateData, true);
			//$message = buildMessage($data, "Please Confirm Email", $htmlView, $textView);

			try{
				$this->load->library('email');

				$this->email->from('no-reply@concertfix.com', 'ConcertFix');
				$this->email->to($data['email']);

				$this->email->reply_to('info@concertfix.com', 'ConcertFix');

				$this->email->bcc('theconcertfix@gmail.com');

				$this->email->subject("Please Confirm Email");
				$this->email->message($htmlView);

				if ($this->email->send())
					echo throwMessage(0,"Account Created Successfully! Please check {$data['email']} for verification message!", true, false);
				else
					echo throwMessage(2,"There was an error sending notification message... try again in a bit.", true, false);
			}
			catch(Exception $e){
				echo throwMessage(2,"There was an error sending notification message... try again in a bit.", true, false);
			}

			/*try {
				$mdrConnection = $this->mandrill->create();
				$async = false;
			    $ip_pool = 'Main Pool';
			    $send_at = NULL; // '2001-12-12 12:12:12';
			    $result = $mdrConnection->messages->send($message, $async);
			    echo throwMessage(0,"Account Created Successfully! Please check {$data['email']} for verification message!", true, false);
			} catch(Mandrill_Error $e) {
			    // Mandrill errors are thrown as exceptions
			    echo throwMessage(2,"There was an error sending notification message... try again in a bit.", true, false);

			}*/
			// var_dump($mess);
		endif;
		// var_dump($data);
	}

	public function signup()
	{
		if(!isAjax()) redirect('/'); //no gsoup
		$email = $this->input->post('email');
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) :
			$data = array(	'name'	=> $this->input->post('name'),
							'email' => $email,
							'signup_date' => date('Y-m-d H:i:s'),
							'confirmed' => 0,
							'status' => 0,
						);
			if($this->global_m->signup($data)):
	    		echo "<p style='color:#C6FFC6;font-weight:bolder;font-size:14px;'>Thank you for subscribing!</p>";
			else:
				echo "<p style='color:#FFE8E8;font-weight:bolder;font-size:14px;'>Error occurred. Please contact us.</p>";
			endif;
		else:
			echo "<p style='color:#FFE8E8;font-weight:bolder;font-size:14px;'>Please Enter valid Email</p>";
		// validate email
		endif;

	}
	public function userexist()
	{
		if(!isAjax()) redirect('/'); //no gsoup
		$this->load->model('user_m');

		$email = trim(strtolower($this->input->post('email')));
		echo json_encode($this->user_m->email_exists($email, 'primary'));
	}

	public function delete_city()
	{
		if(!isAjax()) redirect('/'); //no soup
		$this->load->model('admin_m');

		$id = $this->input->post('id');
		$this->admin_m->delete_city($id);
		echo "DELETED";

	}

	public function update_top_city()
	{
		if(!isAjax()) redirect('/'); //no soup

		$this->load->model('admin_m');

		$id = $this->input->post('id');
		$area = $this->input->post('area');

		$this->admin_m->update_top_city($id, $area);

		echo "<b style='color:green;'>SAVED!</b>";
	}

	public function search_performer($tour = false)
	{

		if(!isAjax()) redirect('/'); //no soup
		$this->load->model('admin_m');
		$string = $this->input->post('query');

		// $result = ($tour) ?  : $this->admin_m->search_performer_tour($string);

		// if($tour):
		if($tour == 'tour'):
			$result = $this->admin_m->search_performers_with_announcements($string);
			$viewData = array('performers' => $result);
			$view = $this->load->view('admin/ajax/performer_search_tour_results', $viewData, true);
		 else:
			$result = $this->admin_m->search_performer($string);
			$viewData = array('performers' => $result);
			$view = $this->load->view('admin/ajax/performer_search_results', $viewData, true);
		endif;


		echo $view;
	}


	public function toggle_featured()
	{
		if(!isAjax()) redirect('/'); //no soup
		$this->load->model('admin_m');
		$pid = $this->input->post('id');
		$type = $this->input->post('type');
		$status = $this->input->post('status');
		$status  = ($status == "true") ? 1 : 0;
		$this->admin_m->toggle_featured($pid, $status, $type);
		echo "<b style='color:green;'>SAVED!</b>";
	}

	public function get_performer_by_id()
	{
		if(!isAjax()) redirect('/'); //no soup
		$this->load->model('admin_m');
		$id = $this->input->post('id');
		$viewData = array('performer' => $this->admin_m->get_performer_by_id($id));
		$view = $this->load->view('admin/ajax/single_performer_edit', $viewData, true);
		echo $view;
	}

	public function change_performer_images_by_id()
	{
		if(!isAjax()) redirect('/'); //no soup
		$this->load->model('admin_m');
		$pid = $this->input->post('id');
		$type = $this->input->post('type');
		$imgPath = $this->input->post('image');
		$this->admin_m->update_lastfm_fb_image($pid, $imgPath, $type);
		$viewData = array('performer' => $this->admin_m->get_performer_by_id($pid));
		$view = $this->load->view('admin/ajax/single_performer_edit', $viewData, true);
		echo $view;
	}

	public function generate_performer_text()
	{
		if(!isAjax()) redirect('/'); //no soup
		$pSlug = $this->input->post('slug');
		$this->soapy = new TN_soap();
		$this->load->model('tours_m');
		$performer = $this->tours_m->get_performer_by_slug($pSlug);
		$params = array(
 						'beginDate' => date('c'),
 						'orderByClause' => 'Date',
 						'performerID' =>  $performer[0]->PerformerID,
 						'whereClause' => 'CountryID = 217 OR CountryID = 38'
 						);
 		$tourDates = $this->soapy->run_soap('GetEvents', $params);
 		$text = $this->tours_m->get_text("performer", $tourDates, $performer[0], 'performer_main', false, true);
 		echo $text;
	}

	public function remove_picture()
	{
		if(!isAjax()) redirect('/'); //no soup
		$this->load->model('admin_m');

		// error_reporting(E_ALL);
		$type = $this->input->post('type');
		$pId = $this->input->post('pid');
		if($pId):
//			$this->s3->setAuth($this->config->item('access_key'), $this->config->item('secret_key'));
			$uri = $this->admin_m->remove_picture($pId, $type);
			$bucket = "ssglobalcdn";
			$this->s3->deleteObject($bucket, $uri);
			echo "<div class='alert alert-success'>Picture was removed!</div>";
		else:
			echo "<div class='alert alert-danger'>Error Occurred: Performer not found!!</div>";
			// echo "";
		endif;
	}

	public function upload_image()
	{
		error_reporting(E_ALL);
		$this->load->model('admin_m');
        $this->load->library('s3');
		// $targetFolder = "/home/bodyrubs/public_html_test/public/img/usercontent"	; // Relative to the root
		// $targetFolder = "/home/bodyrubs/public_html/public/img/usercontent"	; // Relative to the root
		$bucket = "ssglobalcdn";
		$imageSize = $this->input->post('type');
		$imageSlug = $this->input->post('slug');
		$performerId = $this->input->post('performerid');
		// $rname = md5(date('U').rand(1,500).md5('randomhash'));

		if (!empty($_FILES)) {
			$tempFile = $_FILES['Filedata']['tmp_name'];
			$imageParts = pathinfo($_FILES['Filedata']['name']);
			$imageExtension = strtolower($imageParts['extension']);
			$imageTypes = array('jpg','jpeg','gif','png'); // File extensions
			if (in_array($imageExtension,$imageTypes)) {
				//$this->s3->setAuth($this->config->item('access_key'), $this->config->item('secret_key'));
				$uri = "performers/{$imageSize}/{$imageSlug}.{$imageExtension}";
				$header = array("Content-Type" => "image/{$imageExtension}");
				//just in case delete
				//$this->s3->deleteObject($bucket, $uri);

				//resize this shit
				$image = new Imagick($tempFile);
				switch ($imageSize) :
					case 'profile':
						$h = 400;
						$w = 400;
						$image->scaleImage($w,$h);
						break;
					case 'wide':
						$h = 442;
						$w = 900;
						$image->scaleImage($w,$h);
						// echo ($image->writeImage($img)) ? "\n\r written $img " : "\n\r failed on $img ";
					break;
					case 'thumb':
						$h = 100;
						$w = 100;
						$image->scaleImage($w,$h);
						// echo ($image->writeImage($img)) ? "\n\r written $img " : "\n\r failed on $img ";
					break;
					default:
			//			code...
						break;
				endswitch;
				// $image->setImageCompressionQuality(75);

				$image->writeImage($tempFile);

				$input = $this->s3->inputFile($tempFile);
				if($this->s3->putObject($input, $bucket, $uri, "public-read", array(), $header)):
					$this->admin_m->update_performer_image($performerId, $uri, $imageSize);
					echo $uri;
				else:
					echo "/error.png";
				endif;

//                $credentials = new Credentials('AKIAJTGDNXKGSSK3OLTA', 'fe44Lzp1Z7Ex8g51nkmuuvWt0puFcBmvWCCxZChM');
//
//                $s3 = S3Client::factory(array(
//                    'credentials' => $credentials
//                ));
//
//                // Upload data.
//                try {
//                    $result = $s3->putObject(array(
//                        'Bucket' => $bucket,
//                        'Key'    => 'index.html',
//                        'Body'   => '<html><body><h1>Hello, world!</h1></body></html>',
//                        'ACL'    => 'public-read',
//                        'ContentType'  => 'text/html',
//                    ));
//                    echo $result['ObjectURL']. PHP_EOL;
//                } catch (S3Exception $e) {
//                    echo $e->getMessage() . PHP_EOL;
//                }


			} else {
				echo '0';
			}
		}
	}
}

/* End of file ajax.php */
/* Location: ./application/controllers/ajax.php */
