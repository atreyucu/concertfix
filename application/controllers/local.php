<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Local extends MY_SiteController {

	private $soapy;
	// private $twitterAuth;

	public function __construct()
	{
		parent::__construct();

		$this->load->model('concerts_m');
		$this->soapy = new TN_soap();
		// $this->soapy->initialize();
	}

	public function _remap($slug = false, $params = array())
	{
		 //var_dump($slug);

		if(isset($slug)):
			$this->_city($slug);
		else:
			//nothing was passed to url after /buy/
			//	redirect('/');
		endif;
	}

	public function index()
	{
		redirect('/');
	}

	private function _city($slug = false)
	{
		$this->load->library('user_agent');

		$location = deslug_location($slug);
		if(!$this->global_m->verify_location($location)):
			show_404();
		endif;

		if($this->caching):
			$events = $this->cache->model('global_m', 'get_front_page_events', array($location), 600); // keep for 10 min
		else:
			$events = $this->global_m->get_front_page_events($location);
		endif;
		//pdump(count($events));
		$events = array_slice($events,0,100);
		$events = $this->global_m->get_prices($events);
		//$events_cat = $this->global_m->get_childcategoryid_by_performer($events);

		if($this->caching):
			$sidebarFeatured = $this->cache->model('global_m', 'get_fetured_performers', array(), 600); // keep for 10 min
		else:
			$sidebarFeatured = $this->global_m->get_fetured_performers();
		endif;

		$categories = $this->concerts_m->get_categories();
		//
		// $cityText = $this->spinner_m->get_text('city','main', $cityEvents, $slug); //LEGACY
		//
		$cityText2 = $this->spinner_m->get_city_events_text($location, 'city_text',$events, $slug);
		$liText = $this->spinner_m->get_li_text($location, $events, "concert-city", $slug);
		$array = array('100%' => '200%');
		$liText = strtr($liText,$array);


		$cityVenues = $this->concerts_m->get_venues_by_location_slug($location['slug']);
		// $searchQuery = array("concerts", clean_venue($venue->Name));
		// $twitterFeed = $this->global_m->get_twitter_results($searchQuery);

		$starty = isset($events[0]) ?  date('Y', strtotime($events[0]->Date)) : false;
		$endy = ($starty) ? date('Y', strtotime(end($events)->Date)) : false;
		$dateRange = ($starty != $endy) ? $starty ." - ". $endy : $endy;

		if($this->caching):
			$nearbyCities = $this->cache->model('global_m', 'get_nearby_cities', array($location), 600); // keep for 10 min
		else:
			// $sidebarFeatured = $this->global_m->get_fetured_performers();
			$nearbyCities = $this->global_m->get_nearby_cities($location);
		endif;

		$pageData = array(
			// 'spinnedText'        => str_replace('{event_city}', $location['city'], $cityText), //LEGACY SPINNED
			'spinnedText2'      => $cityText2,
			'liText'        => $liText,
			'category'      => false,
			'filterType'    => false,
			'dates'         => false,
			'location'      => $location,
			'dateRange'			=> $dateRange,
			'cityEvents'    => $events,
			'allCityEvents'     => $events,
			//'events_cat' 	=> $events_cat,
			'categories'    => $categories,
			'sidebarFeatured' => $sidebarFeatured,
			'nearbyCities'  => $nearbyCities,
			'nearbyCitiesEvents' => array(),
			'cityVenues'    => $cityVenues,
		);

		//title for title of the page
		$title = "{$location['city']} Concerts $dateRange. {$location['city']}, {$location['state']} Concert Schedule and Calendar";
		$description = "Concerts scheduled in {$location['city']} $dateRange. Find a full {$location['city']}, {$location['state']} concert calendar and schedule";
		$keywords = "{$location['city']}, Concertfix, Concerts, Schedule, $dateRange";
		$yrobot = "noindex, nofollow";

		//array of open graph
		$ogMeta = array(
			'title'         => $title,
			'description'   => $description,
			'keywords'      => $keywords,
			'copyright'     => "Copyright " . Date('Y') . ", Concert Fix",
			'image'         => base_url()."public/img/assets/concert-fix-logo.png",
		);

		//array of meta
		$metaTags = array(
			'description' => $description,
			'keywords' => $keywords,
			'robots' => $yrobot,
			'googlebot' => $yrobot,
			'msnbot' => $yrobot,
		);

		//set title
		$this->template->title($title)
			->prepend_metatags($metaTags) //set meta tags
			->prepend_metatags($ogMeta, true); // $og = true


		$this->template
			->set_layout('flocal')
			->set_partial('header', 'frontend/global/header')
			//->set_partial('sidebar_filters', 'frontend/global/filters_local')
			->set_partial('sidebar_city_venues', 'frontend/global/city_venues_local')
			->set_partial('sidebar_tracker', 'frontend/global/tracker')
			->set_partial('global_city_schedule', 'frontend/concerts/local_city_schedule')
			//->set_partial('nearby_city_schedule', 'frontend/concerts/nearby_city_schedule')
			->set_breadcrumb('Home','/')
			->set_breadcrumb('All Cities','/concerts/all-cities')
			->set_breadcrumb(ucwords($location['city']).", ".strtoupper($location['state']))
			// ->set_partial('sidebar_twitter', 'frontend/global/twitter')
			->set_partial('footer', 'frontend/global/footer')
			->build('frontend/concerts/city_local', $pageData);

	}

}

/* End of file site.php */
/* Location: ./application/controllers/site.php */






