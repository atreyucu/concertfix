<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Concerts extends MY_SiteController {

	private $apiClass;
	private $auth;
	private $soapy;
	private $twitterAuth;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('concerts_m');
		$this->load->library('lastfmapi/lastfmapi');
		$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));

		// Pass the array to the auth class to return a valid auth

		$this->auth = new lastfmApiAuth('setsession');
		// pdump($this->auth);
		$this->apiClass = new lastfmApi();
		// pdump($this->apiClass);

		$this->soapy = new TN_soap();
		// $this->soapy->initialize();
	}

	public function index()
	{

	}

	public function _remap($slug = false, $params = array())
	{
		$_SESSION['uri_string'] = $this->uri->uri_string();

		$urlParams = explode('+', $slug);

		if(isset($urlParams[0]) && isset($urlParams[1])):
			if ($urlParams[1] == '')
				redirect('/concerts/'.$urlParams[0], 'location', 301);

			if($urlParams[1] == 'venues'):
				//all venues in city
				$this->_city_venues($urlParams[0]);
			elseif($urlParams[0] == 'genre'):
				//genre global
				$this->_genre_events($urlParams[1], $params);
			else:
				//city-st+filters
				$this->_city_filter($urlParams[0], $urlParams[1]);
			endif;
		elseif(isset($urlParams[0])):
			if($urlParams[0] == "all-cities"):
				$this->_show_all_cities();
			elseif($urlParams[0] == 'index'):
				redirect('/concerts/all-cities', 'location', 301);
			else:
				//city-st
				$this->_city($urlParams[0]);
			endif;
		else:
		//nothing was passed to url after /concerts/

		endif;
	}

	private function _city($slug = false)
	{
		// $this->output->enable_profiler(TRUE);
		//lets work the slug
		$location = deslug_location($slug);
		if(!$this->global_m->verify_location($location)):
			show_404();
		endif;
		$showAll = $this->input->get('showAll');
		// The beginDate needs to be semi-fixed so that the cache gets the
		// Same parameters
		$params = array(
						'beginDate' => date('c', strtotime('today midnight')),
						'orderByClause' => 'Date',
						'parentCategoryID' => '2',
						'stateProvDesc' => $location['state'],
						'cityZip' => $location['city']." ".$location['state'],
						'whereClause' => 'CountryID = 217 OR CountryID = 38',
						'numberOfEvents' => ($showAll) ? 500 : 100,
						);
		$paramsKey = md5(serialize($params));

		if( ! $cityEvents = $this->cache->get($paramsKey)) {
			$cityEvents = $this->_fetch_concert_events($params); //get them main events
			$this->cache->save($paramsKey, $cityEvents, 300); // store 5m
		}
		if(!$cityEvents)
				header("HTTP/1.1 404 Not Found");

		/*$allCityParams = array(
						'beginDate' => date('c', strtotime('today midnight')),
						'orderByClause' => 'Date',
						'parentCategoryID' => '2',
						'stateProvDesc' => $location['state'],
						'cityZip' => $location['city']." ".$location['state'],
						'whereClause' => 'CountryID = 217 OR CountryID = 38',
						'numberOfEvents' => 500
						);
		$allCityParamsKey = md5(serialize($allCityParams));

		if( ! $allCityEvents = $this->cache->get($allCityParamsKey)) {
			$allCityEvents = $this->_fetch_concert_events($allCityParams); //get them main events
			$this->cache->save($allCityParamsKey, $allCityEvents, 3600); // store 60m
		}*/

		if($this->caching):
			$sidebarFeatured = $this->cache->model('global_m', 'get_fetured_performers', array(), 600); // keep for 10 min
		else:
			$sidebarFeatured = $this->global_m->get_fetured_performers();
		endif;

		$categories = $this->concerts_m->get_categories();
		//
		// $cityText = $this->spinner_m->get_text('city','main', $cityEvents, $slug); //LEGACY
		//
		$cityText2 = $this->spinner_m->get_city_events_text($location, 'city_text',$cityEvents, $slug);
		$liText = $this->spinner_m->get_li_text($location, $cityEvents, "concert-city", $slug);
		$array = array('100%' => '200%');
		//$liText = strtr($liText,$array);

		if($this->caching):
			$nearbyCities = $this->cache->model('global_m', 'get_nearby_cities', array($location), 600); // keep for 10 min
		else:
			// $sidebarFeatured = $this->global_m->get_fetured_performers();
			$nearbyCities = $this->global_m->get_nearby_cities($location);
		endif;

		$nearby_events = array();
		if (count($cityEvents) < 30){
			for ($i = 0; $i < 3 and $i < count($nearbyCities); $i++){
				$temp = $this->_nearby_city_events($nearbyCities[$i]['slug'], 11);
				if (count($temp) > 0)
					$nearby_events[] = $temp;
			}
		}

        $highEvents = array();
		if (count($cityEvents) >= 30) {

            foreach ($cityEvents as $key => $event):
                $highEvents[$key] = ($event->prices) ? (int)$event->prices->highPrice : 0;
            endforeach;
            arsort($highEvents,SORT_NUMERIC);

            $tmp = array();
            foreach ($highEvents as $key => $event) {
                $tmp[$key] = $event;
                if (count($tmp) == 5)
                    break;
            }
            $highEvents = $tmp;
        }

		$cityVenues = $this->concerts_m->get_venues_by_location_slug($location['slug']);
		// $searchQuery = array("concerts", clean_venue($venue->Name));
		// $twitterFeed = $this->global_m->get_twitter_results($searchQuery);

		/*$starty = isset($cityEvents[0]) ?  date('Y', strtotime($cityEvents[0]->Date)) : false;
		$endy = ($starty) ? date('Y', strtotime(end($cityEvents)->Date)) : false;
		$dateRange = ($starty != $endy) ? $starty ." - ". $endy : $endy;*/

        $dates = array();
        foreach ($cityEvents as $event){
            if ((date('Y', strtotime($event->Date)) - date('Y')) > 2)
                break;
            $dates[] = date('Y', strtotime($event->Date));
        }
        $starty = count($dates) ?  $dates[0] : false;
        $endy = ($starty) ? end($dates) : false;
        $dateRange = ($starty != $endy) ? $starty ." - ". $endy : $endy;


        $listItem = array();
        $listItem[0] = ['name' => 'All Cities', 'url'=> base_url().'concerts/all-cities'];
        $listItem[1] = ['name' => ucwords($location['city']).", ".strtoupper($location['state']), 'url'=> base_url().'concerts/'.$location['slug']];

		$pageData = array(
			// 'spinnedText'        => str_replace('{event_city}', $location['city'], $cityText), //LEGACY SPINNED
			'spinnedText2'  => $cityText2,
            'highEvents'    => $highEvents,
			'liText'        => $liText,
			'category'      => false,
			'filterType'    => false,
			'dates'         => false,
			'location'      => $location,
			'dateRange'			=> $dateRange,
			'cityEvents'    => $cityEvents,
			'allCityEvents'     => $cityEvents, //$allCityEvents,
			'categories'    => $categories,
			'sidebarFeatured' => $sidebarFeatured,
			'nearbyCities'  => $nearbyCities,
			'nearbyCitiesEvents' => $nearby_events,
			'cityVenues'    => $cityVenues,
            'listItem'          => $listItem
		);

		//title for title of the page
		$title = "{$location['city']} Concerts $dateRange. {$location['city']}, {$location['state']} Concert Schedule and Calendar";
		$description = "Concerts scheduled in {$location['city']} $dateRange. Find a full {$location['city']}, {$location['state']} concert calendar and schedule";
		$keywords = "{$location['city']}, Concertfix, Concerts, Schedule, $dateRange";
		$yrobot = "index, follow";

		//array of open graph
		$base_url = strtr(base_url(), array('http:'=>'https:'));
		$ogMeta = array(
			'title'         => $title,
			'description'   => $description,
			'keywords'      => $keywords,
			'copyright'     => "Copyright " . Date('Y') . ", Concert Fix",
			'image'         => $base_url."public/img/assets/concert-fix-logo.png",
		);

		//array of meta
		$metaTags = array(
			'description' => $description,
			'keywords' => $keywords,
			'robots' => $yrobot,
			'googlebot' => $yrobot,
			'msnbot' => $yrobot,
		);

		//set title
		$this->template->title($title)
						->prepend_metatags($metaTags) //set meta tags
						->prepend_metatags($ogMeta, true); // $og = true


		$this->template
				->set_layout('frontend')
                ->set_partial('ldjson', 'frontend/global/breadcrumb_ldjson')
				->set_partial('header', 'frontend/global/header')
				->set_partial('sidebar_filters', 'frontend/global/filters')
				->set_partial('sidebar_city_venues', 'frontend/global/city_venues')
				->set_partial('sidebar_tracker', 'frontend/global/tracker')
				->set_partial('global_city_schedule', 'frontend/concerts/global_city_schedule')
				->set_partial('schedule_jsonld', 'frontend/concerts/schedule_jsonld')
				->set_partial('nearby_city_schedule', 'frontend/concerts/nearby_city_schedule')
				->set_partial('nearbycity_jsonld', 'frontend/concerts/nearbycity_jsonld')
				->set_breadcrumb('Home','/')
				->set_breadcrumb('All Cities','/concerts/all-cities')
				->set_breadcrumb(ucwords($location['city']).", ".strtoupper($location['state']))
				// ->set_partial('sidebar_twitter', 'frontend/global/twitter')
				->set_partial('footer', 'frontend/global/footer')
				->build('frontend/concerts/city_index', $pageData);
	}

	private function _nearby_city_events($slug, $limit){
		$location = deslug_location($slug);
		if(!$this->global_m->verify_location($location)):
			return false;
		endif;

		$params = array(
			'beginDate' => date('c', strtotime('today midnight')),
			'orderByClause' => 'Date',
			'parentCategoryID' => '2',
			'stateProvDesc' => $location['state'],
			'cityZip' => $location['city']." ".$location['state'],
			'whereClause' => 'CountryID = 217 OR CountryID = 38',
			'numberOfEvents' => $limit
		);
		$paramsKey = md5(serialize($params));

		$cityEvents = $this->cache->get($paramsKey);
		if(!$cityEvents) {
			$cityEvents = $this->_fetch_concert_events($params); //get them main events
			$this->cache->save($paramsKey, $cityEvents, 300); // store 5m
		}
		if (!$cityEvents)
			return array();
		else
			return $cityEvents;
	}

	private function _nearby_city_genre($slug, $category){
		$location = deslug_location($slug);
		if(!$this->global_m->verify_location($location)):
			return false;
		endif;

		$params = array(
			'beginDate' => date('c', strtotime('today midnight')),
			'orderByClause' => 'Date',
			'parentCategoryID' => '2',
			'cityZip' => $location['city']." ".$location['state'],
			'childCategoryID'   => $category[0]->ChildCategoryID,
			'stateProvDesc' => $location['state'],
			'whereClause' => 'CountryID = 217 OR CountryID = 38',
			'numberOfEvents' => 11
		);
		$paramsKey = md5(serialize($params));

		$cityEvents = $this->cache->get($paramsKey);
		if(!$cityEvents) {
			// die('NO CACHE?');
			$cityEvents = $this->_fetch_concert_events($params); //get them main events
			$this->cache->save($paramsKey, $cityEvents, 300); // store 5m
		}

		if (!$cityEvents)
			return array();
		else
			return $cityEvents;
	}

	private function _show_all_cities()
	{
		// $this->output->cache(600);

		$usCities = $this->concerts_m->get_cities(217);
		$canCities= $this->concerts_m->get_cities(38);
		$categorized = $this->global_m->fetch_categorized_announcements();

        $listItem = array();
        $listItem[0] = ['name' => 'All Cities', 'url'=> base_url().'concerts/all-cities'];

        $pageData = array(
            'usCities' => $usCities,
            'canCities' => $canCities,
            'categorized' => $categorized,
            'cities' => $this->global_m->get_top_cities(),
            'listItem' => $listItem
        );
		$title = "Browse through all Concert Cities | Concertfix.com";
		$description = "Browse through all North American concert cities in the United States and Canada.";
		$keywords = "All cities, Concertfix, Concerts, " . Date('Y');
		$yrobot = "index, follow";
		$base_url = strtr(base_url(), array('http:'=>'https:'));
		$ogMeta = array(
						'title'         => $title,
						'description'   => $description,
						'keywords'      => $keywords,
						'copyright'     => "Copyright " . Date('Y') . ", Concert Fix",
						'image'         => $base_url."public/img/assets/concert-fix-logo.png",
						// 'url'            => "this is where the page URL goes",
						);
		$metaTags = array(
			  'description' => $description,
			  'keywords'    => $keywords,
			  'robots' => $yrobot,
			  'googlebot' => $yrobot,
			  'msnbot' => $yrobot,
						  );
		$this->template->title($title)
						->prepend_metatags($metaTags)
						->prepend_metatags($ogMeta, true);
		$this->template
				->set_layout('frontend')
                ->set_partial('ldjson', 'frontend/global/breadcrumb_ldjson')
				->set_partial('header', 'frontend/global/header')
				// ->set_partial('sidebar_filters', 'frontend/global/filters')
				->set_partial('sidebar_topcities', 'frontend/global/top_cities')
				->set_partial('sidebar_tracker', 'frontend/global/tracker')
				->set_partial('sidebar_announced_categories', 'frontend/global/announced_categories')
				// ->set_partial('sidebar_twitter', 'frontend/global/twitter')
				->set_partial('footer', 'frontend/global/footer')
				->set_breadcrumb('Home','/')
				->set_breadcrumb('All Cities')
				->build('frontend/concerts/all_cities', $pageData);
	}

	//SHOW ALL VENUES IN CITY!!!
	private function _city_venues($slug)
	{
		$location = deslug_location($slug);
		if(!$this->global_m->verify_location($location)):
			show_404();
		endif;
		$venues = $this->global_m->get_venue(false, false, $location);
		$venues_data = array();
		foreach($venues as $venue){
			$replace = array('http://seatics.tickettransaction.com/'=>'https://maps.seatics.com/');
			$url = strtr($venue->SeatChart,$replace);
			$venues_data[$venue->Name] = $url;
		}
		if (count($venues) == 0)
			show_404();
		$sidebarFeatured = $this->global_m->get_fetured_performers();
		$categories = $this->concerts_m->get_categories();
		// $cityText = $this->spinner_m->get_text('city','main', $cityEvents, $slug);
		$nearbyCities = $this->global_m->get_nearby_cities($location);
		$cityVenues = $this->concerts_m->get_venues_by_location_slug($location['slug']);

		if(!$cityVenues)
			show_404();

        $listItem = array();
        $listItem[0] = ['name' => 'All Cities', 'url'=> base_url().'concerts/all-cities'];
        $listItem[1] = ['name' => ucwords($location['city']).", ".strtoupper($location['state']), 'url'=> base_url().'concerts/'.$location['slug']];
        $listItem[2] = ['name' => ucwords(strtolower($location['city']." Venues")), 'url'=> base_url().'concerts/'.$location['slug'].'+venues'];

        $pageData = array(
						'category'      => false,
						'filterType'    => false,
						'dates'         => false,
						'location'      => $location,
						'categories'    => $categories,
						'sidebarFeatured' => $sidebarFeatured,
						'nearbyCities'  => $nearbyCities,
						'cityVenues'    => $cityVenues,
						'venues'        => $venues,
						'venues_data'   => $venues_data,
                        'listItem'      => $listItem
						);

		$title = "Concert Venues in {$location['city']}, {$location['state']} | ConcertFix.com";
		$description = "Browse through all concert venues in {$location['city']}, {$location['state']} with at least one concert scheduled";
		$keywords = "{$location['city']}, Concerts, Concertfix, Venues";
		$yrobot = "index, follow";
		$base_url = strtr(base_url(), array('http:'=>'https:'));
		$ogMeta = array(
						'title'         => $title,
						'description'   => $description,
						'keywords'      => $keywords,
						'copyright'     => "Copyright " . Date('Y') . ", Concert Fix",
						'image'         => $base_url."public/img/assets/concert-fix-logo.png",
						// 'url'            => "this is where the page URL goes",
						);
		$metaTags = array(
						'title'     => $title,
						'description'   => $description,
						'keywords'  => $keywords,
						'robots' => $yrobot,
						'googlebot' => $yrobot,
						'msnbot' => $yrobot,
						  );
		$this->template->title($title)
						->prepend_metatags($metaTags)
						->prepend_metatags($ogMeta, true);
		$this->template
				->set_layout('frontend')
                ->set_partial('ldjson', 'frontend/global/breadcrumb_ldjson')
				->set_partial('header', 'frontend/global/header')
				->set_partial('sidebar_filters', 'frontend/global/filters')
				->set_partial('sidebar_city_venues', 'frontend/global/city_venues')
				->set_partial('sidebar_tracker', 'frontend/global/tracker')
				// ->set_partial('sidebar_twitter', 'frontend/global/twitter')
				->set_partial('footer', 'frontend/global/footer')
				->set_breadcrumb('Home','/')
				->set_breadcrumb('All Cities','/concerts/all-cities')
				->set_breadcrumb(ucwords($location['city'].', '.strtoupper($location['state'])), '/concerts/'.$location['slug'])
                ->set_breadcrumb(ucwords($location['city'])." Venues")
				->build('frontend/concerts/city_venues', $pageData);
	}

	/*********************
	THis is is universal function
	TODO: SPLIT THIS UP (if needed)
	It loads:
		-   single category events by slug (i.e. Pop-Rock, etc)
		-   single venue events by slug
		-   dated events
	***************/

	private function _city_filter($slug = false, $filter = false)
	{
		$pathSlug = $this->uri->segment(2);
		$venue = false;
		$location = deslug_location($slug);
		if(!$this->global_m->verify_location($location)):
			//pdump('option 1');
			show_404();
		endif;
		$dates = false;
		$filterType = false;

		if($category = $this->concerts_m->get_categories(false,$filter)): //GENRE
			$type = 'category';
			$filterType = 'category';
			$params = array(
						'beginDate' => date('c', strtotime('today midnight')),
						'orderByClause' => 'Date',
						'parentCategoryID' => '2',
						'cityZip' => $location['city']." ".$location['state'],
						'childCategoryID'   => $category[0]->ChildCategoryID,
						'stateProvDesc' => $location['state'],
						'whereClause' => 'CountryID = 217 OR CountryID = 38'
						);
			$paramsKey = md5(serialize($params));

			if( ! $cityEvents = $this->cache->get($paramsKey)) {
				// die('NO CACHE?');
				$cityEvents = $this->_fetch_concert_events($params); //get them main events
				$this->cache->save($paramsKey, $cityEvents, 300); // store 5m
			}

			if(!$cityEvents)
				header("HTTP/1.1 404 Not Found");

			$allCityParams = array(
				'beginDate' => date('c', strtotime('today midnight')),
				'orderByClause' => 'Date',
				'parentCategoryID' => '2',
				'stateProvDesc' => $location['state'],
				'cityZip' => $location['city']." ".$location['state'],
				'whereClause' => 'CountryID = 217 OR CountryID = 38',
				'numberOfEvents' => 500
			);

			$allCityParamsKey = md5(serialize($allCityParams));

			if( ! $allCityEvents = $this->cache->get($allCityParamsKey)) {
				// die('NO CACHE?');
				$allCityEvents = $this->_fetch_concert_events($allCityParams); //get them main events
				$this->cache->save($allCityParamsKey, $allCityEvents, 3600); // store 5m
			}

			// $performers = $this->concerts_m->get_performers_by_genre_id($category[0]->ChildCategoryID);
			$categories = $this->concerts_m->get_categories();
			$cityEvents = !$cityEvents ? array() : $cityEvents;

			// die(var_dump($cityEvents));
			$cityGenreText = $this->concerts_m->get_main_text(array(
				"events_count" => count($cityEvents),
				"type" => "genre",
				"city" => $location['city'],
				"genre" => ucwords(strtolower($category[0]->Filter))
			));

			$array = array('125 percent' => '100 percent', '125%' => '100%');
			$cityGenreText = strtr($cityGenreText,$array);

			$liText = $this->spinner_m->get_li_text($location, $cityEvents, "concert-city-genre", $pathSlug, $category[0]->Filter);
			$array = array('100%' => '200%');
			//$liText = strtr($liText,$array);

            $dates = array();
            foreach ($cityEvents as $event){
                if ((date('Y', strtotime($event->Date)) - date('Y')) > 2)
                    break;
                $dates[] = date('Y', strtotime($event->Date));
            }
            $starty = count($dates) ?  $dates[0] : false;
            $endy = ($starty) ? end($dates) : false;
            $dateRange = ($starty != $endy) ? $starty ." - ". $endy : $endy;

			if($this->caching):
				$nearbyCities = $this->cache->model('global_m', 'get_nearby_cities', array($location), 600); // keep for 10 min
			else:
				// $sidebarFeatured = $this->global_m->get_fetured_performers();
				$nearbyCities = $this->global_m->get_nearby_cities($location);
			endif;

			$nearby_events = array();
			if (count($cityEvents) < 30){
				for ($i = 0; $i < 3 and $i < count($nearbyCities); $i++){
					$temp = $this->_nearby_city_genre($nearbyCities[$i]['slug'], $category);
					if (count($temp) > 0)
						$nearby_events[] = $temp;
				}
			}

            $listItem = array();
            $listItem[0] = ['name' => 'All Cities', 'url'=> base_url().'concerts/all-cities'];
            $listItem[1] = ['name' => ucwords($location['city']).", ".strtoupper($location['state']), 'url'=> base_url().'concerts/'.$location['slug']];
            $listItem[2] = ['name' => ucwords(strtolower($location['city']." ".$category[0]->Filter)), 'url'=> base_url().'concerts/'.$location['slug'].'+'.$category[0]->Slug];

            $pageData = array(
				// 'venue'          => $venue,
				// 'dates'          => $dates,
				'category'      => $category,
				'cityGenreText' => $cityGenreText,
				'liText'        => $liText,
				'filterType'    => $filterType,
				'dateRange'		=> $dateRange,
				'location'      => $location,
				// 'performers'     => $performers,
				'cityEvents'    => $cityEvents,
				'allCityEvents' => $allCityEvents,
				'categories'    => $categories,
				'sidebarFeatured' => $this->global_m->get_fetured_performers(),
				'nearbyCities'  => $nearbyCities,
				'nearbyCitiesEvents' => $nearby_events,
				'genre_type' => $category[0]->Filter,
				'cityVenues'    => $this->concerts_m->get_venues_by_location_slug($location['slug']),
                'listItem'      => $listItem
			);

			$category[0]->Filter = ucwords(strtolower($category[0]->Filter));
            if ($dateRange == '') {
                $title = "{$location['city']} {$category[0]->Filter} Concerts. {$location['city']}, {$location['state']} {$category[0]->Filter} Concert Schedule and Calendar";
                $description = "{$category[0]->Filter} concerts scheduled in {$location['city']}. Find a full {$location['city']}, {$location['state']} {$category[0]->Filter} concert calendar and schedule.";
            } else {
                $title = "{$location['city']} {$category[0]->Filter} Concerts $dateRange. {$location['city']}, {$location['state']} {$category[0]->Filter} Concert Schedule and Calendar";
                $description = "{$category[0]->Filter} concerts scheduled in {$location['city']} in " . $dateRange . ". Find a full {$location['city']}, {$location['state']} {$category[0]->Filter} concert calendar and schedule.";
            }
			$keywords = "{$location['city']}, {$category[0]->Filter}, Concertfix, Concerts, " . date('Y');
			$yrobot = "index, follow";
			$base_url = strtr(base_url(), array('http:'=>'https:'));
			$ogMeta = array(
				'title'         => $title,
				'description'   => $description,
				'keywords'      => $keywords,
				'copyright'     => "Copyright " . Date('Y') . ", Concert Fix",
				'image'         => $base_url."public/img/assets/concert-fix-logo.png",
				// 'url'            => "this is where the page URL goes",
			);
			$metaTags = array(
				'title'       => $title,
				'description' => $description,
				'keywords'    => $keywords,
				'robots' => $yrobot,
				'googlebot' => $yrobot,
				'msnbot' => $yrobot,
			);
			$this->template->title($title)
				->prepend_metatags($metaTags)
				->prepend_metatags($ogMeta, true);
			$this->template
				->set_layout('frontend')
                ->set_partial('ldjson', 'frontend/global/breadcrumb_ldjson')
				->set_partial('header', 'frontend/global/header')
				->set_partial('sidebar_filters', 'frontend/global/filters')
				->set_partial('sidebar_city_venues', 'frontend/global/city_venues')
				->set_partial('sidebar_tracker', 'frontend/global/tracker')
				->set_partial('global_city_schedule', 'frontend/concerts/global_city_schedule')
				->set_partial('nearby_city_schedule', 'frontend/concerts/nearby_city_genre_schedule')
				->set_partial('schedule_jsonld', 'frontend/concerts/schedule_jsonld')
				->set_partial('nearbycity_jsonld', 'frontend/concerts/nearbycity_jsonld')
				// ->set_partial('sidebar_twitter', 'frontend/global/twitter')
				->set_partial('footer', 'frontend/global/footer')
				->set_breadcrumb('Home','/')
				->set_breadcrumb('All Cities','/concerts/all-cities')
				->set_breadcrumb(ucwords($location['city']).", ".strtoupper($location['state']), "/concerts/{$location['slug']}")
				->set_breadcrumb(ucwords(strtolower($location['city']." ".$category[0]->Filter)))
				->build('frontend/concerts/city_category_filter_index', $pageData);

		elseif($venue = $this->global_m->get_venue(false, $filter, $location)): //VENUE
			//pdump('venues');
			//$additionalheaders = "<script src='https://maps.googleapis.com/maps/api/js?sensor=false' type='text/javascript'></script>
								  //<script src='/public/js/gmap.js' type='text/javascript'></script>";

			$additionalheaders  = '<script src="/public/js/gmap-v2.js" type="text/javascript"></script>
								   <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmZBXiJoUB2LT3G-zK8T9xm63O5TAG5I8&callback=initMap" type="text/javascript"></script>';

			#This code is to add or remove the map
			// $this->template->set_partial('sidebar_gmap_side', 'frontend/global/gmap_side');

			$type = 'city_venue';
			$params = array(
				'beginDate' => date('c', strtotime('today midnight')),
				'orderByClause' => 'Date',
				'parentCategoryID' => '2',
				'venueID'   => $venue->ID,
				'cityZip' => $location['city'].", ".$location['state'],
				'whereClause' => 'CountryID = 217 OR CountryID = 38'
			);
			$paramsKey = md5(serialize($params));

			if( ! $cityEvents = $this->cache->get($paramsKey)) {
				$cityEvents = $this->_fetch_concert_events($params); //get them main events
				$this->cache->save($paramsKey, $cityEvents, 300); // store 5m
			}

			if(!$cityEvents) //no concerts at the venue shows 404 page
				header("HTTP/1.1 404 Not Found");

			$allCityParams = array(
				'beginDate' => date('c', strtotime('today midnight')),
				'orderByClause' => 'Date',
				'parentCategoryID' => '2',
				'stateProvDesc' => $location['state'],
				'cityZip' => $location['city']." ".$location['state'],
				'whereClause' => 'CountryID = 217 OR CountryID = 38',
				'numberOfEvents' => 500
			);
			$allCityParamsKey = md5(serialize($allCityParams));

			if( ! $allCityEvents = $this->cache->get($allCityParamsKey)) {
				$allCityEvents = $this->_fetch_concert_events($allCityParams); //get them main events
				$this->cache->save($allCityParamsKey, $allCityEvents, 3600); // store 5m
			}

			$categories = $this->concerts_m->get_categories();
			$liText = $this->spinner_m->get_li_text($location, $cityEvents, 'concert-venue', $pathSlug, $venue);
			$array = array('100%' => '200%');
			//$liText = strtr($liText,$array);

			$cityVenueText = $this->concerts_m->get_main_text(array(
				"events_count" => count($cityEvents),
				"type" => "venue",
				"city" => $location['city'],
				"venue" => clean_venue($venue->Name),
			));

			$array = array('125 percent' => '100 percent', '125%' => '100%');
			$cityVenueText = strtr($cityVenueText,$array);

            $dates = array();
            foreach ($cityEvents as $event){
                if ((date('Y', strtotime($event->Date)) - date('Y')) > 2)
                    break;
                $dates[] = date('Y', strtotime($event->Date));
            }
            $starty = count($dates) ?  $dates[0] : false;
            $endy = ($starty) ? end($dates) : false;
            $dateRange = ($starty != $endy) ? $starty ." - ". $endy : $endy;

            $listItem = array();
            $listItem[0] = ['name' => 'All Cities', 'url'=> base_url().'concerts/all-cities'];
            $listItem[1] = ['name' => ucwords($location['city']).", ".strtoupper($location['state']), 'url'=> base_url().'concerts/'.$location['slug']];
            $listItem[2] = ['name' => ucwords(strtolower(clean_venue($venue->Name))), 'url'=> base_url().'concerts/'.$location['slug'].'+'.$venue->VenueSlug];

			$pageData = array(
				'venue'         => $venue,
				'dates'         => $dates,
				'category'      => $category,
				'cityVenueText' => $cityVenueText,
				'liText'        => $liText,
				'filterType'    => $filterType,
				'dateRange'		=> $dateRange,
				'location'      => $location,
				'cityEvents'    => $cityEvents,
				'allCityEvents' => $allCityEvents,
				'categories'    => $categories,
				'sidebarFeatured' => $this->global_m->get_fetured_performers(),
				'nearbyCities'  => $this->global_m->get_nearby_cities($location),
				'cityVenues'    => $this->concerts_m->get_venues_by_location_slug($location['slug']),
                'listItem'      => $listItem
			);
			$title = clean_venue($venue->Name)." Concerts " . $dateRange . ". ".clean_venue($venue->Name)." Concert Schedule and Calendar";
			$description = clean_venue($venue->Name)." concerts scheduled in " . $dateRange . ". Find a full ".clean_venue($venue->Name)." concert calendar and schedule.";
			$keywords = clean_venue($venue->Name).", concerts, concertfix, " . date('Y');
			$base_url = strtr(base_url(), array('http:'=>'https:'));
			$ogMeta = array(
				'title'         => $title,
				'description'   => $description,
				'keywords'      => $keywords,
				'copyright'     => "Copyright " . Date('Y') . ", Concert Fix",
				'image'         => $base_url."public/img/assets/concert-fix-logo.png"
			);
			$yrobot = "index, follow";
			$metaTags = array(
				'title'        => $title,
				'description'  => $description,
				'keywords'     => $keywords,
				'robots' => $yrobot,
				'googlebot' => $yrobot,
				'msnbot' => $yrobot,
			);
			$this->template->title($title)
				->prepend_metatags($metaTags)
				->prepend_metatags($ogMeta, true);
			$this->template
				->set_layout('frontend')
                ->set_partial('ldjson', 'frontend/global/breadcrumb_ldjson')
				->set_partial('header', 'frontend/global/header')
				->set_partial('sidebar_tracker', 'frontend/global/tracker')
				->set_partial('sidebar_filters', 'frontend/global/filters')
				->set_partial('sidebar_city_venues', 'frontend/global/city_venues')
				->set_partial('global_city_schedule', 'frontend/concerts/global_city_schedule')
				->set_partial('schedule_jsonld', 'frontend/concerts/schedule_jsonld')
				// ->set_partial('sidebar_twitter', 'frontend/global/twitter')
				->set_partial('footer', 'frontend/global/footer', array('extra' => $additionalheaders))
				->set_breadcrumb('Home','/')
				->set_breadcrumb('All Cities','/concerts/all-cities')
				->set_breadcrumb(ucwords($location['city'].', '.$location['state']), "/concerts/{$location['slug']}")
				->set_breadcrumb(clean_venue($venue->Name))
				->build('frontend/concerts/city_venue_index', $pageData);

		else: //this is date
			//pdump('option 3');
			$type = 'city_date';
			$filterType = $filter;
			$month = date('n', strtotime($filter));
			$year = date('Y', strtotime($filter));
			$first = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
			$last = date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));

			if(strtotime($last) < time()) {
				show_404();
			}

			$dates = array(
				'start' => date('M j, Y', strtotime($first)),
				'end'   =>date('M j, Y', strtotime($last)),
			);
			// TICKETNETWORK
			// + 60 * 60 * 6 adds 6 hours to the time to account
			// for PCT
			$params = array(
				'beginDate' => date('c', strtotime($first) + 60 * 60 * 6),
				'endDate'   => date('c', strtotime($last) + 60 * 60 * 6),
				'orderByClause' => 'Date',
				'parentCategoryID' => '2',
				'cityZip' => $location['city']." ".$location['state'],
				'stateProvDesc' => $location['state'],
				'whereClause' => 'CountryID = 217 OR CountryID = 38'
			);
			// var_dump($params);
			$paramsKey = md5(serialize($params));

			if( ! $cityEvents = $this->cache->get($paramsKey)) {
				$cityEvents = $this->_fetch_concert_events($params); //get them main events
				$this->cache->save($paramsKey, $cityEvents, 300); // store 60m
			}

			if(!$cityEvents)
				header("HTTP/1.1 404 Not Found");

			$allCityParams = array(
				'beginDate' => date('c', strtotime('today midnight')),
				'orderByClause' => 'Date',
				'parentCategoryID' => '2',
				'stateProvDesc' => $location['state'],
				'cityZip' => $location['city']." ".$location['state'],
				'whereClause' => 'CountryID = 217 OR CountryID = 38',
				'numberOfEvents' => 500
			);
			$allCityParamsKey = md5(serialize($allCityParams));

			if( ! $allCityEvents = $this->cache->get($allCityParamsKey)) {
				$allCityEvents = $this->_fetch_concert_events($allCityParams); // grabs main squeeze
				$this->cache->save($allCityParamsKey, $allCityEvents, 3600); // store 60m
			}

			$categories = $this->concerts_m->get_categories();

			$cityDateText = $this->concerts_m->get_main_text(array(
				"events_count" => count($cityEvents),
				"type" => "date",
				"city" => $location['city'],
				"month" => date('F', strtotime($filter)),
				"year" => date('Y', strtotime($filter))
			));
			$array = array('125 percent' => '100 percent', '125%' => '100%');
			$cityDateText = strtr($cityDateText,$array);

			$liText = $this->spinner_m->get_li_text($location, $cityEvents, 'concert-city-date', $pathSlug, date('F Y', strtotime($dates['start']) ) );
			$array = array('100%' => '200%');
			//$liText = strtr($liText,$array);

            $listItem = array();
            $listItem[0] = ['name' => 'All Cities', 'url'=> base_url().'concerts/all-cities'];
            $listItem[1] = ['name' => ucwords($location['city']).", ".strtoupper($location['state']), 'url'=> base_url().'concerts/'.$location['slug']];
            $listItem[2] = ['name' => ucwords(strtolower($location['city']." ".date('F Y', strtotime($dates['start'])))), 'url'=> base_url().'concerts/'.$location['slug'].'+'.$filter];

			$pageData = array(
				'venue'         => $venue,
				'dates'         => $dates,
				'category'      => $category,
				'cityDateText'  => $cityDateText,
				'liText'        => $liText,
				'filterType'    => $filterType,
				'location'      => $location,
				'cityEvents'    => $cityEvents,
				'allCityEvents' => $allCityEvents,
				'categories'    => $categories,
				'sidebarFeatured' => $this->global_m->get_fetured_performers(),
				'nearbyCities'  => $this->global_m->get_nearby_cities($location),
				'cityVenues'    => $this->concerts_m->get_venues_by_location_slug($location['slug']),
                'listItem'      => $listItem
			);

			$title = "{$location['city']} Concerts ".date('F Y', strtotime($dates['start'])).". {$location['city']}, {$location['state']} ".date('F Y', strtotime($dates['start']))." Concert Schedule and Calendar";
			$description = "{$location['city']} concerts scheduled in ".date('F Y', strtotime($dates['start'])).". Find a full ".date('F Y', strtotime($dates['start']))." {$location['city']}, {$location['state']} concert calendar and schedule.";
			$keywords = "{$location['city']}, ".date('F Y', strtotime($dates['start'])).", Concerts, Concertfix";
			$yrobot = "noindex, follow";
			$base_url = strtr(base_url(), array('http:'=>'https:'));
			$ogMeta = array(
				'title'         => $title,
				'description'   => $description,
				'keywords'      => $keywords,
				'copyright'     => "Copyright " . Date('Y') . ", Concert Fix",
				'image'         => $base_url."public/img/assets/concert-fix-logo.png",
				// 'url'            => "this is where the page URL goes",
			);
			$metaTags = array(
				'title'       => $title,
				'description' => $description,
				'keywords'    => $keywords,
				'robots' => $yrobot,
				'googlebot' => $yrobot,
				'msnbot' => $yrobot,
			  );
			$this->template->title($title)
				->prepend_metatags($metaTags)
				->prepend_metatags($ogMeta, true);
			$this->template
				->set_layout('frontend')
                ->set_partial('ldjson', 'frontend/global/breadcrumb_ldjson')
				->set_partial('header', 'frontend/global/header')
				->set_partial('sidebar_filters', 'frontend/global/filters')
				->set_partial('sidebar_city_venues', 'frontend/global/city_venues')
				->set_partial('sidebar_tracker', 'frontend/global/tracker')
				->set_partial('global_city_schedule', 'frontend/concerts/global_city_schedule')
				->set_partial('schedule_jsonld', 'frontend/concerts/schedule_jsonld')
				// ->set_partial('sidebar_twitter', 'frontend/global/twitter')
				->set_partial('footer', 'frontend/global/footer')
				->set_breadcrumb('Home','/')
				->set_breadcrumb('All Cities','/concerts/all-cities')
				->set_breadcrumb(ucwords($location['city']).', '.ucwords($location['state']), "/concerts/{$location['slug']}")
				->set_breadcrumb(ucwords(strtolower($location['city']." ".date('F Y', strtotime($dates['start']) ))))
				->build('frontend/concerts/city_date_filter_index', $pageData);
		endif;
	}

	/*******************************
	this function shows all genere events
	/concerts/genre+{genre}

	*/
	private function _genre_events($genreSlug, $usesoap = false)
	{

		if($cats = $this->global_m->get_genre_by_slug($genreSlug)):
			$categorized = $this->global_m->fetch_categorized_announcements();
			$performers = $this->concerts_m->get_performers_by_genre_id($cats[0]->ChildCategoryID, 500);

			$categories = $this->concerts_m->get_categories();
			$category = ucwords(strtolower($cats[0]->Filter));
			$filterType = false;

            $listItem = array();
            $listItem[0] = ['name' => 'Top Tours', 'url'=> base_url().'top-tours'];
            $listItem[1] = ['name' => "Top {$category} Tours", 'url'=> base_url()."concerts/genre+{$genreSlug}"];

			$pageData = array(
				'categorized'       => $categorized,
				// 'dates'          => $dates,
				'category'      => $category,
				// 'cityVenueText'  => $cityVenueText,
				'filterType'    => $filterType,
				// 'location'       => $location,
				'performers'    => $performers,
				'categories'    => $categories,
				'sidebarFeatured' => $this->global_m->get_fetured_performers(),
                'listItem'      => $listItem
				// 'nearbyCities'   => $this->global_m->get_nearby_cities($location),
				// 'cityVenues' => $this->concerts_m->get_venues_by_location_slug($location['slug']),
			);
			$title = "Top ".$category." Performers &amp; Concert Tours";
			$description = "The Top ".$category." Performers &amp; Concert Tours in " . date('Y');
			$keywords = $category.", Concerts, performers, artists, tours";
			$yrobot = "index, follow";
			$base_url = strtr(base_url(), array('http:'=>'https:'));
			$ogMeta = array(
				'title'         => $title,
				'description'   => $description,
				'keywords'      => $keywords,
				'copyright'     => "Copyright " . Date('Y') . ", Concert Fix",
				'image'         => $base_url."public/img/assets/concert-fix-logo.png",
				// 'url'            => "this is where the page URL goes",
			);
			$metaTags = array(
				'title'       => $title,
				'description' => $description,
				'keywords'    => $keywords,
				'robots' => $yrobot,
				'googlebot' => $yrobot,
				'msnbot' => $yrobot,
			);
			$this->template->title($title)
				->prepend_metatags($metaTags)
				->prepend_metatags($ogMeta, true);
			$this->template
				->set_layout('frontend')
                ->set_partial('ldjson', 'frontend/global/breadcrumb_ldjson')
				->set_partial('header', 'frontend/global/header')
				->set_partial('sidebar_announced_categories', 'frontend/global/announced_categories')
				// ->set_partial('sidebar_filters', 'frontend/global/filters')
				// ->set_partial('sidebar_city_venues', 'frontend/global/city_venues')
				// ->set_partial('sidebar_twitter', 'frontend/global/twitter')
				->set_partial('sidebar_tracker', 'frontend/global/tracker')
				->set_partial('footer', 'frontend/global/footer')
				->set_breadcrumb('Home','/')
				->set_breadcrumb('Top Tours','/top-tours')
				->set_breadcrumb(ucwords(strtolower("Top ".$category))." Tours")
				->build('frontend/concerts/genre_index', $pageData);
		else:
			redirect('/');
		endif;

	}

	private function _fetch_concert_events($params = false)
	{
		$cityEvents = $this->soapy->run_soap('GetEvents', $params);
		$cityEvents = $this->global_m->get_event_performers($cityEvents);
		$cityEvents = $this->global_m->get_prices($cityEvents);
		return $cityEvents;
	}



}

/* End of file concerts.php */
/* Location: ./application/controllers/concerts.php */
