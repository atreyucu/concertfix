<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//class Tours extends MY_SiteController {
class Tours extends TN_SiteController {

	private $apiClass;
	private $auth;
	public $soapy;


	public function __construct()
	{

		parent::__construct();
		$this->load->model('tours_m');
		$this->load->model('cron_m');
		// $this->load->library('lastfmapi/lastfmapi');

		// Pass the array to the auth class to return a valid auth

		// $this->auth = new lastfmApiAuth('setsession');
		// pdump($this->auth);
		// $this->apiClass = new lastfmApi();
		// pdump($this->apiClass);

		$this->soapy = new TN_soap();
		// $this->soapy->initialize();
	}

	public function _remap($slug = false, $params = array())
	{
		$_SESSION['uri_string'] = $this->uri->uri_string();

		$urlParams = explode('+', $slug);
		// var_dump($urlParams);
		if(isset($urlParams[1]) && isset($urlParams[2])): //performer with city and venue

			$performerSlug = $urlParams[0];
			$venueSlug = $urlParams[1];
			$cityStateSlug = $urlParams[2];
			$this->_load_performer_with_venue($performerSlug, $venueSlug, $cityStateSlug);

		elseif(isset($urlParams[1])): // this is only performer with city
			show_404();

		elseif(isset($urlParams[0])): // just the performer

			if($urlParams[0] == 'all-artists'):
				$this->_load_all();
			elseif($urlParams[0] == 'index'):
				redirect('/tours/all-artists', 'location', 301);
			else:
				$performerSlug = $urlParams[0];
				$this->_load_single_performer($performerSlug);
			endif;

		else: // nothing else

		endif;


	}

	private function _load_all()
	{

		$this->output->cache(600);

		if($this->caching):
			$performers = $this->cache->model('tours_m', 'get_all_performers', array('true'), 600); // keep for 2 minutes
		else:
			$performers = $this->tours_m->get_all_performers(true);
		endif;
		$categorized = $this->global_m->fetch_categorized_announcements();
		$title = "All Tour Dates & Concert Tickets";
		$description = "Browse through all artists with upcoming concerts in the United States and Canada.";
		$keywords = "all Concerts, tickets, all Performers, tour announcements, concertfix";
		$yrobot = "index, follow";
		$pageData = array(
 								'performers' => $performers,
 								'categorized'	=> $categorized,
 								'cities' => $this->global_m->get_top_cities()
 								// 'performerText' => $performerText,
 								// 'lfmPerformer' => $lfmPerformer,
 								// 'lastAlbum' => $lastAlbum,
 								// 'albums' => $albums,
 								// 'tourDates' => $tourDates,
 								// 'sidebarFeatured' => $this->global_m->get_fetured_performers(),
 							);
 		// $ogMeta = "<meta property='og:title' content='{$performer[0]->PerformerName} Tour Dates ".date('Y', strtotime($tourDates[0]->Date))."'>";
		$base_url = strtr(base_url(), array('http:'=>'https:'));
		$ogMeta = array(
 						'title' 		=> $title,
		                'description' 	=> $description,
		                'keywords'		=> $keywords,
		                'copyright'		=> "Copyright " . Date('Y') . ", Concert Fix",
		                'image'			=> $base_url."public/img/assets/concert-fix-logo.png",

		                );
				$metaTags = array(
	                  'description'	=> $description,
	                  'keywords' 	=> $keywords,
	                  'robots' => $yrobot,
					  'googlebot' => $yrobot,
					  'msnbot' => $yrobot,
				                  );
				$this->template->prepend_metatags($metaTags)
							   ->prepend_metatags($ogMeta, true);
				$this->template
				->set_layout('frontend')
				->title($title)
				// ->append_metadata($ogMeta)
				->set_partial('header', 'frontend/global/header')
				->set_partial('sidebar_announced_categories', 'frontend/global/announced_categories')
				->set_partial('sidebar_tracker', 'frontend/global/tracker')
				->set_partial('sidebar_topcities', 'frontend/global/top_cities')
				// ->set_partial('sidebar_album', 'frontend/global/album')
				// ->set_partial('sidebar_tour_cities', 'frontend/global/tour_cities')
				// ->set_partial('sidebar_featured', 'frontend/global/featured')
				->set_partial('footer', 'frontend/global/footer')
				->set_breadcrumb('Home','/')
				->set_breadcrumb('All Performers')
				->build('frontend/tours/all_performers', $pageData);
	}

	private function _load_single_performer($pSlug = false)
	{

		if($this->caching):
			$performer = $this->cache->model('tours_m', 'get_performer_by_slug', array($pSlug), 600); // keep for 2 minutes
		else:
			$performer = $this->tours_m->get_performer_by_slug($pSlug);
		endif;
		if(!$performer) // this will generate 404 if there is no performer at all
			show_404();
		//get performer shit here
//		$artistClass = $this->apiClass->getPackage($this->auth, 'artist');
//		$albumClass = $this->apiClass->getPackage($this->auth, 'album');
		// Setup the variables
		$methodVars = array(
			'artist' => $performer[0]->PerformerName,
		);

		$methodVars2 = array(
			'artist' => $performer[0]->PerformerName,
		);
//		$lfmPerformer = $artistClass->getInfo($methodVars);
		$lfmPerformer = '';

		/*if($this->caching): Comment do it by me
			$albums = $this->cache->model('tours_m', 'get_performer_albums', array($performer[0]->PerformerID, 5), 600); // keep for 2 minutes
		else:
			$albums = $this->tours_m->get_performer_albums($performer[0]->PerformerID, 5);
		endif;*/

		$albums = $this->_getTopAlbums($performer[0]->PerformerName);
		$albums_array = array();
		$count_album = 0;
		foreach($albums as $album){
			$temp = array();
			if (isset($album['name']) && $album['name'] != "" && $album['name'] != "(null)") {
				$temp['name'] = $album['name'];
				$temp['image'] = $album['image']['large'];
				$info = $this->_getAlbumInfo($album['name'], $performer[0]->PerformerName);
				if (isset($info['tracks'])) {
					$tracks = array();
					$count_track = 0;
					foreach($info['tracks'] as $track) {
						$temp2 = array('name' => $track['name']);
						$tracks[] = $temp2;
						$count_track += 1;
						if ($count_track >= 12)
							break;
					}
					$temp['tracks'] = $tracks;
				}

				$albums_array[] = $temp;
				$count_album += 1;
				if ($count_album >= 5)
					break;
			}

		}
		//pdump($albums_array);

		if($this->caching):
			$sidebarFeatured = $this->cache->model('global_m', 'get_fetured_performers', array(), 600); // keep for 10 min
		else:
			$sidebarFeatured = $this->global_m->get_fetured_performers();
		endif;

 		$lastAlbum = (count($albums)) ? $albums_array[0] : false;

		
 		$params = array(
			'beginDate' => date('c'),
			'orderByClause' => 'Date',
			'performerID' =>  $performer[0]->PerformerID,
			'whereClause' => 'CountryID = 217 OR CountryID = 38',
			'numberOfEvents' => 100
		);


 		// if($this->caching):
			// $tourDates = $this->cache->library('soapy', 'run_soap', array('GetEvents', $params), 600); // keep for 10 min
		// else:
	 		$tourDates = $this->soapy->run_soap('GetEvents', $params);
			//$tourDates = $this->global_m->get_event_performers($tourDates);
			$tourDates = $this->global_m->get_prices($tourDates);

		// endif;


 		// var_dump($tourDates);
 		// if(!$tourDates):
 			// show_404();
 		// endif;
		//pdump($tourDates);

 		$searchQuery = array($performer[0]->PerformerName);

 		$performerText = $this->tours_m->get_text('performer', $tourDates, $performer[0], 'performer_main');
 		$liText = $this->tours_m->get_li_text($performer, 'performer');
 		$pageData = array(
			'performer' => $performer,
			'performerText' => $performerText,
			'liText'		=> $liText,
			'lfmPerformer' => $lfmPerformer,
			'lastAlbum' => $lastAlbum,
			//'lastAlbum_pic' => $lastAlbum_pic,
			'albums' => $albums_array,
            //'lastAlbumYear' => $lastAlbumYear,
			'tourDates' => $tourDates,
			'sidebarFeatured' => $sidebarFeatured
		);
 		// $tourDate = (isset($tourDates[0])) ? date('Y', strtotime($tourDates[0]->Date)) : '';
 		$starty = isset($tourDates[0]) ?  date('Y', strtotime($tourDates[0]->Date)) : false;
		$endy = ($starty) ? date('Y', strtotime(end($tourDates)->Date)) : false;
		$td = ($starty != $endy) ? $starty ." - ". $endy : $endy;

 		$title = "{$performer[0]->PerformerName} Tour Dates & Concert Tickets ".$td;
 		$description = "{$performer[0]->PerformerName} tour dates and concert tickets. {$performer[0]->PerformerName} concert tour schedule, albums, and live concert information.";
 		$keywords = "{$performer[0]->PerformerName}, tours, concerts, $td, tickets";
 		$yrobot = "index, follow";
 		$ogMeta = array(
 		                'title'			=> $title,
		                'description' 	=> $description,
		                'keywords'		=> $keywords,
		                'copyright'		=> "Copyright " . Date('Y') . ", Concert Fix",
		                //'image'			=> performerOgImage($performer[0]),
						'image'			=> $performer[0]->fbImage,

		                );
		$metaTags = array(
              'description'	=> $description,
              'keywords' 	=> $keywords,
              'robots' 		=> $yrobot,
			  'googlebot' 	=> $yrobot,
			  'msnbot' 		=> $yrobot,
		                  );
		$this->template->title($title)
						->prepend_metatags($metaTags)
						->prepend_metatags($ogMeta, true);

		$this->template
				->set_layout('frontend')
				->set_partial('header', 'frontend/global/header')
				->set_partial('sidebar_album', 'frontend/global/album')
				->set_partial('sidebar_tour_cities', 'frontend/global/tour_cities')
				->set_partial('sidebar_tracker', 'frontend/global/tracker')
				->set_partial('sidebar_featured', 'frontend/global/featured')
				->set_partial('footer', 'frontend/global/footer')
				->set_breadcrumb('Home','/')
				// ->set_breadcrumb('All Artists','/tours/all-artists')
				->set_breadcrumb($performer[0]->PerformerName)
				->build('frontend/tours/performer_index', $pageData);

	}

	private function _load_performer_with_venue($pSlug = false, $vSlug = false, $lSlug = false)
	{

		$slug = $pSlug."+".$vSlug."+".$lSlug;
		if(!$performer = $this->tours_m->get_performer_by_slug($pSlug)) //no performer, GTFO!!
			show_404();
 		$venue = $this->tours_m->get_venue_by_city($vSlug, $lSlug);
 		if(!$venue) //if no venue, show 404
 			show_404();
  		$location = deslug_location($lSlug);
/*  	if(!$this->global_m->verify_location($location)): // if you made it this far, you have a gift of bypassing 404 pages! Might the power be with you, son!
			show_404();
		endif;*/
		$params = array(
 						'beginDate' => date('c'),
 						'orderByClause' => 'Date',
 						'performerID' =>  $performer[0]->PerformerID,
 						'venueID'		=> $venue->ID,
 						'whereClause' => 'CountryID = 217 OR CountryID = 38'
 						);

 		$tourDatesVenue = $this->soapy->run_soap('GetEvents', $params);
		if(!$tourDatesVenue):
		    header("HTTP/1.0 404 Not Found"); //404 header for no events
		endif;
		$tourDatesVenue = $this->global_m->get_prices($tourDatesVenue);
 		$params = array(
			'beginDate' => ($tourDatesVenue) ? date('c', strtotime(end($tourDatesVenue)->Date ." + 1 day")) : date('c'),
			'orderByClause' => 'Date',
			'venueID' =>  $venue->ID,
			'parentCategoryID'	=> 2,
			'whereClause' => 'CountryID = 217 OR CountryID = 38',
			'numberOfEvents' => 4
		);
		
		//FirePHP::getInstance(true)->fb('$tourDatesVenue='.var_export($tourDatesVenue,true));
		unset($_SESSION['soap_fault']);
 		$eventsRequest = $this->soapy->run_soap('GetEvents', $params);
 		$otherEvents = array();
	 	if($eventsRequest):
	 		foreach ($eventsRequest as $event) :
	 			$event->performers = $this->global_m->event_performers($event->ID);
	 			array_push($otherEvents, $event);
	 		endforeach;
	 	endif;
		$otherEvents = $this->global_m->get_prices($otherEvents);
 		$searchQuery = array($performer[0]->PerformerName);
 		// $searchQuery = array($performer[0]->PerformerName, clean_venue($venue->Name));
		$eventPerformers = ($tourDatesVenue) ? $this->global_m->event_performers($tourDatesVenue[0]->ID) : false;
		$performerVenueText = $this->tours_m->get_text('performer_venue', $tourDatesVenue, $performer, 'tours_pv', $slug, $location);
		$array = array('125 percent' => '100 percent', '125%' => '100%');
		$performerVenueText = strtr($performerVenueText,$array);

		$performerVenueText2 = $this->tours_m->get_text('performer_venue2', $tourDatesVenue, $performer, 'tours_pv2', $slug, $location);
		$array = array('125 percent' => '100 percent', '125%' => '100%');
		$performerVenueText2 = strtr($performerVenueText2,$array);

		$qnaText = false;
		if(is_array($tourDatesVenue)):
 			$qnaText = $this->tours_m->get_qna_text($performer, 'venue', $venue, $location, $tourDatesVenue[0]->Date);
 		endif;
		$sidebarFeatured = $this->global_m->get_fetured_performers();

		$https_map_image = "";
		if ($tourDatesVenue[0]->MapURL){
			$array = array('http://maps.seatics.com/' => 'https://maps.seatics.com/');
			$https_map_image = strtr($tourDatesVenue[0]->MapURL, $array);
		}

        $this->load->helper('url');

        $listItem = array();
        $listItem[0] = ['name' => 'All Artists', 'url'=> base_url().'tours/all-artists'];
        $listItem[1] = ['name' => $performer[0]->PerformerName, 'url'=> base_url().'tours/'.$performer[0]->PerformerSlug];
        $listItem[2] = ['name' => $performer[0]->PerformerName.' '.clean_venue($venue->Name).' '.ucwords($venue->City), 'url'=> current_url()];

 		$pageData = array(
			'performerVenueText'=> $performerVenueText,
			'performerVenueText2'=> $performerVenueText2,
			'qnaText'			=> $qnaText,
			'eventPerformers'	=> $eventPerformers,
			'performer'			=> $performer,
			'tourDatesVenue'	=> $tourDatesVenue,
			'otherEvents'		=> $otherEvents,
			'venue'				=> $venue,
			'sidebarFeatured' 	=> $sidebarFeatured,
			'mobile'			=> $this->agent->is_mobile(),
			//'mobile'			=> true,
			'ios'				=> ($this->agent->platform() == 'iOS' || $this->agent->platform() == 'Mac OS X'),
			'https_map_image'	=> $https_map_image,
            'listItem'          => $listItem
		);
 		$headJs = "
							<script src='https://maps.googleapis.com/maps/api/js?sensor=false' type='text/javascript'></script>
							<script src='/public/js/gmap.js' type='text/javascript'></script>
 								";
 		$name = $performer[0]->PerformerName;
 		$title = "{$name} ".clean_venue($venue->Name)." {$venue->City} Tickets";
 		$description = "Tickets and information to see {$name} perform live at " . clean_venue($venue->Name) . " in {$venue->City}. {$name} tickets are protected with a 100% guarantee at ConcertFix.";
 		$keywords = "{$name}, ".clean_venue($venue->Name).", {$venue->City}, concert, tickets, 2013, 2014, tour";
 		$yrobot = "index, follow";

		$ogMeta = array(
			'title'       => $title,
			'description' => $description,
			'keywords'    => $keywords,
			'copyright'   => "Copyright " . Date('Y') . ", Concert Fix",
			'image'       => $performer[0]->fbImage,
		);
		$metaTags = array(
			'description' => $description,
			'keywords'    => $keywords,
			'robots'      => $yrobot,
			'googlebot'   => $yrobot,
			'msnbot'      => $yrobot,
		);
		$this->template->title($title)
			->prepend_metatags($metaTags)
			->prepend_metatags($ogMeta, true);

		$this->template
			->set_layout('ticketlay')
			//->set_layout('frontend')
			->title($title)
            ->set_partial('ldjson', 'frontend/global/breadcrumb_ldjson')
			->set_partial('header', 'frontend/global/header')
			->set_partial('sidebar_venue', 'frontend/global/venue')
			// ->set_partial('sidebar_facebook', 'frontend/global/facebook')
			->set_partial('sidebar_tracker', 'frontend/global/tracker')
			->set_partial('sidebar_featured', 'frontend/global/featured')
			//->set_partial('footer', 'frontend/global/footer', array('extra' => $headJs))
			->set_partial('footer', 'frontend/global/footer-tix')
			// ->prepend_metadata($cssExtra)
			->set_breadcrumb('Home','/')
			->set_breadcrumb($performer[0]->PerformerName,'/tours/'.$performer[0]->PerformerSlug)
			->set_breadcrumb($performer[0]->PerformerName." in ".clean_venue($venue->Name))
			->build('frontend/tours/performer_venue_index_tickets', $pageData);
			//->build('frontend/tours/performer_venue_index', $pageData);
			//->build('frontend/tours/performer_venue_index_cf', $pageData);
	}

/*	private function _load_performer_by_location($params = false)
	{
		$param = trim($params[0]); //get first parameter passed to the method
		if($venue = $this->tours_m->get_venue_by_slug($param)):

		elseif($location = $this->tours_m->get_location_by_slug($param)):

		else:
			// show 404 or redirect home
			redirect('/');
		endif;
	}*/

	private function _filter_venue_events($events, $id)
	{
		if(!$events) return false;

		$items = array();
		foreach ($events as $e) :
			if($e->VenueID == $id):
				array_push($items, $e);
			endif;
		endforeach;
		array_reverse($items);
		return $items;

	}

	private function _load_performer_with_city($pSlug, $lSlug)
	{
		// echo "TAEAD";
		if($this->caching):
			$performer = $this->cache->model('tours_m', 'get_performer_by_slug', array($pSlug), 600); // keep for 2 minutes
		else:
			$performer = $this->tours_m->get_performer_by_slug($pSlug);
		endif;
		if(!$performer) //you know what this is, no performer, no page
			show_404();
		$location = deslug_location($lSlug);

		if(!$this->global_m->verify_location($location)):
			show_404();
		endif;
		$params = array(
 						'beginDate' => date('c'),
 						'orderByClause' => 'Date',
 						'performerID' =>  $performer[0]->PerformerID,
 						// 'cityZip' => $location['city']." ".$location['state'],
 						'stateProvDesc' => $location['state'],
 						'whereClause' => 'CountryID = 217 OR CountryID = 38'
 						);

		$tourDates = $this->soapy->run_soap('GetEvents', $params);
		 if(!$tourDates):
		 	$this->output->set_header("HTTP/1.0 404 Not Found");
		//	show_404();
		endif;
		$cityEvents = array();
		$otherEvents = array();
		if($tourDates):
		foreach ($tourDates as $event):
			if($event->City == $location['city']):
				array_push($cityEvents, $event);
			else:
				array_push($otherEvents, $event);
			endif;
		endforeach;
		endif;
		// $cityEvents = array_reverse($cityEvents);
		// $otherEvents = array_reverse($otherEvents);
		$upcomingConcerts = $this->tours_m->get_upcoming_concerts($location, $performer[0]->PerformerID); //passing parameter for elimiation
		$searchQuery = array($performer[0]->PerformerName, $location['city']);
 		$performerCityText = $this->tours_m->get_text('performer_city', $tourDates, $performer, "tours_pc", $this->uri->segment(2), $location);
 		$liText = $this->tours_m->get_li_text($performer, 'city', false, $location);
 		$shortText = $this->tours_m->get_short_text($performer, 'city', false, $location);

 		if($this->caching):
			$sidebarFeatured = $this->cache->model('global_m', 'get_fetured_performers', array(), 600); // keep for 10 min
		else:
			$sidebarFeatured = $this->global_m->get_fetured_performers();
		endif;

		$pageData = array(
							'location'			=> $location,
							'liText'			=> $liText,
							'shortText'			=> $shortText,
 							'performer'			=> $performer,
 							'performerCityText'	=> $performerCityText,
 							'cityEvents'		=> $cityEvents,
 							'otherEvents'		=> $otherEvents,
 							'upcomingConcerts'	=> $upcomingConcerts,
 							'sidebarFeatured' 	=> $sidebarFeatured,
 						);
		$title = "{$performer[0]->PerformerName} {$location['city']} Concert Tickets";
		$description = "{$performer[0]->PerformerName} {$location['city']} concert tickets and tickets to other {$performer[0]->PerformerName} concerts in {$location['state']}.";
		$keywords = "{$performer[0]->PerformerName}, {$location['city']}, {$location['state']}, concerts, tours, tickets, 2013, 2014, concertfix";
		$yrobot = "noindex, follow";

 		$ogMeta = array(
 		                'title'			=> $title,
		                'description' 	=> $description,
		                'keywords'		=> $keywords,
		                'copyright'		=> "Copyright " . Date('Y') . ", Concert Fix",
		                'image'			=> performerOgImage($performer[0]),

		                );
		$metaTags = array(
              'description'	=> $description,
              'keywords' 	=> $keywords,
              'robots' 		=> $yrobot,
			  'googlebot' 	=> $yrobot,
			  'msnbot' 		=> $yrobot,
		                  );
		$this->template->title($title)
						->prepend_metatags($metaTags)
						->prepend_metatags($ogMeta, true);

		$this->template
				->set_layout('frontend')
				->title($title)
				->set_partial('header', 'frontend/global/header')
				->set_partial('sidebar_upcoming', 'frontend/global/upcoming_concerts')
				// ->set_partial('sidebar_facebook', 'frontend/global/facebook')
				->set_partial('sidebar_tracker', 'frontend/global/tracker')
				->set_partial('sidebar_featured', 'frontend/global/featured')
				->set_partial('footer', 'frontend/global/footer')
				->set_breadcrumb('Home','/')
				->set_breadcrumb($performer[0]->PerformerName,'/tours/'.$performer[0]->PerformerSlug)
				->set_breadcrumb($performer[0]->PerformerName." in ".$location['city'])
				->build('frontend/tours/performer_city_index', $pageData);
	}

	private function _getAlbumInfo($albumName, $performerName){
		$this->load->library('lastfmapi/lastfmapi');

		// Pass the array to the auth class to return a valid auth

		$this->auth = new lastfmApiAuth('setsession');
		//pdump($this->auth);
		$this->apiClass = new lastfmApi();

		$albumClass = $this->apiClass->getPackage($this->auth, 'album');

		$methodVars_album = array('artist' => $performerName, 'album' => $albumName);
		return $albumClass->getInfo($methodVars_album);
	}

	private function _getTopAlbums($performerName){
		$this->load->library('lastfmapi/lastfmapi');

		// Pass the array to the auth class to return a valid auth

		$this->auth = new lastfmApiAuth('setsession');
		// pdump($this->auth);
		$this->apiClass = new lastfmApi();

		$artistClass = $this->apiClass->getPackage($this->auth, 'artist');

		$methodVars_album = array('artist' => $performerName);

		return $artistClass->getTopAlbums($methodVars_album);
	}

}

/* End of file tours.php */
/* Location: ./application/controllers/tours.php */
