<?php

class _ajax extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $models = array('admin/user_m');
        $this -> load -> model($models);
    }

    function users($action = false)
    {
        switch($action)
        {
            case 'status':
                if($this -> user_m -> updateStatus($this -> input -> post('id'), $this -> input -> post('status')))
                {
                    $response = array('code' => 1);
                } else {
                    $response = array('code' => 0);
                }
                break;
        }

        $this -> output
            -> set_content_type('application/json')
            -> set_output(json_encode($response));
    }
}