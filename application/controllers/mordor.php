<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mordor extends MY_AdminController {

	private $soapy;
	private $apiClass;
	private $auth;
	private $twitterAuth;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_m');

		$_SESSION['uri_string'] = $this->uri->uri_string();

	}

	public function index()
	{
		if($this->session->userdata('logged_in')):
			redirect('/mordor/dashboard');
		else:
			redirect('/mordor/login');
		endif;

	}
	public function logout()
	{
		$userdata = array(
						'user'		=> false,
						'logged_in'	=> false,
						'role'		=> false,
					);
		$this->session->set_userdata($userdata);
		redirect('/mordor/login');
	}

	public function login()
	{
		if($this->session->userdata('logged_in')) redirect('/mordor/dashboard');

		if($_POST):
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			if($user = $this->admin_m->login($username, $password)):
				$userdata = array(
						'user'		=> $user->username,
						'logged_in'	=> true,
						'role'		=> $user->role,
					);
				$this->session->set_userdata($userdata);
				redirect('/mordor/dashboard');
			else:
				$data = array('error' => 'error', 'message' => "Wrong Credentials!");
				$this->session->set_flashdata($data);
				redirect('/mordor/login');
			endif;
		endif;

		$pageData = false;
		$this->template
				->set_layout('admin')
				// ->set_partial('header', 'admin/global/header')
				// ->set_partial('sidebar_filters', 'frontend/global/filters')
				// ->set_partial('sidebar_city_venues', 'frontend/global/city_venues')
				// ->set_partial('sidebar_tracker', 'frontend/global/tracker')
				// ->set_partial('sidebar_twitter', 'frontend/global/twitter')
				// ->set_partial('footer', 'admin/global/footer')
				->build('admin/login/login', $pageData);
	}

	public function dashboard()
	{
		if(!$this->session->userdata('logged_in')) redirect('/mordor/login');
		$date = $this->input->get('date');
		$events = $this->admin_m->what_cat_dragged_in($date);
		$events = $this->global_m->get_event_performers($events);
		$events = $this->global_m->get_prices($events);
		$pageData = array('today' => $events);

		$this->template
				->set_layout('admin')
				->set_partial('header', 'admin/global/header')
				// ->set_partial('sidebar_filters', 'frontend/global/filters')
				// ->set_partial('sidebar_city_venues', 'frontend/global/city_venues')
				// ->set_partial('sidebar_tracker', 'frontend/global/tracker')
				// ->set_partial('sidebar_twitter', 'frontend/global/twitter')
				->set_partial('footer', 'admin/global/footer')
				->build('admin/dashboard/dashboard', $pageData);
	}

	public function performer($section = false, $id = false)
	{
		if(!$this->session->userdata('logged_in')) redirect('/mordor/login');
		//$this->config->load('s3');
                //var_dump($this->config->item('access_key'));
		//$this->s3->setAuth($this->config->item('access_key'), $this->config->item('secret_key'));
		//var_dump($this->s3->listBuckets());
                //var_dump($this->s3->hasAuth());
		//var_dump($this->s3->getBucket('ssglobalcdn'));
		// $uri = 'performers/aaron_linz.jpg';

		// $object = $this->s3->getObject("ssglobalcdn", $uri);

	    // var_dump($object);
		switch ($section) {
			case 'fetured':
					$this->_manage_featured($id);
				break;

			case 'create-announcement' :
				if($_POST):

					$addData = array(
					                 'performerID'	=> $id,
					                 'active' => ($this->input->post('active') == 'on') ? 1 : 0,
									'title' => $this->input->post('title'),
									'announce_text' => $this->input->post('announce_text'),
									'announce_date'	=> date('Y-m-d', strtotime($this->input->post('announce_date'))),
									 'end_date' => date('Y-m-d', strtotime($this->input->post('end_date'))),
									 );
					$aId = $this->admin_m->create_announcement($addData);
					if(isset($_POST['a_category_id'])):
						$this->admin_m->update_cats($aId, $_POST['a_category_id']);
					endif;
					$this->session->set_flashdata(array('message' => "TOUR ANNOUNCEMENT CREATED!", 'type' => "success"));
					redirect('/mordor/performer/tour');
				endif;
				$this->_create_announcement($id);

				// $this->create_announcement();

			break;

			case 'edit-announcement' :
				if($_POST):
					$updateData = array(
					                 'active' => ($this->input->post('active') == 'on') ? 1 : 0,
									'title' => $this->input->post('title'),
									'announce_text' => $this->input->post('announce_text'),
									'announce_date'	=> date('Y-m-d', strtotime($this->input->post('announce_date'))),
									 'end_date' => date('Y-m-d', strtotime($this->input->post('end_date'))),
									 );
			 		$this->admin_m->update_announcement($id, $updateData);
			 		if(isset($_POST['a_category_id'])):
						$this->admin_m->update_cats($id, $_POST['a_category_id']);
					endif;
					$this->session->set_flashdata(array('message' => "TOUR ANNOUNCEMENT UPDATED!", 'type' => "success"));
					redirect('/mordor/performer/tour');
				endif;
				$this->_edit_announcement($id);
			break;

			case 'tour': //LEGACY SHIT
 					if($_POST):
						$PerformerID = $this->input->post('PerformerID');
						$announced = ($this->input->post('announced') == 'on') ? 1 : 0;
						$saveData = array(	'announce_text' => $this->input->post('announce_text'),
											'custom' => 1,
											'title' => $this->input->post('title'),
											'announced' => $announced,
											 'created_on' => date('Y-m-d', strtotime($this->input->post('created_on'))),
											 'announced_on'	=> date('Y-m-d', strtotime($this->input->post('announced_on'))),
											 'refresh_date' => date('Y-m-d', strtotime($this->input->post('refresh_date')))
											 );
						$this->admin_m->update_text($PerformerID, $saveData);
						$this->session->set_flashdata(array('message' => "PERFORMER TEXT UPDATED", 'type' => "success"));

						redirect('/mordor/performer/tour/'.$PerformerID);
						// var_dump($_POST);
					endif;
					$this->_manage_tour($id);

				break;

			case 'images':
					$this->_manage_images($id);
				break;

			case 'text':
				if($_POST):
						$PerformerID = $this->input->post('PerformerID');
					//	$announced = ($this->input->post('announced') == 'on') ? 1 : 0;
						$saveData = array(	'text' => $this->input->post('text'),
											'custom' => 1,
											'title' => $this->input->post('title'),
									//		'announced' => $announced,
											 'created_on' => date('Y-m-d', strtotime($this->input->post('created_on'))),
									//		 'announced_on'	=> date('Y-m-d', strtotime($this->input->post('announced_on'))),
											 'refresh_date' => date('Y-m-d', strtotime($this->input->post('refresh_date')))
											 );
						$this->admin_m->update_text($PerformerID, $saveData);
						$this->session->set_flashdata(array('message' => "PERFORMER TEXT UPDATED", 'type' => "success"));

						redirect('/mordor/performer/text/'.$PerformerID);
						// var_dump($_POST);
					endif;
					$this->_manage_text($id);
				break;

			default: //index
					$pageData = false;
				$this->template
					->set_layout('admin')
					->set_partial('header', 'admin/global/header')
					->set_partial('footer', 'admin/global/footer')
					->build('admin/performer/performer_index', $pageData);
				break;
		}

	}

	public function settings($section = false, $op = false)
	{
		if(!$this->session->userdata('logged_in')) redirect('/mordor/login');

		switch ($section) :
			case 'city':
					if($op == 'add' && $_POST):
						$this->admin_m->insert_city($_POST);
						$this->session->set_flashdata(array('message' => "ADDED CITY $_POST[city],  $_POST[state_short]", 'type' => "success"));
						redirect('/mordor/settings/city');
					endif;
					$pageData = array('cities' => $this->admin_m->get_top_cities());
					$this->template
							->set_layout('admin')
							->set_partial('header', 'admin/global/header')
							// ->set_partial('sidebar_filters', 'frontend/global/filters')
							// ->set_partial('sidebar_city_venues', 'frontend/global/city_venues')
							// ->set_partial('sidebar_tracker', 'frontend/global/tracker')
							// ->set_partial('sidebar_twitter', 'frontend/global/twitter')
							->set_partial('footer', 'admin/global/footer')
							->build('admin/menu_and_pages/city_manager', $pageData);
				# code...
				break;

			default:
				# code...
				break;
		endswitch;

	}

	private function _edit_announcement($aId = false)
	{
		$this->soapy = new TN_soap();
		$this->load->model('tours_m');
		$this->load->library('lastfmapi/lastfmapi');
		$this->load->library('twitterapi/TwitterAPIExchange');

		// Pass the array to the auth class to return a valid auth

		$this->auth = new lastfmApiAuth('setsession');
		// pdump($this->auth);
		$this->apiClass = new lastfmApi();
		// pdump($this->apiClass);

		$this->soapy = new TN_soap();
		// $this->soapy->initialize();
		$performer = $this->admin_m->get_announcment_with_performer($aId);
		$performer->cats = $this->admin_m->get_announcement_cat($aId);
			//get performer shit here
		$artistClass = $this->apiClass->getPackage($this->auth, 'artist');
		$albumClass = $this->apiClass->getPackage($this->auth, 'album');
		// Setup the variables
		$methodVars = array(
			'artist' => $performer->PerformerName,
		);

		$methodVars2 = array(
			'artist' => $performer->PerformerName,
		);
		$lfmPerformer = $artistClass->getInfo($methodVars);
 		$albums = $this->tours_m->get_performer_albums($performer->PerformerID, 20);

 		// $lastAlbum = (count($albums)) ? $albums[0] : false;
 		$params = array(
 						'beginDate' => date('c'),
 						'orderByClause' => 'Date',
 						'performerID' =>  $performer->PerformerID,
 						'whereClause' => 'CountryID = 217 OR CountryID = 38'
 						);
 		// var_dump($params);
  		$events = $this->soapy->run_soap('GetEvents', $params);
 		$events = $this->global_m->get_prices($events);
 		$events = $this->global_m->get_event_performers($events);
 		$searchQuery = array($performer->PerformerName);
 		$twitterFeed = $this->global_m->get_twitter_results($searchQuery);
 		$cats = $this->admin_m->get_cats();
 		//do the text
 	//	$performerText = $this->tours_m->get_text("performer", $events, $performer, 'performer_main');
			//$announcedData = $this->admin_m->get_announcements($performer->PerformerID);
 		$pageData = array(
 		                  'cats'		=> $cats,
 		              //    'announced'	=> $announcedData,
 		                  'lfm'			=> $lfmPerformer,
 		                  'performer' 	=> $performer,
 		                  'events'		=> $events,
 		                  'twitter'		=> $twitterFeed,
 		                  'albums'		=> $albums,

 		                  );
 		$this->template
				->set_layout('admin')
				->set_partial('header', 'admin/global/header')
				->set_partial('footer', 'admin/global/footer')
				->build('admin/performer/performer_edit_announcement', $pageData);

	}

	private function _create_announcement($pId = false)
	{
		$this->soapy = new TN_soap();
		$this->load->model('tours_m');
		$this->load->library('lastfmapi/lastfmapi');
		$this->load->library('twitterapi/TwitterAPIExchange');

		// Pass the array to the auth class to return a valid auth

		$this->auth = new lastfmApiAuth('setsession');
		// pdump($this->auth);
		$this->apiClass = new lastfmApi();
		// pdump($this->apiClass);

		$this->soapy = new TN_soap();
		// $this->soapy->initialize();
		$performer = $this->admin_m->get_performer_profile($pId);

			//get performer shit here
		$artistClass = $this->apiClass->getPackage($this->auth, 'artist');
		$albumClass = $this->apiClass->getPackage($this->auth, 'album');
		// Setup the variables
		$methodVars = array(
			'artist' => $performer->PerformerName,
		);

		$methodVars2 = array(
			'artist' => $performer->PerformerName,
		);
		$lfmPerformer = $artistClass->getInfo($methodVars);
 		$albums = $this->tours_m->get_performer_albums($performer->PerformerID, 20);

 		// $lastAlbum = (count($albums)) ? $albums[0] : false;
 		$params = array(
 						'beginDate' => date('c'),
 						'orderByClause' => 'Date',
 						'performerID' =>  $performer->PerformerID,
 						'whereClause' => 'CountryID = 217 OR CountryID = 38'
 						);
 		// var_dump($params);
  		$events = $this->soapy->run_soap('GetEvents', $params);
 		$events = $this->global_m->get_prices($events);
 		$events = $this->global_m->get_event_performers($events);
 		$searchQuery = array($performer->PerformerName);
 		$twitterFeed = $this->global_m->get_twitter_results($searchQuery);
 		$cats = $this->admin_m->get_cats();

 		//do the text
 		$performerText = $this->tours_m->get_text("performer", $events, $performer, 'performer_main');
			//$announcedData = $this->admin_m->get_announcements($performer->PerformerID);
 		$pageData = array(
 		                  'cats'		=> $cats,
 		              //    'announced'	=> $announcedData,
 		                  'lfm'			=> $lfmPerformer,
 		                  'performer' 	=> $performer,
 		                  'events'		=> $events,
 		                  'twitter'		=> $twitterFeed,
 		                  'albums'		=> $albums,
 		                  'text'		=> $performerText
 		                  );
 		$this->template
				->set_layout('admin')
				->set_partial('header', 'admin/global/header')
				->set_partial('footer', 'admin/global/footer')
				->build('admin/performer/performer_create_announcement', $pageData);
	}

	private function _manage_text($id = false)
	{

		if($id):
			$this->soapy = new TN_soap();
			$this->load->model('tours_m');
			$this->load->library('lastfmapi/lastfmapi');
			$this->load->library('twitterapi/TwitterAPIExchange');

			// Pass the array to the auth class to return a valid auth

			$this->auth = new lastfmApiAuth('setsession');
			// pdump($this->auth);
			$this->apiClass = new lastfmApi();
			// pdump($this->apiClass);

		//	$this->soapy = new TN_soap();
			// $this->soapy->initialize();
			$performer = $this->admin_m->get_performer_profile($id);

				//get performer shit here
			$artistClass = $this->apiClass->getPackage($this->auth, 'artist');
			$albumClass = $this->apiClass->getPackage($this->auth, 'album');
			// Setup the variables
			$methodVars = array(
				'artist' => $performer->PerformerName,
			);

			$methodVars2 = array(
				'artist' => $performer->PerformerName,
			);
			$lfmPerformer = $artistClass->getInfo($methodVars);
	 		$albums = $this->tours_m->get_performer_albums($performer->PerformerID, 20);

	 		// $lastAlbum = (count($albums)) ? $albums[0] : false;
	 		$params = array(
	 						'beginDate' => date('c'),
	 						'orderByClause' => 'Date',
	 						'performerID' =>  $performer->PerformerID,
	 						'whereClause' => 'CountryID = 217 OR CountryID = 38'
	 						);
	 		// var_dump($params);
	  		$events = $this->soapy->run_soap('GetEvents', $params);
	 		$events = $this->global_m->get_prices($events);
	 		$events = $this->global_m->get_event_performers($events);
	 		$searchQuery = array($performer->PerformerName);
	 		$twitterFeed = $this->global_m->get_twitter_results($searchQuery);

	 		//do the text
	 		$performerText = $this->tours_m->get_text("performer", $events, $performer, 'performer_main');
 			//$announcedData = $this->admin_m->get_announcements($performer->PerformerID);
	 		$pageData = array(
	 		              //    'announced'	=> $announcedData,
	 		                  'lfm'			=> $lfmPerformer,
	 		                  'performer' 	=> $performer,
	 		                  'events'		=> $events,
	 		                  'twitter'		=> $twitterFeed,
	 		                  'albums'		=> $albums,
	 		                  'text'		=> $performerText
	 		                  );
	 		$this->template
					->set_layout('admin')
					->set_partial('header', 'admin/global/header')
					->set_partial('footer', 'admin/global/footer')
					->build('admin/performer/performer_text_single_edit', $pageData);
		else:
			$pageData = array('announced' => $this->admin_m->get_announced(false));
			$this->template
					->set_layout('admin')
					->set_partial('header', 'admin/global/header')
					->set_partial('footer', 'admin/global/footer')
					->build('admin/performer/performer_tour', $pageData);
		endif;
	}

	private function _manage_tour($id = false)
	{

		if($id):
			$this->soapy = new TN_soap();
			$this->load->model('tours_m');
			$this->load->library('lastfmapi/lastfmapi');
			$this->load->library('twitterapi/TwitterAPIExchange');

			// Pass the array to the auth class to return a valid auth

			$this->auth = new lastfmApiAuth('setsession');
			// pdump($this->auth);
			$this->apiClass = new lastfmApi();
			// pdump($this->apiClass);

			$this->soapy = new TN_soap();
			// $this->soapy->initialize();
			$performer = $this->admin_m->get_performer_profile($id);

				//get performer shit here
			$artistClass = $this->apiClass->getPackage($this->auth, 'artist');
			$albumClass = $this->apiClass->getPackage($this->auth, 'album');
			// Setup the variables
			$methodVars = array(
				'artist' => $performer->PerformerName,
			);

			$methodVars2 = array(
				'artist' => $performer->PerformerName,
			);
			$lfmPerformer = $artistClass->getInfo($methodVars);
	 		$albums = $this->tours_m->get_performer_albums($performer->PerformerID, 20);

	 		// $lastAlbum = (count($albums)) ? $albums[0] : false;
	 		$params = array(
	 						'beginDate' => date('c'),
	 						'orderByClause' => 'Date',
	 						'performerID' =>  $performer->PerformerID,
	 						'whereClause' => 'CountryID = 217 OR CountryID = 38'
	 						);
	 		// var_dump($params);
	  		$events = $this->soapy->run_soap('GetEvents', $params);
	 		$events = $this->global_m->get_prices($events);
	 		$events = $this->global_m->get_event_performers($events);
	 		$searchQuery = array($performer->PerformerName);
	 		$twitterFeed = $this->global_m->get_twitter_results($searchQuery);

	 		//do the text
	 		$performerText = $this->tours_m->get_text("performer", $events, $performer, 'performer_main');
 			$announcedData = $this->admin_m->get_announcements($performer->PerformerID);

	 		$pageData = array(
	 		                  'announced'	=> $announcedData,
	 		                  'lfm'			=> $lfmPerformer,
	 		                  'performer' 	=> $performer,
	 		                  'events'		=> $events,
	 		                  'twitter'		=> $twitterFeed,
	 		                  'albums'		=> $albums,
	 		                  'text'		=> $performerText
	 		                  );
	 		$this->template
					->set_layout('admin')
					->set_partial('header', 'admin/global/header')
					->set_partial('footer', 'admin/global/footer')
					->build('admin/performer/performer_tour_single_edit', $pageData);
		else:
			$pageData = array('announced' => $this->admin_m->get_announcment_with_performer(false));
			$this->template
					->set_layout('admin')
					->set_partial('header', 'admin/global/header')
					->set_partial('footer', 'admin/global/footer')
					->build('admin/performer/performer_tour', $pageData);
		endif;
	}

	private function _manage_images($id = false)
	{
		$pageData = array('curFeat' => $this->admin_m->get_featured_performers());
				$this->template
					->set_layout('admin')
					->set_partial('header', 'admin/global/header')
					->set_partial('footer', 'admin/global/footer')
					->build('admin/performer/performer_images', $pageData);
	}

	public function menu($op = false, $id = false)
	{
		if(!$this->session->userdata('logged_in')) redirect('/mordor/login');

		if($_POST):
			$this->admin_m->add_menu($_POST);
			$this->session->set_flashdata(array('message' => "ADDED MENU $_POST[menu_title]", 'type' => "success"));
			redirect('/mordor/menu');
		endif;

		if($op && $id):
			switch ($op) {
				case 'delete':
					if($removed = $this->admin_m->delete_menu($id)):
						$text = '';
						foreach ($removed as $m):
							$text .= "<p>Removed: <strong>{$m}</strong></p>";
						endforeach;
						$this->session->set_flashdata(array('message' => "DELETED MENUS $text", 'type' => "success"));
					else:
						$this->session->set_flashdata(array('message' => "COULD NOT DELETE MENU", 'type' => "danger"));
					endif;
					redirect('/mordor/menu');
					break;

				default:
					# code...
					break;
			}

		endif;

		$pageData = array('menulist' => $this->admin_m->get_menu_list(), 'menu' => $this->admin_m->get_menu());
		$this->template
			->set_layout('admin')
			->set_partial('header', 'admin/global/header')
			->set_partial('footer', 'admin/global/footer')
			->set_partial('sidebar', 'admin/menu_and_pages/menu_sidebar')
			->build('admin/menu_and_pages/menu_manager', $pageData);
	}

    function users($action = false)
    {
        $this -> load -> model('admin/user_m');
        $this -> load -> library('pagination');

        switch($action)
        {
            case 'details':
                $data = array(
                    'user'   => $this -> user_m -> get($this -> uri -> segment(4)),
                    'linked' => $this -> user_m -> listLinkedAccounts($this -> uri -> segment(4))
                );

                $this->template
                    ->set_layout('admin')
                    ->set_partial('header', 'admin/global/header')
                    ->set_partial('footer', 'admin/global/footer')
                    ->build('admin/users/details', $data);
            break;

            default:
                $data = array(
                    'users' => $this -> user_m -> listAll($this -> uri -> segment(4), 20, trim($this -> input -> get('keyword')), $this -> input -> get('type')),
                    'total' => $total = $this -> user_m -> listAll(false, false, trim($this -> input -> get('keyword')), $this -> input -> get('type'), true)
                );

                $pagination = array(
                    'base_url' => '/mordor/users/page/',
                    'total_rows' => $total,
                    'per_page' => 20,
                    'uri_segment' => 4
                );

                $this -> pagination -> initialize($pagination);

                $this->template
                    ->set_layout('admin')
                    ->set_partial('header', 'admin/global/header')
                    ->set_partial('footer', 'admin/global/footer')
                    ->build('admin/users/index', $data);
            break;
        }
    }

}

/* End of file mordor.php */
/* Location: ./application/controllers/mordor.php */