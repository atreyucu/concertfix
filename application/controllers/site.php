<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends MY_SiteController {

	// private $apiClass;
	// private $auth;
	private $soapy;
	// private $twitterAuth;

	public function __construct()
	{
		parent::__construct();
		// $this->load->model('concerts_m');
		// $this->load->library('lastfmapi/lastfmapi');

		// Pass the array to the auth class to return a valid auth

		// $this->auth = new lastfmApiAuth('setsession');
		// pdump($this->auth);
		// $this->apiClass = new lastfmApi();
		// pdump($this->apiClass);

		$this->soapy = new TN_soap();
		// $this->soapy->initialize();
	}

	public function index()
	{
		$this->load->library('user_agent');

		// $this->template->set('foo', "bar");
		// $location = array('city' => 'Washington', 'state' =>'DC');
		// $feats = array('events' => , 'featured' => );
		$location	= $this->global_m->where_am_i();
		//print_r($this->db);

		$events = $this->global_m->get_front_page_events_in_city($location);
		$text = 'in';
		if (count($events) < 15) {
			$events = $this->global_m->get_front_page_events_near_city($location, 20);
			$text = 'near';
			if (count($events) < 15) {
				$events = array();
			}
		}

		$sideBarData = array(
			'location' => $location,
			'performers' => $this->global_m->get_fetured_performers('front'),
			'local_events' => $events,
			'cities' => $this->global_m->get_top_cities(),
			'text' => $text
		);

		$title = "ConcertFix: Feed your addiction with tour dates, news & tickets";
		$description = "Concertfix.com: Feed your concert addiction with tour announcements, performer tour dates, concerts in your city, and concert tickets.";
		$keywords = "Concertfix, concert tickets, tour dates, tour announcements, concert schedules";
		// $additionalheaders = "<script src='/public/js/plugins/jquery.masonry.min.js'></script><script src='/public/js/home.js'></script>";
		$yrobot = "index, follow";
		$base_url = strtr(base_url(), array('http:'=>'https:'));
		$ogMeta = array(
			'title' => $title,
			'description' => $description,
			'keywords' => $keywords,
			'copyright' => "Copyright 2013, Concert Fix",
			'image' => $base_url . "public/img/assets/concert-fix-logo.png",
		);
		$metaTags = array(
			'description' => $description,
			'keywords' => $keywords,
			'robots' => $yrobot,
			'googlebot' => $yrobot,
			'msnbot' => $yrobot,
		);

		$this->template
				->prepend_metatags($metaTags)
				->prepend_metatags($ogMeta, true)
				->title($title)
				->set_layout('landing')
				->set_partial('header', 'frontend/global/header')
				// ->set_partial('sidebar_tracker', 'frontend/global/tracker')
				->set_partial('popular', 'frontend/home/popular')
				->set_partial('search', 'frontend/home/search')
				->set_partial('sidebar', 'frontend/home/home_sidebar')
				->set_partial('footer', 'frontend/global/footer');
				// ->append_metadata($additionalheaders);

		if($this->agent->is_mobile()):
			$this->template->build('frontend/home/home_mobile', $sideBarData);
		else:
			$this->template->build('frontend/home/home', $sideBarData);
		endif;

	}

	public function not_found()
	{
		header("HTTP/1.0 404 Not Found");
		$title = "Concert Fix - NOT FOUND! 404";
		$description = "There is nothing here. This is just a 404 page";
		$nrobot = "noindex, nofollow";
		$base_url = strtr(base_url(), array('http:'=>'https:'));
		$ogMeta = array(
				'title' 		=> $title,
								'description' 	=> $description,
								'copyright'		=> "Copyright 2013, Concert Fix",
								'image'			=> $base_url."public/img/assets/concert-fix-logo.png",
								);
		$metaTags = array(
									'description'	=> $description,
									'robots' => $nrobot,
								'googlebot' => $nrobot,
								'msnbot' => $nrobot
									);
		$this->template
				->prepend_metatags($metaTags)
				->prepend_metatags($ogMeta, true)
				->title("404: ConcertFix | Not Found!")
				->set_layout('fullwidth')
				->set_partial('header', 'frontend/global/header')
				// ->set_partial('popular', 'frontend/home/popular')
				->set_partial('search', 'frontend/global/404search')
				// ->set_partial('sidebar', 'frontend/home/home_sidebar')
				->set_partial('footer', 'frontend/global/footer')
				->set_breadcrumb('Home','/')
				->set_breadcrumb('NOT FOUND!')
				->build('frontend/pages/404', false);

	}

	public function announcements($slug = false)
	{

		#$limit = 6;
		$limit = 8;
		$page = $this->input->get('showPage');
		if($page):
			$offset =  ($page * $limit) - $limit;
		else:
			$offset = 0;
		endif;
		// $offset = ($page && $page != 1)  ? $page * $limit : 0;

		$categorized = $this->global_m->fetch_categorized_announcements();
		$posts = $this->global_m->get_upcoming_tour_annoucement($limit, $offset);

		$title = "Latest Tour and Concert Announcements 2020 - 2021";
		$description = "Want to know Whos on Tour right now? ConcertFix will bring you up to the second news on tour announcements, tour guests, concert schedules, locations and venues.";
		$keywords = "Tour announcements, tour dates, concert schdules, tour guests, tour updates, concertfix";
		$yrobot = "index, follow";
		$base_url = strtr(base_url(), array('http:'=>'https:'));
		$ogMeta = array(
			'title'			=> $title,
			'description' 	=> $description,
			'keywords'		=> $keywords,
			'copyright'		=> "Copyright " . Date('Y') . ", Concert Fix",
			'image'			=> $base_url."public/img/assets/concert-fix-logo.png",
		);
		$metaTags = array(
			'description'	=> $description,
			'keywords'	=> $keywords,
			'robots' => $yrobot,
			'googlebot' => $yrobot,
			'msnbot' => $yrobot,
		);
		$this->load->library('pagination');


		$config['base_url'] = "/tour-announcements?";
		$config['total_rows'] = $this->global_m->count_posts();
		$config['per_page'] = $limit;
		$config['page_query_string'] = true;
		$config['use_page_numbers'] = true;
		$config['query_string_segment'] = 'showPage';
		// $config['uri_segment'] = 3;
		$config['num_links'] = 3;
		$config['full_tag_open'] = '<ul>';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = '&gt;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&lt;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a class="current" href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$this->pagination->initialize($config);

		$pagination = $this->pagination->create_links();

		$footerExtra = '';
		if($this->agent->platform() == 'iOS' || $this->agent->platform() == 'Mac OS X'){
			$footerExtra = "<script>
                jQuery(document).ready(function(){
					$('.latest-article > .article-content > .article-header > h3 > a').css({'text-overflow':'initial'})
                });
            </script>";
		}

		$this->template
			->set_layout('frontend')
            ->set_partial('ldjson', 'frontend/global/breadcrumb_ldjson')
			->set_partial('header', 'frontend/global/header')
			->set_partial('footer', 'frontend/global/footer', array('extra' => $footerExtra, 'announcements' => true))
			->title($title)
			->prepend_metatags($metaTags)
			->prepend_metatags($ogMeta, true)
			->set_breadcrumb('Home','/')
			->set_breadcrumb('Tour Announcements')
			->set_partial('sidebar_announced_categories', 'frontend/global/announced_categories')
			->set_partial('sidebar_tracker', 'frontend/global/tracker');

        $listItem = array();
        $listItem[0] = ['name' => 'Tour Announcements', 'url'=> base_url().'tour-announcements'];

		$pageData = array('posts' => $posts, 'pagination' => $pagination, 'categorized' => $categorized, 'listItem' => $listItem);

		$this->template->build('frontend/pages/tour_announcement', $pageData);

	}

	public function specials($slug = false)
	{
		$title = "Find Ticket Specials, Coupon Codes &amp; More";
		$description = "Find great deals on concert tickets including, ticket specials, coupond codes, shipping offers and more";
		$keywords = "Coupon codes, ticket specials, ticket deals, promotions, offers, concertfix.com";
		$yrobot = "index, follow";
        $listItem = array();
        $listItem[0] = ['name' => 'Ticket Specials', 'url'=> base_url().'great-deals-on-tickets'];
        $pageData = array('listItem' => $listItem);
		$base_url = strtr(base_url(), array('http:'=>'https:'));
		$ogMeta = array(
								'description' 	=> $description,
								'keywords'		=> $keywords,
								'copyright'		=> "Copyright 2013, Concert Fix",
								'image'			=> $base_url."public/img/assets/concert-fix-logo.png",
								);
		$metaTags = array(
							'description'	=> $description,
							'keywords' 	=> $keywords,
							'robots' => $yrobot,
				'googlebot' => $yrobot,
				'msnbot' => $yrobot,
											);
		$this->template
				->title($title)
				->set_layout('fullwidth')
                ->set_partial('ldjson', 'frontend/global/breadcrumb_ldjson')
				->set_partial('header', 'frontend/global/header')
				->set_partial('footer', 'frontend/global/footer')
				->set_breadcrumb('Home','/')
				->set_breadcrumb('Ticket Specials', '/'.$this->uri->uri_string())
				->prepend_metatags($metaTags)
				->prepend_metatags($ogMeta, true)
				->build('frontend/pages/ticket_specials', $pageData);
	}

	public function pages($slug = false)
	{
		$pageData = array();
		$this->template
				->set_layout('fullwidth')
				->set_partial('header', 'frontend/global/header')
				// ->set_partial('popular', 'frontend/home/popular')
				// ->set_partial('search', 'frontend/home/search')
				// ->set_partial('sidebar', 'frontend/home/home_sidebar')
				->set_partial('footer', 'frontend/global/footer');
				// ->append_metadata($additionalheaders)
		$base_url = strtr(base_url(), array('http:'=>'https:'));
		switch($slug):

			 case 'privacy':
				 $title = "Terms &amp; Policies for ConcertFix.com";
				 $description = "Terms &amp; Policies for ConcertFix.com including different information used by the site.";
				 $keywords = "Terms, Policies, concerts, tickets, performers, tour ticket specials, concertfix";
				 $yrobot = "index, follow";
                 $listItem = array();
                 $listItem[0] = ['name' => 'Terms & Policies', 'url'=> base_url().'policies'];
                 $pageData = array('listItem' => $listItem);
				 $ogMeta = array(
					'title' => $title,
									'description' 	=> $description,
									'keywords'		=> $keywords,
									'copyright'		=> "Copyright 2013, Concert Fix",
									'image'			=> $base_url."public/img/assets/concert-fix-logo.png",
									);
				 $metaTags = array(
									'description'	=> $description,
									'keywords' 		=> $keywords,
							'robots' => $yrobot,
					'googlebot' => $yrobot,
					'msnbot' => $yrobot,
												);

                 $footerExtra = '<script>
					 $(document).ready(function($) {
                        $(\'iframe\').css({\'min-width\': 0})
                      })
				 </script>';

				 $this->template
					->title($title)
                    ->set_partial('ldjson', 'frontend/global/breadcrumb_ldjson')
					->prepend_metatags($metaTags)
					->prepend_metatags($ogMeta, true)
					->set_breadcrumb('Home','/')
					->set_breadcrumb('Terms & Policies', '/'.$this->uri->uri_string())
                    ->set_partial('footer', 'frontend/global/footer', array('extra' => $footerExtra))
					->build('frontend/pages/privacy', $pageData);
			 break;

			 case 'guarantee':
				 $title = "Concert Ticket Guarantee ConcertFix.com";
				 $description = "All ConcertFix.com concert tickets come with a 100% money back guarantee so you can shop knowing your tickets are secure.";
				 $keywords = "Ticket, Guarantee, concerts, tickets, performers, tour ticket specials, concertfix";
				 $yrobot = "index, follow";
                 $listItem = array();
                 $listItem[0] = ['name' => 'Ticket Guarantee', 'url'=> base_url().'ticket-guarantee'];
                 $pageData = array('listItem' => $listItem);
				 $ogMeta = array(
					'title' => $title,
									'description' 	=> $description,
									'keywords'		=> $keywords,
									'copyright'		=> "Copyright 2013, Concert Fix",
									'image'			=> $base_url."public/img/assets/concert-fix-logo.png",
									);
				 $metaTags = array(
									'description'	=> $description,
									'keywords' 		=> $keywords,
									'robots' => $yrobot,
					'googlebot' => $yrobot,
					'msnbot' => $yrobot,
												);
				 $this->template
					->title($title)
                    ->set_partial('ldjson', 'frontend/global/breadcrumb_ldjson')
					->prepend_metatags($metaTags)
					->prepend_metatags($ogMeta, true)
					->set_breadcrumb('Home','/')
					->set_breadcrumb('Ticket Guarantee', '/'.$this->uri->uri_string())
					->build('frontend/pages/guarantee', $pageData);
			 break;

			 case 'about':
				 $title = "Learn More About ConcertFix.com";
				 $description = "Learn More About ConcertFix.com and our tour announcements, top tours, concert tickets, our concert tracker and more.";
				 $keywords = "about, concerts, tickets, performers, tour ticket specials, concertfix";
				 $yrobot = "index, follow";

                 $listItem = array();
                 $listItem[0] = ['name' => 'About Us', 'url'=> base_url().'about-us'];
				 $pageData = array('listItem' => $listItem);
				 $ogMeta = array(
					'title' => $title,
									'description' 	=> $description,
									'keywords'		=> $keywords,
									'copyright'		=> "Copyright 2013, Concert Fix",
									'image'			=> $base_url."public/img/assets/concert-fix-logo.png",
									);
				 $metaTags = array(
									'description'	=> $description,
									'keywords' 		=> $keywords,
									'robots' => $yrobot,
					'googlebot' => $yrobot,
					'msnbot' => $yrobot,
												);
				 $this->template
					->title($title)
                    ->set_partial('ldjson', 'frontend/global/breadcrumb_ldjson')
					->prepend_metatags($metaTags)
					->prepend_metatags($ogMeta, true)
					->set_breadcrumb('Home','/')
					->set_breadcrumb('About Us', '/'.$this->uri->uri_string())
					->build('frontend/pages/about', $pageData);
			 break;

			 case 'faq':
				$title = "Help Center & FAQ";
				$description = "Frequently asked questions and additional information when purchasing tickets at ConcertFix";
				$keywords = "faq, concerts, tickets, performers, tour ticket specials, concertfix";
				$yrobot = "index, follow";
                 $listItem = array();
                 $listItem[0] = ['name' => 'Help Center', 'url'=> base_url().'faq'];
                 $pageData = array('listItem' => $listItem);
				$ogMeta = array(
					'title' => $title,
					'description' 	=> $description,
					'keywords'		=> $keywords,
					'copyright'		=> "Copyright 2013, Concert Fix",
					'image'			=> $base_url."public/img/assets/concert-fix-logo.png",
				);
				$metaTags = array(
					'description'	=> $description,
					'keywords' 		=> $keywords,
					'robots' => $yrobot,
					'googlebot' => $yrobot,
					'msnbot' => $yrobot,
				);
				$footerExtra = '<script>
					 $(function() {
            				$( ".accordion" ).accordion({collapsible : true, active : "none", autoHeight: false,navigation: true})
        			 })
				</script>';
				$this->template
					->title($title)
                    ->set_partial('ldjson', 'frontend/global/breadcrumb_ldjson')
					->prepend_metatags($metaTags)
					->prepend_metatags($ogMeta, true)
					->set_breadcrumb('Home','/')
					->set_breadcrumb('Help Center', '/'.$this->uri->uri_string())
					->set_partial('footer', 'frontend/global/footer', array('extra' => $footerExtra))
					->build('frontend/pages/faq', $pageData);
				break;

			 case 'toptours':
				 $title = "Top Concert Tours in North America | ConcertFix.com";
				 $description = "Check out the most popular concert performers in their genre and find out where they stack up against other great concert tours.";
				 $keywords = "Top Tours, concerts, tickets, performers, concertfix";
				 $yrobot = "index, follow";
				 $categorized = $this->global_m->fetch_categorized_announcements();

                 $listItem = array();
                 $listItem[0] = ['name' => 'Top Tours', 'url'=> base_url().'top-tours'];

				 $pageData = array(
				     'categorized' => $categorized,
                     'listItem' => $listItem
                 );

				 $ogMeta = array(
					'title' => $title,
									'description' 	=> $description,
									'keywords'		=> $keywords,
									'copyright'		=> "Copyright 2013, Concert Fix",
									'image'			=> $base_url."public/img/assets/concert-fix-logo.png",
									);
				 $metaTags = array(
									'description'	=> $description,
									'keywords' 		=> $keywords,
									'robots' => $yrobot,
					'googlebot' => $yrobot,
					'msnbot' => $yrobot,
												);
				 $this->template
					->set_layout('frontend')
                    ->set_partial('ldjson', 'frontend/global/breadcrumb_ldjson')
					->title($title)
					->prepend_metatags($metaTags)
					->prepend_metatags($ogMeta, true)
					->set_partial('sidebar_announced_categories', 'frontend/global/announced_categories')
					->set_partial('sidebar_tracker', 'frontend/global/tracker')
					->set_breadcrumb('Home','/')
					->set_breadcrumb('Top Tours')
					->build('frontend/pages/top_tours', $pageData);
			 break;

			 case 'concert-tracker':
				$title = "Track Your Favorite Artists Concert Tour";
				$description = "Get Updates and track all your favorite artists concert tours and find out when they will be performing at a venue near you.";
				$keywords = "concerts, tickets, performers, concertfix, concert tracker, email updates, alerts";
				$yrobot = "index, follow";
				$pageData = array();
				 $ogMeta = array(
					'title' => $title,
									'description' 	=> $description,
									'keywords'		=> $keywords,
									'copyright'		=> "Copyright 2013, Concert Fix",
									'image'			=> $base_url."public/img/assets/concert-fix-logo.png",
									);
				 $metaTags = array(
									'description'	=> $description,
									'keywords' 		=> $keywords,
									'robots' => $yrobot,
					'googlebot' => $yrobot,
					'msnbot' => $yrobot,
												);
				 $this->template
					->title($title)
					->prepend_metatags($metaTags)
					->prepend_metatags($ogMeta, true)
					->set_breadcrumb('Home','/')
					->set_breadcrumb('Track Your Favorite Concerts')
					->build('frontend/pages/concert_tracker', $pageData);
			 break;

			 case 'contact':
				 $title = "Customer Support Number and Contact | ConcertFix.com";
				 $description = "Get in touch with our Customer Support department who can help anwser question in regards to all your ticket questions.";
				 $keywords = "Customer Support, Contact, concerts, tickets, performers, concertfix";
				 $yrobot = "index, follow";

                 $listItem = array();
                 $listItem[0] = ['name' => 'Contact Us', 'url'=> base_url().'contact-us'];
                 $pageData = array('listItem' => $listItem);
				 $ogMeta = array(
					'title' => $title,
									'description' 	=> $description,
									'keywords'		=> $keywords,
									'copyright'		=> "Copyright 2013, Concert Fix",
									'image'			=> $base_url."public/img/assets/concert-fix-logo.png",
									);
				 $metaTags = array(
									'description'	=> $description,
									'keywords' 		=> $keywords,
									'robots' => $yrobot,
					'googlebot' => $yrobot,
					'msnbot' => $yrobot,
												);
				 $this->template
					->title($title)
                    ->set_partial('ldjson', 'frontend/global/breadcrumb_ldjson')
					->prepend_metatags($metaTags)
					->prepend_metatags($ogMeta, true)
					->set_breadcrumb('Home','/')
					->set_breadcrumb('Contact Us', '/'.$this->uri->uri_string())
					->build('frontend/pages/contact', $pageData);
			 break;

			 case 'maintenance' :
				header('HTTP/1.1 503 Service Temporarily Unavailable');
				header('Status: 503 Service Temporarily Unavailable');
				header('Retry-After: 300');//300 seconds
				$title = "Site is under maintenance | ConcertFix.com";
				 $description = "We are currently performing updates, please return back in few minutes...";
				 $keywords = "Be Right Back";
				 $pageData = array();
				 $ogMeta = array(
					'title' => $title,
									'description' 	=> $description,
									'keywords'		=> $keywords,
									'copyright'		=> "Copyright 2013, Concert Fix",
									'image'			=> $base_url."public/img/assets/concert-fix-logo.png",
									);
				 $metaTags = array(
									'description'	=> $description,
									'keywords' 		=> $keywords,
												);
				 $this->template
					->title($title)
					->prepend_metatags($metaTags)
					->prepend_metatags($ogMeta, true)
					->set_breadcrumb('Maintenance')
					// ->set_breadcrumb('Contact Us')
					->build('frontend/pages/maintenance', $pageData);
				break;

			default:

			break;

		endswitch;
	}

	public function search($query = false)
	{


		$query = ($query) ? $query : $this->input->post('query');

		if($query):
			$params = array(
						'beginDate' => date('c'),
						'orderByClause' => 'Date',
						// 'parentCategoryID' => '2',
						'searchTerms'	=> $query,
						// 'parentCategoryID' => 2,
						'whereClause' => '(CountryID = 217 OR CountryID = 38) AND ParentCategoryID = 2'
						);
			$tmp = $this->soapy->run_soap('SearchEvents', $params);
			$tmp = $this->global_m->get_event_performers($tmp);
			$tmp = $this->global_m->get_prices($tmp);
		else:
			$tmp = false;
		endif;
		$topCities = $this->global_m->get_top_cities();
		$title = "Search for Artists, Venues, Tours, Cities, and more";
		$description = "Search for Artists, Venues, Tours, Cities, and more";
		$nrobot = "noindex, nofollow";
		$pageData = array('results' => $tmp, 'cities' => $topCities);
		$base_url = strtr(base_url(), array('http:'=>'https:'));
		$ogMeta = array(
						'title' 		=> $title,
										'description' 	=> $description,
										'copyright'		=> "Copyright 2013, Concert Fix",
										'image'			=> $base_url."public/img/assets/concert-fix-logo.png",
										);
				$metaTags = array(
										'description'	=> $description,
										'robots' => $nrobot,
						'googlebot' => $nrobot,
						'msnbot' => $nrobot,
													);
				$this->template->title($title)
								->prepend_metatags($metaTags)
								->prepend_metatags($ogMeta, true);
		$this->template
				->set_layout('frontend')
				->set_partial('header', 'frontend/global/header')
				->set_partial('popular', 'frontend/home/popular')
				->set_partial('search', 'frontend/home/search')
				->set_partial('sidebar_tracker', 'frontend/global/tracker')
				->set_partial('sidebar_top_cities', 'frontend/global/top_cities')
				->set_partial('footer', 'frontend/global/footer')
				->set_breadcrumb('Home','/')
				->set_breadcrumb('Search')
				->build('frontend/pages/search_results', $pageData);
	}


}

/* End of file site.php */
/* Location: ./application/controllers/site.php */






