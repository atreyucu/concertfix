<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Concert Tracker from ConcertFix</title>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<style type="text/css">

/* Override styles in certain mail clients and force our styles and settings */

body {background-color: #E4E4E4; min-width: 100% !important; margin: 0; padding: 0; -webkit-text-size-adjust: none;}
table td {border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0;}
table {border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0;}
td {border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0;}
img {border-collapse: collapse; border: none; margin: 0; padding: 0;}
a {color: #439CED !important; text-decoration: none !important;}
p, h1, h2, h3, h4, h5, h6 {line-height: 20px; margin: 0px; padding: 0px;}
ul {margin: 0px; padding: 0px; list-style-position: inside;}
li {margin: 0px; padding: 0px; list-style-position: inside;}
.yshortcuts {color: #439CED !important; text-decoration: none !important; border-bottom: none !important;}
.ReadMsgBody {background-color: #E4E4E4; width: 100%;}
.ExternalClass {background-color: #E4E4E4; width: 100%; line-height: 100%;}
.ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td {line-height: 100%;}
td[class=contentholdingtable] {min-width: 600px; width: 600px;}

/* Responsivnes settings */

@media screen and (max-width: 599px) {
	body {min-width: 100% !important;}
	table[class=holdingtable] {width: 300px !important;}
	td[class=contentholdingtable] {min-width: 0px !important; width: 300px !important; display: block !important;}
	td[class=spacercol] {float: left !important;}
	td[class=spacercolDELformobile] {display: none !important;}
	td[class=strecher] {width: 276px !important;}
	td[class=centerme] {text-align: center !important;}
	td[class=addpaddingtop10] {padding-top: 10px !important;}
	td[class=addpaddingbottom10] {padding-bottom: 10px !important;}
	td[class=deletepaddingtop] {padding-top: 0px !important;}
	td[class=deletepaddingbottom] {padding-bottom: 0px !important;}
	td[class=fullwidthcontenttable] {width: 300px !important;}
	td[class=fullwidthcontenttableinset] {width: 252px !important;}
	td[class=splited2contenttableinset] {width: 252px !important; float: left !important;}
	td[class=splited3contenttableinset] {width: 252px !important; float: left !important;}
	td[class=splited1in3contenttableinset] {width: 252px !important; float: left !important;}
	td[class=splited2in3contenttableinset] {width: 252px !important; float: left !important;}
	td[class=splited2contenttableconected] {width: 300px !important; float: left !important;}
	td[class=splited3contenttableconected] {width: 300px !important; float: left !important;}
	td[class=splited4contenttableconected] {width: 150px !important; float: left !important;}
	td[class=splited1in3contenttableconected] {width: 300px !important; float: left !important;}
	td[class=splited2in3contenttableconected] {width: 300px !important; float: left !important;}
	img[class=img-600] {width: 300px !important; height: auto !important;}
	img[class=img-400] {width: 300px !important; height: auto !important;}
	img[class=img-300] {width: 300px !important; height: auto !important;}
	img[class=img-264] {width: 252px !important; height: auto !important;}
	img[class=img-168] {width: 252px !important; height: auto !important;}
}
</style>

</head>

<body bgcolor="#E4E4E4" style="background: #E4E4E4; padding: 0px; margin: 0px;">
<!--START main table-->
<table bgcolor="#E4E4E4" width="100%" border="0" cellpadding="0" cellspacing="0" style="background: #E4E4E4; padding: 0px; margin: 0px; border-collapse: collapse;">

<!--START module / header 1 / Logo with unsubscribe link and social icons on top-->
	<tr>
	<td align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">
		<table class="holdingtable" width="600" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
			<tr>
			<td class="contentholdingtable" bgcolor="#439CED" width="600" align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">
				<table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
					<!--START content holding table row / unsubscribe link with social icons-->
					<tbody>
					<!--END content holding table row / unsubscribe link with social icons-->
					<!--START content holding table row / logo-->
					<tr>
					<td align="center" style="padding-top: 18px; padding-right: 0px; padding-bottom: 18px; padding-left: 0px; margin: 0px; border-collapse: collapse;">
						<table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
							<tbody><tr>
							<!--START CONTENT column-->
							<td class="fullwidthcontenttable" width="600" valign="top" align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">
								<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
									<tbody><tr>
									<td align="left" style="padding-top: 0px; padding-right: 24px; padding-bottom: 0px; padding-left: 24px; margin: 0px; border-collapse: collapse;">
									<!--Logo 240 goes here--><img src="<?php echo base_url(); ?>public/img/notifications/cf-logo.png" width="240" alt="image 240" border="no" style="margin: 0px; padding: 0px; display: block; border: none; max-width: 240px;">
									</td>
									</tr>
								</tbody></table>
							</td>
							<!--END CONTENT column-->
							</tr>
						</tbody></table>
					</td>
					</tr>
					<tr>
					<td align="center" style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse;">
						<table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
							<tbody><tr>
							<!--START CONTENT column-->
							<td class="fullwidthcontenttable" width="600" valign="top" align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">
								<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
									<tbody><tr>
									<td align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">
									<!--Graphic 600 goes here--><img class="img-600" src="<?php echo base_url(); ?>public/img/notifications/graphic-splitter-color.jpg" width="600" alt="image 600" border="no" style="margin: 0px; padding: 0px; display: block; border: none; max-width: 600px;">
									</td>
									</tr>
								</tbody></table>
							</td>
							<!--END CONTENT column-->
							</tr>
						</tbody></table>
					</td>
					</tr><tr>
					<td bgcolor="#25262D" align="center" style="padding-top: 10px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px; margin: 0px; border-collapse: collapse;">
						<table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
							<tbody><tr>
							<!--START LEFT CONTENT column-->
							<td class="splited2contenttableconected" width="300" valign="middle" align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">
								<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
									<tbody><tr>
									<td class="addpaddingbottom10" align="left" style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse;">
										<table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
											<tbody><tr>
											<td class="strecher" align="center" style="padding-top: 0px; padding-right: 12px; padding-bottom: 0px; padding-left: 12px; margin: 0px; border-collapse: collapse;">
												<table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
													<tbody><tr>
													<!--Column with icon 1-->
													<td width="24" align="center" valign="middle" style="font-size: 0px; line-height: 0px; padding-top: 0px; padding-right: 2px; padding-bottom: 0px; padding-left: 2px; margin: 0px; border-collapse: collapse;">
													<!--Icon 24 goes here--><a href="https://www.facebook.com/pages/Concertfix/653440138041150" title="" target="_self"><img src="<?php echo base_url(); ?>public/img/notifications/facebook.png" width="24" alt="" border="no" style="margin: 0px; padding: 0px; display: block; border: none; max-width: 24px;"></a>
													</td>
													<!--Column with icon 2-->
													<td width="24" align="center" valign="middle" style="font-size: 0px; line-height: 0px; padding-top: 0px; padding-right: 2px; padding-bottom: 0px; padding-left: 2px; margin: 0px; border-collapse: collapse;">
													<!--Icon 24 goes here--><a href="https://www.twitter.com/concertfix" title="" target="_self"><img src="<?php echo base_url(); ?>public/img/notifications/twitter.png" width="24" alt="" border="no" style="margin: 0px; padding: 0px; display: block; border: none; max-width: 24px;"></a>
													</td>

												</tr>
											</tbody></table>
											</td>
										</tr>
										</tbody></table>
									</td>
									</tr>
								</tbody></table>
							</td>
							<!--END LEFT CONTENT column-->
							<!--START RIGHT CONTENT column-->
							<td class="splited2contenttableconected" width="300" valign="middle" align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">
								<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
									<tbody><tr>
									<td class="centerme" align="right" style="color: #A09FA5; font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; line-height: 11px; padding-top: 0px; padding-right: 12px; padding-bottom: 0px; padding-left: 12px; margin: 0px; border-collapse: collapse;">
									<!--Unsubscribe link--><a href="<?php echo base_url() ?>user/login" title="" target="_self" style="color: #439CED">Login to Unsubscribe</a> from this list
									</td>
									</tr>
								</tbody></table>
							</td>
							<!--END RIGHT CONTENT column-->
							</tr>
						</tbody></table>
					</td>
					</tr><!--END content holding table row / logo-->
					<!--START content holding table row / graphic-->

					<!--END content holding table row / graphic-->
				</tbody></table>
			</td>
			</tr>
		</table>
	</td>
	</tr>
<!--END module-->

<!--START module / one column / Image with title text and large button-->
	<tr>
	<td align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">
		<table class="holdingtable" width="600" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
			<tbody><tr>
			<td class="contentholdingtable" bgcolor="#FFFFFF" width="600" align="center" style="border-bottom: 1px solid #E4E4E4; padding: 0px; margin: 0px; border-collapse: collapse;">
				<table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
					<tbody><tr>
					<td align="center" style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse;">
		<!--START content holding TABLE-->
						<table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
							<tbody><tr>
							<!--START CONTENT column-->
							<td class="fullwidthcontenttable" width="600" valign="top" align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">
								<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
									<!--Table row with image-->
									<tbody><tr>
									<td align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">
									<!--Image 600 goes here--><img class="img-600" src="<?php echo base_url() ?>/public/img/notifications/linen-bg.png" width="600" alt="image 600" border="no" style="margin: 0px; padding: 0px; display: block; border: none; max-width: 600px;">
									</td>
									</tr>
									<!--Table row with title-->
									<tr>
									<td align="left" style="color: #439CED; font-family: Arial, Helvetica, sans-serif; font-size: 18px; font-weight: bold; line-height: 22px; padding-top: 18px; padding-right: 24px; padding-bottom: 12px; padding-left: 24px; margin: 0px; border-collapse: collapse;">
									<!--Title goes here-->Hey, <?php echo $name; ?>
									</td>
									</tr>
									<!--Table row with text-->
									<tr>
									<td align="left" style="color: #A09FA5; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: normal; line-height: 20px; padding-top: 0px; padding-right: 24px; padding-bottom: 18px; padding-left: 24px; margin: 0px; border-collapse: collapse;">
									<!--Text goes here--><span style="color: #54555C;"><strong>This is an email alert from ConcertFix about the concerts you are tracking. </strong></span><br>
									If you want to change the concerts that you are currently tracking you can login to your ConcertFix account <a href="<?php echo base_url(); ?>/user/dashboard">here</a>. Don't forget you can also modify the amount of time in advance you want to be notified and whether or not you want email notifications.
									</td>
									</tr>
								</tbody></table>
							</td>
							<!--END CONTENT column-->
							</tr>
						</tbody></table>
		<!--END content holding TABLE-->
					</td>
					</tr>
				</tbody></table>
			</td>
			</tr>
		</tbody></table>
	</td>
	</tr>
<!--END module-->

<?php if($announced): ?>
<!--START module / two columns INSET / Image with title text and small button-->
	<tr>
		<td align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">
			<table class="holdingtable" width="600" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
				<tbody>
					<tr>
						<td class="contentholdingtable" bgcolor="#FFFFFF" width="600" align="center" style="border-bottom: 1px solid #E4E4E4; padding: 0px; margin: 0px; border-collapse: collapse;">
							<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
								<tbody>
									<tr>
										<td align="center" style="padding-top: 5px; padding-right: 5px; padding-bottom: 5px; padding-left: 5px; margin: 0px; border-collapse: collapse;">
										<!--START content holding TABLE-->
										<table width="95%" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
											<tbody>
												<?php if ($announced): ?>
												<tr>
													<td><!-- putitinhere part1-->
														<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding:0px margin:0px; border-collapse: collapse;">
															<tr>
																<td align="left" style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 10px; margin: 0px; border-collapse: collapse;">
																	 <span style="font-size:16px;color:#29176E;font-weight:bold;text-transform:uppercase;font-family: Arial, Helvetica, sans-serif;">Just Announced Concerts</span>
																	 <p  style="font-size:12px;font-family: Arial, Helvetica, sans-serif;">Here's the latest list of new concerts just announced in/near
                                                                         your home city!</p>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<?php foreach ($announced as $event): ?>
												<tr>
													<td><!-- putitinhere part1-->
														<table width="100%" border="0" cellpadding="0" cellspacing="0" style="background:url('<?php echo base_url();?>/public/img/assets/bg/pattern/bg-strip.png') repeat;padding:0px margin:0px; border-collapse: collapse;">
															<tr>
																<td align="left" style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 10px; margin: 0px; border-collapse: collapse;">
																	 <span style="font-size:16px;font-weight:bold;text-transform:uppercase;font-family: Arial, Helvetica, sans-serif;"><?php echo date('F j, Y', strtotime($event->Date)); ?> in <?php echo $event->City ?></span>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td><!-- putitinhere part1-->
														<table width="100%" border="0" cellpadding="0" cellspacing="0" style="background: rgba(140, 144, 218, 0.1);padding:0px margin:0px; border-collapse: collapse;">
															<tr>
																<td align="left" width="10%" style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 10px; margin: 0px; border-collapse: collapse;">
                                                                    <?php
                                                                    $src = performerImage($event->performers, 'thumb');
                                                                    /*if(strpos($src,'/public/img/artist_') !== false):
                                                                        $src = base_url().$src;
                                                                    endif*/

                                                                    ?>
																	 <img style="border: 1px solid #b0b0b0;-webkit-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2);-moz-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2);box-shadow: 0 0 4px rgba(0, 0, 0, 0.2);" width="70px" height="70px" src="<?php echo $src; ?>">
																</td>
																<td align="left" width="80%" valign="top" style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 10px; margin: 0px; border-collapse: collapse;">
																	 <span style="font-size:14px;font-weight:bold;text-transform:uppercase;font-family: Arial, Helvetica, sans-serif;"><?php echo $event->Name; ?></span>
																	 <br />
																	 <span style="font-size:10px;font-family: Arial, Helvetica, sans-serif;color:#333;"><b>Venue:</b> <?php echo $event->Venue ?></span>
																	 <br />
																	 <span style="font-size:10px;font-family: Arial, Helvetica, sans-serif;color:#333;"><b>Time:</b> <?php echo date('F j, Y H:i:s', strtotime($event->Date)); ?> </span>
																	 <br />
																	 <span style="font-size:10px;font-family: Arial, Helvetica, sans-serif;color:#333;"><b>Performers:</b>
																	 <?php $i = 0; ?>
																	 <?php foreach ($event->performers as $p): ?>
																	 		<?php $i++; ?>
																	 		<?php echo $p->PerformerName; ?><?php echo (count($event->performers) == $i) ? '' :', '; ?>
																	 <?php endforeach ?>
																	  </span>
																</td>
																<td align="left" width="10%" style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 10px; margin: 0px; border-collapse: collapse;">
																	<a style="color:white;" href="<?php echo base_url();?>tickets/<?php echo $event->EventID; ?>?utm_source=email&utm_medium=newcity&utm_campaign=tickets">
																		<img alt="ticket2" width="64px" height="64px" src="<?php echo base_url();?>/public/img/ticket_2.png">
																	</a>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<?php endforeach; ?>
												<?php endif; ?>
											</tbody>
										</table><!--END content holding TABLE-->
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
<!--END module-->
<?php endif; ?>
<!--START module / footer 1 / Logo symbol with company contacts-->
	<tr>
	<td align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">
		<table class="holdingtable" width="600" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
			<tr>
			<td class="contentholdingtable" width="600" align="center" style="border-bottom: 1px solid #FFFFFF; padding: 0px; margin: 0px; border-collapse: collapse;">
				<table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
					<tr>
					<td align="center" style="padding-top: 24px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse;">
						<table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
							<tr>
							<!--START LEFT CONTENT column-->
							<td class="splited1in3contenttableconected" width="200" valign="middle" align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">
								<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
									<tr>
									<td align="left" style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse;">
										<table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
											<tr>
											<td class="strecher" align="center" style="padding-top: 0px; padding-right: 24px; padding-bottom: 24px; padding-left: 24px; margin: 0px; border-collapse: collapse;">
												<table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
													<tr>
													<td align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">
													<!--Image 80 goes here--><img src="<?php echo base_url(); ?>public/img/notifications/cf-logo-off.png" width="80" alt="image 80" border="no" style="margin: 0px; padding: 0px; display: block; border: none; max-width: 80px;"/>
													</td>
													</tr>
												</table>
											</td>
											</tr>
										</table>
									</td>
									</tr>
								</table>
							</td>
							<!--END LEFT CONTENT column-->
							<!--START RIGHT CONTENT column-->
							<td class="splited2in3contenttableconected" width="400" valign="middle" align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">
								<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
									<tr>
									<td class="centerme" align="right" style="color: #A09FA5; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: normal; line-height: 20px; padding-top: 0px; padding-right: 24px; padding-bottom: 24px; padding-left: 24px; margin: 0px; border-collapse: collapse;">
									<!--Text goes here--><span style="color: #25262D; font-weight: bold;">ConcertFix.</span><br/>
									E-mail: <a href="" title="" target="_self" style="color: #439CED; text-decoration: none;">info@concertfix.com</a><br/>
									Phone:  855-428-3860<br/>
									Location: 1001 N Federal Hwy #230 / Hallandale Beach / FL<br/>
									Website: <a href="http://concertfix.com" title="" target="_self" style="color: #439CED; text-decoration: none;">ConcertFix.com</a>
									</td>
									</tr>
								</table>
							</td>
							<!--END RIGHT CONTENT column-->
							</tr>
						</table>
					</td>
					</tr>
				</table>
			</td>
			</tr>
		</table>
	</td>
	</tr>
<!--END module-->

<!--START module / Bottom footer copyright text-->
	<tr>
	<td align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">
		<table class="holdingtable" width="600" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
			<tr>
			<td class="contentholdingtable" width="600" align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">
				<table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
					<tr>
					<td align="center" style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin: 0px; border-collapse: collapse;">
		<!--START content holding TABLE-->
						<table border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
							<tr>
							<!--START CONTENT column-->
							<td class="fullwidthcontenttable" width="600" valign="top" align="center" style="padding: 0px; margin: 0px; border-collapse: collapse;">
								<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; border-collapse: collapse;">
									<!--Table row with text-->
									<tr>
									<td class="centerme" align="right" style="color: #A09FA5; font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: normal; line-height: 20px; padding-top: 24px; padding-right: 24px; padding-bottom: 24px; padding-left: 24px; margin: 0px; border-collapse: collapse;">
									<!--Text goes here-->Copyright &#169; 2020. ConcertFix.
									</td>
									</tr>
								</table>
							</td>
							<!--END CONTENT column-->
							</tr>
						</table>
		<!--END content holding TABLE-->
					</td>
					</tr>
				</table>
			</td>
			</tr>
		</table>
	</td>
	</tr>
<!--END module-->

</table>
<!--END main table-->

</body>
</html>