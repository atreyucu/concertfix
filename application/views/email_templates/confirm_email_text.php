Hello, <?php echo $name; ?>
In order to receive our updates and tracking notifications we need you to confirm you're a human being with a legitimate email :)

<?php echo site_url($url); ?>

Thank you,
ConcertFix.com
