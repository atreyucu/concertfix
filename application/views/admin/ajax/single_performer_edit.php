<h1><?php echo $performer->Name ?> </h1>
<input type="hidden" name="slug" id="slug" value="<?php echo $performer->PerformerSlug ?>">
<input type="hidden" name="performerid" id="performerid" value="<?php echo $performer->PerformerID ?>">
<table class="table">
	<thead>
		<tr>
			<th>Last.FM Image</th>
			<th>Facebook Image</th>
<!--			<th>Wide Image</th>-->
<!--			<th>Profile Image</th>-->
			<th>Thumb Image</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				<img style="width:200px;" src="<?php echo $performer->lfmImagePath ?>">
				<input id="lastfm-image" type="text" value="<?php echo $performer->lfmImagePath ?>" style="width: 190px"/>
				<button id="lastfm-fb-image" class="btn">Change Last.FM Url</button>
			</td>
			<td>
				<img style="width:200px;" src="<?php echo $performer->fbImagePath ?>">
				<input id="fb-image" type="text" value="<?php echo $performer->fbImagePath ?>" style="width: 190px"/>
				<button id="btn-fb-image" class="btn">Change FB Url</button>
			</td>
			<!--<td>
				<div class="imgactions-wide">
					<div class="notice-wide"></div>
					<?php /*if ($performer->wide): */?>
						<a href="#" class="delete-picture btn btn-danger" data-pid="<?php /*echo $performer->PerformerID; */?>" data-img="wide"> Delete</a>
					<?php /*endif */?>
				</div>
				<hr>
				<img id="img-wide" style="width:100px;" src="<?php /*echo ($performer->wide) ?  "https://s3.amazonaws.com/ssglobalcdn/{$performer->wide}" : '/public/img/icons/noimage.png'; */?>">
				<input class="btn btn-success" id="wide_upload" type="file" name="wide_upload" />
				<!-- <div id="w-queue"></div> -->
			<!--</td>
			<td>
				<div class="imgactions-profile">
					<div class="notice-profile"></div>
					<?php /*if ($performer->profile): */?>
						<a href="#" class="delete-picture btn btn-danger" data-pid="<?php /*echo $performer->PerformerID; */?>" data-img="profile"> Delete</a>
					<?php /*endif */?>
				</div>
				<hr>
				<img id="img-profile" style="width:100px;" src="<?php /*echo ($performer->profile) ?  "https://s3.amazonaws.com/ssglobalcdn/{$performer->profile}" : '/public/img/icons/noimage.png'; */?>">
				<input class="btn  btn-success" id="profile_upload" type="file" name="profile_upload" />
				<!-- <div id="p-queue"></div> -->
			<!--</td>-->
			<td>
				<div class="imgactions-thumb">
					<div class="notice-thumb"></div>
					<?php if ($performer->thumb): ?>
						<a href="#" class="delete-picture btn btn-danger" data-pid="<?php echo $performer->PerformerID; ?>" data-img="thumb"> Delete</a>
					<?php endif ?>
				</div>
				<hr>
				<img id="img-thumb" style="width:200px;" src="<?php echo ($performer->thumb) ?  "https://s3.amazonaws.com/ssglobalcdn/{$performer->thumb}" : '/public/img/icons/noimage.png'; ?>">
				<input class="btn  btn-success" id="thumb_upload" type="file" name="thumb_upload" />
				<!-- <div id="t-queue"></div> -->
			</td>
		</tr>
		<tr>
			<td colspan="4"><div id="queue"></div></td>
		</tr>
	</tbody>

</table>



<div class="clear" style="clear:both;"></div>


<script type="text/javascript">
$(document).ready(function(){
	var pid = $('#performerid').val();
	var d = new Date(); //force them refreshes

	$('#wide_upload').uploadifive({
		'formData'     : {
			'timestamp' : '3245435',
			'token'     : '34543t53regrefdg45wy645rgbvsaf',
			'type'		:  'wide',
			'slug'		:  $('#slug').val(),
			'performerid': pid,
		},
		'buttonClass'		: 'btn',
		'auto'             : true,
		'queueID'          : 'queue',
		 'multi'        : false,
		'fileType'     : 'image',
		'uploadScript' : '/ajax/upload_image',
		'onProgress' : function(){
			$('#img-wide').attr("src",'/public/img/icons/loading.gif');
			$('.'+pid+'-img-wide').attr("src",'/public/img/icons/loading.gif');
		},
		'onUploadComplete' : function(file, data) {
			$('#img-wide').attr("src",'https://s3.amazonaws.com/ssglobalcdn/'+data+'?'+d.getTime());
			$('.'+pid+'-img-wide').attr("src",'https://s3.amazonaws.com/ssglobalcdn/'+data+'?'+d.getTime());
			$('.imgactions-wide').html("<div class='notice-wide'></div><a href='#' class='delete-picture btn btn-danger' data-pid='"+pid+"' data-img='wide'> Delete</a>");
		},
		'onUploadError' : function(file, errorCode, errorMsg, errorString) {
			alert('The file ' + file.name + ' could not be uploaded: ' + errorString);

		 },
		'onUploadSuccess' : function(file, data, response) {


		}
	});

   	$('#profile_upload').uploadifive({
		'formData'     : {
			'timestamp' : '3245435',
			'token'     : '34543t53regrefdg45wy645rgbvsaf',
			'type'		:  'profile',
			'slug'		:  $('#slug').val(),
			'performerid': pid,
		},
		'buttonClass'		: 'btn',
		'auto'             : true,
		'queueID'          : 'queue',
		 'multi'        : false,
		'fileType'     : 'image',
		'uploadScript' : '/ajax/upload_image',
		'onUploadComplete' : function(file, data) {
			$('#img-profile').attr("src",'https://s3.amazonaws.com/ssglobalcdn/'+data+'?'+d.getTime());
			$('.'+pid+'-img-profile').attr("src",'https://s3.amazonaws.com/ssglobalcdn/'+data+'?'+d.getTime());
			$('.imgactions-profile').html("<div class='notice-profile'></div><a href='#' class='delete-picture btn btn-danger' data-pid='"+pid+"' data-img='profile'> Delete</a>");
		},
		'onUploadError' : function(file, errorCode, errorMsg, errorString) {
			alert('The file ' + file.name + ' could not be uploaded: ' + errorString);

		 },
		'onUploadSuccess' : function(file, data, response) {


		}
	});


   $('#thumb_upload').uploadifive({
		'formData'     : {
			'timestamp' : '3245435',
			'token'     : '34543t53regrefdg45wy645rgbvsaf',
			'type'		:  'thumb',
			'slug'		:  $('#slug').val(),
			'performerid': pid,
		},
		'buttonClass'		: 'btn',
		'auto'             : true,
		'queueID'          : 'queue',
		 'multi'        : false,
		'fileType'     : 'image',
		'uploadScript' : '/ajax/upload_image',
		'onUploadComplete' : function(file, data) {
			$('#img-thumb').attr("src",'https://s3.amazonaws.com/ssglobalcdn/'+data+'?'+d.getTime());
			$('.'+pid+'-img-thumb').attr("src",'https://s3.amazonaws.com/ssglobalcdn/'+data+'?'+d.getTime());
			$('.imgactions-thumb').html("<div class='notice-thumb'></div><a href='#' class='delete-picture btn btn-danger' data-pid='"+pid+"' data-img='thumb'> Delete</a>");


		},
		'onUploadError' : function(file, errorCode, errorMsg, errorString) {
			alert('The file ' + file.name + ' could not be uploaded: ' + errorString);

		 },
		'onUploadSuccess' : function(file, data, response) {


		}
	});

});

</script>