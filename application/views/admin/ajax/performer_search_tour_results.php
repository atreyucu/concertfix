<div class="sidebar-widget">
	<div class="sidebar-header">
		<h4>Search Results:</h4>
		<div class="separator"></div>
	</div>
	<div class="" id="">
		<ul class="">
			<?php foreach ($performers as $p): ?>
				<li class="sidebar-item" style="margin-bottom:5px; border:1px dotted black;"  >
					<img alt="" src="<?php echo performerImage($p); ?> " class="pull-left" style="padding:5px;width:200px;">
					<h2><?php echo $p->Name ?> </h2>
					<p style="<?php echo ($p->custom == 1) ? 'background-color:#D5EAFF' : ''; ?>">
						<a href="/mordor/performer/text/<?php echo $p->PerformerID ?>"><button  class="">Edit Performer Text</button></a><br>

						<?php echo($p->text); ?>
					</p>
					<?php if($p->created_on): ?>
						<span class="badge">Created on: <?php echo date('F j, Y', strtotime($p->created_on)); ?> </span>
					<?php endif; ?>

					<?php if($p->custom == 1): ?>
						<span class="badge badge-success">Custom</span>
					<?php endif; ?>

					<?php /* if($p->announced == 1): ?>
						<span class="badge badge-primary">Announced!</span>
					<?php endif;*/ ?>
					<?php if(count($p->announcements)): ?>
					<table class="table">
						<thead>
							<tr>
								<th>Edit</th>
								<th>Status</th>
								<th>Announced Date</th>
								<th>End Date</th>
								<th>Title</th>
								<th>Text Excerpt</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($p->announcements as $a): ?>
								<tr>
									<td><a href="/mordor/performer/edit-announcement/<?php echo $a->id ?>" class="btn"> <span class="typ-edit"></span> Edit</a></td>
									<td><?php echo ($a->active) ? "<span style='color:green;font-weight:bolder;'>ACTIVE</span>" : "<span style='color:red;font-weight:bolder;'>DISABLED</span>"; ?></td>
									<td><?php echo $a->announce_date ?></td>
									<td><?php echo $a->end_date ?></td>
									<td><?php echo $a->title ?></td>
									<td><textarea readonly class="span12" rows="5"><?php echo substr($a->announce_text, 0, 100); ?></textarea></td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				<?php endif; ?>
 					<a href="/mordor/performer/create-announcement/<?php echo $p->PerformerID ?>" class="btn btn-primary pull-right">Create Announcement</a>
					<div class="clear" style="clear:both;"></div>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>