<div class="sidebar-widget">
	<div class="sidebar-header">
		<h4>Search Results:</h4>
		<div class="separator"></div>
	</div>
	<div class="" id="">
		<ul class="">
			<?php foreach ($performers as $p): ?>
			<?php
			$color = 'white';
			//do colors
			if(!$p->front && !$p->geo):
				//nowhere
				$color = '#FFFFFF';
			elseif(!$p->front && $p->geo):
				//geo only
				$color = '#C1E0FF';
			elseif($p->front && $p->geo):
				//both geo and front
				$color = '#CEFFE7';
			endif;
			?>
				<li class="sidebar-item" style="margin-bottom:5px; border:1px dotted black;"  >
					<table class="table ">
						<thead>
							<tr>
								<th colspan="4" id="th-<?php echo $p->PerformerID ?>" style="background-color:<?php echo $color; ?>">
									<div class="">
										<div class="featUpdated-<?php echo $p->PerformerID ?>"></div>
										<label class="checkbox"><input type="checkbox" data-feattype="side" id="fs-<?php echo $p->PerformerID ?>" class="feat" data-performerid="<?php echo $p->PerformerID ?>" <?php echo($p->side) ? 'checked="checked"' : ''; ?> > <b style="font-size:12px;">Show In Sidebar</b> </label>
										<label class="radio">
											<input type="radio" placeholder="" data-type="nofeat" class="feat-radio" id="nofeat-<?php echo $p->PerformerID ?>" data-pid="<?php echo $p->PerformerID ?>" name="feat_with_geo+<?php echo $p->PerformerID ?>" <?php echo (!$p->front && !$p->geo) ? 'checked="checked"' : '';?> > Not Featured
										</label>
										<label class="radio">
											<input type="radio" placeholder="" data-type="geo" class="feat-radio" id="geo-<?php echo $p->PerformerID ?>" data-pid="<?php echo $p->PerformerID ?>" name="feat_with_geo+<?php echo $p->PerformerID ?>" <?php echo (!$p->front && $p->geo) ? 'checked="checked"' : '';?>   > Geo Only
										</label>
										<label class="radio">
											<input type="radio" placeholder="" data-type="both" class="feat-radio" id="both-<?php echo $p->PerformerID ?>" data-pid="<?php echo $p->PerformerID ?>" name="feat_with_geo+<?php echo $p->PerformerID ?>" <?php echo ($p->front && $p->geo) ? 'checked="checked"' : '';?> > Front Page and Geo
										</label>

										<?php /*
										<label class="checkbox"><input type="checkbox" data-feattype="front" id="ff-<?php echo $p->PerformerID ?>" class="feat" data-performerid="<?php echo $p->PerformerID ?>" <?php echo($p->front) ? 'checked="checked"' : ''; ?> > <b style="font-size:12px;">Front Page</b> </label>
										*/ ?>
									</div>
									<br>
									<button class="performer-result btn pull-right" data-performerid="<?php echo $p->PerformerID ?>">Quick Edit</button>
									<h4><?php echo $p->Name ?></h4>
									<p><b>Slug:</b><?php echo $p->PerformerSlug ?></p>
								</th>
							</tr>
							<tr>
								<th>LFM IMG</th>
								<th>WIDE</th>
								<th>PROFILE</th>
								<th>THUMB</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><img style="width:75px;" src="<?php echo $p->img; ?>" style="width:70px;padding:5px;" class="pull-left" alt="avatar"></td>
								<td>
									<img class="<?php echo $p->PerformerID."-img-wide"; ?>" style="width:75px;" src="<?php echo ($p->wide) ? "https://s3.amazonaws.com/ssglobalcdn/{$p->wide}" : "/public/img/icons/noimage.png" ?>">
								</td>
								<td>
									<img class="<?php echo $p->PerformerID."-img-profile"; ?>" style="width:75px;" src="<?php echo ($p->profile) ? "https://s3.amazonaws.com/ssglobalcdn/{$p->profile}" : "/public/img/icons/noimage.png" ?>">
								</td>
								<td>
									<img class="<?php echo $p->PerformerID."-img-thumb"; ?>" style="width:75px;" src="<?php echo ($p->thumb) ? "https://s3.amazonaws.com/ssglobalcdn/{$p->thumb}" : "/public/img/icons/noimage.png" ?>">
								</td>
							</tr>
						</tbody>
					</table>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>