<fieldset>
    <h3>Users (<?php echo number_format($total); ?>)</h3>
    <form action="/mordor/users" method="get">
        <input type="text" name="keyword" placeholder="Find users" class="span4"/>
        <select name="type" class="span3">
            <option value="name">By Name</option>
            <option value="email">By Email</option>
        </select>

        <button class="btn btn-primary" type="submit">
            Search
        </button>
    </form>
    <div style="clear:both;" class="clear"></div>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Linked</th>
                <th>Registered</th>
                <th>Profile</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($users as $user): ?>
            <tr>
                <td>
                    <?php echo ucwords(strtolower($user -> name)); ?>
                </td>
                <td>
                    <?php echo strtolower($user -> primary_email); ?>
                </td>
                <td>
                    <?php echo ($user -> accounts > 0) ? $user -> accounts.' connected' : 'N/A'; ?>
                </td>
                <td>
                    <?php echo $user -> register_date; ?>
                </td>
                <td>
                    <a href="/mordor/users/details/<?php echo $user -> id; ?>">
                        view details
                    </a>
                </td>
                <td>
                    <select class="_changeStatus" data-id="<?php echo $user->id; ?>">
                        <option value="1" <?php if($user->active == 1) echo 'selected="selected"'; ?>>Active</option>
                        <option value="0" <?php if( ! $user->active) echo 'selected="selected"'; ?>>Disabled</option>
                    </select>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <div class="clearfix"></div>

    <?php echo $this->pagination->create_links(); ?>
</fieldset>