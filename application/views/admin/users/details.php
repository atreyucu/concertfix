<div class="span12">
    <h3>#<?php echo $user->id.' '.ucwords(strtolower($user->name)); ?></h3>

    <?php if($user->profile_picture): ?>
        <div class="clearfix">
            <img src="<?php echo $user->profile_picture; ?>" alt="profile picture" style="max-width: 100px;"/>
        </div>
    <?php endif; ?>

    <strong>Name</strong>
    <?php echo ucwords(strtolower($user->name)); ?>

    <br class="clearfix" />

    <strong>Email</strong>
    <?php echo strtolower($user->primary_email); ?>

    <br class="clearfix" />

    <strong>Register Date</strong>
    <?php echo $user->register_date; ?>

    <br class="clearfix" />

    <strong>Birth Date</strong>
    <?php echo ($dob = $user->birth_date) ? $dob : 'N/A'; ?>

    <br class="clearfix" />

    <strong>Gender</strong>
    <?php echo $user->gender; ?>

    <br class="clearfix" />

    <strong>Active</strong>
    <?php echo ($user->active == 1) ? 'Yes' : 'No'; ?>
</div>

<br class="clearfix" />

<div class="span12 clearfix">
    <h3>SOCIAL ACCOUNTS</h3>
    <?php if(!$linked): ?>
        <strong>User did not link any social accounts</strong>
    <?php else: ?>
        <ul>
        <?php foreach($linked as $network):
            $url = '';
            switch(strtolower($network->media_type)):
                case 'google':
                    $url = "https://plus.google.com/u/1/{$network->media_user_id}/posts";
                break;

                case 'facebook':
                    $url = "https://facebook.com/{$network->media_user_id}";
                    break;

                case 'twitter':
                    $url = "https://twitter.com/account/redirect_by_id/{$network->media_user_id}";
                break;
            endswitch;
        ?>
            <li>
                <strong>
                    <a href="<?php echo $url; ?>">
                        <?php echo strtoupper($network->media_type); ?>
                    </a>
                </strong>
            </li>
        <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>

<div class="span12 clearfix">
    <br class="clearfix" style="clear:both;" />

    <a href="/mordor/users" class="btn btn-primary">
        BACK TO USERS
    </a>
</div>