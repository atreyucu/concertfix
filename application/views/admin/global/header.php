<div id="nav" class="container">
	<div class="row">
	<!-- Logo & Navigation -->
		<div class="span12" role="navigation" id="mainnav">
			<!-- Site Logo -->
			<!-- Navigation Start -->
			<nav id="main-navigation" class="visible-desktop">
				<ul class="first-level">
					<li class=""><a href="/mordor/dashboard">Dashboard</a></li>
					<li class=""><a href="/mordor/users">Users</a></li>
					<li><a href="#">Performer Admin <span class="icon awe-chevron-down"></span></a>
						<ul class="second-level">
							<li><a href="/mordor/performer/images">Performer Pictures</a></li>
							<li><a href="/mordor/performer/tour">Tour Anoucement</a></li>
						</ul>
					<li><a href="#">Site Settings  <span class="icon awe-chevron-down"></span></a>
						<ul class="second-level">
							<li><a href="/mordor/menu">Menu Manager</a></li>
							<li><a href="/mordor/settings/city">City Manager</a></li>
						</ul>
					</li>
					<li><a href="/mordor/logout">Logout</a></li>
				</ul>
			</nav>
			<!-- Navigation End -->
		</div>
	</div>
</div>