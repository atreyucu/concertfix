<?php 
if($this->session->flashdata('message')):
 ?>
<div class="alert alert-danger">
	<p><?php echo $this->session->flashdata('message'); ?></p>
</div>
<?php endif; ?>
<div class="clear" style="clear:both;"></div>
<form method="post" action="/mordor/login">
<fieldset>
	<legend>One does not simply enter Mordor!</legend>
	<label>Name</label>
	<input type="text" placeholder="" name="user" id="user">
	
	<label>Password</label>
	<input type="password" placeholder="" name="password" id="password">
	<div class="clear" style="clear:both;"></div>

<button type="submit" class="btn">Get the precious!</button>

</fieldset>

</form>