<h1>Menu Manager</h1>
<form method="post" class="form-horizontal" action="/mordor/menu?op=addmenu">
	<div class="control-group">
		<label class="control-label" for="menu_title">Menu Title</label>
		<div class="controls">
			<input type="text" name="menu_title" id="menu_title" placeholder="Canadian Crybaby">
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="menu_link">Link .com/</label>
		<div class="controls">
			<input type="text" name="menu_link" id="menu_link" placeholder="tours/drake">
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="menu_link">Parent and Order</label>
		<div class="controls">
			<select class="parent_id" name="parent_id">
				<option value="0">ROOT</option>
				<?php foreach ($menulist as $menu): ?>
					<option value="<?php echo $menu->id ?>"><?php echo $menu->menu_title; ?></option>
				<?php endforeach ?>
			</select>
			<select class="order input-mini" name="order" id="order">
				<?php 
				for($i = 0; $i < 50; $i++):?>
					<option value="<?php echo $i ?>"><?php echo $i ?></option>
				<?php endfor; ?>
			</select>
		</div>
	</div>
	<div class="control-group">
		<div class="controls">
			<button type="submit" class="btn">Add Menu</button>
		</div>
	</div>
</form>