<h2>Current Menu</h2>
<p>Deleting parent will remove children</p>
<ul class="first-level" style="list-style:none;">
<?php foreach ($menu as $link): ?>
	<li><?php echo $link->order ?>) <a href="/mordor/menu/delete/<?php echo $link->id; ?>"><i class="icon-trash"></i></a>&nbsp;
		<a href="<?php echo $link->menu_link ?>" target="_blank"> 
			<b><?php echo $link->menu_title ?></b> <em>(<?php echo $link->menu_link; ?>)</em>
		</a>
		<?php if(count($link->children)): ?>
			<ul class="second-level"  style="list-style:none;">
			<?php foreach ($link->children as $child): ?>
				<li><a href="/mordor/menu/delete/<?php echo $child->id; ?>"><i class="icon-trash"></i></a>&nbsp;
					<a href="<?php echo $child->menu_link ?>" target="_blank">
						<?php echo $child->menu_title ?> <em>(<?php echo $child->menu_link; ?>)</em>
					</a>
					<?php if(count($child->children)): ?>
						<ul class="third-level"  style="list-style:none;">
							<?php foreach ($child->children as $grand): ?>
								<li><a href="/mordor/menu/delete/<?php echo $grand->id; ?>"><i class="icon-trash"></i></a>&nbsp;
									<a href="<?php echo $grand->menu_link ?>" target="_blank">
										<?php echo $grand->menu_title ?>  <em>(<?php echo $grand->menu_link; ?>)</em>
									</a>
								</li>
							<?php endforeach ?>
						</ul>	
					<?php endif; ?>
				</li>
			<?php endforeach ?>
			</ul>
		<?php endif; ?>
	</li>
<?php endforeach ?>
</ul>