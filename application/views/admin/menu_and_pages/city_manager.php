<div class="row-fluid">
<div class="span12">
<h2>City Admin</h2>
<form method="post" action="/mordor/settings/city/add">
	<legend>Add City</legend>
	<input type="text" name="city" placeholder="Columbus">
	<input type="text" name="state_short" placeholder="OH" class="input-mini" >
	<select name="area">
		<option value="East">East</option>
		<option value="Central">Central</option>
		<option value="West">West</option>
		<option value="Canada">Canada</option>
	</select>

	<button type="submit" placeholder="" value='Go' class="btn btn-success">ADD CITY</button>
</form>
	<table class="table">
		<thead>
			<tr>
				<th>Remove</th>
				<th>Location</th>
				<th>Area</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($cities as $c): ?>
				<tr id="tr-<?php echo $c->id; ?>">
					<td><a href="#remove" class="removeCity" style="color:Red;" data-id="<?php echo $c->id; ?>"><span class="icon awe-trash"></span> Get Out!</a></td>
					<td><?php echo $c->city.", ".$c->state_short ?></td>
					<td>
						<select class="toggle_area" data-id="<?php echo $c->id ?>">
							<option value=""> - </option>
							<option value="East" <?php echo ($c->area === 'East') ? "selected='selected'" : ''; ?>>East</option>
							<option value="Central" <?php echo ($c->area === 'Central') ? "selected='selected'" : ''; ?>>Central</option>
							<option value="West" <?php echo ($c->area === 'West') ? "selected='selected'" : ''; ?>>West</option>
							<option value="Canada" <?php echo ($c->area === 'Canada') ? "selected='selected'" : ''; ?>>Canada</option>
						</select>
						<span id="saved-<?php echo $c->id ?>"></span>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>
</div>