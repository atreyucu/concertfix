<?php $endTour = ($events) ? strtotime(end($events)->Date) : date('U') ; ?>
<div class="row-fluid" style="background-color:#FFFFE1;">
	<div class="span6">
	<h1>Editing Custom Performer Text</h1>
		<a href="/mordor/performer/tour" style="font-size:12px;">< Go Back</a>
		<form method="post" action="/mordor/performer/text/<?php echo $performer->PerformerID; ?>" >
			<h1><?php echo $performer->PerformerName ?> </h1>
			<h3>last Event: <span style='color:blue;font-weight:bolder;'><?php echo ($events) ? date('F j, Y', $endTour) : "NO EVENTS"; ?></span> </h3>
			<div class="row-fluid">
				<div class="span6">
					<label for="created_on">
						Text Creation Date:
					</label>
					<input type="text" id="created_on" name="created_on" placeholder="" class="datepicker span12" value="<?php echo ($performer->created_on) ? date('Y-m-d', strtotime($performer->created_on)) : date('Y-m-d'); ?>" />
				</div>
				<div class="span6">
					<label for="refresh_date">
						Refresh Date:
					</label>
					<input type="text" id="refresh_date" name="refresh_date" placeholder="" class="datepicker span12" value="<?php echo ($performer->refresh_date) ? date('Y-m-d', strtotime($performer->refresh_date)) : date('Y-m-d',$endTour); ?>" />
					<br>
					<p> (usually day of last event or automatic with 0 events)</p>
				</div>
			</div>
			<div class="separator"></div>
			<textarea name="text" class="span12" id="post-text" style="height:400px;"><?php echo $text ?></textarea>
			<input type="hidden" name="PerformerID" value="<?php echo $performer->PerformerID; ?>" placeholder="">
			<a href="#resetDefault" class="resetDefaultText btn btn-danger" data-slug="<?php echo $performer->PerformerSlug ?>">Reset To Default Text</a>
			<span class="clickToSave"></span>
			<input type="submit" placeholder="" value="UPDATE CUSTOM TEXT" class="btn btn-primary pull-right">
		</form>
		<div class="row-fluid">
			<div class="span6">
				<h3>Albums</h3>
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Playcount</th>
							<th>Release</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($albums as $a): ?>
							<tr>
								<td>
									<?php echo $a->Rank ?>
								</td>
								<td>
									<?php echo $a->AlbumName ?>
								</td>
								<td>
									<?php echo $a->PlayCount ?>
								</td>
								<td>
									<?php echo $a->ReleaseDate ?>
								</td>

							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
			<div class="span6">
				<h3>Quick Data</h3>
				<p>Listeners: <?php echo number_format($lfm['stats']['listeners'], 0, '.',',') ?> </p>
				<p>Play count: <?php echo number_format($lfm['stats']['playcount'], 0, '.',','); ?> </p>
				<h4>Similar</h4>
				<?php foreach ($lfm['similar'] as $s): ?>
					<span class="badge">	<?php echo $s['name']; ?></span>
				<?php endforeach ?>
				<h4>Tags</h4>
				<?php foreach ($lfm['tags'] as $t): ?>
					<span class="badge"><?php echo $t['name'] ?> </span>
				<?php endforeach ?>
			</div>
		</div>
	</div>
	<div class="span6">
		<h3>LFM Bio</h3>
		<p><?php echo $lfm['bio']['content'] ?></p>
		<h3>Tours:</h3>
		<table class="table">
			<thead>
				<tr>
					<th>Event</th>
					<th>With</th>
					<th>When/Where</th>
					<th>Tickets</th>
				</tr>
			</thead>
			<tbody>
				<?php if ($events): ?>


				<?php foreach ($events as $e): ?>
					<tr>
						<td>
							<?php echo $e->Name ?><br>
							<?php echo $e->City.", ".$e->StateProvince; ?>
						</td>
						<td>
							<?php if($e->performers): ?>
								<?php foreach ($e->performers as $p): ?>
									<?php echo $p->PerformerName ?><br>
								<?php endforeach ?>
							<?php else: ?>
								- no data yet -
							<?php endif; ?>
						</td>
						<td>
							<?php echo date('F j, Y', strtotime($e->Date)); ?><br>
							<?php echo $e->Venue ?>
						</td>
						<td>
							<?php echo (isset($e->prices->ticketsAvailable)) ? $e->prices->ticketsAvailable." Tickets Left" : 'no tickets yet'; ?><br>
							<?php echo (isset($e->prices->lowPrice)) ? "\$".$e->prices->lowPrice." - ".$e->prices->highPrice : "no prices yet"; ?>
							<?php //var_dump($e->tickets) ?>
						</td>
					</tr>
				<?php endforeach ?>
				<?php else: ?>
				<tr><td colspan="4"> - NO EVENTS -</td></tr>
				<?php endif ?>
			</tbody>
		</table>
		<div class="sidebar-widget" id="twitter-feed">
				<h3>TWitter Trends</h3>
				<div class="twitter comments">
					<ul class="commentlists">
					<?php foreach ($twitter->statuses as $item): ?>
						<li>
							<div class="comment-author image-polaroid">
								<a href="https://twitter.com/<?php echo $item->user->screen_name; ?>" target="_blank" rel="nofollow" title="Follow @<?php echo $item->user->screen_name; ?>"><img src="<?php echo $item->user->profile_image_url_https; ?>" style="width:50px; height:50px;" alt="avatar"></a>
							</div>
							<div class="comment-body">
								<div class="comment-meta">
									<span class="meta-name"><a href="https://twitter.com/<?php echo $item->user->screen_name; ?>" target="_blank" rel="nofollow" title="Follow @<?php echo $item->user->screen_name; ?>"><?php echo $item->user->name." - @".$item->user->screen_name; ?></a></span>
									<span class="meta-date"><?php echo date('m j, Y H:i:s', strtotime($item->created_at)) ?></span>
									<!-- <div class="reply">
										<a href="#">reply</a>
									</div> -->
								</div>
								<p><?php echo twitter_clean($item->text); ?></p>
							</div>
						</li>
					<?php endforeach ?>
					</ul>
				</div>
			</div>

	</div>
</div>h