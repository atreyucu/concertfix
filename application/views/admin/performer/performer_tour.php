<!-- <form method="post" action="/mordor/performer"> -->
<div class="row-fluid">
	<div class="span8">
		<fieldset>
			<legend>Search Performer To Create Custom Tour Announcement</legend>
			<input type="text" placeholder="Lady Gaga" name="querysearch" id="querysearch_tour" class="span12">
		</fieldset>
		<div class="clear" style="clear:both;"></div>
		<div class="results" id="search-results"></div>
	</div>
	<div class="span4" id="announced">
		<div class="sidebar-widget">
			<div class="sidebar-header">
				<h4>Announced:</h4>
				<div class="separator"></div>
			</div>
			<div class="" id="">

				<ul>
					<?php $i = 1; ?>
				<?php foreach ($announced as $p): ?>
					<li class="sidebar-item" style='padding:3px; border-bottom:1px solid #C0C0C0;' >
						<a href="/mordor/performer/edit-announcement/<?php echo $p->aId ?>">
						  <span class="typ-edit pull-right"></span>
							<span style="font-size:13px;font-weight:bold; <?php  echo ($p->active) ? "color:green;" : "color:#CB9696;"; ?>"><?php echo ($p->title) ? $p->title : ' - no title -' ?></span><br>
							<span style="color:black;" >(<?php echo date('M j, y', strtotime($p->announce_date)); ?> - <?php echo date('M j, y', strtotime($p->end_date)); ?>)</span>
						</a>
					</li>
					<?php $i++; ?>
				<?php endforeach ?>
				</ul>
		</div>
	</div>
</div>

<!-- </form> -->