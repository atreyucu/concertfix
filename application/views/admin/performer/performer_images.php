<!-- <form method="post" action="/mordor/performer"> -->
<?php
$cdn = "https://s3.amazonaws.com/ssglobalcdn/";
$showAll = $this->input->get('showAll');

 ?>
<div class="row-fluid">
	<div class="span6">
	<a name="top"></a>
		<fieldset>
			<h3>Search Performer</h3>
			<input type="text" placeholder="Lady Gaga" name="querysearch" id="querysearch" class="span12">
		</fieldset>
		<div class="clear" style="clear:both;"></div>
		<div class="results" id="search-results"></div>

		<h3>Currently Active Featured</h3>
		<p>Artists without any features (side, front, geo) will be removed automatically from featured table daily</p>
		<table class="table">
			<thead>
				<tr>
					<th>
						<?php if ($showAll): ?>
							<a href="?showAll=0">Click to Hide Geo</a>
						<?php else: ?>
							<a href="?showAll=1">Click to Show All (w/geo)</a>
						<?php endif ?>
					<th colspan="5"><input type="text" class="pull-right" placeholder="Quick Table Search" name="search"></th>
				</tr>
				<tr>
					<th>Name (click to edit)</th>
					<th>Featured</th>
					<th>Wide</th>
					<th>Prof</th>
					<th>Thumb</th>
					<th>iLFRM</th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 0; ?>
				<?php foreach ($curFeat as $p): ?>
					<?php $onlyGeo = (!$p->front && $p->geo) ? true : false;  ?>
					<?php
					if($showAll): ?>

					<?php
						$color = 'white';
						//do colors
						if(!$p->front && !$p->geo):
							//nowhere
							$color = '#FFFFFF';
						elseif(!$p->front && $p->geo):
							//geo only
							$color = '#C1E0FF';
						elseif($p->front && $p->geo):
							//both geo and front
							$color = '#CEFFE7';
						endif;
						?>
					<tr class="data-item">
						<td class="data-name"><a href="#top" class="performer-result" data-performerid="<?php echo $p->PerformerID ?>"><?php echo $p->Name ?></a></td>
						<td  id="1_th-<?php echo $p->PerformerID ?>" style="background-color:<?php echo $color; ?>">
							<div class="featUpdated-<?php echo $p->PerformerID ?>"></div>
							<label class="checkbox"><input type="checkbox" data-feattype="side" id="1_fs-<?php echo $p->PerformerID ?>" class="feat" data-performerid="<?php echo $p->PerformerID ?>" <?php echo($p->side) ? 'checked="checked"' : ''; ?> > <b style="font-size:12px;">Show In Sidebar</b> </label>
							<label class="radio">
								<input type="radio" placeholder="" data-type="nofeat" class="feat-radio" id="1_nofeat-<?php echo $p->PerformerID ?>" data-pid="<?php echo $p->PerformerID ?>" name="feat_with_geo+<?php echo $p->PerformerID ?>" <?php echo (!$p->front && !$p->geo) ? 'checked="checked"' : '';?> > Not Featured
							</label>
							<label class="radio">
								<input type="radio" placeholder="" data-type="geo" class="feat-radio" id="1_geo-<?php echo $p->PerformerID ?>" data-pid="<?php echo $p->PerformerID ?>" name="feat_with_geo+<?php echo $p->PerformerID ?>" <?php echo (!$p->front && $p->geo) ? 'checked="checked"' : '';?>   > Geo Only
							</label>
							<label class="radio">
								<input type="radio" placeholder="" data-type="both" class="feat-radio" id="1_both-<?php echo $p->PerformerID ?>" data-pid="<?php echo $p->PerformerID ?>" name="feat_with_geo+<?php echo $p->PerformerID ?>" <?php echo ($p->front && $p->geo) ? 'checked="checked"' : '';?> > Front Page and Geo
							</label>
						</td>

						<td><?php echo ($p->wide) ? '<img src="/public/img/icons/v.png" />' : '<img src="/public/img/icons/x.png" />'; ?>
						</td>
						<td><?php echo ($p->profile) ? '<img src="/public/img/icons/v.png" />' : '<img src="/public/img/icons/x.png" />'; ?>
						</td>
						<td><?php echo ($p->thumb) ? '<img src="/public/img/icons/v.png" />' : '<img src="/public/img/icons/x.png" />'; ?>
						</td>
						<td><?php echo ($p->img) ? '<img src="/public/img/icons/v.png" />' : '<img src="/public/img/icons/x.png" />'; ?>
						</td>
					</tr>
					<?php $i++; ?>
					<?php if($i == 10): ?>
					<tr class="heads"><td colspan="6"><a href="#top" style="color:blue;">(Up Top)</a> </td></tr>
					<tr class="heads">
						<th>Name</th>
						<th>Featured</th>
						<th>Wide</th>
						<th>Prof</th>
						<th>Thumb</th>
						<th>iLFRM</th>
					</tr>
					<?php $i = 0; ?>
					<?php endif ?>
				<?php else: //do not show all ?>
					<?php if (!$onlyGeo): ?>
						<?php
						$color = 'white';
						//do colors
						if(!$p->front && !$p->geo):
							//nowhere
							$color = '#FFFFFF';
						elseif(!$p->front && $p->geo):
							//geo only
							$color = '#C1E0FF';
						elseif($p->front && $p->geo):
							//both geo and front
							$color = '#CEFFE7';
						endif;
						?>
					<tr class="data-item">
						<td class="data-name"><a href="#top" class="performer-result" data-performerid="<?php echo $p->PerformerID ?>"><?php echo $p->Name ?></a></td>
						<td  id="1_th-<?php echo $p->PerformerID ?>" style="background-color:<?php echo $color; ?>">
							<div class="featUpdated-<?php echo $p->PerformerID ?>"></div>
							<label class="checkbox"><input type="checkbox" data-feattype="side" id="1_fs-<?php echo $p->PerformerID ?>" class="feat" data-performerid="<?php echo $p->PerformerID ?>" <?php echo($p->side) ? 'checked="checked"' : ''; ?> > <b style="font-size:12px;">Show In Sidebar</b> </label>
							<label class="radio">
								<input type="radio" placeholder="" data-type="nofeat" class="feat-radio" id="1_nofeat-<?php echo $p->PerformerID ?>" data-pid="<?php echo $p->PerformerID ?>" name="feat_with_geo+<?php echo $p->PerformerID ?>" <?php echo (!$p->front && !$p->geo) ? 'checked="checked"' : '';?> > Not Featured
							</label>
							<label class="radio">
								<input type="radio" placeholder="" data-type="geo" class="feat-radio" id="1_geo-<?php echo $p->PerformerID ?>" data-pid="<?php echo $p->PerformerID ?>" name="feat_with_geo+<?php echo $p->PerformerID ?>" <?php echo (!$p->front && $p->geo) ? 'checked="checked"' : '';?>   > Geo Only
							</label>
							<label class="radio">
								<input type="radio" placeholder="" data-type="both" class="feat-radio" id="1_both-<?php echo $p->PerformerID ?>" data-pid="<?php echo $p->PerformerID ?>" name="feat_with_geo+<?php echo $p->PerformerID ?>" <?php echo ($p->front && $p->geo) ? 'checked="checked"' : '';?> > Front Page and Geo
							</label>
						</td>

						<td><?php echo ($p->wide) ? '<img src="/public/img/icons/v.png" />' : '<img src="/public/img/icons/x.png" />'; ?>
						</td>
						<td><?php echo ($p->profile) ? '<img src="/public/img/icons/v.png" />' : '<img src="/public/img/icons/x.png" />'; ?>
						</td>
						<td><?php echo ($p->thumb) ? '<img src="/public/img/icons/v.png" />' : '<img src="/public/img/icons/x.png" />'; ?>
						</td>
						<td><?php echo ($p->img) ? '<img src="/public/img/icons/v.png" />' : '<img src="/public/img/icons/x.png" />'; ?>
						</td>
					</tr>
					<?php $i++; ?>
					<?php if($i == 10): ?>
					<tr class="heads"><td colspan="6"><a href="#top" style="color:blue;">(Up Top)</a> </td></tr>
					<tr class="heads">
						<th>Name</th>
						<th>Featured</th>
						<th>Wide</th>
						<th>Prof</th>
						<th>Thumb</th>
						<th>iLFRM</th>
					</tr>
					<?php $i = 0; ?>
					<?php endif ?>
					<?php endif ?>
				<?php endif; ?>
				<?php endforeach ?>
				<tr class="heads"><td colspan="6"><a href="#top" style="color:blue;">(Up Top)</a> </td></tr>

			</tbody>
		</table>
	</div>
	<div class="span6" id="performer-edit-panel">

	</div>
</div>

<!-- </form> -->
