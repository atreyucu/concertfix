<?php $endTour = ($events) ? strtotime(end($events)->Date) : date('U') ; ?>
<div class="row-fluid">
	<div class="span6">
		<h1>Creating Announcement For <span><?php echo $performer->PerformerName; ?></span> </h1>
		<a href="/mordor/performer/tour" style="font-size:12px;">< Go Back</a>
		<form method="post" action="/mordor/performer/edit-announcement/<?php echo $performer->aId; ?>">
			<h3>last Event: <span style='color:blue;font-weight:bolder;'><?php echo ($events) ? date('F j, Y', $endTour) : "NO EVENTS"; ?></span> </h3>
			<div class="row-fluid">
				<div class="span4">
					<label class="checkbox">
						<input type="checkbox" name="active" <?php echo ($performer->active) ? 'checked="checked"' : ''; ?> placeholder="" /> Announce!
					</label>
				</div>
				<div class="span4">
					<label for="announce_date">
						Announced On (Y-m-d)
					</label>
					<input type="text" id="announce_date" name="announce_date" placeholder="" class="datepicker span12" value="<?php echo date('Y-m-d', strtotime($performer->announce_date)); ?>" />
				</div>
				<div class="span4">
					<label for="end_date">
						End Date:
					</label>
					<input type="text" id="end_date" name="end_date" placeholder="" class="datepicker span12" value="<?php echo ($performer->end_date) ? date('Y-m-d',strtotime($performer->end_date)) : date('Y-m-d',$endTour) ; ?>" />
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12 form-inline">
					<?php foreach ($cats as $cat): ?>

							<label class="checkbox ">
								<input type="checkbox" name="a_category_id[<?php echo $cat->id ?>]" placeholder="" <?php echo (in_array($cat->id, $performer->cats)) ? "checked='checked'" : ''; ?> /> <?php echo $cat->Category ?>
							</label>

					 &nbsp;
					<?php endforeach ?>

				</div>
			</div>
			<label for="title">Title</label>
			<input type="text" name="title" placeholder="Bieber's Out-of-the-closet Tour" class="input-xxlarge" value="<?php echo ($performer->title) ? $performer->title : $performer->PerformerName." Tour"; ?>">
			<div class="separator"></div>
			<textarea name="announce_text" class="span12" id="post-text" style="height:400px;"><?php echo ($performer->announce_text) ? $performer->announce_text : $performer->pText; ?></textarea>
			<input type="hidden" name="PerformerID" value="<?php echo $performer->PerformerID; ?>" placeholder="">
			<input type="submit" placeholder="" value="UPDATE ANNOUNCEMENT" class="btn btn-primary pull-right">
		</form>
		<div class="row-fluid">
			<div class="span6">
				<h3>Albums</h3>
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Playcount</th>
							<th>Release</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($albums as $a): ?>
							<tr>
								<td>
									<?php echo $a->Rank ?>
								</td>
								<td>
									<?php echo $a->AlbumName ?>
								</td>
								<td>
									<?php echo $a->PlayCount ?>
								</td>
								<td>
									<?php echo $a->ReleaseDate ?>
								</td>

							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
			<div class="span6">
				<h3>Quick Data</h3>
				<p>Listeners: <?php echo number_format($lfm['stats']['listeners'], 0, '.',',') ?> </p>
				<p>Play count: <?php echo number_format($lfm['stats']['playcount'], 0, '.',','); ?> </p>
				<h4>Similar</h4>
				<?php foreach ($lfm['similar'] as $s): ?>
					<span class="badge">	<?php echo $s['name']; ?></span>
				<?php endforeach ?>
				<h4>Tags</h4>
				<?php foreach ($lfm['tags'] as $t): ?>
					<span class="badge"><?php echo $t['name'] ?> </span>
				<?php endforeach ?>
			</div>
		</div>
	</div>
	<div class="span6">
		<h3>LFM Bio</h3>
		<p><?php echo $lfm['bio']['content'] ?></p>
		<h3>Tours:</h3>
		<table class="table">
			<thead>
				<tr>
					<th>Event</th>
					<th>With</th>
					<th>When/Where</th>
					<th>Tickets</th>
				</tr>
			</thead>
			<tbody>
				<?php if ($events): ?>


				<?php foreach ($events as $e): ?>
					<tr>
						<td>
							<?php echo $e->Name ?><br>
							<?php echo $e->City.", ".$e->StateProvince; ?>
						</td>
						<td>
							<?php if($e->performers): ?>
								<?php foreach ($e->performers as $p): ?>
									<?php echo $p->PerformerName ?><br>
								<?php endforeach ?>
							<?php else: ?>
								- no data yet -
							<?php endif; ?>
						</td>
						<td>
							<?php echo date('F j, Y', strtotime($e->Date)); ?><br>
							<?php echo $e->Venue ?>
						</td>
						<td>
							<?php echo (isset($e->prices->ticketsAvailable)) ? $e->prices->ticketsAvailable." Tickets Left" : 'no tickets yet'; ?><br>
							<?php echo (isset($e->prices->lowPrice)) ? "\$".$e->prices->lowPrice." - ".$e->prices->highPrice : "no prices yet"; ?>
							<?php //var_dump($e->tickets) ?>
						</td>
					</tr>
				<?php endforeach ?>
				<?php else: ?>
				<tr><td colspan="4"> - NO EVENTS -</td></tr>
				<?php endif ?>
			</tbody>
		</table>
		<div class="sidebar-widget" id="twitter-feed">
				<h3>TWitter Trends</h3>
				<div class="twitter comments">
					<ul class="commentlists">
					<?php foreach ($twitter->statuses as $item): ?>
						<li>
							<div class="comment-author image-polaroid">
								<a href="https://twitter.com/<?php echo $item->user->screen_name; ?>" target="_blank" rel="nofollow" title="Follow @<?php echo $item->user->screen_name; ?>"><img src="<?php echo $item->user->profile_image_url_https; ?>" style="width:50px; height:50px;" alt="avatar"></a>
							</div>
							<div class="comment-body">
								<div class="comment-meta">
									<span class="meta-name"><a href="https://twitter.com/<?php echo $item->user->screen_name; ?>" target="_blank" rel="nofollow" title="Follow @<?php echo $item->user->screen_name; ?>"><?php echo $item->user->name." - @".$item->user->screen_name; ?></a></span>
									<span class="meta-date"><?php echo date('m j, Y H:i:s', strtotime($item->created_at)) ?></span>
									<!-- <div class="reply">
										<a href="#">reply</a>
									</div> -->
								</div>
								<p><?php echo twitter_clean($item->text); ?></p>
							</div>
						</li>
					<?php endforeach ?>
					</ul>
				</div>
			</div>

	</div>
</div>h