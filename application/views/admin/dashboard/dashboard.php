<div class="row-fluid">
	<div class="span12">
		<h1>Dashboard</h1>
	</div>
</div>
<div class="row-fluid">
	<div class="span12">
	<form action="/mordor/dashboard" method="get" class="form-inline">
	<legend>Recently Updated Events from TicketNetwork</legend>
		<label for="date">Added On (YYYY-MM-DD):
			<input type="text" name="date" id="date" value="<?php echo ($this->input->get('date')) ? $this->input->get('date') : date('Y-m-d'); ?>">
		</label>
		<button type="submit" class="btn btn-primary">View</button>
	</form>
<?php if($today): ?>
<?php $currentEvent = false; ?>
<?php foreach ($today as $event): ?>
	<?php if($currentEvent != $event->Name): ?>
		<h3>EVENT: <?php echo $event->Name; ?></h3>
		<?php $currentEvent = $event->Name; ?>
	<?php endif; ?>
		<div class="" style="margin-bottom:10px;">
			<strong><span style="color:green;font-size:13px;"><?php echo $event->City.", ".$event->StateProvince; ?> - <?php echo date('F j, Y', strtotime($event->Date)); ?></span></strong> <span class="label"><?php echo $event->prices->ticketsAvailable ?></span> Tickets Left
			<br>
			<strong>Price: </strong> $<?php echo $event->prices->lowPrice ?> - <?php echo $event->prices->highPrice ?>
			<br>
			<strong>Artists (click to announce): </strong><br>
			<?php $i = 0; ?>
			<?php foreach ($event->performers as $p): ?>
				<a href="/mordor/performer/create-announcement/<?php echo $p->PerformerID ?>" target="_blank" class="btn btn-link" style=""><?php echo $p->PerformerName; ?></a>
			<?php $i++; ?>
			<?php echo ($i < count($event->performers)) ? " | " : ''; ?>
			<?php endforeach ?>
		</div>

<?php endforeach ?>
<?php else: ?>
	<div class="alert">There are no events posted this date!</div>
<?php endif; ?>
</div>
<script>
	 $(function() {
	 	var pickerOpts = {
        dateFormat:"yy-mm-dd"
    };
    $( "#date" ).datepicker(pickerOpts);
  });
</script>