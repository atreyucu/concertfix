<h1>About ConcertFix</h1>
<div class="separator"></div>
<br />
<p>ConcertFix.com is dedicated to bringing you all news related to concerts and musical tours.
	We collect information from hundreds of different websites and countless fans all over the
	world to bring you tour announcements, top tour charts, ticket announcements, concert tickets,
	and more.</p>
<br />
<hr />
<h3>Tour Announcements</h3>
<p>ConcertFix has a team dedicated to bringing you the latest <a href="/tour-announcements">tour announcements</a>.
	Find out everyone who's on tour right now including when and where they will be performing. We also provide
	information about tour guests and new concerts that are added to tours at later dates. Our
	<a href="https://twitter.com/concertfix">twitter</a>, <a href="https://www.facebook.com/concertfix">facebook</a>,
	and <a href="https://plus.google.com/+Concertfix">google+</a> accounts
	are also great ways to get updates on all the latest tour announcements.</p>
<br />
<hr />
<h3>Top Tours</h3>
<p>ConcertFix pulls sales data, attendance data, and social indicators from vairous reputable sites to determine how
	successful a tour has been up to this point. Concert Perfomers are then ranked in our <a href="/top-tours">top tours</a>
	section for their respective genre. We also allow and encourage fan feedback. If you went to see a concert, let us know
	how the concert was. We are constantly having contests and give aways to fans who provide great reviews, and or pictures
	of the concert they attended. See any of our social accounts for more details.</p>
<br />
<hr />
<h3>Concert Tickets</h3>
<p>ConcertFix carries information and <a href="/great-deals-on-tickets">tickets</a> to every performer, artist, musicaian,
	and musical festival currently scheduled in north america. Want to buy tickets to that sold out event? Need to find
	frontrow, vip, or backstage passes? Want to learn more about a concert? Need directions on how to get there? Concertfix
	specializes in getting you ready for that big tour coming to a city near you. All concert tickets from ConcertFix.com
	come with a <a href="/ticket-guarantee">100% ticket guarentee</a></p>
<br />
<hr />
<h3>Concert Tracker</h3>
<p>Never miss any of your favorite performers concerts again. ConcertFix.com has developed <a href="/user/login">concert
	tracker</a> that will allow you to follow your favorite artists and get alerts everytime your favorite performer has a show,
	or concert near you.</p>
