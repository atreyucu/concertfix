<h1>Concert Tracker - Coming Soon</h1>
<div class="seperator"></div>
<p>The Concertfix.com concert tracker is still under development and 
	should be available soon. We are working to allow you to get alerts 
	when your favorite artists announce concerts in your area and to 
	inform you of any concerts your friends might be going too. Would 
	you like to participate in our beta? We are sending out invites to 
	people that are willing to help us test out concert tracker serction. 
	Beta testers will be entitled to all kinds of rewards including 
	discount codes on tickets, gift cards, honorable mentions and more!
	<br /><br />
	To sign up for our beta please fill out the form below and we will 
	send you a invitation!</p>
<br />
<br />
<div class="span10">
	<div class="span5 signupform" id="signupform" style="margin:auto;">
	<h4>Sign up for Concert Tracker Beta</h4>
		<form class="form-inline">
		    <input type="password" class="input-small" placeholder="Name">
		    <input type="text" class="input-small" placeholder="DOB (mm/dd/yy)">
		    <br /><br />
			<input type="text" class="input-small" placeholder="Email">
		    <input type="password" class="input-small" placeholder="Password">
		    <input type="password" class="input-small" placeholder="Retype Password">
		    <br /><br />
		   <button type="submit" class="btn btn-flatten btn-inverse">Sign up</button>
		</form>
	</div>
</div><div style="clear:both;"></div>