<h1>Ticket Specials</h1>
<p>ConcertFix is always having special ticket promotions including ticket giveaways, coupon codes, and 
	more. Check back here often to see the latest ticket deals offered at ConcertFix.com</p>
<div class="separator"></div>
<br />
<h2>Coupon Codes</h2>
<p>Are you looking for a ConcertFix coupon? Here are a couple of ways to get great coupon codes for ConcertFix.com.</p>
<br /><br />
<div class="span2" style="margin:0px 10px 0px 0px;">
	<a href="https://www.facebook.com/concertfix">
		<img width="100%" src="/public/img/assets/fb-concertfix-coupon.png" alt="Facebook Concertfix Coupon Code" />
	</a>
</div>
<h3>Facebook ConcertFix Coupon Code</h3>
<p>To receive a ConcertFix coupon code through Facebook, visit our Facebook then follow these steps… 
	1) Like our Facebook page 2) Send us a Facebook message asking for a coupon code.  We will reply promptly with 
	your active coupon code.
	<a href="https://www.facebook.com/concertfix">Visit Concertfix.com facebook page</a></p>
<div style="clear:both;"></div>
<br />
<hr />
<div class="span2 pull-right" style="margin:0px 0px 0px 20px;">
	<a href="https://www.twitter.com/concertfix">
		<img width="100%" src="/public/img/assets/twitter-concertfix-coupon.png" alt="twitter Concertfix Coupon Code" />
	</a>
</div>
<h3>Twitter ConcertFix Coupon Code</h3>
<p>To receive a ConcertFix coupon code through Twitter, visit our Twitter then follow these steps… 
	1) Follow us on Twitter 2) Compose a new tweet and mention @concertfix with “#concertfix #coupon”. We will reply promptly with 
	your active coupon code.<br /><br />
	<a href="https://www.twitter.com/concertfix">Visit Concertfix.com on Twitter</a></p>
<div style="clear:both;"></div>
<br />
<hr />
<div class="span3" style="margin:0px 10px 0px 0px;">
	<a href="https://plus.google.com/100760993991371884758/posts/BZSB3qVy29x">
		<img width="100%" src="/public/img/assets/googleplus-concertfix-coupon.png" alt="Google plus Concertfix Coupon Code" />
	</a>
</div>
<h3>Google Plus ConcertFix Coupon Code</h3>
<p>To receive a ConcertFix coupon code through Google+ follow these steps…	1) Plus 1 our Google+ page, 2) Leave 
	a comment on the <a href="https://plus.google.com/100760993991371884758/posts/BZSB3qVy29x" style="color:#0084b4;" target="_blank">"send me a coupon code"</a>  
	post. We will reply promptly with your active coupon code.
	<br /><br />
	<a href="https://plus.google.com/100760993991371884758/posts/BZSB3qVy29x">Visit Concertfix.com Google Plus Coupon Post</a></p>
<div style="clear:both;"></div>