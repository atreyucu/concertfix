<div class="content-inner">
    <h1>ConcertFix - Ticket Guarantee</h1>
    <br>

    <p>At ConcertFix, we know how much your concert means to you. That's why we stand behind you if there happens to be
        any problems with your order. ConcertFix is a resale marketplace where concertgoers like yourself can find the
        tickets you need. Tickets sold at ConcertFix are sold by independent sellers, from fans to professional ticket
        brokers.</p>
    <br>

    <h2>Our 100% Guarantee. See details below.</h2>
    <b>At ConcertFix.com, we offer a 100% refund for your tickets if:</b>
    <br><br>
    <ul>
        <li>Your order was accepted but not delivered by the seller.</li>
        <li>Your order was accepted but not delivered in time for the event.</li>
        <li>Your tickets were not valid for entry. (1)</li>
        <li>Your event is cancelled and is not rescheduled. (2)</li>
    </ul>
    <br>
    <b>Notes:</b>
    <br>
    (1) Verified proof must be provided in letter form from the venue. Written or stamped "voids" do not constitute
    verified proof.
    <br>
    (2) 100% refund for a cancelled event does not include shipping.
    <br>
    <br>

    <p>Our guarantee allows you to purchase tickets with peace of mind knowing we have you covered. Once your order is
        accepted, just sit back and enjoy the show. Questions about our ticket guarantee, refunds, exchanges or order
        status can be directed to customer service at 855-428-3860.</p>
</div>