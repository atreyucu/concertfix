<h1>Contact us</h1>
<div class="separator"></div>
<br />
<p>If you need to get in touch with us in regards to any ticket issues or questions, please contact us 
	using the information located under customer support. If you have a questions about content on the
	site or if you have any suggestions please drop send us a email at <a href="mailto:info@concertfix.com">
	info@concertfix.com</a></p>
<br />
<hr />
<div class="span3" style="margin:8px 10px 20px 0;">
	<img src="/public/img/assets/customer-support.png" />
</div>
<h3>Customer Support for Ticket Sales</h3>
<p>ConcertFix is a great place to get tickets for your favorite events. If you would like to speak with one of
	our customer service representatives you may do so by either calling our toll free number or emailing us.</p>
<ul>
	<li>Phone: (855) 428-3860</li>
	<li>Email: <a href="mailto:customersupport@concertfix.com">customersupport@concertfix.com</a></li>
</ul>
<br />
<hr />
<h3>Head Quarters</h3>
<div style="float:left;border:1px solid #bebebe; overflow:hidden; border-radius:6px;margin-right:12px;">
	<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3586.142204795245!2d-80.14526508497424!3d25.996158683530844!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88d9ab82792a0ce3%3A0x9e2a9ac9fd711b24!2s1001%20N%20Federal%20Hwy%2C%20Hallandale%20Beach%2C%20FL%2033009!5e0!3m2!1sen!2sus!4v1571424000295!5m2!1sen!2sus"></iframe>
</div>
<ul>
	<li>ConcertFix.com</li>
	<li>1001 N Federal Hwy #230</li>
	<li>Hallandale Beach, FL 33009<br/><br/></li>
	<li>Email: <a href="mailto:info@concertfix.com">info@concertfix.com</a></li>
</ul>
<div style="clear:both;"></div>

