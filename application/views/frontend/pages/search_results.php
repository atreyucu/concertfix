<article>
	<div class="article-header">
		<h1 class="title">Search Results:
			<?php // echo ($dates) ? $dates['start']." - ".$dates['end'] : ''; ?>
		</h1>
		<!-- <ul class="article-info"> -->
			<!-- <li><span class="icon awe-home"></span><a href="#"><?php // echo $location['city']; ?> concerts</a></li> -->
			<!-- <li><span class="icon ent-music-2"></span><?php // echo ($category) ? $category[0]->Filter : 'ALL'; ?></li> -->
		<!-- </ul> -->
		<div class="separator"></div>
	</div>
	<div class="article-content">
		<?php if($results): ?>
			<p class="city-content"><?php // echo $cityText; ?></p>
			<div class="table" id="city-events">
				<?php $activeDate = false; ?>
				<?php foreach ($results as $event): ?>

					<?php
					$trueLocation = array('city' => $event->City, 'state' => $event->StateProvince);
					$url = ($event->performers) ? "tours/{$event->performers[0]->PerformerSlug}+".
						slug_venue($event->Venue)."+".
						slug_location($trueLocation) :
						"concerts/".slug_location($trueLocation)."+".slug_venue($event->Venue);
					?>

					<?php if($activeDate != date('Y-m-d', strtotime($event->Date))): ?>

						<?php //$activeDate = date('Y-m-d', strtotime($event->Date)); //set active date ?>

						<?php if ((date('Y', strtotime($event->Date)) - date('Y')) > 2):?>
							<?php $date = date('M j', strtotime($event->Date)).' (PPD)' ?>
						<?php else: ?>
							<?php $date = date('D M j, Y', strtotime($event->Date)) ?>
						<?php endif; ?>
						<div class="date-events">
							<h3><?php echo $date //date('D M j, Y', strtotime($event->Date)); ?> Concerts in <?php echo $event->City; ?></h3>
						</div>

					<?php endif; ?>

					<div class="eventblock">
						<div class="event-thumbnail image-polaroid" style="overflow:hidden;height:70px;">

							<a href="/<?php echo $url; ?>" title="<?php echo ($event->performers) ? $event->performers[0]->PerformerName : ''; ?> <?php echo $event->City; ?> Concert Tickets">
								<img src="<?php echo performerImage($event->performers[0], 'thumb'); ?>" alt="<?php echo ($event->performers) ? $event->performers[0]->PerformerName : ''; ?>">
							</a>

						</div>

						<div class="event-cta">

							<?php $link = event_link($event, $trueLocation); ?>

							<a itemprop="url" <?php echo ($event->performers) ? 'href="'.$link.'"' : 'href="'.$link.'"'; ?> class="btn btn-info" title="<?php echo ($event->performers) ? $event->performers[0]->PerformerName : ''; ?> <?php echo $event->City; ?> Concert Tickets">
								TICKETS <i class="icon ent-ticket"></i>
							</a><br>

							<span>
								<?php echo ($event->prices) ? $event->prices->ticketsAvailable : "Few " ?> tickets <?php echo ($event->prices) ? "left starting from $". $event->prices->lowPrice : " left - check prices!"; ?>
							</span>

						</div>

						<div class="event-details">

							<h4 style="margin-top:4px;">
								<?php if ($event->performers): ?>
									<a itemprop="url" title="<?php echo $event->Name ?>" href="/<?php echo $url; ?>">
										<div itemprop="name"><?php echo $event->Name; ?></div>
									</a>
								<?php else: ?>
									<div itemprop="name"><?php echo $event->Name; ?></div>
								<?php endif ?>
							</h4>

							<p itemscope itemtype="http://schema.org/location"><b>Venue:</b>
								<a itemprop="url" href="/concerts/<?php echo seoUrl($event->City." ".$event->StateProvince)."+".slug_venue($event->Venue); ?>" >
									<span itemprop="location" itemtype="http://schema.org/Place">
										<span itemprop="name"><?php echo clean_venue($event->Venue); ?></span>
									</span>
								</a>
							</p>

							<p><b>Time:</b>
								<?php echo (preg_match('/tba/i',$event->DisplayDate))
									? '<meta itemprop="startDate" content="' . $event->Date . '">To Be Announced'
									: '<meta itemprop="startDate" content="' . $event->Date . '">' .
									date('g:i A', strtotime($event->Date)); ?>
							</p>

							<p><b>Featuring:</b>
								<?php if ($event->performers): ?>
									<?php $i = 0; ?>
									<?php foreach ($event->performers as $p): ?>
										<?php $i++; ?>
										<a href="/tours/<?php echo $p->PerformerSlug ?>" alt="<?php echo $p->PerformerName ?>"><span class=""><?php echo $p->PerformerName ?></span></a><?php echo ($i < count($event->performers)) ? ' | ' : ''; ?>
									<?php endforeach ?>
								<?php else: ?>
									<span class="">Various Artists</span>
								<?php endif ?>
							</p>

						</div>
					</div>
				<?php endforeach ?>
			</div>
		<?php else: ?>
		<h2>Sorry, there are no results for your search</h2>
		<?php endif; ?>


		<div class="separator"></div>
		<?php /*
		*/ ?>
	</div>
</article>