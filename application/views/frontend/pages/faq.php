<div class="content-inner">
    <h1>Help Center & FAQ</h1>
    <br>

    <h2 style="color: red">Coronavirus Update</h2>
    <div>
        <p style="color: red">
            To our valued customers:
            <br><br>
            We are continuing to monitor the development of the COVID-19 pandemic and working hard to minimize its impact on our customers, partners, and employees.
            <br><br>
            Our preparations are focused on the need to ensure the health of our community and the continuity of our business activities. While we all love live events and experiences, we remain mindful of the general health of our community and especially individuals in high-risk groups. This communication is designed to answer your questions about what happens if your live event is canceled or postponed.
            <br><br>
            Our industry-leading guarantee continues to protect your every purchase, and nothing about this situation has or will change that.
            <br><br>
            If your event is canceled, we will notify you as soon as possible. You may have the option of accepting a voucher good for 120% of the value of your original purchase, less applicable delivery fees - valid for one year from the date of acceptance. You will also have the option of accepting a refund of your original purchase price, less applicable delivery fees.
            <br><br>
            If your event is postponed or rescheduled, rest assured that your ticket will be honored on the new date of the event.
            <br><br>
            Our customer support team is ready to answer any questions you may have.
            <br><br>
            Sincerely,
            <br><br>
            Your Customer Care Team
        </p>
    </div>
    <br>

    <div class="accordion">

        <h3><a href="#">Am I eligible for a Credit Voucher?</a></h3>
        <div>
            <p>
                If you purchased tickets to an event that has been cancelled and not rescheduled, you may be eligible for
                a Credit Voucher. If you purchased tickets to an event that still takes place but has been closed to the
                public (no audience games, for example) due to Covid-19, you are also eligible for a Credit Voucher.
            </p>
        </div>


        <h3><a href="#">How much is my Credit Voucher for a cancelled performance worth?</a></h3>
        <div>

            <p>
                We are offering customers a Credit Voucher good for 120 percent of the value of their original order,
                less applicable delivery charges. Example: you paid $100 for tickets to an event that has been cancelled
                due to Covid-19, plus $8.95 shipping and handling. The voucher for this order would be good for $120
                towards a future purchase.
            </p>
        </div>


        <h3><a href="#">How will I receive my Credit Voucher?</a></h3>
        <div>
            <p>
                If your event is cancelled or closed to public (no audience games), we will contact you with details via
                an e-mail. It will include the amount of the Credit Voucher being offered, as well as details on
                accepting the offer.
            </p>

        </div>


        <h3><a href="#">How can I use my Credit Voucher?</a></h3>
        <div>
            <p>
                Your Credit Voucher will be valued at 120% of our original order (less applicable delivery fees). It
                will be valid for 365 days from the date of issue, and can be applied towards future purchases for
                events through the website of your original purchase.
            </p>
        </div>


        <h3><a href="#">What if I want a refund?</a></h3>
        <div>
            <p>
                If you would prefer a refund over a voucher, you will receive a full refund for the original order (less
                applicable delivery charges). You will have the option to choose a refund when contacted regarding the
                cancellation of your event.
            </p>
            <p>
                Refunds will typically take two weeks to process from the date of acceptance.
            </p>
        </div>


        <h3><a href="#">Why will it take 2 weeks?</a></h3>
        <div>
            <p>
                We are dealing with an unprecedented situation caused by a global pandemic, and processing times are
                impacted by the sheer volume of events impacted.
            </p>
        </div>


        <h3><a href="#">My event is postponed, can I get a Credit Voucher instead?</a></h3>
        <div>
            <p>
                Unfortunately, we cannot offer refunds for events that organizers have chosen to reschedule or postpone
                indefinitely until the Covid-19 impact is lessened. Your original tickets will remain valid for the
                rescheduled event. You will be contacted when a new date is set for your event.
            </p>
        </div>


        <h3><a href="#">What happens if I don’t want to or unable go to an event that I have purchased tickets for?</a></h3>
        <div>
            <p>
                If an event is not cancelled, we cannot offer a refund or a Credit Voucher. Consumers can list tickets
                that they do not intend to use for resale at tickettocash.com.
            </p>
        </div>


        <h3><a href="#">What happens if I want to purchase tickets, but they cost more (or less) than my Credit Voucher?</a></h3>
        <div>
            <p>
                You can use the value of your Credit Voucher for separate purchases, as long as any portion of the value
                remains. However, you must use all value remaining on the voucher by the ‘valid through’ date set at
                your acceptance. Any remaining value will be forfeited at the expiration of that 365-day period from the
                date of the offer acceptance (the day your Credit Voucher was issued to you).
            </p>
        </div>


        <h3><a href="#">Can use my Credit Voucher to buy tickets in another country or from another website?</a></h3>
        <div>
            <p>
                The voucher issued will be valid only on the website of your original purchase, and in the original
                currency of your purchase. If the Site where your original purchase was made is no longer in service,
                Your Credit Voucher will still be honored.
            </p>
        </div>


        <h3><a href="#">Can I sell my Credit Voucher?</a></h3>
        <div>
            <p>
                The voucher has no cash value and cannot be transferred to another individual.
            </p>
        </div>


    </div>
    <br>

    <h2>Questions about ConcertFix</h2>
    <br>

    <div class="accordion">

        <h3><a href="#">What is ConcertFix.com?</a></h3>
        <div>
            <p>
                ConcertFix.com is an online marketplace that connects buyers and sellers of live event tickets. Aside from just
                tickets, we’ve created one of the largest databases of concert information including tour announcements,
                discographies, city concert schedules, venue details and more. Our concert database consists of over 4,000
                artists and more than 800 cities across North America.
            </p>
        </div>


        <h3><a href="#">Where do the tickets listed on ConcertFix come from?</a></h3>
        <div>
            <p>
                ConcertFix’s online exchange functions as a massive marketplace for tickets. Sellers include firms, box
                office promoters, licensed ticket sellers, and individuals like you with tickets that they don’t need.
            </p>
            <p>
                Sellers use the ConcertFix online exchange to list their extra inventory. When a customer orders tickets
                on our website, the order is then filled by the ticket seller who listed them. So, while ConcertFix
                manages the online exchange on which the tickets are posted, we do not hold the actual tickets.
            </p>
        </div>


        <h3><a href="#">Is my transaction safe?</a></h3>
        <div>
            <p>
                When you shop at ConcertFix.com, you can be assured that you are conducting a safe transaction.
            </p>
            <p>
                We have worked hard to go above and beyond what is considered standard internet security, to ensure that
                our website and offices are PCI Compliant and McAfee Secure. Any questions you have about your order
                will be handled swiftly and efficiently by our professional customer service team. Additionally, if
                there are any issues with a seller fulfilling your order, we guarantee your purchase 100%.*
            </p>
            <p>
                *Visit our <a href="/ticket-guarantee">ticket guarantee page</a> to read more about our 100% guarantee.
            </p>
        </div>


        <h3><a href="#">How can I contact you?</a></h3>
        <div>
            <p>
                The ConcertFix Customer Service center is open seven days a week, including holidays, from 7 a.m. - 1
                a.m. EST!
            </p>
            <p>
                If you have any questions about an order you would like to make, or an existing order, feel free to
                contact us by phone at 855-428-3860, or by email at <a href="mailto:customersupport@concertfix.com">customersupport@concertfix.com</a>.
            </p>
        </div>


        <h3><a href="#">Have feedback about our website?</a></h3>
        <div>
            <p>
                We’d love to hear it. Email us at <a href="mailto:customersupport@concertfix.com">customersupport@concertfix.com</a>.
            </p>
        </div>


        <h3><a href="#">Do you donate tickets to charity or non-profit organizations?</a></h3>
        <div>
            <p>
                ConcertFix is happy to support organizations that operate to improve our communities! Instead of
                donating tickets to a specific event, we'll donate a ConcertFix.com gift certificate for a flat dollar
                amount, which can be raffled off, auctioned, or otherwise used as a fund-raising incentive for
                non-profit organizations. We support all kinds of organizations ranging from cancer research foundations
                to animal shelters to community soup kitchens, and more.
            </p>
            <p>
                If you run or know of an organization that could benefit from this type of donation, please email us at
                <a href="mailto:customersupport@concertfix.com">customersupport@concertfix.com</a>. Let us know the name
                of your organization, the cause it supports, its website and how the donation will be used. We'll review
                the request and get back to you via email (all submissions are subject to approval and we reserve the
                right to donate an amount of our choosing or to refuse to donate at our discretion).
            </p>
        </div>


    </div>
    <br>

    <h2>Questions about Delivery</h2>
    <br>

    <div class="accordion">
        <h3><a href="#">When will my tickets ship, and how soon will they arrive?</a></h3>
        <div>
            <p>
                Many ticket sellers make their tickets available to you before they have even been printed, which gives
                customers an edge on getting great seats in advance. Because this sometimes happens, tickets may not be “in
                hand” at the time of purchase.
            </p>
            <p>
                Seller notes often indicate when an order will ship (at the latest), so customers know when to expect their
                tickets. They may also note that tickets are “in hand,” which means they can ship immediately. After your
                tickets have shipped, you will receive a FedEx tracking number, so you may track your purchase.
            </p>
            <p>
                Either way, we guarantee your tickets will arrive in time. View our <a href="/ticket-guarantee">100%
                    Guarantee</a> for more details.
            </p>
        </div>


        <h3><a href="#">How are my tickets shipped?</a></h3>
        <div>
            <p>
                Tickets are shipped via FedEx. Shipping via FedEx is secure and safe for both ticket buyers and sellers on our
                website. Shipping through FedEx allows ticket sellers to ensure that the tickets they sell have arrived to the
                proper recipient, and ticket buyers can track the progress of the package.
            </p>
        </div>


        <h3><a href="#">Will a signature be needed for my ticket package?</a></h3>
        <div>
            <p>
                Yes, generally expect the sellers who list tickets on our exchange to require a signature for ticket delivery.
                This is to ensure that the tickets are physically received by someone rather than left out in the open. That
                being said, the decision to require a signature depends on the seller shipping the tickets.
            </p>
            <p>
                Of course, if the signature requirement is problematic, customers can contact their ticket seller directly to
                waive the signature requirement, otherwise you could always…
            <ul>
                <li>Pick up your tickets after-hours at the local facility mentioned by FedEx on the delivery door tag.</li>
                <li>Call FedEx and ask them to keep the tickets at their local facility so you can pick them up during the
                    day.
                </li>
                <li>Call FedEx and ask if they could deliver tickets at a time when it’s more likely someone will be there to
                    receive them.
                </li>
                <li>Leave a note for FedEx delivery personnel to leave the tickets.</li>
            </ul>
            </p>
        </div>


        <h3><a href="#">The event is soon and I need a ticket. What can I do?</a></h3>
        <div>
            <p>
                Many ticket sellers will choose to deliver tickets close to the event by one of three methods: will-call, local
                pickup, or email.
            <ul>
                <li>“Will-call” means that you will pick up the ticket at the venue box office window.</li>
                <li>“Local pickup” will require that you pick up your tickets at a location up to 30 minutes away from the
                    venue.
                </li>
                <li>“Email” means that the seller will email you the tickets before the show.</li>
            </ul>
            Please note that these delivery methods are at the discretion of the seller, so be sure to choose tickets where the
            listing specifically states one of these methods, or contact the seller after buying a ticket to confirm that you
            will be able to get a ticket by will-call, local pick-up, or email. Same-day order tickets are subject to the $15.00
            Near-Term Delivery option.
            </p>
        </div>


        <h3><a href="#">Can I ship to a P.O. Box?</a></h3>
        <div>
            <p>
                Yes, tickets can be shipped to P.O. Boxes. Just un-check the box next to “use my billing address as the shipping
                address” in checkout and enter your P.O. Box information.
            </p>
        </div>


        <h3><a href="#">Can tickets be shipped to somewhere other than the billing address?</a></h3>
        <div>
            <p>
                Yes, customers are allowed to input an alternate shipping address for most orders placed through ConcertFix.
                However, there are certain restrictions that are based on the ticket order total. Additionally, sellers may
                request a signed authorization from you if they desire proof that you accept the alternate shipping address.
            </p>
            <p>
                The alternate shipping policy is as follows:
            <ul>
                <li>If the order is less than $750, the seller is expected to ship the tickets to the alternate shipping
                    address.
                </li>
                <li>If the order is between $750 and $5000, the seller can reject the ticket order due to the address issue or
                    else accept it and ship the tickets to the shipping address.
                </li>
                <li>If the order is $5000 or greater, no alternate shipping address is allowed.</li>
            </ul>
            </p>
        </div>


        <h3><a href="#">Can I change my shipping address after ordering?</a></h3>
        <div>
            <p>
                Customers will need to contact their seller directly with any shipping address changes after an order is placed.
                Sellers are very security-conscious about where they ship tickets and will likely only change your delivery
                information if it is completely necessary.
            </p>
        </div>


        <h3><a href="#">When will I receive my email tickets?</a></h3>
        <div>
            <p>
                Some sellers may list their tickets as being available for email delivery. However, this does not mean that your
                tickets will be emailed immediately. You will be notified by email when your tickets are available for
                downloading and printing.
            </p>
        </div>


        <h3><a href="#">Will I receive my “Instant” tickets as soon as I place my ticket order?</a></h3>
        <div>
            <p>
                In most cases, e-tickets marked as “Instant” will be available for you to download and print within minutes of
                placing your order. However, in order to protect against fraudulent purchases, some orders may require
                additional processing time. If this applies to your order, it will be noted in your order confirmation email,
                and your e-tickets will usually be available for you to download within one (1) business day.
            </p>
        </div>


        <h3><a href="#">What if something happens with my order? Is there a purchase guarantee?</a></h3>
        <div>
            <p>
                We want to make sure that ticket sellers provide customers with the tickets they were promised. In order to
                protect you, ConcertFix offers the following money back guarantees:
                At ConcertFix.com, we offer a 100% Money Back Guarantee, if:
            <ul>
                <li>Your order is accepted by, but not delivered by the seller.</li>
                <li>Your order is accepted by, but shipped too late by the seller for the tickets to arrive in time for the
                    event
                </li>
                <li>You were denied entry because of the tickets or invalid tickets were provided by the seller.(1)</li>
                <li>Your event is cancelled entirely with no rescheduled date.(2)</li>
            </ul>
            Notes:
            (1) Verifiable proof must be provided by the venue in written letter format. Written or stamped "voids" do not constitute verifiable proof.
            (2) 100% refund for a cancelled event excludes shipping.
            </p>
        </div>


        <h3><a href="#">Why was my order rejected?</a></h3>
        <div>
            <p>
                Due to the nature of our exchange, there is usually some lag time between when tickets are purchased and when
                the ticket listings are updated. As tickets are sold, sellers must manually update their listings. This means
                that your tickets may have been ordered by another customer before you had the chance to submit yours.
            </p>
            <p>
                This is a rare occurrence, but if it should happen to you, please contact us at 855-428-3860 and our customer
                service representatives would be happy to help you find a similar seat or a seat at a similar price.
            </p>
        </div>


        <h3><a href="#">Can I combine shipping?</a></h3>
        <div>
            <p>
                Similar to other large online shopping entities (such as Amazon), the inventory in our exchange comes from many
                different sellers. Therefore, we cannot guarantee combined shipping.
            </p>
            <p>
                However, in the case that some or all of your tickets are from the same seller, you may contact them after your
                order is confirmed to request that the shipping be combined. This service is at the discretion of the seller.
            </p>
        </div>


        <h3><a href="#">Can I purchase tickets as a gift for someone else?</a></h3>
        <div>
            <p>
                Yes! Event tickets make great gifts. Even though tickets may not ship immediately after you purchase them, we
                guarantee they'll arrive in time for the event.
            </p>
        </div>
    </div>
    <br>

    <h2>Questions about Ticket Listings</h2>
    <br>

    <div class="accordion">
        <h3><a href="#">Will my seats be together?</a></h3>
        <div>
            <p>
                Tickets are guaranteed to be together, unless the seller’s notes say otherwise. If tickets are listed in a
                general category, zone, section, or row, we guarantee that they will be together.
            </p>
            <p>
                Examples of these sorts of notes are: “Zone A,” “Section 200,” “Row 102,” etc.
                However, sometimes the notes say something like “Section 2, rows A-Z” or “Section 2, Rows A and B, piggybacked”
                which lets you know that the seats may not be together. If they are “piggybacked,” it means one seat is in front
                of the other, but in 2 separate rows.
            </p>
        </div>


        <h3><a href="#">How do I find the tickets I'm looking for?</a></h3>
        <div>
            <p>
                You may use our simple search bar at the top of the page to search by Artist, Venue, City or State. When you
                have found the event you want, click the “Tickets” button to see what groups of tickets are available. From a
                desktop computer, selections of tickets with quantities and price per ticket will be shown in a table on the
                left-hand side of the page. On the right will be the venue map. From a mobile device, selections of tickets with
                quantities and price per ticket will be shown in a table below a venue map. Some of our maps are simply images,
                some are interactive, and some even have pictures from the seats to the stage, all to help you find what you’re
                looking for.
            </p>
            <p>
                When you choose your tickets, click the “Go to Secure Checkout” button, and proceed to checkout. If you ever
                have any questions about finding or buying tickets on our website, you may contact us at any time by phone at
                855-428-3860 or by email at <a href="mailto:customersupport@concertfix.com">customersupport@concertfix.com</a>.
            </p>
        </div>


        <h3><a href="#">Why can't I purchase a certain quantity of tickets?</a></h3>
        <div>
            <p>
                Sellers prefer to list tickets at least in pairs so as to increase the chance that they’ll be able to sell all
                the tickets in a listing. Entertainment events are social occasions that people usually attend with friends or
                family. Single tickets are very hard to sell and so sellers try to avoid being stuck with them by buying and
                selling tickets in larger quantities.
            </p>
        </div>


        <h3><a href="#">Why aren’t seat numbers listed for the tickets?</a></h3>
        <div>
            <p>
                Ticket sellers don’t list seat numbers to prevent double-booking tickets. Because transactions on ConcertFix.com
                take place in real time, it is possible for a set of tickets to be purchased at the exact same time by two
                different individuals. In order to prevent this confusion, ticket sellers instead list general rows and
                sections. They often have several groups of tickets, so if multiple orders come in, they can successfully fill
                them (without anyone getting upset or disappointed!).
            </p>
        </div>


        <h3><a href="#">An event I want to attend doesn't have any available tickets. What can I do?</a></h3>
        <div>
            <p>
                When searching for an event that does not have any tickets available, you can sign up for our <a
                    href='/user/login'>Concert Tracker</a> to receive email alerts when your favorite artists are coming to a
                city near you. With our Concert Tracker, you can:
            <ul>
                <li>Follow your favorite performers and cities.</li>
                <li>Receive email alerts when new shows are announced in your city.</li>
                <li>Create your own custom concert schedules updated instantly.</li>
                <li>Never miss a show again.</li>
            </ul>
            </p>
        </div>


        <h3><a href="#">How do I use the interactive maps?</a></h3>
        <div>
            <p>
                Interactive maps can be incredibly useful tools for finding the tickets you want. We want to make the interactive maps easy to understand and use. Here are a few tips:
                Click on a shaded section to get a pop-up bubble showing how many tickets are listed in the section and a range
                of prices. Clicking a section will also filter the ticket list on the left side so that only tickets in that
                section are showing. You can click on more than one section to add more tickets to the list. Click a section
                again remove its tickets from the list.
            </p>
            <p>
                Use the plus and minus (+, -) buttons, or your mouse's scroll wheel, to zoom the map in to see sections in
                greater detail. Use the directional buttons or drag the map to reveal hidden sections
                Some interactive maps include a feature that gives you an idea of the view from each section. If this feature is
                enabled, a photo of the approximate view will display next to the map when you click a section. To see the view
                from a different section, simply click on the section on the map.
                To highlight a section on the map, move the mouse over the entries in the ticket list.
                If an event has view-from-seat photos, simply click the ticket list entry to see the image.
            </p>
        </div>


        <h3><a href="#">What is Zone Seating?</a></h3>
        <div>
            <p>
                Zone seating is a recent introduction in the secondary ticket market that borrows entertainment trends from
                Europe. The seating itself involves venues being geographically demarcated into various chunks according to some
                visual model. The chunks concerned are often dubbed “Zones” and randomly drawn and marked with varying
                creativity. (Zones could be labeled as A, B, C or GOLD, SILVER, BRONZE or any such system of categorization.)
                Event-goers are then given options to purchase seats based solely on their Zone location. (Some sellers will
                choose to specify seating location in greater detail in their ticket notes.)
            </p>
        </div>
    </div>
    <br>

    <h2>Questions about Tickets</h2>
    <br>

    <div class="accordion">
        <h3><a href="#">Why is there someone else’s name on my ticket?</a></h3>
        <div>
            <p>
                The name on a customer’s ticket will be the name of the original purchaser. Therefore, your name will not be on
                the ticket purchased through ConcertFix. However, please note that the name on the tickets will not affect your
                ability to access the event. The most important aspect is that the bar code on the tickets scans when entering
                the event.
            </p>
        </div>


        <h3><a href="#">What happens if my tickets are lost or stolen?</a></h3>
        <div>
            <p>
                Tickets are generally one-of-a-kind, irreplaceable items that can seldom be re-accessed or re-printed. The
                purpose of such restrictions is to prevent ticketing fraud via multiple printings of the same ticket.
                Regardless, customers should contact their seller to see if it is possible to re-access lost tickets.
            </p>
        </div>


        <h3><a href="#">What is an E-Ticket?</a></h3>
        <div>
            <p>
                Electronic tickets, or e-tickets, are delivered to you electronically through <a
                    href="http://www.mytickettracker.com" target="_blank">My Ticket Tracker</a>. You must download and print
                your e-tickets, which will have a barcode and will be valid for entry into the event.
            </p>
            <p>
                Some things to know about your e-tickets:
            <ul>
                <li>When your e-tickets are ready to download, you will receive an email with instructions for accessing your
                    e-tickets through our secure website, MyTicketTracker.
                </li>
                <li>For your protection, MyTicketTracker requires you to log in with order-specific details, including a unique
                    order PIN that will be included in your download instruction email.
                </li>
                <li>Some e-tickets may not be available for download immediately, but are guaranteed to be read in time for your
                    event.
                </li>
            </ul>
            </p>
        </div>


        <h3><a href="#">What is My Ticket Tracker, and how do I use it?</a></h3>
        <div>
            <p>
                MyTicketTracker is a secure platform where you can view details about your ticket orders and download any
                e-tickets you may have purchased.
                To download e-tickets from MyTicketTracker:
            <ul>
                <li>Go to <a href="http://www.mytickettracker.com" target="_blank">MyTicketTracker.com</a>, and enter your
                    email address and unique order PIN to log in. You'll find your PIN in the order receipt you received via
                    email.
                </li>
                <li>After logging in, you'll see purchase details for your order, along with a link to download your
                    tickets. This link is located next to the "Delivery Method" section of your purchase details and will
                    say "Download Tickets".
                </li>
                <li>For security purposes, you then will be asked to confirm that you agree to the MyTicketTracker Terms and
                    Conditions. To proceed with your download, select "Yes" to confirm your agreement and click "Continue".
                </li>
                <li>You can now download your e-tickets! You'll see the message "Click here to view and print your tickets."
                    Just click the words "Click here," which will be a blue link.
                </li>
                <li>You'll see a pop-up window that allows you to either open or save the e-ticket file. You should choose to
                    open the file. Adobe Acrobat Reader will automatically open with the e-tickets. Within Adobe Acrobat Reader,
                    there will be one page per ticket. (i.e. If you bought four tickets and downloaded one file, the Adobe file
                    will be four pages long. If you bought four tickets and downloaded four Adobe files, there will be one page
                    for each file).
                </li>
                <li>You should then print your tickets and take them to the event.</li>
            </ul>
            </p>
            <p>
                If the tickets are downloaded and the file is blank, you will need to update your version of Adobe Acrobat
                Reader. Adobe Acrobat Reader is a free and widely used program for opening the type of document used to deliver
                the tickets. <a href="http://get.adobe.com/reader" target="_blank">Click here</a> to download the latest
                version.
            </p>
        </div>


        <h3><a href="#">What is a Paperless Ticket?</a></h3>
        <div>
            <p>
                Paperless tickets aren’t like traditional paper tickets, and they don’t involve having a physical ticket in
                hand. Instead, these tickets require the ticket seller to accompany you to the venue’s box office. Our ticket
                listings will indicate which tickets are paperless with notes such as “I will be attending the event, and will
                accompany you to the box office at the venue”.
            </p>
        </div>


        <h3><a href="#">What is a Flash Ticket?</a></h3>
        <div>
            <p>
                Flash tickets are a new innovation used for ticket distribution by event promoters. Customers with “flash”
                tickets access their tickets by swiping a credit card (the one used for the original ticket purchase) at a venue
                kiosk. This kiosk will then print out a ticket receipt that the event goer can present to venue personnel as a
                means to gain entry to the event.
            </p>
            <p>
                Ticket sellers usually arrange flash ticketing by mailing customers a gift card that was used by the seller
                initially to purchase the tickets. This card would contain the initial purchase information for the order and
                would generate a viable receipt. (Customer should contact their seller directly with any additional questions
                about flash ticketing.)
            </p>
        </div>

    </div>
    <br>

    <h2>Questions about Prices and Fees</h2>
    <br>

    <div class="accordion">
        <h3><a href="#">Why is the price on my tickets different than what I paid?</a></h3>
        <div>
            <p>
                Similar to Amazon, ConcertFix is a massive online exchange, where ticket sellers can list their inventory.
                Because of this, we do not charge your credit card, own, price, or ship the tickets listed on our website. All
                of those parts of your transaction are handled by the individual sellers listing their tickets on our exchange.
                ConcertFix’s purpose is simply to connect ticket buyers with <em>a lot</em> of ticket options.
            </p>
            <p>
                Generally speaking, sellers decide to resell their tickets at a price that reflects the market, and prices often
                rise and fall with supply and demand. The result is that tickets on our website are often sold either above or
                below face value.
            </p>
            <p>
                In addition to the market value fluctuating, sellers often have a large number of expenses to cover in order to
                get their hands on great seats. Sellers often must pay face value, plus fees (or membership fees to special
                clubs), and sometimes they stand in line for hours to get the best seats available. By marking up the tickets,
                ticket sellers make a small profit. The benefit for ticket buyers is they don’t have to wait in line, on their
                computers, or on the phone for hours months in advance of an event to get a good seat. Instead, customers can
                find great seats even a couple weeks before from an event.
            </p>
        </div>


        <h3><a href="#">Do I have to pay sales tax?</a></h3>
        <div>
            <p>
                Taxes on tickets actually vary substantially from state-to-state and even between localities in various states -
                an ever-changing situation. This means that local and/or state taxes are not calculated on our checkout page
                during the purchase process. (Ticketing taxes are based on the location of the ticket seller supplying an order
                and most prevalent in Texas, Chicago, California, Michigan and Canada.
            </p>
        </div>


        <h3><a href="#">Why was I charged immediately for my ticket purchase?</a></h3>
        <div>
            <p>
                The sellers who list tickets with us usually charge customers automatically when an order is placed so as to
                reserve your place in line for tickets (since sellers often receive numerous orders simultaneously). On that
                note, ticketing is a dynamic business (prices rising and falling all the time) so it’s important to claim your
                ticket early at the price set at the time of the purchase.
            </p>
        </div>
    </div>
    <br>

    <h2>Questions about Other Issues</h2>
    <br>

    <div class="accordion">
        <h3><a href="#">What if the event is cancelled?</a></h3>
        <div>
            <p>
                If an event gets permanently cancelled, you are eligible for a 100% refund.
                Please note that the refund constitutes the price you paid for the tickets, and does <em>not</em> include
                shipping costs that you may have incurred as part of the purchase process. For more details, please refer to the
                <a href="/policies">ConcertFix Terms and Policies</a> for the purchase agreement.
            </p>
        </div>


        <h3><a href="#">Can I cancel an order after it is placed?</a></h3>
        <div>
            <p>
                As a general rule, all sales on ConcertFix.com are final. Please be absolutely sure you want to purchase the
                tickets before ordering.
            </p>
        </div>


        <h3><a href="#">How can I sell extra tickets that I own through your website?</a></h3>
        <div>
            <p>
                Please visit <a href="https://ticketnetwork.tickettocash.com/">our ticket selling page</a> to sign-up for
                an account and start selling your tickets. You can find additional assistance here: <a
                    href="https://ticketnetwork.tickettocash.com/">Learn more about selling your tickets.</a>.
            </p>
        </div>
    </div>
    <br>

    <h2>Questions from International Customers</h2>
    <br>

    <div class="accordion">
        <h3><a href="#">How do I access US event tickets if I’m an international customer?</a></h3>
        <div>
            <p>
                There are three main ways to access event tickets for US-based events if you’re an international  customer:
            <ul>
                <li>Order tickets and input a US-based shipping address: You should use this option if the event is more
                    near-term in nature and you could potentially have the tickets shipped to a friend’s house in the US or even
                    a hotel.
                </li>
                <li>Order tickets marked as being available for will call, email, local pickup (etc): Tickets are only available
                    using one of the above methods if that method is specifically advertised on the listing notes and/or during
                    checkout.
                </li>
                <li>Order tickets and input your home address for shipment: You should use this option if the event is a while
                    away and thus likely to ship before your travels. Please note that you can always contact your ticket
                    supplier directly to provide an alternate (US-based) address (if needed).
                </li>
            </ul>
            </p>
        </div>


        <h3><a href="#">Are ticket orders charged in US dollars?</a></h3>
        <div>
            <p>
                Ticket orders placed through ConcertFix are charged in US dollars. Please note that exchange charges (if
                applicable) for non-US customers may apply to ticket orders placed through ConcertFix.
            </p>
        </div>


        <h3><a href="#">Why is there an international shipping fee for my order?</a></h3>
        <div>
            <p>
                Delivery fees for ConcertFix customers are based on the location of the ticket seller versus the location of the
                end customer (the event location is irrelevant). Canadian customers with Canadian events through US sellers will
                thus have International Express fees. US customers with US events through Canadian sellers will also have
                International Express fees. An international delivery fee therefore means that the seller supplying your order
                is not based in your home country and thus must ship internationally.
            </p>
        </div>


        <h3><a href="#">I'm not from the U.S. Can I sell tickets through your website?</a></h3>
        <div>
            <p>
                Unfortunately, we currently only allow U.S. based sellers to sell tickets on ConcertFix.
            </p>
        </div>
    </div>

</div>