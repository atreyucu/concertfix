<div class="four-o-four container">
    <div class="w50p">
      <h1>Page not found</h1>
      <div class="seperator"></div>
      <p>This is embarrassing but we can't seem to find the page you are looking for.
        You have only a few options from here. You can either try searching for
        what you were looking for but typing in your search above, or you can try
        naviagting to the right page by using the these links listed below. The
        choice is yours. Good Luck!</p><br />

    </div>
    <div class="w40p marLeft30 marTop50">
      <p>
        <ul>
          <li><a href="/">Start from the Home Page</a></li>
          <li><a href="/concerts/all-cities">Search Concerts by City</a></li>
          <li><a href="/tours/all-artists">Search for a Specific Artist</a></li>
          <li><a href="/top-tours">Check out the Charts</a></li>
          <li><a href="/tour-announcements">New Tour Announcements</a></li>
        </ul>
      </p>
    </div>
</div>
