<h1>Top Tours</h1>
<p>Check out the most popular concert performers in their genre 
	and find out where they stack up against other great concert 
	tours. See the top concert tours for each genre including 
	country, rap, pop, rock, electronic, and latin.</p> 
<div class="separator"></div>
<div class="team row-fluid">
	<div class="member span4">
		<div class="member-thumbnail">
			<a href="#">
				<img src="/public/img/assets/country.jpg" alt="avatar">
			</a>
		</div>
		<ul class="member-menu">
			<li class="path-wrapper">
				<span class="overlay"></span>
				<div class="member-data">
					<h3>Country</h3>
					<div class="position">Top Country Tours</div>
				</div>
				<ul class="path">
					<li class="email-menu menu top-right"><a href="/concerts/genre+country-folk" title="View Top Country Tours"><span class="ent-graph"></span></a></li>
					<li class="home-menu menu right"><a href="/tour-announcements" title="View The Latest Tour Announcements"><span class="ent-info"></span></a></li>
					<li class="social-menu menu bottom-right"><a href="/user/login" title="Get Updates on Country Tour Announcements"><span class="ent-add-to-list"></span></a></li>
				</ul>
			</li>
		</ul>
	</div>
	<div class="member span4">
		<div class="member-thumbnail">
			<a href="#">
				<img src="/public/img/assets/rap.jpg" alt="avatar">
			</a>
		</div>
		<ul class="member-menu">
			<li class="path-wrapper">
				<span class="overlay"></span>
				<div class="member-data">
					<h3>Rap/Hip-Hop</h3>
					<div class="position">Top Rap/Hip-Hop Tours</div>
				</div>
				<ul class="path">
					<li class="email-menu menu top-right"><a href="/concerts/genre+rap-hip-hop" title="View Top Rap/Hip-Hop Tours"><span class="ent-graph"></span></a></li>
					<li class="home-menu menu right"><a href="/tour-announcements" title="View The Latest Tour Announcements"><span class="ent-info"></span></a></li>
					<li class="social-menu menu bottom-right"><a href="/user/login" title="Get Updates on Rap/Hip-Hop Tour Announcements"><span class="ent-add-to-list"></span></a></li>
				</ul>
			</li>
		</ul>
	</div>
	<div class="member span4">
		<div class="member-thumbnail">
			<a href="#">
				<img src="/public/img/assets/pop.jpg" alt="avatar">
			</a>
		</div>
		<ul class="member-menu">
			<li class="path-wrapper">
				<span class="overlay"></span>
				<div class="member-data">
					<h3>Pop</h3>
					<div class="position">Top Pop Tours</div>
				</div>
				<ul class="path">
					<li class="email-menu menu top-right"><a href="/concerts/genre+pop-rock" title="View Top Pop Tours"><span class="ent-graph"></span></a></li>
					<li class="home-menu menu right"><a href="/tour-announcements" title="View The Latest Tour Announcements"><span class="ent-info"></span></a></li>
					<li class="social-menu menu bottom-right"><a href="/user/login" title="Get Updates on Pop Tour Announcements"><span class="ent-add-to-list"></span></a></li>
				</ul>
			</li>
		</ul>
	</div>
	<div class="member span4">
		<div class="member-thumbnail">
			<a href="#">
				<img src="/public/img/assets/rock.jpg" alt="avatar">
			</a>
		</div>
		<ul class="member-menu">
			<li class="path-wrapper">
				<span class="overlay"></span>
				<div class="member-data">
					<h3>Rock</h3>
					<div class="position">Top Rock Tours</div>
				</div>
				<ul class="path">
					<li class="email-menu menu top-right"><a href="/concerts/genre+pop-rock" title="View Top Rock Tours"><span class="ent-graph"></span></a></li>
					<li class="home-menu menu right"><a href="/tour-announcements" title="View The Latest Tour Announcements"><span class="ent-info"></span></a></li>
					<li class="social-menu menu bottom-right"><a href="/user/login" title="Get Updates on Rock Tour Announcements"><span class="ent-add-to-list"></span></a></li>
				</ul>
			</li>
		</ul>
	</div>
	<div class="member span4">
		<div class="member-thumbnail">
			<a href="#">
				<img src="/public/img/assets/electronic.jpg" alt="avatar">
			</a>
		</div>
		<ul class="member-menu">
			<li class="path-wrapper">
				<span class="overlay"></span>
				<div class="member-data">
					<h3>Electronic</h3>
					<div class="position">Top Electronic Tours</div>
				</div>
				<ul class="path">
					<li class="email-menu menu top-right"><a href="/concerts/genre+techno-electronic" title="View Top Electronic Tours"><span class="ent-graph"></span></a></li>
					<li class="home-menu menu right"><a href="/tour-announcements" title="View The Latest Tour Announcements"><span class="ent-info"></span></a></li>
					<li class="social-menu menu bottom-right"><a href="/user/login" title="Get Updates on Electronic Tour Announcements"><span class="ent-add-to-list"></span></a></li>
				</ul>
			</li>
		</ul>
	</div>
	<div class="member span4">
		<div class="member-thumbnail">
			<a href="#">
				<img src="/public/img/assets/latin.jpg" alt="avatar">
			</a>
		</div>
		<ul class="member-menu">
			<li class="path-wrapper">
				<span class="overlay"></span>
				<div class="member-data">
					<h3>Latin</h3>
					<div class="position">Top Latin Tours</div>
				</div>
				<ul class="path">
					<li class="email-menu menu top-right"><a href="/concerts/genre+latin" title="View Top Latin Tours"><span class="ent-graph"></span></a></li>
					<li class="home-menu menu right"><a href="/tour-announcements" title="View The Latest Tour Announcements"><span class="ent-info"></span></a></li>
					<li class="social-menu menu bottom-right"><a href="/user/login" title="Get Updates on Latin Tour Announcements"><span class="ent-add-to-list"></span></a></li>
				</ul>
			</li>
		</ul>
	</div>
</div>										