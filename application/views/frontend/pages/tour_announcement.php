<div class="post-lists grid" id="post">
	<div class="article-header">
		<h1 class="title">Tour and Concert Announcements</h1>
		<p>Want to know Whos on Tour right now? ConcertFix will bring you up to the second news on tour announcements, tour guests, concert schedules, locations and venues. For all the latest information for whos on tour in your area, visit us frequently and follow <a href="https://twitter.com/Concertfix" target="_blank">@ConcertFix</a> for more tour announcements.</p>
		<div class="separator"></div><br />
	</div>
	<?php foreach ($posts as $post): ?>
		<article class="latest-article grid-item">
			<div class="article-info">
				<div class="time">
					<span><?php echo date('F', strtotime($post->announced_on)) ?> <?php echo date('j', strtotime($post->announced_on)) ?>, <?php echo date('Y', strtotime($post->announced_on)) ?>
					</span>
				</div>
			</div>
			<div class="article-thumbnail">
				<a href="/tours/<?php echo $post->PerformerSlug ?>"
				   title="<?php echo ($post->title) ? $post->title : $post->PerformerName; ?>">
					<img src="<?php echo $post->img ?>" alt="<?php echo $post->PerformerName ?> live">
				</a>
			</div>
			<div class="article-content">
				<div class="article-header">
					<h3><a style="font-size: 20px!important;" href="/tours/<?php echo $post->PerformerSlug ?>" title="<?php echo ($post->title) ? $post->title : $post->PerformerName; ?>"><?php echo ($post->title) ? $post->title : $post->PerformerName; ?></a></h3>
				</div>
				<div class="article-excerpt">
					<p>
						<?php echo shortenText($post->text, 225); ?>
						<a href="/tours/<?php echo $post->PerformerSlug ?>" title="read more">continue reading →</a>
					</p>
				</div>
			</div>
		</article>
		<!--<div class="separator"></div>-->
	<?php endforeach ?>
</div>
<div class="separator"></div>
<span style="float: left">Photos: Last.fm</span>
<div class="pagination center" style="margin: 38px 0 !important;">
   <?php echo $pagination;	?>
</div>
