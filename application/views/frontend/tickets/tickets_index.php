<!--<script type="text/javascript">
  bid = 3282; //the customer id
  site = 14; //the site number\
  wbi = 14132 // Web Config ID
  event_id = <?php /*echo $event[0]->ID ?: -1 */?>;
</script>-->

<?php if ($noTickets): ?>
    <p>
      There is currently no online inventory available for this event. <br> <br>
      Please contact <span class="tn_results_notfound_name">  ConcertFix.com</span> at
      <span class="tn_results_notfound_phone">  (855) 428-3860</span> or
      <a href="mailto:customersupport@concertfix.com" class="tn_results_notfound_email">
        customersupport@concertfix.com
      </a>
      to check availability for this event.
    </p>

<?php endif?>

<script type="application/javascript" language='javascript' src='https://mapwidget2.seatics.com/js?eventId=<?php echo $event[0]->ID ?: -1 ?>&websiteConfigId=14132'></script>

<script type="text/javascript">

  Seatics.config.ticketListOnRight = false;
  Seatics.config.c3CheckoutDomain = "checkout.concertfix.com";
  Seatics.config.c3CurrencyCode = "USD";
  Seatics.config.useC3 = true;

  Seatics.config.smallScreenMapLayout = Seatics.SmallScreenMapOptions.HalfShown;

  Seatics.config.mapFinishedRenderingHandler = function(){

    jQuery('#price-filter-min').focus(function(){
      jQuery('.header-container').css({"position": "fixed"})
    })

    jQuery('#price-filter-max').focus(function(){
      jQuery('.header-container').css({"position": "fixed"})
    })

  };


</script>

<?php

  //$hiddenUrl = "<script language='javascript' src='http://tickettransaction.com/?bid=3282&sitenumber=14&tid=ticket_results2&vfs=true&version=103Test&evtid=".$event[0]->ID."'></script>";

  //echo($hiddenUrl);

?>
