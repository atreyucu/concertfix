
<!--<script type="application/javascript">
bid = 3282; //the customer id
site = 14; //the site number
</script>
<script type="application/javascript" language='javascript' src='http://tickettransaction.com/?bid=3282&sitenumber=14&tid=ticket_results&evtid=<?php /*echo $event[0]->ID;*/?>'></script>
-->

<?php if ($noTickets): ?>
	<p>
		There is currently no online inventory available for this event. <br> <br>
		Please contact <span class="tn_results_notfound_name">  ConcertFix.com</span> at
		<span class="tn_results_notfound_phone">  (855) 428-3860</span> or
		<a href="mailto:customersupport@concertfix.com" class="tn_results_notfound_email">
			customersupport@concertfix.com
		</a>
		to check availability for this event.
	</p>

<?php endif?>

<script type="application/javascript" language='javascript' src='https://mapwidget2.seatics.com/js?eventId=<?php echo $event[0]->ID?>&websiteConfigId=14132'></script>

<script type="text/javascript">

	Seatics.config.c3CheckoutDomain = "checkout.concertfix.com";
	Seatics.config.c3CurrencyCode = "USD";
	Seatics.config.useC3 = true;

	Seatics.config.smallScreenMapLayout = Seatics.SmallScreenMapOptions.HalfShown;

	Seatics.config.mapFinishedRenderingHandler = function(){

		jQuery('#price-filter-min').focus(function(){
			jQuery('.header-container').css({"position": "fixed"})
		})

		jQuery('#price-filter-max').focus(function(){
			jQuery('.header-container').css({"position": "fixed"})
		})

		//========================== SideBar Menu Children Slide In ============================//

		$('.menu-widget ul li a, #responsive-nav ul li a').click( function (e){
			$(this).find('span.icon').toggleClass(function() {
				if ($(this).is('.awe-search')) {
					$(this).removeClass('awe-search');
					return 'awe-chevron-up';
				} else {
					$(this).removeClass('awe-chevron-up');
					return 'awe-search';
				}
			});
			$(this).siblings('ul').slideToggle();
		});

		$('#responsive-nav .collapse-menu .collapse-trigger').click( function (e) {
			$(this).toggleClass(function() {
				if ($(this).hasClass('.awe-chevron-up')) {
					$(this).removeClass('awe-chevron-up');
					return 'awe-search';
				} else {
					$(this).removeClass('awe-search');
					return 'awe-chevron-up';
				}
			});
			$(this).parent().siblings('ul').slideToggle();
		});

		//========================== End  ============================//

		var previousScroll = 0;

		jQuery(window).scroll(function(){
			var currentScroll = jQuery(this).scrollTop();
			if (currentScroll > previousScroll){
				jQuery('.header-container').removeClass('up-tix')
				jQuery('.header-container').addClass('down-tix')
				//console.log('down '+currentScroll);
			} else {
				if (currentScroll == 0){
					jQuery('.header-container').removeClass('down-tix')
					jQuery('.header-container').addClass('up-tix')
				}
				//console.log('up '+currentScroll);
			}
			previousScroll = currentScroll;

		});

		jQuery('.seatics-section-has-tickets').click(function(){

			jQuery('#venue-ticket-list').css('height', function(){
				var height = jQuery('#tickets-table').css('height');
				var temp = height.split('px')
				temp = parseInt(temp[0])+240;
				height = temp + 'px';
				return height;
			})
		});

		/*jQuery('.event-info-name').css({"text-align": "center"});*/
		jQuery('.event-info-date-loc').css({"text-align": "center"});

		<?php if ($ios) : ?>
			jQuery('div#sea-mobile-header > div#event-info-cnt > div.event-info-col > h1.event-info-name').css({'text-overflow':'initial'})
		<?php endif;?>
	};

</script>
