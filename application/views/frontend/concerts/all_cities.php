<style>
    .trip-list ul > li {line-height: 30px!important;}
</style>

<h1>All Cities</h1>

<br/>
<div class="separator"></div>
<br/>

<h2>Concerts in United States</h2>
<?php $listOfStates = array(); ?>
<?php foreach ($usCities as $city): ?>
    <?php $listOfStates[$city->StateProvince] = $city->StateProvince; ?>
<?php endforeach; ?>
<?php $i = 0; ?>
<?php foreach ($listOfStates as $state): ?>
    <a href="#<?php echo $state ?>" style="padding: 2px!important; line-height: 20px!important; font-size: 14px;"><?php echo $state; ?></a>
    <?php $i++;
    echo ($i < count($listOfStates)) ? " | " : ''; ?>
<?php endforeach ?>



<?php $currentState = ''; ?>
<div class="trip-list">
    <ul>
        <?php foreach ($usCities as $city): ?>
            <?php if ($currentState != $city->StateProvince): ?>
                    </ul>
                </div>

                <a name="<?php echo $city->StateProvince; ?>"></a>
                <h3><?php echo toggle_state($city->StateProvince) ?></h3>

                <div class="trip-list">
                    <ul>
                        <?php $currentState = $city->StateProvince; ?>
            <?php endif; ?>

            <li>
                <a href="/concerts/<?php echo seoUrl($city->slug); ?>"
                   title="<?php echo $city->City ?> Concert Schedule">
                    <span><?php echo $city->City ?></span>
                </a>
            </li>

        <?php endforeach; ?>
    </ul>
</div>
<br/>
<div class="separator"></div>
<br/>











<h2>Concerts in Canada</h2>
<?php $listOfStates = array(); ?>
<?php foreach ($canCities as $city): ?>
    <?php $listOfStates[$city->StateProvince] = $city->StateProvince; ?>
<?php endforeach; ?>
<?php $i = 0; ?>
<?php foreach ($listOfStates as $state): ?>
    <a href="#<?php echo $state ?>" style="padding: 2px!important; line-height: 20px!important; font-size: 14px;"><?php echo $state; ?></a>
    <?php $i++;
    echo ($i < count($listOfStates)) ? " | " : ''; ?>
<?php endforeach ?>
<?php $currentState = ''; ?>
<div class="trip-list">
    <ul>
        <?php foreach ($canCities as $city): ?>
        <?php if ($currentState != $city->StateProvince): ?>
    </ul>
</div>
<a name="<?php echo $city->StateProvince; ?>"></a>
<h3><?php echo toggle_state($city->StateProvince) ?></h3>
<div class="trip-list">
    <ul class="">
        <?php $currentState = $city->StateProvince; ?>
        <?php endif; ?>
        <li><a href="/concerts/<?php echo seoUrl($city->slug); ?>"
               title="<?php echo $city->City ?> Concert Schedule"><?php echo $city->City ?></a></li>

        <?php endforeach; ?>
    </ul>
</div>