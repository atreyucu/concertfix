<article>
	<div class="article-header">
		<h1 class="title">Concert Venues in <?php echo $location['city'] ?></h1>
		<div class="separator"></div>
	</div>
	<div class="article-content">
		<div class="table" id="city-venues">
			<?php foreach ($venues as $venue): ?>

				<div class="eventblock">

					<div class="event-thumbnail image-polaroid">
						<a href="/concerts/<?php echo
						$venue->RegionSlug."+".
						$venue->VenueSlug; ?>"
						title="<?php echo $venue->Name; ?> Concert Schedule">
							<img src="<?php echo ($venue->SeatChart) ? $venues_data[$venue->Name] : '/public/img/assets/placeholder/seatingchart.jpg'; ?>" alt="<?php echo $venue->Name; ?> Seating Chart">
						</a>
					</div>
					<div class="event-details">
						<h4>
							<a href="/concerts/<?php echo $venue->RegionSlug."+".$venue->VenueSlug; ?>">
								<span><?php echo $venue->Name; ?></span>
							</a>
						</h4>
						<p><b>Where:</b> <?php echo $venue->Street1.", ".$venue->City.", ".$venue->StateProvince." - ".$venue->ZipCode; ?></p>
						<p><b>Events:</b> <?php echo ($venue->EventCount) ? $venue->EventCount : 'No events scheduled at this time' ?></p>
						</div>
				</div>
			<?php endforeach ?>
		</div>
	</div>
</article>
