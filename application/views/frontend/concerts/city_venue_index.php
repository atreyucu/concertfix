<article>
	<div class="article-header">
		<h1 class="title"><?php echo clean_venue($venue->Name);?> Concerts</h1><!--put the right venue in here-->
		<div class="separator"></div>
	</div>
	<div class="article-content">
		<p><?php echo $cityVenueText; ?></p>
		<?php
		// this is a little bit ghetto, but saves on editing
		echo ($template['partials']['global_city_schedule']);
		echo ($template['partials']['schedule_jsonld']);
		?>
		<h3 class="post-lead"><?php echo clean_venue($venue->Name);?> Concert Information</h3>
			<?php echo $liText ?>
	</div>
</article>