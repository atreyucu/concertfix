<article>
	<div class="article-header">
		<h1 class="title"><?php echo $location['city']; ?>, <?php echo $location['state']; ?> Concerts <?php echo $dateRange ?></h1>
		<ul class="article-info">
			<li><span class="icon awe-home"></span><a href="/concerts/<?php echo $location['slug'] ?>+venues"><?php echo $location['city']; ?> Venues</a></li>
		</ul>
		<div class="separator"></div>
	</div>
	<div class="article-content">
		<p><?php // LEGACY SPINNED echo $spinnedText; ?> </p>
		<?php if (count($highEvents) > 0): ?>
			<h3 style="margin-top:4px;">TOP UPCOMING CONCERTS IN <?php echo $location['city']; ?>, <?php echo $location['state']; ?></h3>
			<ul>
				<?php foreach ($highEvents as $key => $event): ?>
					<?php
						$trueLocation = array('city' => $cityEvents[$key]->City, 'state' => $cityEvents[$key]->StateProvince);
						$url = ($cityEvents[$key]->performers) ? "tours/{$cityEvents[$key]->performers[0]->PerformerSlug}+".slug_venue($cityEvents[$key]->Venue)."+".slug_location($trueLocation) : "concerts/".slug_location($trueLocation)."+".slug_venue($cityEvents[$key]->Venue);
						if ((date('Y', strtotime($cityEvents[$key]->Date)) - date('Y')) > 2)
							$date = date('M j', strtotime($cityEvents[$key]->Date)).' (PPD)';
						else
							$date = date('D M j, Y', strtotime($cityEvents[$key]->Date));
					?>

					<li>
						<a href="/<?php echo $url?>"><?php echo $cityEvents[$key]->Name . " @ " . clean_venue($cityEvents[$key]->Venue) . " on " . $date //date('D M j, Y', strtotime($cityEvents[$key]->Date))?></a>
					</li>
				<?php endforeach;?>
			</ul>
			<br/>
		<?php endif;?>

	<?php
	// this is a little bit ghetto, but saves on editing
	echo ($template['partials']['global_city_schedule']);
	echo ($template['partials']['schedule_jsonld']);
	echo ($template['partials']['nearby_city_schedule']);
	echo ($template['partials']['nearbycity_jsonld']);
	?>
	<h3><?php echo $location['city']; ?> Concert and Event Information</h3>
	<?php
	echo $liText
	?>
	</div>
</article>