<?php if ($nearbyCitiesEvents): ?>

    <section>

        <h2><?php echo $genre_type?> Concerts near <?php echo $location['city'] ?></h2>

        <div class="table" id="city-events">
            <?php foreach ($nearbyCitiesEvents as $cityEvents) : ?>

                <?php $activeDate = false; ?>

                <?php if (count($cityEvents) > 0) : ?>
                    <h3><?php echo $genre_type?> Concerts in <?php echo $cityEvents[0]->City.', '. $cityEvents[0]->StateProvince ?></h3>
                <?php endif?>

                <?php if (count($cityEvents) > 10) : ?>
                    <?php $city_events = array_slice($cityEvents, 0, 10)?>
                <?php elseif (count($cityEvents) > 0) : ?>
                    <?php $city_events = $cityEvents?>
                <?php else : ?>
                    <?php $city_events = array()?>
                <?php endif ?>

                <?php foreach ($city_events as $event): ?>
                    <article>
                        <?php
                        $trueLocation = array('city' => $event->City, 'state' => $event->StateProvince);
                        $url = ($event->performers) ? "tours/{$event->performers[0]->PerformerSlug}+" . slug_venue($event->Venue) . "+" . slug_location($trueLocation) : "concerts/" . slug_location($trueLocation) . "+" . slug_venue($event->Venue);
                        ?>

                        <?php if ($activeDate != date('Y-m-d', strtotime($event->Date))): ?>

                            <?php $activeDate = date('Y-m-d', strtotime($event->Date)); //set active date ?>

                            <?php if ((date('Y', strtotime($event->Date)) - date('Y')) > 2):?>
                                <?php $date = date('M j', strtotime($event->Date)).' (PPD)' ?>
                            <?php else: ?>
                                <?php $date = date('D M j, Y', strtotime($event->Date)) ?>
                            <?php endif; ?>

                            <div class="date-events">

                                <h3>
                                    <i class="awe-calendar"></i>
                                    <?php //echo date('D M j, Y', strtotime($event->Date)); ?>
                                    <?php echo $date ?><br>
                                </h3>
                            </div>

                        <?php endif; ?>

                        <div class="eventblock">

                            <div class="event-thumbnail image-polaroid" style="overflow:hidden;height:70px;">
                                <a href="/<?php echo $url; ?>"
                                   title="<?php echo ($event->performers) ? $event->performers[0]->PerformerName : ''; ?> <?php echo $event->City; ?> Concert Tickets">
                                    <img src="<?php echo performerImage($event->performers[0], 'thumb'); ?>"
                                         alt="<?php echo ($event->performers) ? $event->performers[0]->PerformerName : ''; ?>"/>
                                </a>
                            </div>

                            <div class="event-cta">
                                <!--<a target="_blank" class="btn btn-info" title="RSVP on Facebook"
                                   href="https://www.facebook.com/sharer/sharer.php?s=100&p[url]=<?php /*echo urlencode(base_url() . $url); */?>&p[images][0]=<?php /*echo urlencode(performerOgImage($event->performers[0])); */?>&p[title]=<?php /*echo urlencode($event->Name) */?>&p[summary]=checkout%20ConcertFix">RSVP
                                    on <i class="ent-facebook-3"></i></a>-->
                                <?php
                                /*
                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(base_url().$url); ?>" target="_blank" class="btn btn-info" title="RSVP on Facebook">RSVP on <i class="ent-facebook-3"></i></a>
                                */
                                $link = event_link($event, $trueLocation);

                                if (!empty($dates) AND $event->performers) {
                                    $link = '/tours/' .
                                        $event->performers[0]->PerformerSlug . '+' .
                                        slug_venue($event->Venue) . '+' .
                                        slug_location($trueLocation);
                                }
                                ?>

                                <a <?php echo ($event->performers) ? 'href="' . $link . '"' : 'href="' . $link . '"'; ?>
                                   class="btn btn-info"
                                   title="<?php echo ($event->performers) ? $event->performers[0]->PerformerName : ''; ?> <?php echo $event->City; ?> Concert Tickets">
                                    TICKETS <i class="icon ent-ticket"></i>
                                </a>
                                <br>
                            <span>
                                <span><?php echo ($event->prices) ? $event->prices->ticketsAvailable : "Few " ?></span>
                                tickets

                                <?php if ($event->prices) : ?>
                                    left starting from $<span><?php echo $event->prices->lowPrice; ?></span>
                                <?php else: ?>
                                    left - check prices
                                <?php endif; ?>
                            </span>
                            </div>


                            <div class="event-details">

                                <h4 style="margin-top:4px;">
                                    <?php if ($event->performers): ?>
                                        <a title="<?php echo $event->Name; ?>" href="/<?php echo $url; ?>">
                                            <div><?php echo $event->Name; ?></div>
                                        </a>
                                    <?php else: ?>
                                        <div><?php echo $event->Name; ?></div>
                                    <?php endif ?>
                                </h4>

                                <p>
                                    <?php $venue = $this->global_m->get_venue($event->VenueID); ?>

                                    <b>Venue:</b>
                                    <a href="/concerts/<?php echo seoUrl($event->City . " " . $event->StateProvince) . "+" . slug_venue($event->Venue); ?>">
                                        <span><?php echo clean_venue($event->Venue); ?></span>
                                    </a>
                                </p>

                                <p>
                                    <b>Time:</b>
                                    <?php echo (preg_match('/tba/i', $event->DisplayDate)) ? 'To Be Announced' : date('g:i A', strtotime($event->Date)); ?>
                                </p>

                                <p><b>Featuring:</b>

                                    <?php if ($event->performers): ?>

                                        <?php $i = 0; ?>
                                        <?php foreach ($event->performers as $p): ?>
                                            <?php $i++; ?>
                                            <a href="/tours/<?php echo $p->PerformerSlug ?>"
                                               alt="<?php echo $p->PerformerName ?>">
                                            <span>
                                                <?php echo $p->PerformerName ?>
                                            </span>
                                            </a>
                                            <?php echo ($i < count($event->performers)) ? ' | ' : ''; ?>
                                        <?php endforeach ?>

                                    <?php else: ?>
                                        <span class="">Various Artists</span>
                                    <?php endif ?>
                                </p>

                            </div>

                        </div>

                    </article>

                <?php endforeach ?>
                <?php if (count($cityEvents) > 10): ?>
                    <a href="/concerts/<?php echo strtolower(seoUrl($cityEvents[0]->City." ".$cityEvents[0]->StateProvince)).'+'.$category[0]->Slug?>" class="btn btn-inverse pull-right"><span style="color:#fff;"><?php echo "Show All {$genre_type} in {$cityEvents[0]->City}"?></span></a>
                    <div style="clear:both;"></div>
                <?php endif ?>

            <?php endforeach; ?>

        </div>

    </section>

<?php endif; ?>
<div class="separator"></div>
