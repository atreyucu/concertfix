<article>
	<div class="article-header">
		<h1 class="title">Top <?php echo $category; ?> Concert Tours:</h1>
		<div class="separator"></div>
		<p>Check out all top <?php echo $category; ?> Tours. Using sales data, attendance data, and
			social indicators from vairous reputable sites we ranked the most popular <?php echo $category; ?>
			tours has been up to this point.</p>
	</div>
	<div class="article-content">
        <?php if($performers): ?>
            <?php $performers = array_slice($performers, 0, 100); ?>
            <?php //<p class="city-content"><?php // echo $spinnedText; ?></p>

            <div class="table" id="city-events">
                <?php $activeDate = false; ?>
                <?php foreach ($performers as $p): ?>
                    <?php if ($p->event): ?>
                        <?php $url = "tours/{$p->PerformerSlug}"; ?>
                        <div class="eventblock">

                            <div class="event-thumbnail image-polaroid" style="overflow:hidden;height:70px;">
                                <a href="/<?php echo $url; ?>">
                                    <img src="<?php echo performerImage($p, 'thumb'); ?>" alt="<?php echo $p->Name; ?> Tour Dates">
                                </a>
                            </div>

                            <div class="event-cta">
                                <a href="/<?php echo $url; ?>" class="btn btn-info" title="<?php echo $p->Name; ?> Tour Dates &amp; Concert Tickets">
                                    Tour Dates
                                    <i class="icon ent-calendar"></i>
                                </a><br>
                            </div>
                            <div class="event-details">
                                <h4>
                                    <a href="/<?php echo $url; ?>">
                                        <span><?php echo $p->Name; ?></span> Concerts
                                    </a>
                                </h4>
                                <?php if($p->event): ?>
                                <p><b>Tour Dates: </b> <?php echo $p->event_count." concerts remaining" ?></p>

                                <p>

                                    <b>Next Concert: </b>
                                    <?php if ((date('Y', strtotime($p->event->Date)) - date('Y')) > 2):?>
                                        <?php $date = date('M j', strtotime($p->event->Date)).' (PPD)' ?>
                                    <?php else: ?>
                                        <?php $date = date('D M j, Y', strtotime($p->event->Date)) ?>
                                    <?php endif;?>
                                    <a href="/tours/<?php echo "{$p->PerformerSlug}+{$p->event->VenueSlug}+{$p->event->RegionSlug}"; ?>">
                                        <?php echo $p->event->City." on ". $date //date('M j, Y', strtotime($p->event->Date)); ?>
                                    </a>
                                </p>

                                <?php else: ?>
                                <p>	<?php echo $p->Name ?> has not scheduled any concerts, check back soon!</p>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif ?>
                <?php endforeach ?>
            </div>

        <?php else: ?>
            <h2>Sorry, no performers here </h2>
        <?php endif; ?>
		<div class="separator"></div>
	</div>
</article>
