<?php if($cityEvents): ?>
	<?php
		$originalCount = count($cityEvents);
		$showAll = $this->input->get('showAll');
	 ?>
	 <?php $cityEvents  = ($showAll) ? $cityEvents : array_slice($cityEvents, 0, 100); ?>

	<?php /*<p class="city-content"><?php // echo $spinnedText; ?></p>*/ ?>

    <section>

        <div class="table" id="city-events">
            <?php $activeDate = false; ?>

            <?php foreach ($cityEvents as $event): ?>

                <article itemscope itemtype="http://schema.org/MusicEvent">

                    <meta itemprop="startDate" content="<?php echo $event->Date; ?>">

                    <?php
                        $trueLocation = array('city' => $event->City, 'state' => $event->StateProvince);
                        $url = ($event->performers) ? "tours/{$event->performers[0]->PerformerSlug}+".slug_venue($event->Venue)."+".slug_location($trueLocation) : "concerts/".slug_location($trueLocation)."+".slug_venue($event->Venue);
                     ?>

                    <?php if($activeDate != date('Y-m-d', strtotime($event->Date))): ?>

                        <?php $activeDate = date('Y-m-d', strtotime($event->Date)); //set active date ?>

                        <div class="date-events">
                            <?php if ((date('Y', strtotime($event->Date)) - date('Y')) > 2):?>
                                <?php $date = date('M j', strtotime($event->Date)).' (PPD)' ?>
                            <?php else: ?>
                                <?php $date = date('D M j, Y', strtotime($event->Date)) ?>
                            <?php endif; ?>
                            <h3>
                                <i class="awe-calendar"></i>
                                <?php echo $date //date('D M j, Y', strtotime($event->Date)); ?>
                            </h3>
                        </div>

                    <?php endif; ?>

                    <div class="eventblock">

                        <div class="event-thumbnail image-polaroid" style="overflow:hidden;height:70px;">
                            <a  href="/<?php echo $url; ?>" title="<?php echo ($event->performers) ? $event->performers[0]->PerformerName : ''; ?> <?php echo $event->City; ?> Concert Tickets">
                                <img src="<?php echo performerImage($event->performers[0], 'thumb'); ?>"
                                     alt="<?php echo ($event->performers) ? $event->performers[0]->PerformerName : ''; ?>" />
                            </a>
                        </div>

                        <div class="event-cta">
                            <!--<a target="_blank" class="btn btn-info"  title="RSVP on Facebook" href="https://www.facebook.com/sharer/sharer.php?s=100&p[url]=--><?php //echo urlencode(base_url().$url); ?><!--&p[images][0]=--><?php //echo urlencode(performerOgImage($event->performers[0])); ?><!--&p[title]=--><?php //echo urlencode($event->Name) ?><!--&p[summary]=checkout%20ConcertFix">RSVP on <i class="ent-facebook-3"></i></a>-->
                            <?php
                            /*
                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(base_url().$url); ?>" target="_blank" class="btn btn-info" title="RSVP on Facebook">RSVP on <i class="ent-facebook-3"></i></a>
                            */
                            $link = event_link($event, $trueLocation);

                            if (!empty($dates) AND $event->performers) {
                                $link = '/tours/' .
                                    $event->performers[0]->PerformerSlug . '+' .
                                    slug_venue($event->Venue) . '+' .
                                    slug_location($trueLocation);
                            }
                            ?>

                            <a itemprop="url" <?php echo ($event->performers) ? 'href="'.$link.'"' : 'href="'.$link.'"'; ?> class="btn btn-info" title="<?php echo ($event->performers) ? $event->performers[0]->PerformerName : ''; ?> <?php echo $event->City; ?> Concert Tickets">
                                TICKETS <i class="icon ent-ticket"></i>
                            </a>
                            <br>
                            <span itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">
                                <span itemprop="offerCount"><?php echo ($event->prices) ? $event->prices->ticketsAvailable : "Few " ?></span>
                                tickets

                                <link itemprop="url" href="<?php echo $link; ?>" />
                                <meta itemprop="priceCurrency" content="USD" />

                                <?php if ($event->prices) : ?>
                                    left starting from $<span itemprop="lowPrice"><?php echo $event->prices->lowPrice; ?></span>
                                <?php else: ?>
                                    left - check prices
                                    <meta itemprop="lowPrice" content="0"/>
                                <?php endif; ?>
                            </span>
                        </div>


                        <div class="event-details">

                            <h4 style="margin-top:4px;">
                                <?php if ($event->performers): ?>
                                    <a title="<?php echo $event->Name ?>" href="/<?php echo $url; ?>">
                                        <div itemprop="name"><?php echo $event->Name; ?></div>
                                    </a>
                                <?php else: ?>
                                    <div itemprop="name"><?php echo $event->Name; ?></div>
                                <?php endif ?>
                            </h4>

                            <p itemprop="location" itemscope itemtype="http://schema.org/MusicVenue">
                                <?php $venue = $this->global_m->get_venue(false, $event->VenueSlug, $location); ?>
                                <meta itemprop="address"
                                      content="<?php echo (!empty($venue) ? $venue->Street1." ".$venue->Street2.", ".$venue->City.", ".$venue->StateProvince." - ".$venue->ZipCode : $event->Venue); ?>"/>

                                <b>Venue:</b>
                                <a itemprop="url" href="/concerts/<?php echo seoUrl($event->City." ".$event->StateProvince)."+".slug_venue($event->Venue); ?>" >
                                    <span itemprop="name"><?php echo clean_venue($event->Venue); ?></span>
                                </a>
                            </p>

                            <p>
                                <b>Time:</b>
                                <?php echo date('g:i A', strtotime($event->Date)); ?>
                            </p>

                            <p><b>Featuring:</b>

                                <?php if ($event->performers): ?>

                                    <?php $i = 0; ?>
                                    <?php foreach ($event->performers as $p): ?>
                                        <?php $i++; ?>
                                        <a itemprop="performer" itemscope itemtype="http://schema.org/MusicGroup" href="/tours/<?php echo $p->PerformerSlug ?>" alt="<?php echo $p->PerformerName ?>">
                                            <span itemprop="name">
                                                <?php echo $p->PerformerName ?>
                                            </span>
                                        </a>
                                        <?php echo ($i < count($event->performers)) ? ' | ' : ''; ?>
                                    <?php endforeach ?>

                                <?php else: ?>
                                    <span class="">Various Artists</span>
                                <?php endif ?>
                            </p>

                        </div>

                    </div>

                </article>
            <?php endforeach ?>

            <?php if ($originalCount > 99 && !$showAll): ?>
                <a href="?showAll=true" class="btn btn-inverse pull-right"><span style="color:#fff;">Show All</span></a>
                <div style="clear:both;"></div>
            <?php endif ?>

        </div>

    </section>

<?php else: ?>
<h2>Sorry, there are no events in <?php echo $location['city']; ?> </h2>
<?php endif; ?>
<div class="separator"></div>
