<article>
	<div class="article-header">
		<h1 class="title"><?php echo $location['city'] ?> <?php echo $category[0]->Filter ?> Concerts</h1><!--put the right venue in here-->
		<div class="separator"></div>
	</div>
	<div class="article-content">
		<p><?php echo $cityGenreText; ?> </p>
		<?php
			// this is a little bit ghetto, but saves on editing
			echo ($template['partials']['global_city_schedule']);
			echo ($template['partials']['schedule_jsonld']);
			echo ($template['partials']['nearby_city_schedule']);
			echo ($template['partials']['nearbycity_jsonld']);
		?>

		<h3 class="post-lead"><?php echo $location['city'] ?> <?php echo $category[0]->Filter ?> Concert Information</h3>
		<?php echo $liText; ?>
	</div>
</article>
