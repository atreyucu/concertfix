<script type="application/ld+json">
	[
<?php $count_total = 0 ?>
<?php foreach ($nearbyCitiesEvents as $cityEvents) : ?>

	<?php if (count($cityEvents) > 10) : ?>
		<?php $city_events = array_slice($cityEvents, 0, 10)?>
	<?php elseif (count($cityEvents) > 0) : ?>
		<?php $city_events = $cityEvents?>
	<?php else : ?>
		<?php $city_events = array()?>
	<?php endif ?>
		<?php $count_partial = 0 ?>
		<?php foreach ($city_events as $event): ?>
			<?php $trueLocation = array('city' => $event->City, 'state' => $event->StateProvince); ?>
			<?php $link = event_link_full($event, $trueLocation); ?>
			<?php $venue = $this->global_m->get_venue($event->VenueID); ?>
            <?php $eventName = strtr($event->Name, array('"'=>"'"))?>
            <?php $postponed = (date('Y', strtotime($event->Date)) - date('Y')) > 2; ?>
                        {
							"@context":"https://schema.org",
							"@type":"Event",
							"description": "<?php echo "Tickets and information to see {$eventName} perform live at {$event->Venue} in {$event->City}. {$eventName} tickets are protected with a 100% guarantee at ConcertFix." ?>",
							"endDate": "<?php echo explode('T', $event->Date)[0]?>",
							"eventAttendanceMode": "https://schema.org/OfflineEventAttendanceMode",
      						<?php if ($postponed): ?>
							"eventStatus": "https://schema.org/EventPostponed",
							<?php else: ?>
							"eventStatus": "https://schema.org/EventScheduled",
							<?php endif; ?>
							<?php if ($event->performers) : ?>
                            "image": "<?php echo $event->performers[0]->img?>",
                            <?php $p_count = 1; ?>
                            "performer": [
                                <?php foreach ($event->performers as $eperformer): ?>
                                	{
										"@context": "https://schema.org",
										"name": "<?php echo strtr($eperformer->PerformerName, array('"'=>"'")); ?>",
										"sameAs": "<?php echo base_url().'tours/'.$eperformer->PerformerSlug?>"
                                	}<?php echo ($p_count < count($event->performers) ) ? ',': ''?>
                                <?php $p_count += 1; ?>
                            <?php endforeach; ?>
                            ],
							<?php else : ?>
                            "image": "<?php echo performerImage($event->performers[0], 'thumb') ?>",
                            "performer": [
                                	{
										"@context": "https://schema.org",
										"name": "<?php echo $eventName; ?>"
                                	}
                            ],
							<?php endif; ?>
							"name":"<?php echo $eventName; ?>",
							"startDate":"<?php echo $event->Date; ?>",
							"url":"<?php echo $link; ?>",
							"location":{
							            "@type":"Place",
										"name":"<?php echo $event->Venue ?>",
										"address":{
										    "@type":"PostalAddress",
											"addressLocality":"<?php echo $event->City ?>",
											"addressRegion": "<?php echo $event->StateProvince ?>"
										}
							},
							"offers":{
							          "@type":"Offer","category":"Secondary","priceCurrency":"USD",
									  <?php if ($event->prices) : ?>
									  "price":"<?php echo $event->prices->lowPrice; ?>",
									  <?php endif ?>
									  "url":"<?php echo $link; ?>",
									  "availability":"https://schema.org/InStock",
									  "validFrom": "<?php echo date('Y-m-d').'T00:00:00'?>"
							}
						}<?php echo ($count_partial < count($city_events) - 1 or $count_total < count($nearbyCitiesEvents) - 1) ? ',': ''?>
                        <?php $count_partial += 1?>
        <?php endforeach ?>
        <?php $count_total += 1?>
<?php endforeach ?>
	]

</script>



