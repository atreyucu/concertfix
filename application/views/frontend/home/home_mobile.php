<?php if ($local_events): ?>
<?php $local_events = array_slice($local_events, 0, 15); //15 only ?>
<?php endif;?>
<?php if (count($local_events) > 4): ?>
<h2>Upcoming
    Concerts <?php echo $text ?> <?php echo $location['city'] . ", " . $location['state']; ?>  <?php //echo ip2long($this->input->ip_address()); ?> </h2>
<p>Check out all the great upcoming concerts near <?php echo $location['city'] . ", " . $location['state']; ?>. If this
    location doesn't work for you then you can change your location <a herf="/all-cities.html">here</a></p>
<div id="main-masonry" style="" class="masonry">
    <?php $i = 1; ?>
    <?php foreach ($local_events as $event): ?>
        <?php $type = ($i % 5) ? "profile" : "wide" ?>
        <div class="masonryitem masonry-brick" id="msn_<?php echo $i; ?>">
            <!--<div class="--><?php //echo ($i % 5) ? "img-block" : "img-block-long" ?><!--">-->
            <div class="img-block">
                <div class="date-brick">
                    <span><?php echo date('M j', strtotime($event->Date)); ?></span>
                </div>
                <!--<img src="--><?php //echo performerImage($event->performers[0], $type); ?><!--" style="">-->
                <img src="<?php echo $event->performers[0]->img ?>" style="width: 300px">
            </div>
            <div class="block-details" id="p-<?php echo $i ?>">
                <h4>
                    <a href="/tours/<?php echo $event->performers[0]->PerformerSlug; ?>+<?php echo $event->VenueSlug; ?>+<?php echo $event->RegionSlug; ?>"><?php echo $event->Name; ?></a>
                </h4>

                <p><b>Where: </b><a
                        href="/concerts/<?php echo "{$event->RegionSlug}+{$event->VenueSlug}" ?>"><?php echo clean_venue($event->Venue); ?></a>
                </p>

                <p>
                    <b>When: </b> <?php echo (preg_match('/tba/i', $event->Date)) ? "TBA" : date('M j, Y', strtotime($event->Date)) ?>
                </p>
                <ul class="mob-icons-block">
                    <li>
                        <a href="/tours/<?php echo $event->performers[0]->PerformerSlug; ?>"
                           title="View <?php echo $event->Name; ?> Tour Dates">
                            <span class="icon ent-calendar"> Tour Dates </span>
                        </a>
                    </li>
                    <li>
                        <a href="/tours/<?php echo $event->performers[0]->PerformerSlug; ?>+<?php echo $event->VenueSlug; ?>+<?php echo $event->RegionSlug; ?>"
                           title="<?php echo $event->Name; ?> <?php echo date('M j, Y g:ia', strtotime($event->Date)); ?> Tickets">
                            <span class="icon ent-calendar"> Tickets</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <?php $i++; ?>
    <?php endforeach ?>
    <?php if (count($local_events) > 15): ?>
        <?php if ($text == 'in') : ?>
            <a style="margin-right: 4px" href="/concerts/<?php echo $location['slug']?>" class="btn btn-inverse pull-right">
                <span style="color:#fff;">Show All</span>
            </a>
        <?php else: ?>
            <a style="margin-right: 4px" rel="nofollow" href="/local/<?php echo $location['slug']?>" class="btn btn-inverse pull-right">
                <span style="color:#fff;">Show All</span>
            </a>
        <?php endif; ?>
        <div style="clear:both;"></div>
    <?php endif ?>
</div>
<?php endif; //if under 5 ?>

<h2>Featured Performers </h2>
<p>Check out all the top concert performers in North America. If you are lookig for a specific performer and do not see
    him below you can visit our <a herf="/all-artists">all artists</a> page or perform a search.</p>
<div class="separator"></div>
<div id="main-masonry2" style="" class="masonry">
    <?php if ($performers): ?>
        <?php $i = 1; ?>
        <?php foreach ($performers as $p): ?>
            <?php $type = ($i % 5) ? "profile" : "wide" ?>
            <div class="masonryitem masonry-brick" id="msn_<?php echo $i;?>">

                <div class="img-block">
                    <img src="<?php echo $p->img; ?>" style="width: 300px">
                </div>

                <div class="block-details" id="p-<?php echo $i; ?>">
                    <h4><a href="/tours/<?php echo $p->PerformerSlug; ?>"><?php echo $p->Name; ?></a></h4>

                    <p><b>Tour Dates: </b> <?php echo $p->event_count ?> concerts remaining </p>

                    <p><b>Next Concert: </b> <a
                            href="/tours/<?php echo "{$p->PerformerSlug}+{$p->event->VenueSlug}+{$p->event->RegionSlug}"; ?>"><?php echo $p->event->City . " on " . date('M j, Y', strtotime($p->event->Date)); ?></a>
                    </p>
                    <ul class="mob-icons-block">
                        <li>
                            <a href="/tours/<?php echo $p->PerformerSlug; ?>"
                               title="View <?php echo $p->Name; ?> Tour Dates">
                                <span class="icon ent-calendar"> Tour Dates</span> <!--Tour Dates-->
                            </a>
                        </li>
                        <li>
                            <a href="/tours/<?php echo $p->PerformerSlug; ?>"
                               title="Tickets for <?php echo $p->Name; ?>">
                                <span class="icon ent-ticket"> Buy Tickets</span> <!--Buy Tickets-->
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <?php $i++; ?>
        <?php endforeach ?>
    <?php endif; ?>
</div>

<div style="float: left;width: 100%;margin-bottom: 20px;"><p>Photos: Last.fm</p></div>
<div class="separator"></div>

<h2>Welcome to ConcertFix.com</h2>
<p>Feed your concert addiction at ConcertFix! Here at ConcertFix, we know how
    important it is for you to see your favorite performers live in concert.
    That's why we have created one of the largest databases of concert information
    including tour dates, concert schedules, discographies, and more for over
    4,000 popular artists and performers. We show you all <a href="/tours/all-artists">concert
        performers</a> most recent <a href="/tour-announcements">tour announcements</a>,
    upcoming tour dates and tour album information. We also rank the top performing tours
    in each genre in our <a href="/top-tours">top tours</a> section. You can find
    upcoming local concerts in your <a href="/concerts/all-cities">city</a> or check out
    concert schedules at local venues. Our site is updated by the minute to make sure
    you’re always getting the most recent concert information. ConcertFix gives you
    everything you need to be ready for the show.
    <br><br>
    Once you find a concert your interested in going to, ConcertFix will gather tickets
    from different ticket sellers all over the world and find you great deals on all
    tickets. It doesn't matter if the concert is already sold out, or if you want front
    row or VIP tickets right before the show. We can help you get those tickets you want.
    We use interactive seating charts of the concerts venue so you will know exactly where you
    will be sitting for the concert. Additionally, ConcertFix has eTickets available for
    purchase which means you can buy your tickets at the last minute and still make the show
    on time.</p>