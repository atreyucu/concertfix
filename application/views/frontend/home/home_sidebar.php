 <?php /* if ($sidebarFeaturedPerformers): ?>
	<h2>Featured Artists</h2>
	<ul class="nav nav-pills nav-stacked">
		<?php foreach ($sidebarFeaturedPerformers as $performer): ?>
			  <li><a href="/tours/<?php echo $performer->PerformerSlug ?>"><?php echo $performer->Name ?></a></li>
		<?php endforeach ?>
	</ul>
<?php endif */ ?>
<div class="sidebar-widget sidebar-block sidebar-color">
	<div class="sidebar-header">
		<h4>Concert Tracker</h4>
		<div class="pull-left marg-right" style="width:89px;">
			<img src="/public/img/assets/concert-tracker-2.png" alt="track your favorite concerts" />
		</div>
		<p>Sign up for email alerts whenever your favorite artists and cities announce events.</p>
	</div>
	<div class="sidebar-content login-widget">
		<!--form>
			<div class="input-prepend">
				<span class="add-on "><i class="icon-user"></i></span>
				<input type="email" class="input-medium" placeholder="username">
			</div>
			<div class="input-prepend">
				<span class="add-on "><i class="icon-lock"></i></span>
				<input type="password" class="input-medium" placeholder="password">
			</div>
		</form-->
		<div class="controls">
			<!--button class="btn btn-flatten btn-inverse signin" id="signup" type="submit">Sign In</button-->
			<a href="/user/login" style="color:#fff;margin-right:60px;" class="pull-right"><button class="btn btn-flatten btn-inverse signup">Sign up</button></a>
		</div>
	</div>
</div><div style="clear:both;"></div>
<?php /*
<hr><br>
<div class="sidebar-widget" id="fb-rsvp">
	<div class="sidebar-header">
		<h4>Top Concerts in {Geo Location} </h4>
		<div class="separator"></div>
	</div>
	<img width="100%" src="/public/img/rsvp.jpg">
</div>
<hr><br>
*/ ?>
<div class="sidebar-widget" id="fb-rsvp">
	<div class="sidebar-header">
		<h4>Twitter <a href="https://twitter.com/concertfix" target="_blank">@ConcertFix</a> Tweets</h4>
		<div class="separator"></div>
	</div>
<a class="twitter-timeline" href="https://twitter.com/Concertfix" data-widget-id="416659905156612096">Tweets by @Concertfix</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</div>
<hr><br>
<!-- Latest Review Widget -->
<div class="sidebar-widget" id="sidebar-tabs">
	<div class="sidebar-header">
		<h4>Top Tour Cites</h4>
		<div class="separator"></div>
	</div>
	<div class="sidebar-header">
		<ul class="nav nav-tabs" id="tab1">
			<?php $cur = true; ?>
			<?php arsort($cities); ?>
			<?php foreach ($cities as $area => $data): ?>
			<li class="<?php echo ($cur) ? 'active' : ''; ?>"><a href="#<?php echo $area ?>"><?php echo $area ?></a></li>
			<?php $cur = false; ?>
			<?php endforeach ?>
		</ul>
	</div>
	<div class="sidebar-content tab-content">
		<?php $cur = true; ?>
		<?php foreach ($cities as $area => $data): ?>
			<div class="sidebar-item tab-pane tag-widget <?php echo ($cur) ? 'active' : ''; ?>" id="<?php echo $area; ?>">
				<?php $cur = false; ?>
				<?php if($data): ?>
					<?php foreach ($data as $city): ?>
						<a href="/concerts/<?php echo $city->slug ?>" class="active"><?php echo $city->city ?></a>
					<?php endforeach ?>
				<?php endif; ?>
			</div>
		<?php endforeach ?>
	</div>
</div>
