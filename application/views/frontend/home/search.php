<div class="container" id="searchbar">
	<div class="form-contain">
		<form class="main-search" method="post" action="/search">
			<h1 class="search-label">Never Miss a Concert Again</h1>
			<div class="barhold">
				<input id="big_search_home" class="input span8 search-box" type="text" placeholder="Search artists, venues, cities..." name="query">
				<button class="btn btn-inverse searchbtn"><i class="awe-search"></i></button>
				<div id="big_jaxResults" class="smokedline module span8 jaxResults" style="display:none;"></div>
			</div>
		</form>
	</div>	
</div>