<div class="popular-item span3">
	<div class="popular-image">
		<a href="/tour-announcements" title="Concert &amp; Tour Announcements"><img src="/public/img/assets/tour-announcements.jpg" alt="image"></a>
	</div>
	<div class="popular-content">
		<h4><a href="/tour-announcements" title="Concert &amp; Tour Announcements">Get Tour Announcements</a></h4>
		<p>
			See all the latest tour announcements and concert schedules from todays biggest artists.
		</p>
	</div>
</div>
<div class="popular-item span3">
	<div class="popular-image">
		<a href="/user/login" title="Track your favorite artists"><img src="/public/img/assets/tour-tracker.jpg" alt="image"></a>
	</div>
	<div class="popular-content">
		<h4><a href="/user/login" title="Track your favorite artists">Track your Favorite Artists</a></h4>
		<p>
			Track your favorite artists concerts or events in your area &amp; never miss a great concert again.
		</p>
	</div>
</div>
<div class="popular-item span3">
	<div class="popular-image">
		<a href="/concerts/all-cities" title="Find local concert schedules for your city"><img src="/public/img/assets/local-concerts.jpg" alt="image"></a>
	</div>
	<div class="popular-content">
		<h4><a href="/concerts/all-cities" title="Find local concert schedules for your city">Find Local Concerts</a></h4>
		<p>
			Browse upcoming concerts and events coming to city or area. Sort through genres &amp; venues.
		</p>
	</div>
</div>
<div class="popular-item span3">
	<div class="popular-image">
		<a href="/great-deals-on-tickets" title="Ticket specials &amp; Coupon Codes"><img src="/public/img/assets/ticket-deals.jpg" alt="image"></a>
	</div>
	<div class="popular-content">
		<h4><a href="/great-deals-on-tickets" title="Ticket specials &amp; Coupon Codes">Great Deals on Tickets</a></h4>
		<p>
			We will let you know when tickets become available and offer exclusive discounts for members.
		</p>
	</div>
</div>
