<?php if ($local_events): ?>
    <?php $levents = array_slice($local_events, 0, 15); //15 only ?>
    <?php if (count($local_events) > 4): ?>
        <h2>Upcoming
            Concerts <?php echo $text ?> <?php echo $location['city'] . ", " . $location['state']; ?>  <?php //echo ip2long($this->input->ip_address()); ?> </h2>
        <p>Check out all the great upcoming concerts near <?php echo $location['city'] . ", " . $location['state']; ?>.
            If this location doesn't work for you then you can change your location <a
                href="/concerts/all-cities">here</a></p>
        <div class="separator"></div>
        <div id="main-masonry" style="" class="masonry">
            <?php $i = 1; ?>
            <?php foreach ($levents as $event): ?>
                <?php $type = ($i % 5) ? "profile" : "wide" ?>
                <div class="masonryitem masonry-brick" id="msn_<?php echo $i; ?>">
                    <!--<div class="--><?php //echo ($i % 5) ? "img-block" : "img-block-long" ?><!--">-->
                    <div class="img-block">
                        <div class="date-brick">
                            <span><?php echo date('M j', strtotime($event->Date)); ?></span>
                        </div>
                        <!--<img src="--><?php //echo performerImage($event->performers[0], $type); ?><!--" style="">-->
                        <img src="<?php echo $event->performers[0]->img ?>">
                    </div>
                    <div class="block-details" id="p-<?php echo $i ?>">
                        <h4>
                            <a href="/tours/<?php echo $event->performers[0]->PerformerSlug; ?>+<?php echo $event->VenueSlug; ?>+<?php echo $event->RegionSlug; ?>"><?php echo $event->Name; ?></a>
                        </h4>

                        <p><b>Where: </b><a
                                href="/concerts/<?php echo "{$event->RegionSlug}+{$event->VenueSlug}" ?>"><?php echo clean_venue($event->Venue); ?></a>
                        </p>

                        <p>
                            <b>When: </b> <?php echo (preg_match('/tba/i', $event->Date)) ? "TBA" : date('M j, Y', strtotime($event->Date)) ?>
                        </p>
                        <ul class="icons-block">
                            <li>
                                <a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fconcertfix.com%2Ftours%2F<?php echo $event->performers[0]->PerformerSlug; ?>%2B<?php echo $event->VenueSlug; ?>%2B<?php echo $event->RegionSlug; ?>"
                                   target="_blank" class="first" title="Share that I'm Going">
                                    <span class="ent-facebook-3"></span> <!--I'm Going-->
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/share?url=http%3A%2F%2Fconcertfix.com%2Ftours%2F<?php echo $event->performers[0]->PerformerSlug; ?>%2B<?php echo $event->VenueSlug; ?>%2B<?php echo $event->RegionSlug; ?>&text=Im going to see <?php echo $event->Name; ?> at <?php echo $event->Venue; ?> -->"
                                   target="_blank" title="Tweet I'm Going">
                                    <span class="ent-twitter"></span>
                                </a>
                            </li>
                            <!--<li>
                                <a href="https://plus.google.com/share?url=http%3A%2F%2Fconcertfix.com%2Ftours%2F<?php //echo $event->performers[0]->PerformerSlug; ?>%2B<?php //echo $event->VenueSlug; ?>%2B<?php //echo $event->RegionSlug; ?>"
                                   target="_blank" title="Share this on Google +">
                                    <span class="icon ent-googleplus"></span> <!--Add Calendar-->
                            <!--</a>
                            </li>-->
                            <li>
                                <a href="/tours/<?php echo $event->performers[0]->PerformerSlug; ?>"
                                   title="View <?php echo $event->Name; ?> Tour Dates">
                                    <span class="icon ent-calendar"></span> <!--Tour Dates-->
                                </a>
                            </li>
                            <li>
                                <a href="/tours/<?php echo $event->performers[0]->PerformerSlug; ?>+<?php echo $event->VenueSlug; ?>+<?php echo $event->RegionSlug; ?>"
                                   title="<?php echo $event->Name; ?> <?php echo date('M j, Y g:ia', strtotime($event->Date)); ?> Tickets">
                                    <span class="icon ent-ticket"></span> <!--Buy Tickets-->
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <?php $i++; ?>
            <?php endforeach ?>

        </div>
        <?php if (count($local_events) > 15): ?>
            <?php if ($text == 'in') : ?>
                <a style="margin-right: 4px" href="/concerts/<?php echo $location['slug']?>" class="btn btn-inverse pull-right">
                    <span style="color:#fff;">Show All</span>
                </a>
            <?php else: ?>
                <a style="margin-right: 4px" rel="nofollow" href="/local/<?php echo $location['slug']?>" class="btn btn-inverse pull-right">
                    <span style="color:#fff;">Show All</span>
                </a>
            <?php endif; ?>
            <div style="clear:both;"></div>
        <?php endif ?>
    <?php endif; //if under 5 ?>
<?php endif; ?>


<h2>Featured Performers </h2>
<p>Check out all the top concert performers in North America. If you are looking for a specific performer &amp; don't
    see them below you can visit our <a href="/tours/all-artists">all artists</a> page or try searching.</p>
<div class="separator"></div>
<div id="main-masonry2" style="" class="masonry ovh">
    <?php if ($performers): ?>
        <?php $i = 1; ?>
        <?php foreach ($performers as $p): ?>
            <?php $type = ($i % 5) ? "profile" : "wide" ?>
            <div class="masonryitem masonry-brick" id="msn_<?php echo $i; ?>">
                <!--<div class="<?php //echo ($i % 5) ? "img-block" : "img-block-long" ?>">-->
                <div class="img-block">
                    <img src="<?php echo $p->img; ?>" style="">
                </div>
                <div class="block-details" id="p-<?php echo $i ?>">
                    <h4><a href="/tours/<?php echo $p->PerformerSlug; ?>"><?php echo $p->Name; ?></a></h4>

                    <p><b>Tour Dates: </b> <?php echo $p->event_count ?> concerts remaining </p>
                    <?php $nextConcert = ($p->event) ? "<a href='/tours/{$p->PerformerSlug}+{$p->event->VenueSlug}+{$p->event->RegionSlug}'>{$p->event->City} on " . date('M j, Y', strtotime($p->event->Date)) . "</a>" : "TBD"; ?>
                    <p><b>Next Concert: </b><?php echo $nextConcert ?></p>
                    <ul class="mob-icons-block">
                        <li>
                            <a href="/tours/<?php echo $p->PerformerSlug; ?>"
                               title="View <?php echo $p->Name; ?> Tour Dates">
                                <span class="icon ent-calendar"> Tour Dates</span> <!--Tour Dates-->
                            </a>
                        </li>
                        <li>
                            <a href="/tours/<?php echo $p->PerformerSlug; ?>"
                               title="Tickets for <?php echo $p->Name; ?>">
                                <span class="icon ent-ticket"> Buy Tickets</span> <!--Buy Tickets-->
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <?php $i++; ?>
        <?php endforeach ?>
    <?php endif; ?>
</div>

<div style="float: left;width: 100%;margin-bottom: 20px;"><p>Photos: Last.fm</p></div>
<div class="separator"></div>

<h2>Welcome to ConcertFix.com</h2>
<p>Feed your concert addiction at ConcertFix! Here at ConcertFix, we know how
    important it is for you to see your favorite performers live in concert.
    That's why we have created one of the largest databases of concert information
    including tour dates, concert schedules, discographies, and more for over
    4,000 popular artists and performers. We show you all <a href="/tours/all-artists">concert
        performers</a> most recent <a href="/tour-announcements">tour announcements</a>,
    upcoming tour dates and tour album information. We also rank the top performing tours
    in each genre in our <a href="/top-tours">top tours</a> section. You can find
    upcoming local concerts in your <a href="/concerts/all-cities">city</a> or check out
    concert schedules at local venues. Our site is updated by the minute to make sure
    you’re always getting the most recent concert information. ConcertFix gives you
    everything you need to be ready for the show.
    <br><br>
    Once you find a concert your interested in going to, ConcertFix will gather tickets
    from different ticket sellers all over the world and find you great deals on all
    tickets. It doesn't matter if the concert is already sold out, or if you want front
    row or VIP tickets right before the show. We can help you get those tickets you want.
    We use interactive seating charts of the concerts venue so you will know exactly where you
    will be sitting for the concert. Additionally, ConcertFix has eTickets available for
    purchase which means you can buy your tickets at the last minute and still make the show
    on time.</p>
