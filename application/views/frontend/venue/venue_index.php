<article>
	<div class="article-header">
		<h1 class="title">Concert <?php echo $venue->Name; ?> <?php echo (preg_match("/{$venue->City}/i", $venue->Name)) ? '' : "in $venue->City"; ?></h1>
		<div class="separator"></div>
	</div>
	<div class="article-content">
		<p class="city-content">{venue} in {city} has released a schedule {season}. {venue} has {# of events scheduled} events currently penciled in so far, starting with {performer 1} on {date 1}. Other popular events that are now scheduled at {venue} including The {performer 2} on {date 2}, {performer 3}, {performer 4} &amp; {performer 5} on {date 3}, and {performer} on {date 4}. At this time , the final event booked at {venue} in {city} is {performer} on {date last}. We also carry parking passes, VIP packages and front row tickets for {venue}. Check out #{venue} or follow @{sitename} for everything you need for new events at {venue} or any changes to these currently scheduled events.</p>
		<div class="table" id="city-venues">
			<?php if ($venueList): ?>
				<?php foreach ($venueList as $venue): ?>
				<div class="eventblock">
					<div class="event-thumbnail image-polaroid">
						<img src="/img/assets/avatar/pink.jpg">
					</div>	
					<div class="event-details">
						<h4>{Venue} -Formerly Data</h4>
						<p><b>Where:</b> {venue Address}</p>
						<p><b>Events:</b> {# of concerts} concerts</p>					
					</div>
				</div>
					<div class="eventblock">
						<div class="event-thumbnail image-polaroid">
							<?php echo (isset($event->performers[0]) && strlen($event->performers[0]->img)) ? "<img style='margin: auto; top: 0; left: 0; right: 0; bottom: 0;width:70px;' src='{$event->performers[0]->img}' />" : "<img src='http://placehold.it/70x70' />" ?> 
						</div>	
						<div class="event-details">
							<h4><?php echo $event->Name; ?></h4>
							<p><b>Date: </b><?php echo date('D M j, Y', strtotime($event->Date)); ?></p>
							<p><b>Time: </b><?php echo (preg_match('/tba/i',$event->DisplayDate)) ? "To Be Announced" : date('g:i A', strtotime($event->Date)); ?></p>
							<!-- <p><b>Events:</b> {# of concerts} concerts</p>				 -->
						</div>
					</div>
					<?php endforeach ?>	
			<?php else: ?>
				<h2>Sorry, no events...</h2>
			<?php endif ?>


			<?php /*
			<div class="eventblock">
				<div class="event-thumbnail image-polaroid">
					<img src="/img/assets/avatar/pink.jpg">
				</div>	
				<div class="event-details">
					<h4>{Venue} -Formerly Data</h4>
					<p><b>Where:</b> {venue Address}</p>
					<p><b>Events:</b> {# of concerts} concerts</p>					
				</div>
			</div>
			<div class="eventblock">
				<div class="event-thumbnail image-polaroid">
					<img src="/img/assets/avatar/pink.jpg">
				</div>	
				<div class="event-details">
					<h4>{Venue} -Formerly Data</h4>
					<p><b>Where:</b> {venue Address}</p>
					<p><b>Events:</b> {# of concerts} concerts</p>					
				</div>
			</div>
			<div class="eventblock">
				<div class="event-thumbnail image-polaroid">
					<img src="/img/assets/avatar/pink.jpg">
				</div>	
				<div class="event-details">
					<h4>{Venue} -Formerly Data</h4>
					<p><b>Where:</b> {venue Address}</p>
					<p><b>Events:</b> {# of concerts} concerts</p>					
				</div>
			</div>
			<div class="eventblock">
				<div class="event-thumbnail image-polaroid">
					<img src="/img/assets/avatar/pink.jpg">
				</div>	
				<div class="event-details">
					<h4>{Venue} -Formerly Data</h4>
					<p><b>Where:</b> {venue Address}</p>
					<p><b>Events:</b> {# of concerts} concerts</p>					
				</div>
			</div>
			<div class="eventblock">
				<div class="event-thumbnail image-polaroid">
					<img src="/img/assets/avatar/pink.jpg">
				</div>	
				<div class="event-details">
					<h4>{Venue} -Formerly Data</h4>
					<p><b>Where:</b> {venue Address}</p>
					<p><b>Events:</b> {# of concerts} concerts</p>					
				</div>
			</div>
			<div class="eventblock">
				<div class="event-thumbnail image-polaroid">
					<img src="/img/assets/avatar/pink.jpg">
				</div>	
				<div class="event-details">
					<h4>{Venue} -Formerly Data</h4>
					<p><b>Where:</b> {venue Address}</p>
					<p><b>Events:</b> {# of concerts} concerts</p>					
				</div>
			</div>
			<div class="eventblock">
				<div class="event-thumbnail image-polaroid">
					<img src="/img/assets/avatar/pink.jpg">
				</div>	
				<div class="event-details">
					<h4>{Venue} -Formerly Data</h4>
					<p><b>Where:</b> {venue Address}</p>
					<p><b>Events:</b> {# of concerts} concerts</p>					
				</div>
			</div>
			*/ ?>
		</div>											
		<h3 class="post-lead">{Performer} Tour and Concert Information</h3>
		
		<ul>
			<li>Tickets for {performer} are now on sale for all upcoming concerts</li>
			<li>Tour dates and all other events are updated by the minute</li>
			<li>Schedule for the {performer} tour can be found above</li>
			<li>ETickets, last minute tickets and downloadable tickets are also available.</li>
			<li>VIP tickets, front row tickets and luxury box tickets are often kept in stock.</li>
			<li>Seats are purchased with safe and secure checkout, fast delivery and 100% guarantee.</li>
		</ul>
		<p>
			{Performer} could be coming to a city near you. Check out the {performer} schedule above and click to see our entire ticket inventory. Look through our selection of {performer} front row tickets, luxury boxes and VIP tickets. Once you find the {performer} tickets you want, you can purchase your tickets from our safe and secure checkout. Orders taken before 5pm are usually shipped within the same business day. To purchase last minute {performer} tickets, check out the eTickets that can be downloaded instantly.
		</p>
		<div class="separator"></div>
		<div class="comments">
			<h3>{performer} reviews and tour comments</h3>
			<div class="separator"></div>
			<ul class="commentlists">
				<li>
					<div class="comment-author image-polaroid">
						<img src="img/assets/avatar/avatar-2.png" alt="avatar">
					</div>
					<div class="comment-body">
						<div class="comment-meta">
							<span class="meta-name"><a href="#">John Doe</a></span>
							<span class="meta-date">December 31, 2012 at 11:00 AM</span>
							<div class="reply">
								<a href="#">reply</a>
							</div>
						</div>
						<p>
							Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.
						</p>
					</div>
					<ul class="comment-children">
						<li>
							<div class="comment-author image-polaroid">
								<img src="img/assets/avatar/avatar-5.png" alt="avatar">
							</div>
							<div class="comment-body">
								<div class="comment-meta">
									<span class="meta-name"><a href="#">John Doe</a></span>
									<span class="meta-date">December 31, 2012 at 11:00 AM</span>
									<div class="reply">
										<a href="#">reply</a>
									</div>
								</div>
								<p>
									Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.
								</p>
							</div>
						</li>
						<li>
							<div class="comment-author image-polaroid">
								<img src="img/assets/avatar/avatar-4.png" alt="avatar">
							</div>
							<div class="comment-body">
								<div class="comment-meta">
									<span class="meta-name"><a href="#">John Doe</a></span>
									<span class="meta-date">December 31, 2012 at 11:00 AM</span>
									<div class="reply">
										<a href="#">reply</a>
									</div>
								</div>
								<p>
									Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.
								</p>
							</div>
						</li>
					</ul>
				</li>
				<li>
					<div class="comment-author image-polaroid">
						<img src="img/assets/avatar/avatar-6.png" alt="avatar">
					</div>
					<div class="comment-body">
						<div class="comment-meta">
							<span class="meta-name"><a href="#">John Doe</a></span>
							<span class="meta-date">December 31, 2012 at 11:00 AM</span>
							<div class="reply">
								<a href="#">reply</a>
							</div>
						</div>
						<p>
							Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.
						</p>
					</div>
				</li>
			</ul>
		</div>
		<h3 class="post-lead">About {Performer} Tour Dates</h3>
		<p>
			The Strokes are an American rock band from New York City, New York, United States, formed in 1998. The band rose to fame in the early 2000s as a leading group in garage rock/post-punk revival. The band consists of Julian Casablancas (lead vocals), Nick Valensi (lead guitar), Albert Hammond, Jr. (rhythm guitar), Nikolai Fraiture (bass guitar) and Fabrizio Moretti (drums and percussion).
		</p>
	</div>
</article>