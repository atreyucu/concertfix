<!-- Latest Review Widget -->
<?php if(isset($allCityEvents) && $allCityEvents) {
	$cityEvents = $allCityEvents;
} ?>
<?php if(isset($cityEvents) && $cityEvents): ?>
<div class="sidebar-widget filter-widget" id="filter-tabs">
	<div class="sidebar-header">
		<h4><?php echo $location['city']; ?> concert filters</h4>
		<div class="separator"></div>
	</div>
	<div class="sidebar-header">
		<ul class="nav nav-tabs" id="tab1">
			<li class="<?php echo ($filterType != 'category') ? 'active' : ''; ?>"><a href="#date">Date</a></li>
			<li class="<?php echo ($filterType == 'category') ? 'active' : ''; ?>"><a href="#genre">Genre</a></li>
		</ul>
	</div>
	<div class="sidebar-content tab-content">
		<div class="sidebar-item tab-pane tag-widget <?php echo ($filterType != 'category') ? 'active' : ''; ?>" id="date">
			<?php
			$curr = date('U');
			$diff = date('j') - 1;
			$curr = strtotime( "-$diff days", $curr );
			?>
			<?php for ($i=0; $i < 12; $i++) : ?>
				<?php
				$now = strtotime( "+{$i} month", $curr );
				$now_adv = getdate($now);
				$now_id = $now_adv["month"] . ":" . $now_adv["year"];
				// var_dump($now_id);
				$link_href = "javascript:;";
				foreach ($cityEvents as $idx => $event) {
					// var_dump();
					$event_date = getdate(strtotime($event->Date));
					$event_id = $event_date["month"] . ":" . $event_date["year"];
					if($event_id == $now_id) {
						$link_href = "\"/concerts/" . $location['slug'] . "+" . strtolower(date('F-Y', $now)) . "\" class=\"active\"";
					}
				}
				// Loop through all events,
				?>
				<a href=<?php echo $link_href; ?> title="<?php echo $location['city']; ?> Concerts in <?php echo date('F Y', $now); ?>"><?php echo date('M Y', $now); ?></a>
			<?php endfor; ?>
		</div>
		<div class="sidebar-item tab-pane tag-widget <?php echo ($filterType == 'category') ? 'active' : ''; ?>" id="genre">
			<?php //var_dump($cityEvents); ?>
			<?php foreach ($categories as $cat): ?>
				<?php
				$cat_id = $cat->ChildCategoryID;
				$link_href = "\"javascript:;\"";
				foreach ($cityEvents as $idx => $event) {
					$event_cat_id = $event->ChildCategoryID;
					if($cat_id == $event_cat_id) {
						$link_href = "\"/concerts/" . $location['slug'] . "+" . $cat->Slug . "\" class=\"active\"";
					}
				}
				// var_dump($cat_id);
				?>
				<a href=<?php echo $link_href; ?> title="<?php echo ucwords($cat->Filter) ?> Concerts in <?php echo $location['city']; ?>"><?php echo $cat->Filter ?></a>
			<?php endforeach ?>
		</div>
	</div>
</div><div style="clear:both;"></div>
<?php endif; ?>
