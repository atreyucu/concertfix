<div class="container footer-container">
	<section class="row">

		<div class="footer-extra span3">
			<div class="footer-box">

				<p><a href="/"><img src="/public/img/assets/logo-fix4-off.png" alt="ConcertFix"></a></p>
				<p class="color"><em>Concert Tickets, Tour Announcements And More!.</em></p>

			</div>
		</div>

		<div class="footer-extra span3">
			<div class="footer-box">
				<address style="margin-top: 20px!important;">
					<!--<p><strong>ConcertFix</strong></p>-->
					<!--<p><span class="awe-home">physical address</span></p>-->
					<p><span class="awe-phone"></span><abbr title="Phone"> P:</abbr> <a href="tel:18554283860" style="color:#fff;">(855) 428-3860</a></p>
					<p><span class="awe-envelope-alt"></span>&nbsp;Ticket Inquiries<br /><a style="color:#929292;" href="mailto:customersupport@concertfix.com"> CustomerSupport@concertfix.com</a></p>
					<p><span class="awe-envelope-alt"></span>&nbsp;General Inquiries<br /><a style="color:#929292;" href="mailto:info@concertfix.com"> info@concertfix.com</a></p>
				</address>
			</div>
		</div>

		<div class="footer-extra span3">
			<div class="footer-box">
				<h4>Quick Links</h4>
				<ul class="toes">
					<li><a href="/user/login">Concert Tracker</a></li>
					<li><a href="/tour-announcements">Tour Announcements</a></li>
					<li><a href="/top-tours">Top Tours</a></li>
					<li><a href="/about-us">About us</a></li>
					<li><a href="/contact-us">Contact Us</a></li>
					<li><a href="/policies">Terms &amp; Policies</a></li>
					<li><a href="/great-deals-on-tickets">Ticket Specials</a></li>
					<li><a href="/ticket-guarantee">Ticket Guarantee</a></li>
					<li><a href="/faq">Help Center</a></li>
				</ul>
			</div>
		</div>

		<div class="footer-extra span3">
			<div class="footer-box">
				<h4>Follow Us</h4>
				<ul class="social-media">
					<li><a href="https://www.facebook.com/pages/Concertfix/653440138041150"><span class="awe-facebook"></span></a></li>
					<li><a href="https://www.twitter.com/concertfix"><span class="awe-twitter"></span></a></li>
					<!-- <li><a href="#"><span class="awe-pinterest"></span></a></li> -->
					<!-- <li><a href="https://plus.google.com/+Concertfix"><span class="awe-google-plus"></span></a></li>-->
					<!-- <li><a href="#"><span class="awe-linkedin"></span></a></li> -->
				</ul>
			</div>
		</div>

	</section>
</div>
<div class="container" role="navigation" id="footer-nav">
	<section class="row">
		<div class="footer-menu">
			<ul>
				<li><a href="/about-us" title="About ConcertFix">About Us</a></li>
				<li><a href="/contact-us" title="Contact ConcertFix">Contact Us</a></li>
				<li><a href="/policies" title="ConcertFix Terms &amp; Policies">Policy</a></li>
			</ul>
		</div>
		<div class="copyright">
			<p>Copyright &copy; <?php echo Date('Y'); ?> ConcertFix. All rights reserved.
			</p>
		</div>
	</section>
</div>
<!-- JS Libs -->

<!--<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js'></script>
<script src='https://code.jquery.com/jquery-1.12.4.min.js'></script>-->

<script>
	//jQuery.noConflict();
</script>
<!-- <script src='/public/js/bootstrap/bootstrap.js'></script>-->
<!--<script src='/public/js/libs/modernizr.js'></script>-->
<!--<script src='/public/js/libs/selectivizr-min.js'></script>-->
<!--<script src='/public/js/plugins/slides.jquery.js'></script> -->
<script src='/public/js/facebook_api.js'></script>
<!--<script src='/public/js/tickets.js'></script>-->
<!--<script src='/public/js/mobile_tickets.js'></script>-->
<!--<script src='/public/js/custom_new.js'></script>-->

<?php if(isset($extra)): ?>
	<?php echo $extra; ?>
<?php endif; ?>

<script type="text/javascript">
	var tn_query = window.location.search.replace('?','&') +"&";
	var tn_ppc_src_start = tn_query.indexOf("&ppcsrc=") + 8;
	if(tn_ppc_src_start  != 7)
	{
		var tn_ppc_src =tn_query.substring(tn_ppc_src_start, tn_query.indexOf('&',tn_ppc_src_start)).replace(/;/g,'');
		var tn_expDate = new Date();
		tn_expDate.setDate(tn_expDate.getDate()+1);
		document.cookie = "tn_ppc_src="+tn_ppc_src +"; expires="+tn_expDate.toGMTString()+"; path=/";
	}
</script> 

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-46612702-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46582605-1', 'concertfix.com');
  ga('send', 'pageview');

</script>

<!-- Google Code for Remarketing Tag -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 977514744;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/977514744/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- End Google Remarketing Tag -->

<div id="fb-root"></div>
