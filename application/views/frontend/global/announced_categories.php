<div class="sidebar-widget" id="sidebar-tabs">
	<div class="sidebar-header">
		<h4>Top Tour Announcements</h4>
		<div class="separator"></div>
		<ul class="nav nav-tabs" id="tab1">
			<?php $cur = true; ?>
			<?php foreach ($categorized as $key => $value): ?>
				<li class="<?php echo ($cur) ? 'active' : ''; ?>"><a href="#<?php echo seoUrl($key); ?>" class=""><?php echo $key; ?></a></li>
				<?php $cur = false; ?>
			<?php endforeach ?>
		</ul>
	</div>
	<div class="sidebar-content tab-content" id="by-genre">
		<?php $cur = true; ?>
		<?php foreach ($categorized as $cat => $items): ?>
		<div class="sidebar-item tab-pane post-widget <?php echo ($cur) ? 'active ' : ''; ?>" id="<?php echo seoUrl($cat); ?>">
			<?php $cur = false; ?>
			<ul>
				<?php $items = array_slice($items, 0,15); ?>
				<?php $i = 0; ?>
				<?php foreach ($items as $item): ?>
				<li class="sidebar-item">
					<a href="/tours/<?php echo $item->PerformerSlug; ?>" class="image-polaroid" title="<?php echo $item->title; ?>">
						<img src="<?php echo performerImage($item, 'thumb') ?>" alt="Image">
					</a>
					<p>
						<a href="/tours/<?php echo $item->PerformerSlug; ?>"><b><?php echo $item->title; ?></b></a>
						<p>
							<?php echo shortenText($item->text, 100); ?>
						</p>
						<?php $i++; ?>
						<span class="post-date"><b>Announced </b><em><?php echo date('F j, Y', strtotime($item->announced_on)); ?></em></span>
					</p>
				</li>
				<?php endforeach ?>
			</ul>
		</div>
		<?php endforeach ?>
	</div>
</div>