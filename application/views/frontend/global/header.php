<?php if (isset($this->cfUser['network'])): ?>
    <?php switch ($this->cfUser['network']):
        case "facebook":
            $pUrl = "https://graph.facebook.com/{$this->cfUser['facebook']['id']}/picture";
            $pName = $this->cfUser['facebook']['first_name'];
            $networkIcon = "facebook";
        break;
        case "twitter":
            $pUrl = $this->cfUser['twitter']['thumb'];
            $pName = $this->cfUser['twitter']['name'];
            $networkIcon = "twitter";
        break;
        case "google":
            $pUrl = $this->cfUser['google']['profile_picture'];
            $pName = $this->cfUser['google']['name'];
            $networkIcon = "google-plus";
        break;
        case "cf":
            $pUrl = ($this->cfUser['cf']['profile_picture']) ? $this->cfUser['cf']['profile_picture'] : '/public/img/noprofile.jpg';
            $pName = $this->cfUser['cf']['name'];
            $networkIcon = "edit";
        break;
        default:
            $pUrl = "Not Logged In";
            $pName = '';
        break;
    endswitch; ?>
<?php else:
    $pUrl = "Not Logged In";
    $pName = '';
 endif ?>


<div id="fb-root"></div>

<div id="nav" class="container">
	<div class="row">

        <!-- Logo & Navigation -->
		<div class="span12" role="navigation" id="mainnav">

            <!-- Site Logo -->
            <div id="logo" class="visible-desktop"><a href="/">ConcertFix</a></div>

            <!-- Navigation Start -->
            <nav id="responsive-nav" class="hidden-desktop">

                <div class="collapse-menu">
                    <a href="/" class="brand">ConcertFix</a>
                    <ul class="user-menu">
                        <li>
                            <a class="phone" href="/" title="Home">
                                <i class="awe-home"></i>
                                <span> Home</span>
                            </a>
                        </li>
                        <?php if(isset($this->cfUser['loggedin']) && $this->cfUser['loggedin']): ?>
                        <li>
                            <a class="phone" href="/user/dashboard" title="My Account">
                                <i class="awe-user"></i>
                                <span> <?php echo $pName; ?></span>
                            </a>
                        </li>
                        <li>
                            <a class="phone" href="/user/logoff" title="Sign Out">
                                <i class="awe-signout"></i>
                                <span> Sign Out</span>
                            </a>
                        </li>
                    <?php else: ?>
                        <li>
                            <a class="phone" href="/user/login">
                                <i class="awe-user"></i>
                                <span> Sign in</span>
                            </a>
                        </li>
                    <?php endif; ?>
                        <li>
                            <a class="phone" href="tel:8554283860" title="Call us">
                                <i class="awe-phone"></i> <span>855-428-3860</span>
                            </a>
                        </li>
                    </ul>
                    <span class="collapse-trigger icon awe-reorder" style="font-size: 25px"></span>
                </div>

                <ul class="first-level dropdown-nav">
                    <form class="search" method="post" action="/search">
                        <input id="top_search_m" class="input-large" type="text" placeholder="Search…" name="query">
                        <div id="jaxResults_topm" class="smokedline module input-large jaxResults" style="display:none;"></div>
                    </form>
                    <?php
                    $i = 0;
                    foreach ($this->menuLinks as $link): ?>
                        <li <?php echo ($i == 0) ? "class='current'" : '';?> >
                            <?php if (count($link->children)) :?>
                                <a href="#">
                            <?php else :?>
                                <a href="<?php echo $link->menu_link ?>">
                            <?php endif;?>
                                    <span><?php echo $link->menu_title ?></span>
                                    <?php echo (count($link->children)) ? '<span class="icon awe-chevron-down"></span>' : ''; ?>
                                </a>
                            <?php $i++; ?>
                            <?php if(count($link->children)): ?>
                                <ul class="second-level">
                                    <?php foreach ($link->children as $child): ?>
                                        <li>
                                            <?php if (count($child->children)) :?>
                                                <a href="#">
                                                    <span><?php echo $child->menu_title ?></span>
                                                    <?php echo (count($child->children)) ? '<span class="icon awe-chevron-right"></span>' : ''; ?>
                                                </a>
                                            <?php else :?>
                                                <a href="<?php echo $child->menu_link ?>">
                                                    <span><?php echo $child->menu_title ?></span>
                                                    <?php echo (count($child->children)) ? '<span class="icon awe-chevron-right"></span>' : ''; ?>
                                                </a>
                                            <?php endif;?>

                                            <?php if(count($child->children)): ?>
                                                <ul class="third-level">
                                                    <?php foreach ($child->children as $grand): ?>
                                                        <li>
                                                            <a href="<?php echo $grand->menu_link ?>">
                                                                <span><?php echo $grand->menu_title ?></span>
                                                            </a>
                                                        </li>
                                                    <?php endforeach ?>
                                                </ul>
                                            <?php endif; ?>
                                        </li>
                                    <?php endforeach ?>
                                </ul>
                            <?php endif; ?>
                        </li>
                    <?php endforeach ?>
                </ul>

            </nav>

            <nav id="main-navigation" class="visible-desktop">
                <ul class="first-level">
                    <?php
                     $i = 0;
                     foreach ($this->menuLinks as $link): ?>
                        <li <?php echo ($i == 0) ? "class='current'" : '';?> >
                            <a href="<?php echo $link->menu_link ?>">
                                <span><?php echo $link->menu_title ?></span>
                                <?php echo (count($link->children)) ? '<span class="icon awe-chevron-down"></span>' : ''; ?>
                            </a>
                            <?php $i++; ?>
                            <?php if(count($link->children)): ?>
                                <ul class="second-level">
                                <?php foreach ($link->children as $child): ?>
                                    <li>
                                        <a href="<?php echo $child->menu_link ?>">
                                            <span><?php echo $child->menu_title ?></span>
                                            <?php echo (count($child->children)) ? '<span class="icon awe-chevron-right"></span>' : ''; ?>
                                        </a>
                                        <?php if(count($child->children)): ?>
                                            <ul class="third-level">
                                                <?php foreach ($child->children as $grand): ?>
                                                    <li>
                                                        <a href="<?php echo $grand->menu_link ?>">
                                                            <span><?php echo $grand->menu_title ?></span>
                                                        </a>
                                                    </li>
                                                <?php endforeach ?>
                                            </ul>
                                        <?php endif; ?>
                                    </li>
                                <?php endforeach ?>
                                </ul>
                            <?php endif; ?>
                        </li>
                    <?php endforeach ?>
                    </ul>
                    <ul class="pull-right">
                    <?php if(isset($this->cfUser['loggedin']) && $this->cfUser['loggedin']): ?>
                    <li class="user-menu">
                        <a href="/user/dashboard" title="My Account"><i class="awe-user"></i><span> <?php echo $pName; ?><span>
                            <?php if ($this->cfUser['network'] != 'cf'): ?>
                                <img src="<?php echo $pUrl;?>" style="width:50px;height:50px;" alt=""></a>
                            <?php endif; ?>
                    </li>
                    <li class="user-menu">
                        <a href="/user/logoff" title="Sign Out">
                            <i class="awe-signout"></i>
                            <span>Sign Out</span>
                        </a>
                    </li>
                    <?php else: ?>
                    <li class="user-menu">
                        <a href="/user/login" title="My Account">
                            <i class="awe-user"></i>
                            <span>Sign in </span>
                        </a>
                    </li>
                    <?php endif; ?>

                </ul>
                </nav>
                <nav id="secondary-navigation" class="visible-desktop">
                <div class="social-media">
                    <!--facebook button-->
                    <div class="fb1">
                        <div class="fb-share-button width60" data-type="button"></div>
                    </div>
                    <div class="tw1">
                        <!--twitter button-->
                        <a href="https://twitter.com/share" class="twitter-share-button" data-lang="en" data-count="none">Tweet</a>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                    </div>

                </div>

                <div class="pull-right">
                    <ul>
                        <li>
                            <ul>
                                <li>
                                    <a class="phone" href="tel:8554283860">
                                        <i class="awe-phone"></i> <span class="mob-dis">855-428-3860</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <form class="search" method="post" action="/search">
                                <input id="top_search_d" class="input-xlarge" type="text" placeholder="Search…" name="query">
                                <div id="jaxResults_topd" class="smokedline module input-xlarge jaxResults" style="display:none;"></div>
                            </form>
                        </li>
                    </ul>
                </div>
            </nav>
            <!-- Navigation End -->
		</div>
	</div>
</div>
