<!-- Latest Review Widget -->
<div class="sidebar-widget" id="sidebar-tabs">
	<div class="sidebar-header">
		<h4>Top Tour Cites</h4>
		<div class="separator"></div>
	</div>
	<div class="sidebar-header">
		<ul class="nav nav-tabs" id="tab1">
			<?php $cur = true; ?>
			<?php arsort($cities); ?>
			<?php foreach ($cities as $area => $data): ?>
			<li class="<?php echo ($cur) ? 'active' : ''; ?>"><a href="#<?php echo $area ?>"><?php echo $area ?></a></li>
			<?php $cur = false; ?>
			<?php endforeach ?>
		</ul>
	</div>
	<div class="sidebar-content tab-content">
		<?php $cur = true; ?>
		<?php foreach ($cities as $area => $data): ?>
			<div class="sidebar-item tab-pane tag-widget <?php echo ($cur) ? 'active' : ''; ?>" id="<?php echo $area; ?>">
				<?php $cur = false; ?>
				<?php if($data): ?>
					<?php foreach ($data as $city): ?>
						<a href="/concerts/<?php echo $city->slug ?>" class="active"><?php echo $city->city ?></a>
					<?php endforeach ?>
				<?php endif; ?>
			</div>
		<?php endforeach ?>
	</div>
</div>
