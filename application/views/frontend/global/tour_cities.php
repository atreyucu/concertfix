<?php if ($tourDates): ?>
<?php $uniqueTourCities = array(); ?>
<?php foreach ($tourDates as $tour): ?>
	<?php // performer-venue-city ?>
	<?php $key = seoUrl(slug_venue($tour->Venue)).'+'.seoUrl($tour->City.' '.$tour->StateProvince); ?>
	<?php // $key = seoUrl($tour->City." ".$tour->StateProvince); ?>
	<?php $uniqueTourCities[$key] = $tour->City.", ".$tour->StateProvince; ?>
<?php endforeach ?>
<div class="sidebar-widget" id="sidebar-tabs">
	<div class="sidebar-header">
		<ul class="nav nav-tabs" id="tab1">
			<li class="active"><a href="#tags">Tour Cities</a></li>
		</ul>
	</div>
	<div class="sidebar-content tab-content">
			<div class="sidebar-item tab-pane active tag-widget" id="tags">
			<?php foreach ($uniqueTourCities as $tslug => $tname): ?>
				<a rel="tag" href="/tours/<?php echo $performer[0]->PerformerSlug; ?>+<?php echo $tslug; ?>" class="active"><?php echo $tname; ?></a>
			<?php endforeach; ?>
			</div>
	</div>
</div>
<?php endif ?>
