<?php if ($performers): ?>
    <div class="searchHead">
        <i class="icon-star-empty"></i> Artists
    </div>
    <div class="smokedline module">
        <ul class="comment-list">
            <?php foreach ($performers as $performer): ?>
                <li class="comment"><a href="/tours/<?php echo $performer->PerformerSlug; ?>"
                                       title="<?php echo $performer->Name ?>">
                        <?php echo $performer->Name ?></a></li>
            <?php endforeach ?>
        </ul>
    </div>
<?php endif; ?>

<?php if ($cities): ?>
    <div class="searchHead">
        <i class="icon-star-empty"></i> Cities
    </div>
    <div class="smokedline module">
        <ul class="comment-list">
            <?php foreach ($cities as $city): ?>
                <li class="comment"><a href="/concerts/<?php echo seoUrl($city->slug); ?>"
                                       title="<?php echo $city->value ?>">
                        <?php echo $city->value ?></a></li>
            <?php endforeach ?>
        </ul>
    </div>
<?php endif; ?>

<?php if ($venues): ?>
    <div class="searchHead">
        <i class="icon-star-empty"></i> Venues
    </div>
    <div class="smokedline module">
        <ul class="comment-list">
            <?php foreach ($venues as $venue): ?>
                <li class="comment"><a href="/concerts/<?php echo $venue->RegionSlug.'+'.$venue->VenueSlug ?>">
                        <?php echo $venue->Name ?></a></li>
            <?php endforeach ?>
        </ul>
    </div>
<?php endif; ?>