 <?php if($lastAlbum): ?>
	<div class="sidebar-widget" id="album-side">
		<div class="sidebar-header">
			<h4><?php echo $performer[0]->PerformerName; ?> Top Tour Album</h4>
			<div class="separator"></div>
		</div>
		<div class="sidebar-content post-widget">
			<div class="albart image-polaroid">
				<?php if($lastAlbum['image'] != '' and $lastAlbum['image'] != null):?>
					<img src="<?php echo $lastAlbum['image'] ?>" height="150" width="150"
						 alt="<?php echo $performer[0]->PerformerName?>: <?php echo $lastAlbum['name']; ?>">
				<?php else:?>
					<img src="/public/img/album/photo_not_available.png" height="150" width="150">
				<?php endif?>
			</div>
			<div class="albdescription">
				<h5><?php echo $performer[0]->PerformerName; ?></h5>
				<span><b><?php echo $lastAlbum['name']; ?></b></span>

				<?php if (isset($lastAlbum['releasedate'])) : ?>
					<p>Released: <?php echo date('M j, Y', strtotime($lastAlbum['releasedate'])) ?></p>
				<?php else :?>
					<p></p>
		 		<?php endif ?>

				<a href="#morealbums">View More Albums</a>
			</div>
		</div>
	</div>
	<div style="clear:both;"></div>
<?php endif; ?>