<?php if($tourDatesVenue): ?>
<!-- Latest Review Widget -->
<div class="sidebar-widget" id="venue-side">
	<div class="sidebar-content post-widget">
			<a name="mapa"></a>
			<div class="venue-directions" id="map_canvas"></div>

		<div class="vendescription">
			<div class="pull-left">
				<span><b>Venue Location</b></span>
				<?php $rawAddy = $venue->Street1.", ".$venue->Street2.", ".$venue->City.", ".$venue->StateProvince.", ".$venue->ZipCode; ?>
				<p><?php echo $venue->Street1." ".$venue->Street2."<br /> ".$venue->City.", ".$venue->StateProvince." - ".$venue->ZipCode; ?></p>
			</div>
			<div class="pull-right">
				<a target="_blank" href="https://maps.google.com/maps?q=<?php echo urlencode($rawAddy) ?>">Get Directions</a> <br>
			</div>
		</div>
	</div>
</div>
<div style="clear:both;"></div>
<?php endif; ?>
