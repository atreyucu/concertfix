<?php $position = 0; ?>
<script type="application/ld+json"> {
    "@context": "https://schema.org",
    "@type": "BreadcrumbList",
    "itemListElement":
    [
        <?php foreach ($listItem as $item): ?>
            {
                "@type": "ListItem",
                "position": "<?php echo $position += 1; ?>",
                "item": {
                    "@id": "<?php echo $item['url'] ?>",
                    "name": "<?php echo strtr($item['name'], array('"'=>"'")) ?>"
                }
            } <?php echo ($position < count($listItem) ? ',' : '')?>

        <?php endforeach; ?>
    ]
}
</script>