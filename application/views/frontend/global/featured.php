<?php if ($sidebarFeatured): ?>
<div class="sidebar-widget">
	<div class="sidebar-header">
		<h4>Trending Tours</h4>
		<div class="separator"></div>
	</div>
	<div class="sidebar-item tab-pane active post-widget" id="comments">
		<ul>
			<?php foreach ($sidebarFeatured as $featured): ?>
				<li class="sidebar-item">
					<a href="/tours/<?php echo $featured->PerformerSlug; ?>" class="perform-image image-polaroid">
						<img src="<?php echo performerImage($featured, 'thumb'); ?>" alt="<?php echo $featured->Name ?>">
						<div class="img-label">
							<p class="performer-name"><?php echo $featured->Name ?></p>
						</div>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>
<?php endif ?>