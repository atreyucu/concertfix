<div style="clear:both;"></div>
<div class="sidebar-widget" id="twitter-feed">
	<div class="sidebar-header">
		<h4><?php echo (isset($performer[0]->PerformerName)) ? $performer[0]->PerformerName : ''; ?> Twitter News</h4>
		<div class="separator"></div>
	</div>
	<div class="timeline-header">
		<a href="http://twitter.com/concertfix"><button class="btn btn-info pull-right"><i class="awe-twitter"></i> Follow Us</button></a>
		<h3><a href="http://twitter.com/concertfix">Tweets</a></h3>
	</div>
	<div class="comments">
		<ul class="commentlists">
		<?php foreach ($twitterFeed->statuses as $item): ?>
			<li>
				<div class="comment-author image-polaroid">
					<a href="https://twitter.com/<?php echo $item->user->screen_name; ?>" target="_blank" title="Follow @<?php echo $item->user->screen_name; ?>"><img src="<?php echo $item->user->profile_image_url_https; ?>" style="width:50px; height:50px;" alt="avatar"></a>
				</div>
				<div class="comment-body">
					<div class="comment-meta">
						<span class="meta-name"><a href="https://twitter.com/<?php echo $item->user->screen_name; ?>" target="_blank" title="Follow @<?php echo $item->user->screen_name; ?>"><?php echo $item->user->name." - @".$item->user->screen_name; ?></a></span>
						<span class="meta-date"><?php echo date('m j, Y H:i:s', strtotime($item->created_at)) ?></span>
						<!-- <div class="reply">
							<a href="#">reply</a>
						</div> -->
					</div>
					<p><?php echo twitter_clean($item->text); ?></p>
				</div>
			</li>
		<?php endforeach ?>
		</ul>
	</div>
	<!--code insertion point-->

	<div class="timeline-footer">
		<a class="tweet-box" href="https://twitter.com/share">#<?php echo str_replace(' ', '', $performer[0]->PerformerName) ?> </a>
	</div>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
</div>

