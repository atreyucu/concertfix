<!-- Sidebar Block Widget -->
<div class="sidebar-widget sidebar-block sidebar-color">
	<div class="sidebar-header">
		<h4>Concert Tracker</h4>
		<div class="pull-left marg-right" style="width:89px;">
			<img src="/public/img/assets/concert-tracker-2.png" alt="track your favorite concerts" />
		</div>
		<p>
			<ul style="list-style: outside none none;margin: 0;padding: 0;font-size: 10px;">
				<li style="background: transparent url('/public/img/assets/footer-bullet.png') no-repeat scroll left 7px;
				padding-left: 15px;">Follow your favorite performers and cities</li>
				<li style="background: transparent url('/public/img/assets/footer-bullet.png') no-repeat scroll left 7px;
				padding-left: 15px;">Receive alerts when new shows are announced</li>
				<li style="background: transparent url('/public/img/assets/footer-bullet.png') no-repeat scroll left 7px;
				padding-left: 15px;">Get updates for the latest concert schedules</li>
				<li style="background: transparent url('/public/img/assets/footer-bullet.png') no-repeat scroll left 7px;
				padding-left: 15px;">Never miss a show again!</li>
			</ul>
		</p>

	</div>
	<div class="sidebar-content login-widget" id="signup-form">
		<!-- <form>
			<div class="input-prepend">
				<span class="add-on "><i class="icon-user"></i></span>
				<input type="email" id="ml_name" class="input-large" placeholder="Your Name">
			</div>
			<div class="input-prepend">
				<span class="add-on "><i class="icon-lock"></i></span>
				<input type="password" id="ml_email" class="input-large" placeholder="Your Email">
			</div>
 			<div class="input-prepend">
				<span class="add-on "><i class="icon-lock"></i></span>
				<input type="password" class="input-large" placeholder="password">
			</div> 
 			<label class="checkbox">
			<input type="checkbox">
				Remember me
			</label> -->
			<div class="controls">
				<!--button class="btn btn-flatten btn-inverse signin" id="signup" type="submit">Sign In</button-->
				<a href="/user/login" style="color:#fff;margin-right:60px;" class="pull-right"><button class="btn btn-flatten btn-inverse">Sign up</button></a>
			</div>
			<!--<div id="signupnotice"></div>
		</form>-->
	</div>
</div><div style="clear:both;"></div>