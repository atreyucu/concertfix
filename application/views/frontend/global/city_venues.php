<div class="sidebar-widget" id="sidebar-tabs">
	<div class="sidebar-header">
		<h4><?php echo $location['city']; ?> Venues and nearby cities</h4>
		<div class="separator"></div>
	</div>
	<div class="sidebar-header">
		<ul class="nav nav-tabs" id="tab1">
			<li class="active"><a href="#venue">Venues</a></li>
			<li class=""><a href="#cities">Nearby Cities</a></li>
		</ul>
	</div>
	<div class="sidebar-content tab-content">
		<div class="sidebar-item tab-pane tag-widget active" id="venue">
			<?php foreach ($cityVenues as $venue): ?>
			<?php if ($venue->cnt > 0): ?>
				<a href="/concerts/<?php echo $location['slug']."+".$venue->VenueSlug; ?>" class="active" title="<?php echo clean_venue($venue->Name); ?> Concert Schedule"><?php echo clean_venue($venue->Name); ?></a>
			<?php endif ?>
			<?php endforeach ?>
			<div style="clear:both;"></div>
			<a href="/concerts/<?php echo $location['slug'] ?>+venues" class="active" title="All Venues in <?php echo $location['city']; ?>">All Venues in <?php echo $location['city']; ?></a>
		</div>
		<div class="separator"></div>
		<div class="sidebar-item tab-pane tag-widget" id="cities">
			<?php if($nearbyCities): ?>
				<?php foreach ($nearbyCities as $city): ?>
					<a href="/concerts/<?php echo slug_location($city); ?>" class="active" title="<?php echo $city['city'] ?> Concerts"><?php echo $city['city'] ?></a>
				<?php endforeach ?>
			<?php endif; ?>
		</div>
	</div>
</div><div style="clear:both;"></div>
