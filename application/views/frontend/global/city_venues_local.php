<div class="sidebar-widget" id="sidebar-tabs">
	<div class="sidebar-header">
		<h4><?php echo $location['city']; ?> Nearby cities</h4>
		<div class="separator"></div>
	</div>
	<div class="sidebar-header">
		<ul class="nav nav-tabs" id="tab1">
			<li class="active"><a href="#cities">Nearby Cities</a></li>
		</ul>
	</div>
	<div class="sidebar-content tab-content">

		<div class="sidebar-item tab-pane tag-widget active" id="cities">
			<?php if($nearbyCities): ?>
				<?php foreach ($nearbyCities as $city): ?>
					<a href="/concerts/<?php echo slug_location($city); ?>" class="active" title="<?php echo $city['city'] ?> Concerts"><?php echo $city['city'] ?></a>
				<?php endforeach ?>
			<?php endif; ?>
		</div>
	</div>
</div><div style="clear:both;"></div>
