<div class="sidebar-widget">
	<div class="sidebar-header">
		<h4><a href="/concerts/<?php echo $location['slug'] ?>">Upcoming Concerts in <?php echo $location['city'].", ".$location['state']; ?></a></h4>
		<div class="separator"></div>
	</div>
	<div class="sidebar-item tab-pane active post-widget" id="comments">
		<ul>
			<?php foreach ($upcomingConcerts as $p): ?>
			<li class="sidebar-item">
				<a href="/tours/<?php echo $p->PerformerSlug ?>+<?php echo $p->event->VenueSlug ?>+<?php echo $p->event->RegionSlug; ?>" class="perform-image image-polaroid">
					<!-- <span style="display:inline-block;height:100%;vertical-align:middle;"></span> -->
					<img src="<?php echo performerImage($p, 'thumb'); ?>" alt="<?php echo $p->PerformerName ?>" style="vertical-align:middle;display:inline-block;overflow:hidden;">
					<div class="img-label">
						<p class="performer-name"><?php echo $p->PerformerName ?> <br><?php echo date('M j', strtotime($p->Date)); ?></p>
					</div>
				</a>
			</li>
			<?php endforeach ?>
		</ul>
	</div>
</div>