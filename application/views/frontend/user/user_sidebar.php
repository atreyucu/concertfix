<?php switch ($this->cfUser['network']):
	case "facebook":
		$pUrl = "https://graph.facebook.com/{$this->cfUser['facebook']['id']}/picture";
		$pName = $this->cfUser['facebook']['first_name'];
	break;
	case "twitter":
		$pUrl = $this->cfUser['twitter']['thumb'];
		$pName = $this->cfUser['twitter']['name'];
	break;
	case "google":
		$pUrl = $this->cfUser['google']['profile_picture'];
		$pName = $this->cfUser['google']['name'];
	break;
	case "cf":
		$pUrl = ($this->cfUser['cf']['profile_picture']) ? $this->cfUser['cf']['profile_picture'] : '/public/img/noprofile.jpg';
		$pName = $this->cfUser['cf']['name'];
	break;
	default:
		$pUrl = "Not Logged In";
		$pName = '';
	break;
endswitch;
		?>
<!-- Latest Review Widget -->

<div class="sidebar-widget" id="latest-reviews">
	<div class="sidebar-header">
		<h4>Hey <?php echo $pName ?>!</h4>
		<div class="separator"></div>
	</div>
	<div class="sidebar-content post-widget">
		<ul>
			<li class="sidebar-item">
				<?php if ($this->cfUser['network'] != 'cf'): ?>

				<a href="#" class="image-polaroid" title="<?php echo $this->cfUser['network'] ?>">
				 <img src="<?php echo $pUrl; ?>" style="width:100px;" alt=""></a>
				<?php endif ?>
				 <div class="pull-right"><?php echo ($this->cfUser['network'] == 'cf') ? "Registerd User" : "Via {$this->cfUser['network']}" ?></div>
			</li>
		</ul>
	</div>
</div><div style="clear:both;"></div>
<br>
<!-- Latest Review Widget -->
<div class="sidebar-widget sidebar-block">
	<div class="sidebar-header">
		<h4>My Account</h4>
		<div class="separator"></div>
	</div>
	<div class="sidebar-content menu-widget">
		<ul>
			<li class="current">
				<a href="/user/dashboard"><span class="ent-rss"></span> Dashboard</a>
			</li>
			<li>
				<a href="/user/rules" title="Title"><span class="ent-list"></span> Concert Tracker</a>
			</li>
			<li>
				<a href="/user/settings" title="Title"><span class="awe-cogs"></span> Account Settings</a>
			</li>
			<li>
				<a href="/user/logoff" title="Title"><span class="ent-logout"></span> Log Out</a>
			</li>
		</ul>
	</div>
</div><div style="clear:both;"></div>

<div class="ruleNotification"></div>
