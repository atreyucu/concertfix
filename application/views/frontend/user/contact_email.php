
<form action="" method="post" class="form form-inline">
	<fieldset>
		<legend>Choose Primary Email</legend>
		<label for="email">Enter a valid email address to continue</label>
		<input type="email" name="email" id="email" />
		<button type="submit" class="btn btn-primary">Add Email</button>
	</fieldset>
</form>
