<?php if (isset($user)):?>
<form action="" method="post" name="change-password">
	<legend>Reset Password</legend>
	<span class="help-block">You can use your registered email and password to login into ConcertFix</span>
	<label for="new_password1">New Password:</label>
	<input type="password" name="new_password1" id="new_password1">
	<label for="new_password2">Repeat Password:</label>
	<input type="password" name="new_password2" id="new_password2">
	<br>
	<button type="submit" class="btn btn-primary">Change!</button>
</form>
<?php else: ?>
<form action="" method="post" class="form form-inline">
	<fieldset>
		<legend>Password Reset</legend>
		<label for="email">Enter Your Email Please</label>
		<input type="text" name="email" id="email" />
		<button type="submit" class="btn btn-primary">Request Reset Link</button>
	</fieldset>
</form>
<?php endif; ?>