<div class="row-fluid">
	<div class="span6">
		<h1>Concert Tracker</h1>

		<ul>
			<li>Follow your favorite performers and cities</li>
			<li>Receive email alerts when new shows are announced in your city</li>
			<li>Create your own custom concert schedules updated instantly</li>
			<li>Never miss a show again</li>
		</ul>
		<br /><br />
		<h3>Login To Your ConcertFix Account</h3>

		<p style="font-size: 14px;" class="usmob-dis">or create a new account <a style="color:#00A6C5;" href="#register">here</a></p>
		<form action="/user/process_login/cf" method="post">
			<label for="user_email">Email:</label>
			<input type="text" name="user_email" />
			<label for="password">Password:</label>
			<input type="password" name="password">
			<input type="hidden" name="next" value="<?php echo $next?>">
			<br>
			<input type="submit" class="btn btn-primary"> <a class="btn btn-link" href="/user/passreset">I Forgot The Password!</a>
		</form>
		<h3>Login With Your Favorite Social Network</h3>
		<a class="btn btn-primary" href="<?php echo $urls['fbLoginUrl'] ?>"><span class="awe-facebook"></span> Facebook</a>&nbsp;
		<a class="btn btn-info" href="<?php echo $urls['twLoginUrl'] ?>"><span class="awe-twitter"></span> Twitter</a>&nbsp;
		<!--<a class="btn btn-danger" href="--><?php //echo $urls['gpLoginUrl']; ?><!--"><span class="awe-google-plus"></span> Google</a>&nbsp;-->
		<br />
	</div>
	<a name="register"></a>
	<hr class="usmob-dis" />
	<div class="span6">
		<h3>Register For ConcertFix</h3>

		<div id="reg-field">
			<form action="ajax-submit" id='reg-form'>
				<div class="control-group reg-name-group">
					<label for="reg-name" class="control-label">Your Name</label>
					<div class="controls">
						<input type="text" class='validator' name="reg_name" id="reg-name" placeholder="">
						<span class="help-inline reg-name-error"></span>
					</div>
				</div>
				<div class="control-group reg-email-group">
					<label for="reg-email" class="control-label">Your Email</label>
					<div class="controls">
						<input type="email" name="reg_email" id="reg-email" class='validator' required placeholder="">
						<span class="help-inline reg-email-error"></span>
					</div>
				</div>
				<div class="control-group reg-password-group">
					<label for="reg-password"  class="control-label">Choose Password</label>
					<div class="controls">
						<input type="password" class='validator' name="reg_password" id="reg-password">
						<span class="help-inline reg-password-error"></span>
					</div>
				</div>
				<div class="control-group reg-password2-group">
					<label for="reg-password2" class="control-label">Password Again</label>
					<div class="controls">
						<input type="password" class='validator' name="reg_password2" id="reg-password2">
						<span class="help-inline reg-password2-error"></span>
					</div>
				</div>

				<div class="control-group reg-birthday-group">
					<label for="">Birthday</label>
					<div class="controls">
						<select name="reg-month" id="reg-month" class="input-mini validator" >
							<?php
								$i = 1;
								$month = strtotime('2011-01-01');
								while($i <= 12)
								{
								    $month_name = date('M', $month);
								    $month_num = date('m', $month);
								    echo '<option value="'. $month_num. '">'.$month_name.'</option>';
								    $month = strtotime('+1 month', $month);
								    $i++;
								}
							 ?>
						</select>
						<select name="reg-day" class="input-mini validator" id="reg-day">
							<?php for ($i=1; $i < 32; $i++):?>
								<option value="<?php echo $i ?>"><?php echo $i ?></option>
							<?php endfor; ?>
						</select>
						<select name="reg-year" class="input-small validator"  id="reg-year">
							<?php $thisYear = date('Y'); ?>
							<?php for ($i=1920; $i < $thisYear; $i++):?>
								<option value="<?php echo $i ?>"><?php echo $i ?></option>
							<?php endfor; ?>
						</select>
					</div>
					<div class="control-group">
						<div class="pull-left gender-checks">
						<label class=" radio">
							<input type="radio" name="gender" value="male" class="gender" id="gender-male" checked="checked"> Male
						</label>
						</div>&nbsp;
						<div class="pull-left gender-checks">
						<label class=" radio">
							<input type="radio" name="gender" value="Female" class="gender" id="gender-Female"> Female
						</label>
						</div>&nbsp;
						<div class="pull-left gender-checks">
						<label class=" radio">
							<input type="radio" name="gender" value="Neither" class="gender" id="gender-Neither"> Other
						</label>
						</div>
					</div>
				</div>
				<hr style="opacity: 0">
				<!-- <input type="text" name="reg_birthdate" id="reg-birthday"> -->
				<button type="button" disabled="disabled" id='submit-registration-form' class="btn">Sign Up</button>
			</form>
		</div>
	</div>
	<div class="span12" style="height: 30px;"></div>
	<div class="span5" style="margin-left: 0px;"><img alt="ss1" style="height: 520px" src="/public/img/tracker/ss1.jpg"></div>
	<div class="span4" style="margin-left: 3%"><img alt="ss2" style="height: 520px" src="/public/img/tracker/ss2.jpg"></div>
	<div class="span3" style="margin-left: 2%"><img alt="ss3" style="height: 520px" src="/public/img/tracker/ss3.jpg"></div>
</div>
