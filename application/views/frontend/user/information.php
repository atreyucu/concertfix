<table style="width:100%" align="center" class="w3-table-all">
    <tbody>
        <tr>
            <th>Performers Name</th>
        </tr>

        <?php foreach ($performers as $performer) :?>
            <tr>
                <td><?php echo $performer->performerName?></td>
            </tr>
        <?php endforeach?>

    </tbody>
</table>

<table style="width:100%" align="center" class="w3-table-all">
    <tbody>
    <tr>
        <th>Cities</th>
    </tr>

    <?php foreach ($cities as $cities) :?>
        <tr>
                <td><?php echo $cities?></td>
        </tr>
    <?php endforeach?>

    </tbody>
</table>

<table style="width:100%" align="center" class="w3-table-all">
    <tbody>
    <tr>
        <th>New Concerts in Home City</th>
    </tr>
    <tr>
        <td><label class="radio">
                <input disabled type="radio" name="newconcertcity" class="newconcertcity" <?php if(count($user) > 0 && $user->new_concert == 1): echo 'checked'; endif?> value="1"> Once a day
            </label></td>
    </tr>
    <tr>
        <td><label class="radio">
                <input disabled type="radio" name="newconcertcity" class="newconcertcity" <?php if((count($user) > 0 && $user->new_concert == 2) || (count($user) == 0)): echo 'checked'; endif?> value="2" > Once a week
            </label></td>
    </tr>
    <tr>
        <td><label class="radio">
                <input disabled type="radio" name="newconcertcity" class="newconcertcity" <?php if(count($user) > 0 && $user->new_concert == 3): echo 'checked'; endif?> value="3"> Once a month
            </label></td>
    </tr>
    <tr>
        <td><label class="radio">
                <input disabled type="radio" name="newconcertcity" class="newconcertcity" <?php if(count($user) > 0 && $user->new_concert == 0): echo 'checked'; endif?> value="0"> Never
            </label></td>
    </tr>
    </tbody>
</table>

<table style="width:100%" align="center" class="w3-table-all">
    <tbody>
    <tr>
        <th>Upcoming Concerts in Home City</th>
    </tr>
    <tr>
        <td><label class="radio">
                <input disabled type="radio" name="upcomingconcertcity" class="upcomingconcertcity" <?php if(count($user) > 0 && $user->upcoming_concert == 0): echo 'checked'; endif?> value="0"> Never
            </label></td>
    </tr>
    <tr>
        <td><label class="radio">
                <input disabled type="radio" name="upcomingconcertcity" class="upcomingconcertcity" <?php if((count($user) > 0 && $user->upcoming_concert == 1) || (count($user) == 0)): echo 'checked'; endif?> value="1" > Once a week
            </label></td>
    </tr>
    </tbody>
</table>

<table style="width:100%" align="center" class="w3-table-all">
    <tbody>
    <tr>
        <th>Genre(s)</th>
    </tr>
    <?php foreach ($categories as $category) :?>
        <tr>
            <td><?php echo $category?></td>
        </tr>
    <?php endforeach?>
    </tbody>
</table>




