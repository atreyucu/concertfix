<a class="pull-right btn btn-info btn-flatten" type="button" href="/user/rules"><span class="ent-plus-3"></span> Create New Tracking Alert</a>
<h1>Dashboard</h1>
<p>Check out the most popular concert performers in their genre
	and find out where they stack up against other great concert
	tours. See the top concert tours for each genre including
	country, rap, pop, rock, electronic, and latin.</p>
<div class="separator"></div>
<h2>Performers Near You:</h2>
<?php if ($performers): ?>
<?php $currentDate = false; ?>
<div class="table" id="city-events">
	<?php foreach ($performers as $event): ?>
	<?php if (date('y-m-d', strtotime($event->Date)) != $currentDate): ?>
		<div class="date-events">
			<h3><i class="awe-calendar"></i> <?php echo date('F d, Y', strtotime($event->Date)); ?></h3>
		</div>
	<?php $currentDate = date('y-m-d', strtotime($event->Date)); ?>
	<?php endif ?>
	<div class="eventblock">
		<div class="event-thumbnail image-polaroid">
			<img src="<?php echo performerImage($event->performers); ?>">
		</div>
		<div class="event-cta">
<!-- 			<button class="btn btn-info" href="http://facebook.com">RSVP on
	<i class="ent-facebook-3"></i>
</button> -->

			<a class="btn btn-info" href="/tickets/<?php echo $event->EventID; ?>">TICKETS
				<i class="icon ent-ticket"></i>
			</a>
			<br>
			<div class="fb-share-button" data-href="<?php echo base_url(); ?>/tours/<?php echo $event->performers[0]->PerformerSlug ?>+<?php echo slug_venue($event->Venue) ?>+<?php echo $event->LocationSlug  ?>" data-type="button"></div>
			<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo base_url(); ?>/tours/<?php echo $event->performers[0]->PerformerSlug ?>+<?php echo slug_venue($event->Venue) ?>+<?php echo $event->LocationSlug  ?>" data-lang="en">Tweet</a>
			<br>
			<span><?php echo ($event->prices->ticketsAvailable); ?>  tickets left starting from $<?php echo number_format($event->prices->lowPrice); ?> </span>
		</div>
		<div class="event-details">
			<h4><a href="/tours/<?php echo $event->performers[0]->PerformerSlug ?>+<?php echo slug_venue($event->Venue) ?>+<?php echo $event->LocationSlug  ?>"><?php echo $event->performers[0]->PerformerName;  ?></a></h4>
				<p><b>Venue:</b> <?php echo clean_venue($event->Venue) ?></p>
				<p><b>Time:</b> <?php echo date('F d, Y H:i', strtotime($event->Date)); ?></p>
				<p><b>City:</b> <?php echo $event->City.", ".$event->StateProvince; ?></p>

		</div>
	</div>
	<?php endforeach ?>
</div>
<?php else: ?>
	<div class="alert">
		<h3>No events based on your performers and cities.</h3>
	</div>
<?php endif; ?>
<?php /*
<h2>By Genre</h2>
<?php if ($genres): ?>
<?php $currentGenreId = false; ?>
<div class="table" id="city-events">
	<?php foreach ($genres as $event): ?>
	<?php if ($event->ChildCategoryID != $currentGenreId): ?>
		<div class="date-events">
			<h3><i class="awe-note"></i> <?php echo $event->Filter; ?></h3>
		</div>
	<?php $currentGenreId = $event->ChildCategoryID; ?>
	<?php endif ?>
	<div class="eventblock">
		<div class="event-thumbnail image-polaroid">
			<img src="<?php echo performerImage($event->performers); ?>">
		</div>
		<div class="event-cta">
			<button class="btn btn-info" href="http://facebook.com">RSVP on
				<i class="ent-facebook-3"></i>
			</button>
			<button class="btn btn-info" href="/tickets/<?php echo $event->EventID; ?>">TICKETS
				<i class="icon ent-ticket"></i>
			</button><br>
			<span>Tickets from $42</span>
		</div>
		<div class="event-details">
			<h4><a href="/tours/<?php echo $event->performers[0]->PerformerSlug ?>+<?php echo slug_venue($event->Venue) ?>+<?php echo $event->LocationSlug  ?>"><?php echo $event->performers[0]->PerformerName;  ?></a></h4>
				<p><b>Venue:</b> <?php echo clean_venue($event->Venue) ?></p>
				<p><b>Time:</b> <?php echo date('F d, Y H:i', strtotime($event->Date)); ?></p>
				<p><b>City:</b> <?php echo $event->City.", ".$event->StateProvince; ?></p>
		</div>
	</div>
	<?php endforeach ?>
</div>
<?php else: ?>
	<div class="alert">
		<h3>You are not tracking any genres.</h3>
	</div>
<?php endif; ?>
*/ ?>
<div class="separator"></div>
<h3>You are Following:</h3>
<?php $alsoTrackException = array(); ?>

 <?php $i = 0; $cdn = get_cfg('cdn'); ?>
<div class="follow row-fluid">
	<?php foreach ($user_performers as $p): ?>
	<?php array_push($alsoTrackException, $p->pId); ?>
	<?php if($i % 4):
			echo "</div>";
			if ($i == 16):?>
				<div class="show-all-area hidden">
				<button class="btn link" id="toggle-all">Show All</button>
			<?php
			endif;
			echo '<div class="follow row-fluid">';
		endif;
	?>
	<?php if($p->img):
			$pic = $cdn.$p->img;
		else:
			$pic = ($p->limg) ? $p->limg : '/public/img/artist_thumb.jpg';
		endif;
		 ?>

	<div class="member span3" id="<?php echo $p->rId ?>-track">
		<div class="member-thumbnail">
			<a href="#">
				<img src="<?php echo $pic; ?>" alt="avatar">
			</a>
		</div>
		<ul class="member-menu">
			<li class="path-wrapper">
				<span class="overlay"></span>
				<div class="member-data">
					<h3>You Are Tracking</h3>
					<div class="position"><?php echo $p->performerName; ?></div>
				</div>
				<ul class="path">
					<li class="email-menu menu top-right"><a href="/tours/<?php echo $p->pSlug ?>" tager="_blank" title="View <?php echo $p->performerName; ?> Tour Dates"><span class="ent-info-2"></span></a></li>
					<!-- <li class="home-menu menu right"><a href="/tour-announcements" title="Create a New Rule with this Aritst"><span class="ent-add-to-list"></span></a></li> -->
					<li class="social-menu menu bottom-right"><a href="#removerule" class="dropSingleRule" data-name="<?php echo $p->performerName; ?>" data-id="<?php echo $p->rId; ?>" title="Stop Tracking this Artist"><span class="ent-minus-2"></span></a>
					<div class="mob-help">
						View Tour Dates
					</div>
					</li>
				</ul>
			</li>
			<h3 class="followtitle"><?php echo $p->performerName; ?></h3>
		</ul>
	</div>
	<?php $i++ ?>
	<?php endforeach ?>
	<?php if($i > 16): ?>
		</div>
	<?php endif; ?>
</div>

<div class="separator"></div>
<h3>You may also want to follow:</h3>
<div class="follow row-fluid">
	<?php $i = 0; ?>
	<?php foreach ($similar as $sp): ?>
		<?php if (!in_array($sp['pId'], $alsoTrackException)): ?>
			<?php if($i == 4):
				echo "</div>";
				echo '<div class="follow row-fluid">';
				$i=0;
			endif;
		?>
		<?php if($sp['img']):
				$pic = $cdn.$sp['img'];
			else:
				$pic = ($sp['limg']) ? $sp['limg'] : '/public/img/artist_thumb.jpg';
			endif;
			 ?>

		<div class="member span3" id="add-<?php echo $sp['pId'] ?>">
			<div class="member-thumbnail">
				<a href="#">
					<img src="<?php echo $pic; ?>" alt="avatar">
				</a>
			</div>
			<ul class="member-menu">
				<li class="path-wrapper">
					<span class="overlay"></span>
					<div class="member-data">
						<h3><?php echo $sp['performerName']; ?></h3>
						<!-- <div class="position"></div> -->
					</div>
					<ul class="path">
						<li class="email-menu menu top-right"><a href="/tours/<?php echo $sp['pSlug'] ?>" tager="_blank" title="View <?php echo $sp['performerName']; ?> Tour Info"><span class="ent-info-2"></span></a></li>
						<!-- <li class="home-menu menu right"><a href="/tour-announcements" title="Create a New Rule with this Aritst"><span class="ent-add-to-list"></span></a></li> -->
						<li class="social-menu menu bottom-right"><a href="#addrule" class="addSingleRule" data-name="<?php echo $sp['performerName']; ?>" data-id="<?php echo $sp['pId']; ?>" title="Track This Artist"><span class="ent-plus-2"></span></a></li>
					</ul>
				</li>
				<h3 class="followtitle"><?php echo $sp['performerName']; ?></h3>
			</ul>
		</div>
		<?php $i++ ?>
	<?php endif ?>
	<?php /*
	<div class="member span3">
		<div class="member-thumbnail">
			<a href="#">
				<img src="/img/assets/avatar/ladygaga.jpg" alt="avatar">
			</a>
		</div>
		<ul class="member-menu">
			<li class="path-wrapper">
				<span class="overlay"></span>
				<div class="member-data">
					<h3>Wanna Track</h3>
					<div class="position">Lady Gaga</div>
				</div>
				<ul class="path">
					<li class="email-menu menu top-right"><a href="/concerts/genre+country-folk" title="Tack This Artist"><span class="ent-user-add"></span></a></li>
					<li class="home-menu menu right"><a href="/tour-announcements" title="Create a Rule With this Artist">
					<span class="ent-add-to-list"></span></a></li>
					<li class="social-menu menu bottom-right"><a href="/track-your-favorite-artists" title="Don't Recommened this Artist again"><span class="ent-cross-3"></span></a></li>
				</ul>
			</li>
			</ul>
	</div>
	*/ ?>
	<?php endforeach ?>

</div>
<div id="dialog-confirm"></div>