<?php /*
THIS IS LEGACY NOT USED
*/ ?>
<div class="span6 pull-right">
	<h4> Rules</h4>
	<div class="small-help">
		<a href="#all">All</a> |
		<a href="#email"><span class="awe-envelope"></span> Email</a> |
		<a href="#feed"><span class="awe-list"></span>Feed </a> |
		<span class="awe-bell"></span> 30 - Days Before
	</div>
	<?php $groupId = false; ?>

	<?php foreach ($rules as $r): ?>
		<div class="alert">
			<button type="button" class="close" ><span class="awe-trash"></span></button>
			<?php echo "<a href='/tours/".seoUrl($r['performerName'])."' target='_blank'><em>{$r['performerName']}</em></a> plays in <strong>{$r['cityName']}</strong>" ?>
				<div class="">
					<?php echo ($r['notifyByEmail']) ? "<span class='awe-envelope'> </span>" : ''; ?>
					<?php echo ($r['notifyByFeed']) ? "<span class='awe-list'> </span>" : ''; ?>
					<?php echo ($r['notifyDays']) ? "<span class='awe-bell'> </span> {$r['notifyDaysNum']}" : ''; ?>
			 		| <span class="small-help"> Added: <?php echo date('F j, Y H:i') ?> </span>
				</div>
		</div>
	<?php endforeach ?>
</div>
