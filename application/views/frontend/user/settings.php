<h1>Account Settings</h1>
<h2 style="margin-left:10px;margin-top:20px">Manage Email Alerts</h2>

<div class="row-fluid" style="padding-left: 10px;border: 1px solid rgba(130, 130, 130, 0.27);width: 99%;margin-bottom:20px">
	<div class="span12">
		<h3>New Concerts by Performer in City
			<a class="info" title="Add your favorite performers and cities to get notified as soon as new concerts are announced.">
				<img style="width:16px;height:16px" src="/public/img/icons/icon_info-16.png" alt="icon1">
			</a>
		</h3>
	</div>

	<div class="span12" style="margin-left: 0px; margin-bottom: 10px">
		<a class="pull-left btn btn-info btn-flatten" type="button" href="/user/rules">Add Performers and Cities</a>
	</div>

	<div class="span6" style="width: 47%!important;margin-left: 0px!important;">
		<form action="" method="post" name="notifications">
			<fieldset>
				<legend>Notify Me
					<a class="info" title="Choose to receive email notifications when your favorite performers announce concerts in your selected cities, or only show on your Dashboard.">
						<img style="width:16px;height:16px" src="/public/img/icons/icon_info-16.png" alt="icon1">
					</a>
				</legend>
				<label class="radio">
					<input type="radio" name="notifytype" class="notifytype" value="1" <?php echo ($settings->notify == 1) ? "checked='checked'" : ''; ?> > Dashboard Only
				</label>
				<label class="radio">
					<input type="radio" name="notifytype" class="notifytype" value="2" <?php echo ($settings->notify == 2) ? "checked='checked'" : ''; ?> > Dashboard And Email (same day as event)
				</label>
				<!--<label class="radio">
					<input type="radio" name="notifytype" class="notifytype" value="0" <?php /*echo ($settings->notify == 0) ? "checked='checked'" : ''; */?> > Do Not Notify (halt all emails)
				</label>-->
			</fieldset>
		</form>
		<div class="hidden alert" id="notifyUpdateMessage"></div>
	</div>
	<div class="span6" style="width: 47%!important;">
		<form action="" method="post" name="reminderform">
			<legend>Remind Me
				<a class="info" title="Choose to send reminders 60 days and 14 days before your favorite performers will be in your selected cities.">
					<img style="width:16px;height:16px" src="/public/img/icons/icon_info-16.png" alt="icon1">
				</a>
			</legend>
			<label class="checkbox">
				<input type="checkbox" value="remind60" class="reminder" name="reminder" <?php echo ($settings->remind60 == 1) ? "checked='checked'" : ''; ?> > Send reminder <strong>60 days before event</strong>
			</label>
			<label class="checkbox">
				<input type="checkbox" value="remind14" class="reminder" name="reminder" <?php echo ($settings->remind14 == 1) ? "checked='checked'" : ''; ?> > Send reminder <strong>14 days before event</strong>
			</label>
		</form>
		<div class="hidden alert" id="reminderUpdateMessage"></div>
	</div>
</div>

<div class="row-fluid" style="padding-left: 10px;border: 1px solid rgba(130, 130, 130, 0.27);width: 99%;margin-bottom:20px">
	<div class="span12">
		<h3>Concerts by Home City
			<a class="info" title="Add your Home City for receiving the latest concert updates in/near your preferred city. You can also filter your concerts by your favorite genre(s).">
				<img style="width:16px;height:16px" src="/public/img/icons/icon_info-16.png" alt="icon1">
			</a>
		</h3>
	</div>
    <div class="span12" style="margin-bottom: 20px;margin-left: 0px">
        <div class="span5">
            <input type="text" placeholder="" name="pc-search-city" class="city-search-as pull-left"
                   style="width: 150px;margin-right: 4px" <?php if(count($rule_concert) > 0 && $rule_concert->cityName != null):?>value="<?php echo $rule_concert->cityName?>" <?php endif?>>
            <input type="hidden" name="city_slug" id="city_slug">
            <input type="hidden" name="city_name" id="city_name">

            <a id="addhomecity" class="pull-left btn btn-info btn-flatten" type="button" style="cursor:pointer;">Add Home City</a>

            <ul id="search-city-results"></ul>
        </div>
        <div class="span6">
            <form action="" method="post" name="homecitymiles" style="margin-bottom: 40px">
                <fieldset class="fieldgroup" style="width: auto; float: left;">
                    <span class="pull-left" style="margin-right:20px">within</span>
                    <input type="radio" name="miles" class="miles" <?php if(count($rule_concert) > 0 && $rule_concert->miles == 10): echo 'checked'; endif?> value="10"> 10 miles
                    <input style="margin-left:10px" type="radio" name="miles" class="miles" <?php if(count($rule_concert) > 0 && $rule_concert->miles == 25): echo 'checked'; endif?> value="25"> 25 miles
                    <input style="margin-left:10px" type="radio" name="miles" class="miles" <?php if(count($rule_concert) > 0 && $rule_concert->miles == 50): echo 'checked'; endif?> value="50"> 50 miles
                </fieldset>
            </form>

        </div>
        <div class="alert span12" style="display: none; margin-left: 0px" id="homecityUpdateMessage"></div>

    </div>
    <div class="span12" style="margin-left: 0px;">
        <div class="span6" style="">
            <form action="" method="post" name="notifications">
                <fieldset>
                    <legend>New Concerts in Home City
                        <a class="info" title="Get email notifications with the newest concerts just announced in/near your Home City.">
                            <img style="width:16px;height:16px" src="/public/img/icons/icon_info-16.png" alt="icon1">
                        </a>
                    </legend>
                    <label class="radio">
                        <input type="radio" name="newconcertcity" class="newconcertcity" <?php if(count($rule_concert) > 0 && $rule_concert->new_concert == 1): echo 'checked'; endif?> value="1"> Once a day
                    </label>
                    <label class="radio">
                        <input type="radio" name="newconcertcity" class="newconcertcity" <?php if((count($rule_concert) > 0 && $rule_concert->new_concert == 2) || (count($rule_concert) == 0)): echo 'checked'; endif?> value="2" > Once a week
                    </label>
                    <label class="radio">
                        <input type="radio" name="newconcertcity" class="newconcertcity" <?php if(count($rule_concert) > 0 && $rule_concert->new_concert == 3): echo 'checked'; endif?> value="3"> Once a month
                    </label>
                    <label class="radio">
                        <input type="radio" name="newconcertcity" class="newconcertcity" <?php if(count($rule_concert) > 0 && $rule_concert->new_concert == 0): echo 'checked'; endif?> value="0"> Never
                    </label>
                </fieldset>
            </form>
            <div class="alert" style="display: none" id="newfrequencyUpdateMessage"></div>
        </div>
        <div class="span6" style="">
            <form action="" method="post" name="notifications">
                <fieldset>
                    <legend>Upcoming Concerts in Home City
                        <a class="info" title="Get weekly email updates with the upcoming concerts in/near your Home City for the next week.">
                            <img style="width:16px;height:16px" src="/public/img/icons/icon_info-16.png" alt="icon1">
                        </a>
                    </legend>
                    <label class="radio">
                        <input type="radio" name="upcomingconcertcity" class="upcomingconcertcity" <?php if(count($rule_concert) > 0 && $rule_concert->upcoming_concert == 0): echo 'checked'; endif?> value="0"> Never
                    </label>
                    <label class="radio">
                        <input type="radio" name="upcomingconcertcity" class="upcomingconcertcity" <?php if((count($rule_concert) > 0 && $rule_concert->upcoming_concert == 1) || (count($rule_concert) == 0)): echo 'checked'; endif?> value="1" > Once a week
                    </label>
                </fieldset>
            </form>
            <div class="alert" style="display: none" id="upcomingfrequencyUpdateMessage"></div>
        </div>
    </div>
    <div class="span12" style="width: 98%!important;margin-left: 0px!important;">
        <form action="" method="post" name="notifications">
            <fieldset>
                <legend>Genre(s)
                    <a class="info" title="Filter your email notifications by your favorite genre(s).">
                        <img style="width:16px;height:16px" src="/public/img/icons/icon_info-16.png" alt="icon1">
                    </a>
                </legend>

                <label class="checkbox">
                    <input type="checkbox" name="newconcertgenre" class="genre all" value="all"> ALL
                </label>
				<span class="span6" style="margin-left:0px">
					<?php $array1 = array_slice($filters, 0, count($filters)/2)?>
                    <?php foreach($array1 as $filter):?>
                        <label class="checkbox">
                            <input type="checkbox" name="newconcertgenre" class="genre" value="<?php echo $filter['id']?>" <?php if($filter['checked']): echo 'checked'; endif?> > <?php echo $filter['Filter']?>
                        </label>
                    <?php endforeach?>
				</span>
                <span class="span6" style="margin-left:0px">
					<?php $array2 = array_slice($filters, count($filters)/2, count($filters))?>
                    <?php foreach($array2 as $filter):?>
                        <label class="checkbox">
                            <input type="checkbox" name="newconcertgenre" class="genre" value="<?php echo $filter['id']?>" <?php if($filter['checked']): echo 'checked'; endif?> >  <?php echo $filter['Filter']?>
                        </label>
                    <?php endforeach?>
				</span>
            </fieldset>
        </form>
        <div class="alert" style="display: none" id="genreUpdateMessage"></div>
    </div>


</div>

<h2 style="margin-left:15px;margin-top:20px">Manage Profile</h2>

<div class="row-fluid">

	<div class="span6">
		<form action="" method="post" class="form-inline">
			<fieldset>
				<legend>Change Name</legend>
				<input type="text" name="cfName" id="cfName" value="<?php echo $this->cfUser['cf']['name'] ?>" >
				<button type="button" id="update-name" class="btn btn-primary">Update</button>
			</fieldset>
		</form>
		<div class="hidden alert" id="nameUpdateMessage"></div>

			<?php /*
		<form action="" method="post" name="picture" enctype="file/text">
			<fieldset>
				<legend>Your Avatar</legend>
				<select name="social_profile_picture" id="choose-profile-picture">
					<option value="twitter">Your Twitter</option>
					<option value="facebook">Your Facebook</option>
					<option value="google">Your Google+</option>
					<option value="cf">Upload Your Own</option>
				</select>
				<div class="uploader hidden">
					<input type="file" class="" name="profile_picture">
				</div>
				<div class="current-profile-image">
					<img src="" alt="">
				</div>
				<br>
				<button type="submit" class="btn btn-primary">Update</button>
			</fieldset>
		</form>
	*/ ?>
	</div>
	<div class="span6">
		<form action="" method="post" name="change-password">
			<?php if($this->cfUser['cf']['password']): ?>
				<legend>Change Password:</legend>
			<?php else: ?>
				<legend>Create Password:</legend>
			<?php endif ?>
		<!--<span class="help-block">You can use your registered email and password to login into ConcertFix</span>-->
			<?php /* if($this->cfUser['cf']['password']): ?>
				<label for="old_password">Old Password:</label>
				<input type="password" name="old_password" id="old_password">
			<?php endif */?>
			<p><strong>Username:</strong> <?php echo $this->cfUser['cf']['primary_email']; ?></p>

			<label for="new_password1">New Password:</label>
			<input type="password" name="new_password1" id="new_password1">
			<label for="new_password2">Repeat Password:</label>
			<input type="password" name="new_password2" id="new_password2">
			<br>
			<button type="submit" class="btn btn-primary">Change!</button>
		</form>
	</div>
</div>

<h2 style="margin-left:15px;margin-top:20px">Manage Linked Accounts</h2>

<div class="row-fluid">
	<div class="span12">
	<!--<h3>Linked Accounts</h3>-->
		<table class="table">
			<thead>
				<tr>
					<th>Account</th>
					<th>Linked Date</th>
					<th>Unlink</th>
				</tr>
			</thead>
			<tbody>
				<?php if($this->cfUser['twitter'] && $this->cfUser['network'] != 'twitter' ): ?>
				<tr>
					<td><h4><span class="awe-twitter"></span> Twitter</h4></td>
					<td><?php echo date('F j, Y', strtotime($this->cfUser['twitter']['added_date'])) ?></td>
					<td>
						<form name="remove-twitter" id="remove-twitter" action="" method="post">
							<input type="hidden" name="network" value="twitter">
							<input type="hidden" name="op" value="remove">
							<button type="button" data-network="twitter" class="remove-link btn btn-warning" name="op" value="remove">Remove</button>
						</form>
					</td>
				</tr>
				<?php endif; ?>
				<?php if($this->cfUser['google'] && $this->cfUser['network'] != 'google' ): ?>
				<tr>
					<td><h4><span class="awe-google-plus"></span> google</h4></td>
					<td><?php echo date('F j, Y', strtotime($this->cfUser['google']['added_date'])) ?></td>
					<td>
						<form name="remove-google" id="remove-google" action="" method="post">
							<input type="hidden" name="network" value="google">
							<input type="hidden" name="op" value="remove">
							<button type="button" data-network="google" class="remove-link btn btn-warning" name="op" value="remove">Remove</button>
						</form>
					</td>
				</tr>
				<?php endif; ?>
				<?php if($this->cfUser['facebook'] && $this->cfUser['network'] != 'facebook' ): ?>
				<tr>
					<td><h4><span class="awe-facebook"></span> facebook</h4></td>
					<td><?php echo date('F j, Y', strtotime($this->cfUser['facebook']['added_date'])) ?></td>
					<td>
						<form name="remove-facebook" id="remove-facebook" action="" method="post">
							<input type="hidden" name="network" value="facebook">
							<input type="hidden" name="op" value="remove">
							<button type="button" data-network="facebook" class="remove-link btn btn-warning" name="op" value="remove">Remove</button>
						</form>
					</td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
		<form action="" id="remove-all" method="post" name="remove-all">
			<button type="button" data-network="cf" class="remove-link btn btn-danger"  id="delete-account">DELETE THIS ACCOUNT</button>
			<input type="hidden" name="network" value="cf">
			<input type="hidden" name="op" value="remove">
			<p>Also removes all associated accounts (twitter, facebook, google+, etc)</p>
		</form>
	</div>
</div>
<div id="dialog-confirm"></div>





