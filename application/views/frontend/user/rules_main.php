<div class="row-fluid">
	<div class="span12">
		<div class="sidebar-widget">
			<div class="sidebar-header">
				<ul id="rule-panels" class="nav nav-tabs">
					<li class="active"><a href="#performer">Track Performers</a></li>
				<?php /*		<li class="" ><a href="#genres">Track Genres</a></li>*/ ?>
					<li class=""><a href="#city">Track Cities</a></li>
				</ul>
				<div class="tab-content">
					<div id="performer" class="rule-panel tab-pane  active">
						<h2>Add Performers</h2>
						<div class="row-fluid">
							<div class="span12">
								<label for="pc-search-performer">Search Performer:</label>
								<input type="text" placeholder="Type something.." name="pc-search-performer" class="performer-search">
								<ul id="search-performer-results"></ul>
							</div>
						</div>
						<br />
						<div class="row-fluid">
							<div class="span12">
								<h4>Following Artists</h4>
								<div id="current-rules-performer"></div>
							</div>
						</div>
					</div>
					<div id="city" class="rule-panel tab-pane  ">
						<h2>Add Cities</h2>
						<div class="row-fluid">
							<div class="span12">
								<label for="pc-search-city">Search City:</label>
								<input type="text" placeholder="Columbus, OH" name="pc-search-city" class="city-search">
								<ul id="search-city-results"></ul>
							</div>
						</div>
						<br />
						<div class="row-fluid">
							<div class="span12">
								<h4>You're Tracking These Cities</h4>
								<div id="current-rules-city"></div>
							</div>
						</div>
					</div>
					<?php /*
					<div id="genres" class="rule-panel  tab-pane   ">
						<div class="row-fluid">
							<div class="span12">
								<h4>Track Those Genres</h4>
								<div id="genre-list">
									<ul>
									<?php foreach ($genres as $g): ?>
										<li class="<?php echo (in_array($g->id, $userGenres)) ? "genreActive" : ''; ?>"><label class="checkbox"><input <?php echo (in_array($g->id, $userGenres)) ? "checked='checked'" : ''; ?> type="checkbox" class="genre-rule" data-genreid="<?php echo $g->id; ?>"><?php echo $g->Filter ?></label> </li>
									<?php endforeach ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
					*/ ?>
				</div>
			</div>
		</div>
	</div>
</div>