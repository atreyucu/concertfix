<?php if ($rules): ?>
	<ul>
	<?php foreach ($rules as $rule): ?>
		<li>
			<a href="/concerts/<?php echo $rule->cSlug; ?>" target="_blank">
				<?php echo $rule->cityName; ?>
			</a>
			<button class="btn btn-link remove-city-rule" data-pname="<?php echo $rule->cityName ?>" data-ruleid="<?php echo $rule->rId ?>" type="button">
				<span class="awe-remove"></span>
			</button>
		</li>
	<?php endforeach ?>
	</ul>
<?php else: ?>
	<div class="alert">Haven't added any cities yet :(</div>
<?php endif ?>