<?php if ($rules): ?>
	<?php $cdn = get_cfg('cdn'); ?>
	<ul class="ajx-rl">
	<?php foreach ($rules as $rule): ?>
		<?php if($rule->img):
			$pic = $cdn.$rule->img;
		else:
			$pic = ($rule->limg) ? $rule->limg : '/public/img/artist_thumb.jpg';
		endif;
		 ?>
		<li>
			<a href="/tours/<?php echo $rule->pSlug; ?>" target="_blank">
				<img src="<?php echo $pic; ?>" /><?php echo $rule->performerName; ?>
			</a>
			<button class="btn btn-link remove-performer-rule" data-pname="<?php echo $rule->performerName ?>" data-ruleid="<?php echo $rule->rId ?>" type="button">
				<span class="awe-remove"></span>
			</button>
		</li>
	<?php endforeach ?>
	</ul>
<?php else: ?>
	<div class="alert">Haven't added any artists yet :(</div>
<?php endif ?>