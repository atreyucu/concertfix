<ul class="genre-list">
<?php foreach ($genres as $g): ?>
	<li><label for="checkbox">
		<input type="checkbox" name="genre[]" data-genreid="<?php echo $g->ChildCategoryID ?>" data-genre="<?php echo ucwords(strtolower($g->Filter)) ?>" class="add-genre" checked="checked"><?php echo ucwords(strtolower($g->Filter)) ?>
	</label></li>
<?php endforeach ?>
</ul>