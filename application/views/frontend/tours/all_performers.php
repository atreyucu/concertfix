<style>
    .trip-list ul > li {line-height: 30px!important;}
</style>

<h1>All Performers</h1>
<a href="#non-alpha">#</a> |
<?php $i =0; ?>
<?php foreach (range('a', 'z') as $char): ?>
    <a href='#<?php echo $char; ?>' style="padding: 2px!important; line-height: 20px!important; font-size: 14px;"><?php echo strtoupper($char); ?></a>
    <?php $i++; echo ($i < count(range('a','z'))) ? ' | ' : ''; ?>
<?php endforeach; ?>
<hr>
<?php $letter = '#'; ?>
<h2><a name='#non-alpha'>#</a></h2>
<div class="trip-list">
<ul>
<?php foreach ($performers as $p): ?>

	<?php $fl = strtoupper($p->PerformerName[0]); ?>

	<?php if($letter != $fl): ?>
		<?php if(preg_match("/^[a-zA-Z]$/", $fl)): ?>
			</ul>
		</div>
	<h2><a name="<?php echo strtolower($fl); ?>" href='#<?php echo $p->PerformerName[0]; ?>'><?php echo $p->PerformerName[0]; ?></a></h2>
	<div class="trip-list">
		<ul>
		<?php $letter = $fl; ?>
		<?php else: ?>
			<?php $letter = '#'; ?>
		<?php endif; ?>
	<?php endif;
	$letter = $fl;
    ?>
	<li>
        <a href="/tours/<?php echo $p->PerformerSlug;  ?>" title="<?php echo $p->PerformerName ?> Concert Schedule">
            <span><?php echo $p->PerformerName ?></span>
        </a>
    </li>
<?php endforeach ?>
</ul>
</div>