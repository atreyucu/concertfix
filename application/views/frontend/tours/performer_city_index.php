<article>
	<div class="venue-header">
		<h1 class="title"><?php echo $performer[0]->PerformerName ?> <?php echo $location['city'] ?> Tickets</h1>
		<?php if ($otherEvents): ?>
			<ul class="tour-guests">
				<li><span class="icon-map-marker"></span>Concert Cities Near <?php echo $location['city'] ?>:</li>
				<li>
				<?php $i = 0;

				$cities = array();
				foreach ($otherEvents as $e):
					$slugie = seoUrl($e->City." ".$e->StateProvince);
					$cities[$slugie] = $e->City;
				endforeach;
				 ?>
				<?php foreach ($cities as $slug => $concert): ?>
					<?php $i++; ?>
					<a href="/concerts/<?php echo $slug; ?>"><?php echo $concert ?></a><?php echo ($i < count($cities)) ? ' | ' : ''; ?>
				<?php endforeach ?>
				</li>
			</ul>
			<div style="clear:both;"></div><br>
		<?php endif ?>
		<div class="separator"></div>
	</div>
	<div class="city-thumbnail">
		<a href="/tours/<?php echo $performer[0]->PerformerSlug; ?>" title="<?php echo $performer[0]->PerformerName ?> Tour Dates">
			<img class="image-polaroid" src="<?php echo performerImage($performer, 'profile'); ?>" alt="<?php echo $performer[0]->PerformerName ?> <?php echo $location['city']; ?> Tickets">
		</a>
	</div>
	<div class="venue-content">
		<p> <?php echo $performerCityText; ?> </p>
	</div><div style="clear:both;"></div>
	<h3 class="post-lead"><?php echo $performer[0]->PerformerName; ?> <?php echo $location['city']; ?>, <?php echo $location['state'] ?> Concert Schedule</h3>
	<?php if ($cityEvents): ?>
	<?php
		$originalCount = count($cityEvents);
		$showAll = $this->input->get('showAll');
	 ?>
	 <?php $cityEvents  = ($showAll) ? $cityEvents : array_slice($cityEvents, 0, 5); ?>
	<div class="table table-condensed smoke" id="performer-events">
		<?php foreach ($cityEvents as $tour): ?>
            <?php $link = tour_link($tour, $performer[0]->PerformerName); ?>
			<div class="eventrow" itemscope="itemscope" itemtype="http://schema.org/Event" onclick="javascript:location.href='<?php echo $link?>'">
				<div class="event-date">
					<meta itemprop="startDate" content="<?php echo $tour->Date ?>">
					<span class="artdate"><?php echo date('M j, Y', strtotime($tour->Date)) ?><br>
						<span class="arttime"><?php echo (preg_match('/tba/i',$tour->DisplayDate)) ? "Time: TBA" : date('D g:i a', strtotime($tour->Date)) ?></span>
					</span>
				</div>
				<div class="event-details">
					<a itemprop="url" href="/tours/<?php echo $performer[0]->PerformerSlug."+".slug_venue($tour->Venue)."+".seoUrl($tour->City."-".$tour->StateProvince); ?>" title="<?php echo $tour->Name." - ".clean_venue($tour->Venue); ?> Tickets ">
						<span class="artname" itemprop="name performer"><?php echo $tour->Name; ?></span> <br>
						<span itemprop="location" itemscope="" itemtype="http://schema.org/Place">
							<span itemprop="name" class="artvenue"><?php echo clean_venue($tour->Venue); ?> - <?php echo $tour->City.", ".$tour->StateProvince; ?></span>
						</span><br>
						<span class="artdate"><?php echo date('M j, Y', strtotime($tour->Date)) ?> <?php echo (preg_match('/tba/i',$tour->DisplayDate)) ? " | Time: TBA" : date('| D g:i a', strtotime($tour->Date)) ?></span>
					</a>
				</div>
				<div class="event-action" style="vertical-align:middle">
					<a itemprop="url" href="/tickets/<?php echo $tour->ID ?>" title="Buy <?php echo $tour->Name; ?> <?php echo clean_venue($tour->Venue) ?> Tickets for <?php echo date('D M j, Y', strtotime($tour->Date)) ?> at <?php echo (preg_match('/tba/i',$tour->DisplayDate)) ? "Time: TBA" : date('g:i a', strtotime($tour->Date)) ?>" class="pull-right btn btn-info btn l-pad">
						 Tickets <i class="ent-ticket"></i>
					</a>
					<a itemprop="url" href="/tickets/<?php echo $tour->ID ?>" class="m-btn">
						<i class="icon-chevron-right"></i>
					</a>
				</div>
			</div><div style="clear:both;"></div>
		<?php endforeach ?>
		<?php if ($originalCount > 4 && !$showAll): ?>
			<a href="?showAll=true" class=""><span>Show All</span></a>
		<?php endif ?>
	</div>
	<?php else: ?>
		<h4>No Events :(</h4>
	<?php endif; ?>

		<h3 class="post-lead">Other <?php echo $performer[0]->PerformerName; ?> Concerts in <?php echo toggle_state($location['state']) ?></h3>
		<?php if ($otherEvents): ?>
			<div class="table table-condensed smoke" id="performer-events">

				<?php foreach ($otherEvents as $tour): ?>
					<?php
						$originalCount = count($otherEvents);
						$showAll = $this->input->get('showAll');
					 ?>
					 <?php $otherEvents  = ($showAll) ? $otherEvents : array_slice($otherEvents, 0, 10); ?>
                    <?php $link = tour_link($tour, $performer[0]->PerformerName); ?>
					<div class="eventrow" itemscope="itemscope" itemtype="http://schema.org/Event" onclick="javascript:location.href='<?php echo $link?>'">
						<div class="event-date">
							<meta itemprop="startDate" content="<?php echo $tour->Date ?>">
							<span class="artdate"><?php echo date('M j, Y', strtotime($tour->Date)) ?><br>
								<span class="arttime"><?php echo (preg_match('/tba/i',$tour->DisplayDate)) ? "Time: TBA" : date('D g:i a', strtotime($tour->Date)) ?></span>
							</span>
						</div>
						<div class="event-details">
							<a itemprop="url" href="/tours/<?php echo $performer[0]->PerformerSlug."+".slug_venue($tour->Venue)."+".seoUrl($tour->City."-".$tour->StateProvince); ?>" title="<?php echo $tour->Name." - ".clean_venue($tour->Venue); ?> Tickets ">
								<span class="artname" itemprop="name performer"><?php echo $tour->Name; ?></span> <br>
								<span itemprop="location" itemscope="" itemtype="http://schema.org/Place">
									<span itemprop="name" class="artvenue"><?php echo clean_venue($tour->Venue); ?> - <?php echo $tour->City.", ".$tour->StateProvince; ?></span>
								</span><br>
								<span class="artdate"><?php echo date('M j, Y', strtotime($tour->Date)) ?> <?php echo (preg_match('/tba/i',$tour->DisplayDate)) ? " | Time: TBA" : date('| D g:i a', strtotime($tour->Date)) ?></span>
							</a>
						</div>
						<div class="event-action" style="vertical-align:middle">
							<a itemprop="url" href="/tickets/<?php echo $tour->ID; ?>" title="Buy <?php echo $tour->Name; ?> <?php echo clean_venue($tour->Venue) ?> Tickets for <?php echo date('D M j, Y', strtotime($tour->Date)) ?> at <?php echo (preg_match('/tba/i',$tour->DisplayDate)) ? "Time: TBA" : date('g:i a', strtotime($tour->Date)) ?>" class="pull-right btn btn-info btn l-pad">
								 Tickets <i class="ent-ticket"></i>
							</a>
							<a itemprop="url" href="/tickets/<?php echo $tour->ID; ?>" class="m-btn">
								<i class="icon-chevron-right"></i>
							</a>
						</div>
					</div><div style="clear:both;"></div>

				<?php endforeach ?>
				<?php if ($originalCount > 10 && !$showAll): ?>
					<a href="?showAll=true" class=""><span>Show All</span></a>
				<?php endif ?>
			</div>
		<?php else: ?>
			<h4>No Other Events :(</h4>
		<?php endif ?>


	<h3 class="post-lead"><?php echo $performer[0]->PerformerName; ?> <?php echo $location['city']; ?> Concert Ticket Information</h3>
	<?php echo $liText; ?>
	<div class="separator"></div>
	<div class="comments">
		<h3><?php echo $performer[0]->PerformerName; ?> <?php echo $location['city']; ?> Concert Questions &amp; Comments</h3>
		<div class="separator"></div>
		<div id="fb-root" class=" fb_reset"><div style="position: absolute; top: -10000px; height: 0px; width: 0px;"><div><iframe name="fb_xdm_frame_http" frameborder="0" allowtransparency="true" scrolling="no" id="fb_xdm_frame_http" aria-hidden="true" title="Facebook Cross Domain Communication Frame" tab-index="-1" src="http://static.ak.facebook.com/connect/xd_arbiter.php?version=28#channel=f3076f74a4&amp;channel_path=%2Fperformer-city-tickets.html%3Ffb_xd_fragment%23xd_sig%3Df3cc106dcc%26&amp;origin=http%3A%2F%2Fconcertfix.com" style="border: none;"></iframe><iframe name="fb_xdm_frame_https" frameborder="0" allowtransparency="true" scrolling="no" id="fb_xdm_frame_https" aria-hidden="true" title="Facebook Cross Domain Communication Frame" tab-index="-1" src="https://s-static.ak.facebook.com/connect/xd_arbiter.php?version=28#channel=f3076f74a4&amp;channel_path=%2Fperformer-city-tickets.html%3Ffb_xd_fragment%23xd_sig%3Df3cc106dcc%26&amp;origin=http%3A%2F%2Fconcertfix.com" style="border: none;"></iframe></div></div><div style="position: absolute; top: -10000px; height: 0px; width: 0px;"><div><iframe name="fd21fe3f" frameborder="0" allowtransparency="true" scrolling="no" src="https://www.facebook.com/connect/ping?client_id=180467872098969&amp;domain=concertfix.com&amp;origin=1&amp;redirect_uri=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D28%23cb%3Df1a20a280c%26domain%3Dconcertfix.com%26origin%3Dhttp%253A%252F%252Fconcertfix.com%252Ff3076f74a4%26relation%3Dparent&amp;response_type=token%2Csigned_request%2Ccode&amp;sdk=joey" style="display: none;"></iframe></div></div></div>
		<?php /*
			<script>(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=180467872098969";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
			*/ ?>
		<div class="fb-comments" data-href="<?php echo current_url(); ?>" data-colorscheme="" data-numposts="5" data-width="" style="width:100%;" data-version="v2.3"></div>
	</div>
	<h3 class="post-lead">About <?php echo $performer[0]->PerformerName; ?> <?php echo $location['city']; ?> Tickets</h3>
	<p>
		<?php echo $shortText; ?>
	</p>
</article>
