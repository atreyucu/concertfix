<article>
	<div class="venue-header">
		<h1 class="title"><?php echo $performer[0]->PerformerName;  ?> <?php echo clean_venue($venue->Name); ?> <?php echo ucwords($venue->City); ?> Tickets</h1>
		<ul class="tour-guests">
			<?php /* error_reporting(E_ERROR); */ ?>
			<?php /* $eventPerformers = array(
				new stdClass,
				new stdClass,
				new stdClass,
				new stdClass,
				new stdClass,
				new stdClass,
				new stdClass,
				new stdClass,
				new stdClass,
				new stdClass,
				new stdClass
			); */?>
			<?php if (isset($eventPerformers) && count($eventPerformers) > 1): ?>
				<li><span class="icon awe-group"></span>Concert Performers: </li>
				<?php $i = 1; ?>
                <li>
                    <?php foreach ($eventPerformers as $p): ?>

                        <?php if($i == 7): ?>
                            <?php $more = true; ?>
                            <span class="more hide">
                        <?php endif; ?>
                        <!-- TEST -->
                        <a href="/tours/<?php echo $p->PerformerSlug ?>" title="<?php echo $p->PerformerName ?> Tour Dates">
                            <span><?php echo $p->PerformerName; ?></span>
                        </a>

                        <?php echo ($i < count($eventPerformers)) ? " | " : ''; ?>

                        <?php if((isset($more) && $more) && $i == count($eventPerformers)): ?>
                            </span>
                            <a href="#;" class="expand-more">More...</a>
                        <?php endif; ?>
                        <?php $i++; ?>
                    <?php endforeach ?>
                </li>
			<?php endif ?>
		</ul>
        <div style="clear:both;"></div><br>
		<div class="separator"></div>
		<input type="hidden" id="address" value="<?php echo $venue->Street1." ".$venue->Street2.", ".$venue->City.", ".$venue->StateProvince." - ".$venue->ZipCode; ?>" />

	</div>

    <div class="venue-thumbnail">
        <a href="/tours/<?php echo $performer[0]->PerformerSlug ?>" title="<?php echo $performer[0]->PerformerName ?> Tour Dates">
            <img class="image-polaroid" src="<?php echo $performer[0]->fbImage ?>" alt="<?php echo $performer[0]->PerformerName ?> live">
        </a>
    </div>

	<div class="venue-content">
		<p><?php echo $performerVenueText; ?></p>
		<?php if(!$tourDatesVenue): ?>
			<a href="/user/login">Track <?php echo $performer[0]->PerformerName; ?>!</a>
		<?php endif; ?>
	</div>
    <div style="clear:both;"></div>

	<?php if($tourDatesVenue): ?>
		<?php
			$originalCount = count($tourDatesVenue);
			$showAll = $this->input->get('showAll');
		 ?>
		<?php $tourDatesVenue  = ($showAll) ? $tourDatesVenue : array_slice($tourDatesVenue, 0, 10); ?>
		<div class="table table-condensed smoke marTop15" id="performer-events">
			<?php foreach ($tourDatesVenue as $vEvent): ?>
                <?php $link = tour_link($vEvent, $performer[0]->PerformerName); ?>
                <div class="eventrow" onclick="javascript:location.href='<?php echo $link?>'">
                    <div class="event-date">

                        <span class="artdate"><?php echo date('M j, Y', strtotime($vEvent->Date)) ?> <br>
                            <span class="arttime"><?php echo (preg_match('/tba/i',$vEvent->DisplayDate)) ? "Time: TBA" : date('D g:i a', strtotime($vEvent->Date)) ?></span>
                        </span>
                    </div>


                    <div class="event-details">
                        <a href="<?php echo $link; ?>" title="<?php echo $performer[0]->PerformerName." - ".clean_venue($vEvent->Venue); ?> Tickets ">
                            <span class="artname"><?php echo $vEvent->Name; ?></span> <br>

                            <span>

                                <span class="artvenue">
                                    <?php echo clean_venue($vEvent->Venue); ?> - <?php echo $vEvent->City.", ".$vEvent->StateProvince; ?>
                                </span>
                            </span><br>

                            <span class="artdate"><?php echo date('M j, Y', strtotime($vEvent->Date)) ?> <?php echo (preg_match('/tba/i',$vEvent->DisplayDate)) ? " | Time: TBA" : date('| D g:i a', strtotime($vEvent->Date)) ?></span>
                        </a>
                    </div>
                        <div class="event-action" style="vertical-align:middle">
                            <a style="margin-bottom: 5px!important;color: #fff !important;font-size: 15px" href="<?php echo $link; ?>" title="Buy <?php echo $vEvent->Name ?> <?php echo clean_venue($vEvent->Venue) ?> Tickets for <?php echo date('D M j, Y', strtotime($vEvent->Date)) ?> at <?php echo (preg_match('/tba/i',$vEvent->DisplayDate)) ? "Time: TBA" : date('g:i a', strtotime($vEvent->Date)) ?>" class="pull-right btn btn-info hidden-phone hidden-tablet">
                               TICKETS <i class="ent-ticket"></i>
                            </a>
                            <br>
                            <span class="pull-right hidden-phone hidden-tablet" style="color: #008d06;font-weight: bold;text-align: right;width: 250px;font-size: 11px">
								<span><?php echo ($vEvent->prices) ? $vEvent->prices->ticketsAvailable : "Few " ?></span>
								tickets

                                <?php if ($vEvent->prices) : ?>
                                    left starting from $<span><?php echo $vEvent->prices->lowPrice; ?></span>
                                <?php else: ?>
                                    left - check prices

                                <?php endif; ?>

                            </span>

                            <a href="<?php echo $link; ?>" class="m-btn">
                                <i class="icon-chevron-right"></i>
                            </a>
                        </div>
                </div>
			    <div style="clear:both;"></div>
			<?php endforeach ?>

            <script type="application/ld+json">
					[
					<?php $count = 0?>
					<?php foreach ($tourDatesVenue as $tour): ?>
                    <?php $link = tour_link_full($tour, $performer[0]->PerformerName); ?>
						{
							"@context":"https://schema.org",
							"@type":"Event",
							"name":"<?php echo $tour->Name; ?>",
							"startDate":"<?php echo $tour->Date; ?>",
							"url":"<?php echo $link; ?>",
							"location":{"@type":"Place",
										"name":"<?php echo $tour->Venue ?>",
										"address":{"@type":"PostalAddress",
											"addressLocality":"<?php echo $tour->City ?>",
											"addressRegion": "<?php echo $tour->StateProvince ?>"}
										},
							"offers":{"@type":"Offer","category":"Secondary","priceCurrency":"USD",
									  <?php if ($tour->prices) : ?>
									  "price":"<?php echo $tour->prices->lowPrice; ?>",
									  <?php endif ?>
									  "url":"<?php echo $link; ?>",
									  "availability":"https://schema.org/InStock"}
						}
						<?php $count += 1?>

                        <?php if ($count != count($tourDatesVenue)) : ?>
						,
						<?php endif?>

                    <?php endforeach ?>
					]
				</script>

        </div>

        <?php if ($originalCount > 10 && !$showAll): ?>
            <a href="?showAll=true" class="btn btn-flat btn-info mar-bot-30">
                <span class="fg-white">Show All</span>
            </a>
        <?php endif ?>

    <?php endif; ?>

    <div id="askToChooseSidebar"></div>
    <div class="venimg image-polaroid">
        <?php if(count($tourDatesVenue) > 1): ?>
            <a href="#" class="chooseEventBeforeClick" title="<?php echo $performer[0]->PerformerName; ?> <?php echo clean_venue($venue->Name);  ?> Concert Tickets">
        <?php else: ?>
            <?php $link = tour_link($tourDatesVenue[0], $performer[0]->PerformerName); ?>
            <a href="<?php echo $link; ?>" title="<?php echo $performer[0]->PerformerName; ?> <?php echo clean_venue($venue->Name);  ?> Concert Tickets">
        <?php endif; ?>
                <img src="<?php echo ($tourDatesVenue[0]->MapURL) ? $https_map_image : '/public/img/assets/placeholder/seatingchart.jpg'; ?>" alt="<?php echo $performer[0]->PerformerName; ?> <?php echo clean_venue($venue->Name);  ?> Seating Chart">
            </a>
    </div>



    <p class="marTop15">
        <?php echo $performerVenueText2; ?>
    </p>

    <?php if($qnaText): ?>
		<?php echo $qnaText; ?>
		<hr>
		<div class="fb-comments" data-href="<?php echo strtr(current_url(), array('https:'=>'http:')); ?>" data-colorscheme="" data-numposts="5" data-width="" style="width:100%;"></div>
    <?php endif; ?>

    <?php if (isset($otherEvents) && count($otherEvents) > 0): ?>
		<?php
            $otherEvents = array_splice($otherEvents, 0, 3);
            $originalCount = count($otherEvents);
		?>

        <h4>Next Concerts at <?php echo clean_venue($venue->Name); ?></h4>
        <div class="table table-condensed smoke" id="performer-events">

            <?php foreach ($otherEvents as $tour): ?>
                <?php // if ($tour->performers): ?>
                    <?php $link = tour_link($tour, $tour->Name); ?>

                    <div class="eventrow" onclick="javascript:location.href='<?php echo $link?>'">

                        <div class="event-date">
                            <span class="artdate"><?php echo date('M j, Y', strtotime($tour->Date)) ?><br>
                                <span class="arttime"><?php echo (preg_match('/tba/i',$tour->DisplayDate)) ? "Time: TBA" : date('D g:i a', strtotime($tour->Date)) ?></span>
                            </span>
                        </div>

                        <div class="event-details">


                            <?php if(count($tour->performers) == 0): ?>
                                <a href="<?php echo $link; ?>" title="<?php echo $tour->Name." - ".clean_venue($tour->Venue); ?> Tickets ">
                                    <span class="artname"><?php echo $tour->Name; ?></span>
                                </a>
                                <br>
                                <span>

                                    <a href="<?php echo $link; ?>" title="<?php echo $tour->Name." - ".clean_venue($tour->Venue); ?> Tickets ">
                                        <span class="artvenue"><?php echo clean_venue($tour->Venue); ?> - <?php echo $tour->City.", ".$tour->StateProvince; ?></span>
                                    </a>
                                </span><br>
                                <span class="artdate"><?php echo date('M j, Y', strtotime($tour->Date)) ?> <?php echo (preg_match('/tba/i',$tour->DisplayDate)) ? " | Time: TBA" : date('| D g:i a', strtotime($tour->Date)) ?></span>

                            <?php elseif(count($tour->performers) == 1): ?>
                                <?php $i = 0; ?>
                                <?php foreach ($tour->performers as $perf): ?>
                                    <a href="/tours/<?php echo $perf->PerformerSlug."+".slug_venue($tour->Venue)."+".seoUrl($tour->City."-".$tour->StateProvince); ?>" title="<?php echo $perf->PerformerName." - ".clean_venue($tour->Venue); ?> Tickets ">
                                        <span class="artname"><?php echo $perf->PerformerName; ?></span>
                                    </a><?php $i++; echo ($i < count($tour->performers)) ? " | " : ''; ?>
                                <?php endforeach ?>
                                <br>
                                <span>

                                    <a href="/tours/<?php echo $perf->PerformerSlug."+".slug_venue($tour->Venue)."+".seoUrl($tour->City."-".$tour->StateProvince); ?>" title="<?php echo $tour->Name." - ".clean_venue($tour->Venue); ?> Tickets ">
                                        <spanclass="artvenue"><?php echo clean_venue($tour->Venue); ?> - <?php echo $tour->City.", ".$tour->StateProvince; ?></span>
                                    </a>
                                </span><br>
                                <span class="artdate"><?php echo date('M j, Y D g:i a', strtotime($tour->Date)) ?></span>
                            <?php else: ?>
                                <?php $i = 0; // var_dump($tour->performers) ?>
                                <?php foreach ($tour->performers as $perf): ?>
                                    <a href="/tours/<?php echo $perf->PerformerSlug."+".slug_venue($tour->Venue)."+".seoUrl($tour->City."-".$tour->StateProvince); ?>" title="<?php echo $tour->Name." - ".clean_venue($tour->Venue); ?> Tickets ">
                                        <span class="artname"><?php echo $perf->PerformerName; ?></span>
                                    </a><?php $i++; echo ($i < count($tour->performers)) ? " | " : ''; ?>
                                <?php endforeach ?>
                                <br>
                                <span>

                                    <a href="<?php echo $link; ?>" title="<?php echo $tour->Name." - ".clean_venue($tour->Venue); ?> Tickets ">
                                        <span class="artvenue"><?php echo clean_venue($tour->Venue); ?> - <?php echo $tour->City.", ".$tour->StateProvince; ?></span>
                                    </a>
                                </span><br>
                                <span class="artdate"><?php echo date('M j, Y D g:i a', strtotime($tour->Date)) ?></span>
                            <?php endif; ?>
                        </div>
                        <div class="event-action" style="vertical-align:middle">
                            <a style="margin-bottom: 5px!important;color: #fff !important;font-size: 15px" href="<?php echo $link; ?>" title="Buy <?php echo $tour->Name; ?> <?php echo clean_venue($tour->Venue) ?> Tickets for <?php echo date('D M j, Y', strtotime($tour->Date)) ?> at <?php echo (preg_match('/tba/i',$tour->DisplayDate)) ? "Time: TBA" : date('g:i a', strtotime($tour->Date)) ?>" class="pull-right btn btn-info hidden-phone hidden-tablet">
                                TICKETS <i class="ent-ticket"></i>
                            </a>
                            <br>
                            <span class="pull-right hidden-phone hidden-tablet" style="color: #008d06;font-weight: bold;text-align: right;width: 250px;font-size: 11px">
								<span><?php echo ($tour->prices) ? $tour->prices->ticketsAvailable : "Few " ?></span>
								tickets


                                <?php if ($tour->prices) : ?>
                                    left starting from $<span><?php echo $tour->prices->lowPrice; ?></span>
                                <?php else: ?>
                                    left - check prices
                                <?php endif; ?>

                            </span>

                            <a href="<?php echo $link; ?> " class="m-btn">
                                <i class="icon-chevron-right"></i>
                            </a>
                        </div>
                    </div>

                <div style="clear:both;"></div>
                <?php // endif ?>

            <?php endforeach; ?>

            <script type="application/ld+json">
					[
					<?php $count = 0?>
					<?php foreach ($otherEvents as $tour): ?>
                    <?php $link = tour_link_full($tour, $tour->Name); ?>
						{
							"@context":"https://schema.org",
							"@type":"Event",
							"name":"<?php echo $tour->Name; ?>",
							"startDate":"<?php echo $tour->Date; ?>",
							"url":"<?php echo $link; ?>",
							"location":{"@type":"Place",
										"name":"<?php echo $tour->Venue ?>",
										"address":{"@type":"PostalAddress",
											"addressLocality":"<?php echo $tour->City ?>",
											"addressRegion": "<?php echo $tour->StateProvince ?>"}
										},
							"offers":{"@type":"Offer","category":"Secondary","priceCurrency":"USD",
									  <?php if ($tour->prices) : ?>
									  "price":"<?php echo $tour->prices->lowPrice; ?>",
									  <?php endif ?>
									  "url":"<?php echo $link; ?>",
									  "availability":"https://schema.org/InStock"}
						}
						<?php $count += 1?>

                    <?php if ($count != count($otherEvents)) : ?>
						,
						<?php endif?>

                <?php endforeach ?>
					]
				</script>

        </div>
        <a href="/concerts/<?php echo seoUrl($tour->City."-".$tour->StateProvince); ?>+<?php echo slug_venue($tour->Venue); ?>" class="btn btn-flat btn-info pull-left"><span style="color:#fff;">Show All</span></a><div style="clear:both;"></div><br />
    <?php endif; ?>
</article>
