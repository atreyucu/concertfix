<article>
    <?php if (!$mobile) : ?>
        <div class="venue-thumbnail">
            <a href="/tours/<?php echo $performer[0]->PerformerSlug ?>" title="<?php echo $performer[0]->PerformerName ?> Tour Dates">
                <img class="image-polaroid" src="<?php echo performerImage($performer, 'profile'); ?>" alt="<?php echo $performer[0]->PerformerName ?> live">
            </a>
        </div>

        <div class="venue-header">
            <h1 class="title"><?php echo $performer[0]->PerformerName;  ?> <?php echo clean_venue($venue->Name); ?> <?php echo ucwords($venue->City); ?> Tickets</h1>
            <ul class="tour-guests">

                <?php if (isset($eventPerformers) && count($eventPerformers) > 1): ?>
                    <li><span class="icon awe-group"></span>Concert Performers: </li>
                    <?php $i = 1; ?>
                    <li>
                        <?php foreach ($eventPerformers as $p): ?>

                            <?php if($i == 7): ?>
                                <?php $more = true; ?>
                                <span class="more hide">
                            <?php endif; ?>
                            <!-- TEST -->
                            <a href="/tours/<?php echo $p->PerformerSlug ?>" title="<?php echo $p->PerformerName ?> Tour Dates">
                                <span><?php echo $p->PerformerName; ?></span>
                            </a>

                            <?php echo ($i < count($eventPerformers)) ? " | " : ''; ?>

                            <?php if((isset($more) && $more) && $i == count($eventPerformers)): ?>
                                </span>
                                <a href="#;" class="expand-more">More...</a>
                            <?php endif; ?>
                            <?php $i++; ?>
                        <?php endforeach ?>
                    </li>
                <?php endif ?>
            </ul>
            <div style="clear:both;"></div><br>
            <div class="separator"></div>
            <input type="hidden" id="address" value="<?php echo $venue->Street1." ".$venue->Street2.", ".$venue->City.", ".$venue->StateProvince." - ".$venue->ZipCode; ?>" />

        </div>

        <div class="venue-content">
            <p><?php echo $performerVenueText; ?></p>
            <?php if(!$tourDatesVenue): ?>
                <a href="/user/login">Track <?php echo $performer[0]->PerformerName; ?>!</a>
            <?php endif; ?>
        </div>

        <div style="clear:both;"></div>
    <?php else : ?>
        <div class="panel-group">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title">
                        <span id="arrow_orientation" class="glyphicon glyphicon-chevron-right"></span>
                        <a id="accordion-button" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            Event Description
                        </a>
                    </span>
                </div>
                <div id="collapseOne" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="venue-thumbnail">
                            <a href="/tours/<?php echo $performer[0]->PerformerSlug ?>" title="<?php echo $performer[0]->PerformerName ?> Tour Dates">
                                <img class="image-polaroid" src="<?php echo performerImage($performer, 'profile'); ?>" alt="<?php echo $performer[0]->PerformerName ?> live">
                            </a>
                        </div>

                        <div class="venue-header">
                            <h1 class="title"><?php echo $performer[0]->PerformerName;  ?> <?php echo clean_venue($venue->Name); ?> <?php echo ucwords($venue->City); ?> Tickets</h1>
                            <ul class="tour-guests">

                                <?php if (isset($eventPerformers) && count($eventPerformers) > 1): ?>
                                    <li><span class="icon awe-group"></span>Concert Performers: </li>
                                    <?php $i = 1; ?>
                                    <li>
                                        <?php foreach ($eventPerformers as $p): ?>

                                            <?php if($i == 7): ?>
                                                <?php $more = true; ?>
                                                <span class="more hide">
                                            <?php endif; ?>
                                            <!-- TEST -->
                                            <a href="/tours/<?php echo $p->PerformerSlug ?>" title="<?php echo $p->PerformerName ?> Tour Dates">
                                                <span><?php echo $p->PerformerName; ?></span>
                                            </a>

                                            <?php echo ($i < count($eventPerformers)) ? " | " : ''; ?>

                                            <?php if((isset($more) && $more) && $i == count($eventPerformers)): ?>
                                                </span>
                                                <a href="#;" class="expand-more">More...</a>
                                            <?php endif; ?>
                                            <?php $i++; ?>
                                        <?php endforeach ?>
                                    </li>
                                <?php endif ?>
                            </ul>
                            <div style="clear:both;"></div><br>
                            <div class="separator"></div>
                            <input type="hidden" id="address" value="<?php echo $venue->Street1." ".$venue->Street2.", ".$venue->City.", ".$venue->StateProvince." - ".$venue->ZipCode; ?>" />

                        </div>

                        <div class="venue-content">
                            <p><?php echo $performerVenueText; ?></p>
                            <?php if(!$tourDatesVenue): ?>
                                <a href="/user/login">Track <?php echo $performer[0]->PerformerName; ?>!</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

	<?php if($tourDatesVenue): ?>
		<?php
			$originalCount = count($tourDatesVenue);
			$showAll = $this->input->get('showAll');
		 ?>
		<?php $tourDatesVenue  = ($showAll) ? $tourDatesVenue : array_slice($tourDatesVenue, 0, 10); ?>
		<div class="table table-condensed smoke marTop15" id="performer-events">
            <script type="application/ld+json">
					[
					<?php $count = 0?>
					<?php foreach ($tourDatesVenue as $tour): ?>
                    <?php $link = tour_link_full($tour, $performer[0]->PerformerName); ?>
						{
							"@context":"https://schema.org",
							"@type":"Event",
							"name":"<?php echo $tour->Name; ?>",
							"startDate":"<?php echo $tour->Date; ?>",
							"url":"<?php echo $link; ?>",
							"location":{"@type":"Place",
										"name":"<?php echo $tour->Venue ?>",
										"address":{"@type":"PostalAddress",
											"addressLocality":"<?php echo $tour->City ?>",
											"addressRegion": "<?php echo $tour->StateProvince ?>"}
										},
							"offers":{"@type":"Offer","category":"Secondary","priceCurrency":"USD",
									  <?php if ($tour->prices) : ?>
									  "price":"<?php echo $tour->prices->lowPrice; ?>",
									  <?php endif ?>
									  "url":"<?php echo $link; ?>",
									  "availability":"https://schema.org/InStock"}
						}
						<?php $count += 1?>

                        <?php if ($count != count($tourDatesVenue)) : ?>
						    ,
						<?php endif?>

                    <?php endforeach ?>
					]
				</script>
            <?php if (count($tourDatesVenue) > 1) : ?>
                <?php foreach ($tourDatesVenue as $vEvent): ?>
                <?php $link = tour_link($vEvent, $performer[0]->PerformerName); ?>
                <div class="eventrow" onclick="javascript:location.href='<?php echo $link ?>'">
                    <div class="event-date">

                        <span class="artdate"><?php echo date('M j, Y', strtotime($vEvent->Date)) ?> <br>
                            <span
                                class="arttime"><?php echo (preg_match('/tba/i', $vEvent->DisplayDate)) ? "Time: TBA" : date('D g:i a', strtotime($vEvent->Date)) ?></span>
                        </span>
                    </div>

                    <div class="event-details">
                        <a href="<?php echo $link; ?>"
                           title="<?php echo $performer[0]->PerformerName . " - " . clean_venue($vEvent->Venue); ?> Tickets ">
                            <span class="artname"><?php echo $vEvent->Name; ?></span> <br>

                            <span>

                                <span class="artvenue">
                                    <?php echo clean_venue($vEvent->Venue); ?>
                                    - <?php echo $vEvent->City . ", " . $vEvent->StateProvince; ?>
                                </span>
                            </span><br>

                            <span
                                class="artdate"><?php echo date('M j, Y', strtotime($vEvent->Date)) ?><?php echo (preg_match('/tba/i', $vEvent->DisplayDate)) ? " | Time: TBA" : date('| D g:i a', strtotime($vEvent->Date)) ?></span>
                        </a>
                    </div>
                    <div class="event-action" style="vertical-align:middle">
                        <a style="margin-bottom: 5px!important;color: #fff !important;font-size: 15px"
                           href="<?php echo $link; ?>"
                           title="Buy <?php echo $vEvent->Name ?> <?php echo clean_venue($vEvent->Venue) ?> Tickets for <?php echo date('D M j, Y', strtotime($vEvent->Date)) ?> at <?php echo (preg_match('/tba/i', $vEvent->DisplayDate)) ? "Time: TBA" : date('g:i a', strtotime($vEvent->Date)) ?>"
                           class="pull-right btn btn-info hidden-phone hidden-tablet">
                            TICKETS <i class="ent-ticket"></i>
                        </a>
                        <br>
                            <span class="pull-right hidden-phone hidden-tablet"
                                  style="color: #008d06;font-weight: bold;text-align: right;width: 250px;font-size: 11px">
								<span><?php echo ($vEvent->prices) ? $vEvent->prices->ticketsAvailable : "Few " ?></span>
								tickets

                                <?php if ($vEvent->prices) : ?>
                                    left starting from $<span><?php echo $vEvent->prices->lowPrice; ?></span>
                                <?php else: ?>
                                    left - check prices

                                <?php endif; ?>

                            </span>

                        <a href="<?php echo $link; ?>" class="m-btn">
                            <i class="icon-chevron-right"></i>
                        </a>
                    </div>
                </div>
                <div style="clear:both;"></div>
                <?php endforeach ?>
            <?php else : ?>
                <!--<script type="application/javascript" language='javascript' src='http://mapwidget2.seatics.com/js?eventId=--><?php //echo $tourDatesVenue[0]->ID ?: -1 ?><!--&websiteConfigId=14132'></script>-->
                <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'></script>
                <script src='https://code.jquery.com/ui/1.12.1/jquery-ui.min.js'></script>
                <script type="text/javascript" src="https://mapwidget2.seatics.com/js?mobileOptimized=true&amp;websiteConfigId=14132&amp;eventId=<?php echo $tourDatesVenue[0]->ID ?: -1 ?>&amp;includeJQuery=false"></script>
                <?php if ($mobile) : ?>
                    <script type="text/javascript">

                    Seatics.config.c3CheckoutDomain = "checkout.concertfix.com";
                    Seatics.config.c3CurrencyCode = "USD";
                    Seatics.config.useC3 = true;

                    Seatics.config.smallScreenMapLayout = Seatics.SmallScreenMapOptions.HalfShown;

                    Seatics.config.mapFinishedRenderingHandler = function(){

                        if(!Modernizr.mq('(max-width: 990px) and (min-width: 540px) and (orientation: landscape)')){
                            jQuery('.header-container').addClass('up-tix-v2')

                            var previousScroll = 0;
                            jQuery(window).scroll(function(){
                                var currentScroll = jQuery(this).scrollTop();
                                if (currentScroll > previousScroll){
                                    jQuery('.header-container').removeClass('up-tix-v2')
                                    jQuery('.header-container').addClass('down-tix')
                                    //console.log('down '+currentScroll);
                                } else {
                                    if (currentScroll < 1){
                                        jQuery('.header-container').removeClass('down-tix')
                                        jQuery('.header-container').addClass('up-tix-v2')
                                    }
                                    // console.log('up '+currentScroll);
                                }
                                previousScroll = currentScroll;

                            })

                            jQuery('#accordion-button').click(function(){
                                if(jQuery('#sea-mobile-header').css('display') == 'none') {
                                    jQuery('#sea-mobile-header').css({'display': 'block'})
                                    jQuery('#venue-ticket-list').css({'margin-top': '340px'})
                                }
                                else {
                                    jQuery('#sea-mobile-header').css({'display': 'none'})
                                    jQuery('#venue-ticket-list').css({'margin-top': '-1px'})
                                }
                            })
                        }

                        jQuery('#price-filter-min').focus(function(){
                            jQuery('.header-container').css({"position": "fixed"})
                        })

                        jQuery('#price-filter-max').focus(function(){
                            jQuery('.header-container').css({"position": "fixed"})
                        })

                        jQuery("#responsive-nav .collapse-menu .collapse-trigger").click(function (e) {
                            jQuery(this).toggleClass(function () {
                                if (jQuery(this).is(".awe-chevron-up")) {
                                    jQuery(this).removeClass("awe-chevron-up");
                                    jQuery('.header-container').removeClass("superup-tix");
                                    return "awe-search";
                                } else {
                                    jQuery(this).removeClass("awe-search");
                                    jQuery('.header-container').addClass("superup-tix");
                                    return "awe-chevron-up";
                                }
                            });
                            jQuery(this).parent().siblings("ul").slideToggle();
                        });


                        jQuery('.seatics-section-has-tickets').click(function(){

                            jQuery('#venue-ticket-list').css('height', function(){
                                var height = jQuery('#tickets-table').css('height');
                                var temp = height.split('px')
                                temp = parseInt(temp[0])+240;
                                height = temp + 'px';
                                return height;
                            })
                        });

                        <?php if ($ios) : ?>
                            jQuery('div#sea-mobile-header > div#event-info-cnt > div.event-info-col > h1.event-info-name').css({'text-overflow':'initial'})
                        <?php endif;?>
                    };

                </script>
                <?php else : ?>
                    <script type="text/javascript">

                        Seatics.config.ticketListOnRight = false;
                        Seatics.config.c3CheckoutDomain = "checkout.concertfix.com";
                        Seatics.config.c3CurrencyCode = "USD";
                        Seatics.config.useC3 = true;

                        Seatics.config.smallScreenMapLayout = Seatics.SmallScreenMapOptions.HalfShown;

                        Seatics.config.mapFinishedRenderingHandler = function(){
                            jQuery('#map-list-holder').css({"height": "700px"})

                            jQuery('#price-filter-min').focus(function(){
                                jQuery('.header-container').css({"position": "fixed"})
                            })

                            jQuery('#price-filter-max').focus(function(){
                                jQuery('.header-container').css({"position": "fixed"})
                            })

                        };

                    </script>
                <?php endif; ?>

            <?php endif; ?>
        </div>

        <?php if ($originalCount > 10 && !$showAll): ?>
            <a href="?showAll=true" class="btn btn-flat btn-info mar-bot-30">
                <span class="fg-white">Show All</span>
            </a>
        <?php endif ?>

    <?php endif; ?>

    <div id="askToChooseSidebar"></div>
    <?php if (count($tourDatesVenue) > 1) : ?>
        <div class="venimg image-polaroid">
            <?php if(count($tourDatesVenue) > 1): ?>
            <a href="#" class="chooseEventBeforeClick" title="<?php echo $performer[0]->PerformerName; ?> <?php echo clean_venue($venue->Name);  ?> Concert Tickets">
                <?php else: ?>
                <?php $link = tour_link($tourDatesVenue[0], $performer[0]->PerformerName); ?>
                <a href="<?php echo $link; ?>" title="<?php echo $performer[0]->PerformerName; ?> <?php echo clean_venue($venue->Name);  ?> Concert Tickets">
                    <?php endif; ?>
                    <img src="<?php echo ($tourDatesVenue[0]->MapURL) ? $tourDatesVenue[0]->MapURL : '/public/img/assets/placeholder/seatingchart.jpg'; ?>" alt="<?php echo $performer[0]->PerformerName; ?> <?php echo clean_venue($venue->Name);  ?> Seating Chart">
                </a>
        </div>
    <?php endif; ?>

    <p class="marTop15">
        <?php echo $performerVenueText2; ?>
    </p>

    <?php if($qnaText): ?>
		<?php echo $qnaText; ?>
		<hr>
		<div class="fb-comments" data-href="<?php echo current_url(); ?>" data-colorscheme="" data-numposts="5" data-width="" style="width:100%;"></div>
    <?php endif; ?>

    <?php if (isset($otherEvents) && count($otherEvents) > 0): ?>
		<?php
            $otherEvents = array_splice($otherEvents, 0, 3);
            $originalCount = count($otherEvents);
		?>

        <h4>Next Concerts at <?php echo clean_venue($venue->Name); ?></h4>
        <div class="table table-condensed smoke" id="performer-events">

            <?php foreach ($otherEvents as $tour): ?>
                <?php // if ($tour->performers): ?>
                    <?php $link = tour_link($tour, $tour->Name); ?>

                    <div class="eventrow" onclick="javascript:location.href='<?php echo $link?>'">

                        <div class="event-date">
                            <span class="artdate"><?php echo date('M j, Y', strtotime($tour->Date)) ?><br>
                                <span class="arttime"><?php echo (preg_match('/tba/i',$tour->DisplayDate)) ? "Time: TBA" : date('D g:i a', strtotime($tour->Date)) ?></span>
                            </span>
                        </div>

                        <div class="event-details">


                            <?php if(count($tour->performers) == 0): ?>
                                <a href="<?php echo $link; ?>" title="<?php echo $tour->Name." - ".clean_venue($tour->Venue); ?> Tickets ">
                                    <span class="artname"><?php echo $tour->Name; ?></span>
                                </a>
                                <br>
                                <span>

                                    <a href="<?php echo $link; ?>" title="<?php echo $tour->Name." - ".clean_venue($tour->Venue); ?> Tickets ">
                                        <span class="artvenue"><?php echo clean_venue($tour->Venue); ?> - <?php echo $tour->City.", ".$tour->StateProvince; ?></span>
                                    </a>
                                </span><br>
                                <span class="artdate"><?php echo date('M j, Y', strtotime($tour->Date)) ?> <?php echo (preg_match('/tba/i',$tour->DisplayDate)) ? " | Time: TBA" : date('| D g:i a', strtotime($tour->Date)) ?></span>

                            <?php elseif(count($tour->performers) == 1): ?>
                                <?php $i = 0; ?>
                                <?php foreach ($tour->performers as $perf): ?>
                                    <a href="/tours/<?php echo $perf->PerformerSlug."+".slug_venue($tour->Venue)."+".seoUrl($tour->City."-".$tour->StateProvince); ?>" title="<?php echo $perf->PerformerName." - ".clean_venue($tour->Venue); ?> Tickets ">
                                        <span class="artname"><?php echo $perf->PerformerName; ?></span>
                                    </a><?php $i++; echo ($i < count($tour->performers)) ? " | " : ''; ?>
                                <?php endforeach ?>
                                <br>
                                <span>

                                    <a href="/tours/<?php echo $perf->PerformerSlug."+".slug_venue($tour->Venue)."+".seoUrl($tour->City."-".$tour->StateProvince); ?>" title="<?php echo $tour->Name." - ".clean_venue($tour->Venue); ?> Tickets ">
                                        <spanclass="artvenue"><?php echo clean_venue($tour->Venue); ?> - <?php echo $tour->City.", ".$tour->StateProvince; ?></span>
                                    </a>
                                </span><br>
                                <span class="artdate"><?php echo date('M j, Y D g:i a', strtotime($tour->Date)) ?></span>
                            <?php else: ?>
                                <?php $i = 0; // var_dump($tour->performers) ?>
                                <?php foreach ($tour->performers as $perf): ?>
                                    <a href="/tours/<?php echo $perf->PerformerSlug."+".slug_venue($tour->Venue)."+".seoUrl($tour->City."-".$tour->StateProvince); ?>" title="<?php echo $tour->Name." - ".clean_venue($tour->Venue); ?> Tickets ">
                                        <span class="artname"><?php echo $perf->PerformerName; ?></span>
                                    </a><?php $i++; echo ($i < count($tour->performers)) ? " | " : ''; ?>
                                <?php endforeach ?>
                                <br>
                                <span>

                                    <a href="<?php echo $link; ?>" title="<?php echo $tour->Name." - ".clean_venue($tour->Venue); ?> Tickets ">
                                        <span class="artvenue"><?php echo clean_venue($tour->Venue); ?> - <?php echo $tour->City.", ".$tour->StateProvince; ?></span>
                                    </a>
                                </span><br>
                                <span class="artdate"><?php echo date('M j, Y D g:i a', strtotime($tour->Date)) ?></span>
                            <?php endif; ?>
                        </div>
                        <div class="event-action" style="vertical-align:middle">
                            <a style="margin-bottom: 5px!important;color: #fff !important;font-size: 15px" href="<?php echo $link; ?>" title="Buy <?php echo $tour->Name; ?> <?php echo clean_venue($tour->Venue) ?> Tickets for <?php echo date('D M j, Y', strtotime($tour->Date)) ?> at <?php echo (preg_match('/tba/i',$tour->DisplayDate)) ? "Time: TBA" : date('g:i a', strtotime($tour->Date)) ?>" class="pull-right btn btn-info hidden-phone hidden-tablet">
                                TICKETS <i class="ent-ticket"></i>
                            </a>
                            <br>
                            <span class="pull-right hidden-phone hidden-tablet" style="color: #008d06;font-weight: bold;text-align: right;width: 250px;font-size: 11px">
								<span><?php echo ($tour->prices) ? $tour->prices->ticketsAvailable : "Few " ?></span>
								tickets


                                <?php if ($tour->prices) : ?>
                                    left starting from $<span><?php echo $tour->prices->lowPrice; ?></span>
                                <?php else: ?>
                                    left - check prices
                                <?php endif; ?>

                            </span>

                            <a href="<?php echo $link; ?> " class="m-btn">
                                <i class="icon-chevron-right"></i>
                            </a>
                        </div>
                    </div>

                <div style="clear:both;"></div>
                <?php // endif ?>

            <?php endforeach; ?>

        </div>
        <a href="/concerts/<?php echo seoUrl($tour->City."-".$tour->StateProvince); ?>+<?php echo slug_venue($tour->Venue); ?>" class="btn btn-flat btn-info pull-left"><span style="color:#fff;">Show All</span></a><div style="clear:both;"></div><br />
    <?php endif; ?>
</article>
