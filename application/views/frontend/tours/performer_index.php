<article>
    <div class="article-header">

		<h1 class="title"><span><?php echo $performer[0]->PerformerName; ?></span> Tour Dates <?php echo $td; ?></h1>
		<ul class="article-info">
			<?php if (count($dates)): ?>
                <?php
                $originalCount = count($tourDates);
                $showAll = $this->input->get('showAll');
                $tourDates  = ($showAll) ? $tourDates : array_slice($tourDates, 0, 69);
                ?>
				<li>
                    <span class="icon awe-calendar"></span>
                    <?php
                    echo ($dates) ? $dates[0]." - " : '';
                    echo ($dates) ? end($dates) : '';
                    ?>
                </li>
			<?php endif ?>
			<?php // if($lfmPerformer['tags']): ?>
			<li>
				<span class="icon ent-music-2"></span>
                <a href="<?php echo ($performer[0]->cat) ? "/concerts/genre+{$performer[0]->cslug}" : '#' ?>" title="<?php echo ($performer[0]->cat) ? ucwords($performer[0]->cat). ' concerts' : '' ?>">
                    <span><?php echo ($performer[0]->cat) ? ucwords($performer[0]->cat) : '' ?></span>
                </a>
				
			</li>

		</ul>
		<div class="separator"></div>
	</div>

	<?php
		//$img = performerImage($performer, 'wide');
		$img = $performer[0]->fbImage;
		//$pars = filter_var($img, FILTER_VALIDATE_URL) ? getimagesize($img) : getimagesize(base_url().$img);
		//$width = $pars[0];
		$width = 200;
	?>

	<div class="article-thumbnail" style="max-height: inherit">
		<a href="/tours/<?php echo $performer[0]->PerformerSlug ?>">
			<!--<img src="--><?php //echo $img; ?><!--" alt="--><?php //echo $performer[0]->PerformerName ?><!-- tour dates" style="--><?php //echo ($width < 500) ? "float: left; padding-right: 10px;" : 'left:0;right:0;' ?><!--">-->
			<img id="tour-image-performer" src="<?php echo $img; ?>" alt="<?php echo $performer[0]->PerformerName ?> tour dates" style="width: 200px;height: 200px;float: left; padding-right: 10px;">
		</a>
		<?php if($width > 500): ?>
			<div class="caption"><?php echo $performer[0]->PerformerName; ?> Live in Concert</div>
		<?php else: ?>
			<p><?php echo $performerText; ?></p>
		<?php endif; ?>
	</div>

	<div class="article-content">

        <?php if($width > 500): ?>
			<p><?php echo $performerText; ?></p>
		<?php endif; ?>

		<h2 class="postpost-lead"><?php echo $performer[0]->PerformerName; ?> Concert Schedule</h2>

        <?php if ($tourDates): ?>
            <div class="table table-condensed smoke" id="performer-events">
                <?php foreach ($tourDates as $tour): ?>
                    <?php $link = tour_link($tour, $performer[0]->PerformerName); ?>
					<?php if ((date('Y', strtotime($tour->Date)) - date('Y')) > 2):?>
						<?php $date = date('M j', strtotime($tour->Date)).' (PPD)' ?>
					<?php else: ?>
						<?php $date = date('M j, Y', strtotime($tour->Date)) ?>
					<?php endif;?>
                    <div class="eventrow" onclick="javascript:location.href='<?php echo $link?>'">

                        <div class="event-date">
                            <span class="artdate"><?php echo $date ?><br>
                                <span class="arttime"><?php echo (preg_match('/tba/i',$tour->DisplayDate)) ? "Time: TBA" : date('D g:i a', strtotime($tour->Date)) ?></span>
                            </span>
                        </div>

                        <div class="event-details">

                            <?php //$venue = $this->global_m->get_venue($tour->VenueID); ?>

                            <a href="/tours/<?php echo $performer[0]->PerformerSlug."+".slug_venue($tour->Venue)."+".seoUrl($tour->City."-".$tour->StateProvince); ?>" title="<?php echo $tour->Name." - ".clean_venue($tour->Venue); ?> Tickets ">
                                <span class="artname"><?php echo $tour->Name; ?></span> <br>
                                <span class="artvenue">
                                    <?php echo clean_venue($tour->Venue); ?> - <?php echo $tour->City.", ".$tour->StateProvince; ?>
                                </span>
                                <br>
                                <span class="artdate">
                                    <?php //echo date('M j, Y', strtotime($tour->Date)) ?> <?php //echo (preg_match('/tba/i',$tour->DisplayDate)) ? " | Time: TBA" : date('| D g:i a', strtotime($tour->Date)) ?>
									<?php echo $date ?> <?php echo (preg_match('/tba/i',$tour->DisplayDate)) ? " | Time: TBA" : date('| D g:i a', strtotime($tour->Date)) ?>
                                </span>
                            </a>
                        </div>


                        <div class="event-action" style="vertical-align:middle;">
                            <a style="margin-bottom: 5px!important;color: #fff !important;font-size: 15px" href="<?php echo $link; ?>" title="Buy <?php echo $tour->Name; ?> <?php echo clean_venue($tour->Venue) ?> Tickets for <?php echo $date//date('D M j, Y', strtotime($tour->Date)) ?> at <?php echo (preg_match('/tba/i',$tour->DisplayDate)) ? "Time: TBA" : date('g:i a', strtotime($tour->Date)) ?>" class="pull-right btn btn-info hidden-phone hidden-tablet">
								TICKETS <i class="ent-ticket"></i>
                            </a>
							<br>
                            <span class="pull-right hidden-phone hidden-tablet" style="color: #008d06;font-weight: bold;text-align: right;width: 250px;font-size: 11px">
								<span><?php echo ($tour->prices) ? $tour->prices->ticketsAvailable : "Few " ?></span>
								tickets

								<?php if ($tour->prices) : ?>
									left starting from $<span><?php echo $tour->prices->lowPrice; ?></span>
								<?php else: ?>
									left - check prices
								<?php endif; ?>

                            </span>

                            <a href="<?php echo $link; ?>" class="m-btn">
                                <i class="icon-chevron-right"></i>
                            </a>
                        </div>
                    </div>

                    <div style="clear:both;"></div>
                <?php endforeach; ?>

				<script type="application/ld+json">
					[
					<?php $count = 0?>
					<?php $performerName0 = strtr($performer[0]->PerformerName, array('"'=>"'"))?>
					<?php foreach ($tourDates as $tour): ?>
						<?php $link = tour_link_full($tour, $performer[0]->PerformerName); ?>
						<?php $postponed = (date('Y', strtotime($tour->Date)) - date('Y')) > 2; ?>
						<?php //$venue = $this->global_m->get_venue($tour->VenueID); ?>
						{
							"@context":"https://schema.org",
							"@type":"Event",
							"description": "<?php echo "Tickets and information to see {$performerName0} perform live at {$tour->Venue} in {$tour->City}. {$performerName0} tickets are protected with a 100% guarantee at ConcertFix." ?>",
							<?php if ($postponed): ?>
							"eventStatus": "https://schema.org/EventPostponed",
							<?php else: ?>
							"eventStatus": "https://schema.org/EventScheduled",
							<?php endif; ?>
							"startDate":"<?php echo $tour->Date; ?>",
							"endDate": "<?php echo explode('T', $tour->Date)[0]?>",
							"eventAttendanceMode": "https://schema.org/OfflineEventAttendanceMode",
							"image": "<?php echo $performer[0]->img?>",
							<?php if (count($eventPerformers[$count]) > 0): ?>
							<?php $p_count = 1; ?>
							"performer": [
                                <?php foreach ($eventPerformers[$count] as $eperformer): ?>
                                	{
										"@context": "https://schema.org",
										"name": "<?php echo strtr($eperformer->PerformerName, array('"'=>"'")); ?>",
										"sameAs": "<?php echo base_url().'tours/'.$eperformer->PerformerSlug?>"
                                	}<?php echo ($p_count < count($eventPerformers[$count]) ) ? ',': ''?>
								<?php $p_count += 1; ?>
							<?php endforeach; ?>
                            ],
							<?php else: ?>
							"performer": [
                                	{
										"@context": "https://schema.org",
										"name": "<?php echo $performerName0; ?>"
                                	}
                            ],
							<?php endif; ?>
							"name": "<?php echo $tour->Name; ?>",
							"url":"<?php echo $link; ?>",
							"location":{
							            "@type":"Place",
										"name":"<?php echo $tour->Venue ?>",
										"address":{
										    "@type":"PostalAddress",
											"addressLocality":"<?php echo $tour->City ?>",
											"addressRegion": "<?php echo $tour->StateProvince ?>"
										}
							},
							"offers":{
							          "@type":"Offer","category":"Secondary","priceCurrency":"USD",
									  <?php if ($tour->prices) : ?>
									  "price":"<?php echo $tour->prices->lowPrice; ?>",
									  <?php endif ?>
									  "url":"<?php echo $link; ?>",
									  "availability":"https://schema.org/InStock",
									  "validFrom": "<?php echo date('Y-m-d').'T00:00:00'?>"
							}
						}<?php echo ($count < count($tourDates) - 1) ? ',': ''?>
						<?php $count += 1?>

					<?php endforeach ?>
					]
				</script>

                <?php if ($originalCount > 69 && !$showAll): ?>
                    <a href="?showAll=true" class=""><span>Show All</span></a>
                <?php endif ?>
            </div>
        <?php else: ?>
            <h4 class="inline-block">No Events! =(</h4>
            <a class="fs-18 pad-left-10" href=/user/login>Track <?php echo $performer[0]->PerformerName; ?>!</a>
        <?php endif; ?>

		<?php if (isset($albums) and count($albums) > 0): ?>
			<a href="#" name="morealbums"></a>
			<?php if ($performer[0]->aboutText != '' && $performer[0]->aboutText != 'does not have any albums'): ?>
				<h2 class="post-lead">About <?php echo $performer[0]->PerformerName; ?> Tour Albums</h2>
				<p><?php echo $performer[0]->aboutText ?></p>
			<?php endif ?>
		<?php $countAls = count($albums); ?>
		<h3 class="post-lead margbot"><?php echo $performer[0]->PerformerName; ?> Tour Albums and Songs</h3>
		<div class="carousel-widget" id="slider-widget">
			<div class="widget-content carousel slide" id="carousel-widget">
				<ol class="carousel-indicators">
					<li class="" data-slide-to="0" data-target="#carousel-widget" class="active"></li>
					<?php for ($i=1; $i < $countAls; $i++) :?>
						<li data-slide-to="<?php echo $i; ?>" data-target="#carousel-widget" class=""></li>
					<?php endfor; ?>
				</ol>
				<!-- Begin Carousel Inner -->
				<div class="carousel-inner">
					<?php $active = "active"; ?>
					<?php foreach ($albums as $album): ?>
					<!-- Begin Slide Section / Carousel Item -->
					<div class="slide-section item <?php echo $active; ?>">
					    <div class="slide-item media">
						    <a class="pull-left media-thumbnail image-polaroid" href="#">
                                <?php if($album['image'] != '' and $album['image'] != null):?>
						    		<img class="media-object" src="<?php echo $album['image'] ?>"
										 alt="<?php echo $performer[0]->PerformerName?>: <?php echo $album['name']?>">
                                <?php else:?>
						    		<img class="media-object" src="/public/img/album/photo_not_available.png" alt="thumbnail">
                                <?php endif?>

						    </a>
						    <div class="media-body">
							    <h4 class="media-heading"><?php echo $performer[0]->PerformerName; ?>: <?php echo $album['name'] ?></h4>
							</div>
							<div class="song-list">
 								<ul>
 									<?php $i = 0; ?>
									<?php if (isset($album['tracks'])) : ?>
										<?php foreach ($album['tracks'] as $track): ?>
											<li><span title="<?php echo $track['name']; ?>"><?php echo (strlen($track['name']) > 23) ? substr($track['name'],0,20).'...' : $track['name']; ?></span></li>
										<?php endforeach ?>
									<?php endif ?>

								</ul>
						    </div>
						</div>
					</div>
					<!-- End Slide Section / Carousel Item -->
					<?php $active = ''; ?>
					<?php endforeach ?>
				</div>
				<!-- End Carousel Inner -->
					<span style="font-size:10px;">Data supplied by Last.fm</span>
			</div>
		</div><div style="clear:both;"></div>
		<div class="separator"></div>
		<?php endif ?>
		<div style="clear:both;"></div>
		<div class="comments">
			<h3><?php echo $performer[0]->PerformerName; ?> Concert Tour Questions &amp; Comments</h3>
			<div class="separator"></div>
			<?php /*
			<script>

			(function(d, s, id) {

			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1appId=166562666886722";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));

			</script>
			*/ ?>
			<div class="fb-comments" data-href="<?php echo strtr(current_url(), array('https:'=>'http:')) ; ?>" data-colorscheme="" data-numposts="5" data-width="" style="width:100%;"></div>
		</div>
		<h2 class="post-lead"><?php echo $performer[0]->PerformerName; ?> Tour and Concert Ticket Information</h2>
		<?php echo $liText; ?>
	</div>
</article>