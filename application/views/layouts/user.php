<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js ie8 ie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9 ie" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class=" js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths" lang="en">
<!--<![endif]-->
	<head>
		<title><?php echo $template['title']; ?></title>
		<?php echo $template['metadata']; ?>

	</head>
	<body>
		<div id="bg"><img src="/public/img/assets/bg/overlay.png" alt="background image"></div>
		<!-- Fixed navbar -->
	    <header class="header">
	    	<?php /* if($this->caching): ?>
		    	<div class="alert alert-success"><h3>Caching is Enabled!</h3></div>
		    <?php endif; */ ?>
		<?php echo isset($template['partials']['header']) ? $template['partials']['header'] : ''; ?>
		</header>

		<!-- search -->
		<?php echo isset($template['partials']['search']) ? $template['partials']['search'] : ''; ?>
		<!-- search -->
	    <!-- container -->
		<div class="container" role="main">
			<?php if($deezbug = $this->load->get_var('debug-output')): ?>
			<div class="jumbotron">
		        <h5>Pretty Dump:</h5>
				<?php echo $deezbug; ?>
			</div>
			<?php endif; ?>
			<section id="main-content" class="row">
	    	    <div id="content" class="span8">
	    	    	<?php if(isset($template['breadcrumbs'])): ?>
		    	    	<ul class="breadcrumb">
		    	    		<?php foreach ($template['breadcrumbs'] as $key => $crumb): ?>
								<li class="<?php echo ($key) ? 'typ-pin' : 'typ-home'; ?>"  ><span itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
									<?php echo ($crumb['uri']) ? "<a href='{$crumb['uri']}'><span itemprop='title'>{$crumb['name']}</span></a> <span class='divider'>»</span>" : "<span itemprop='title'>{$crumb['name']}</span>"; ?>
								</span></li>
		    	    		<?php endforeach ?>
					    </ul>
					<?php endif; ?>
					<?php if($this->session->flashdata('type')): ?>
						<div class=" alert <?php echo $this->session->flashdata('type'); ?>">
							<h3><?php echo ucwords(str_replace('alert-', '', $this->session->flashdata('type')))."!"; ?></h3>
								<p><?php echo $this->session->flashdata('message'); ?></p>
						</div>
					<?php endif?>
					<div class="post-single" id="post">
       					<div class="content-outer">
          					<div class="content-inner">
	    	    				<?php echo $template['body']; ?>
	    	    			</div>
	    	    		</div>
	    	    	</div>
	    	    </div>
	    	    <div id="right-sidebar" class="span4">
		        	<?php
		        	foreach ($template['partials'] as $partialName => $partial):
	        			if(preg_match("/sidebar_/i", $partialName)):
	        				echo $partial;
        				endif;
		        	endforeach;
		        	?>
		        </div>
		    </section>

		</div><!-- /.container	 -->


		<footer class="footer">
			<?php echo isset($template['partials']['footer']) ? $template['partials']['footer'] : ''; ?>
		</footer>

	</body>
</html>
