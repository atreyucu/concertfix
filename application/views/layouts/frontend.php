<!DOCTYPE html>

<html class=" js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths" lang="en">


    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?php echo $template['title']; ?></title>

		<?php if(isset($template['partials']['ldjson'])): ?>
			<?php echo $template['partials']['ldjson']; ?>
		<?php endif; ?>

		<link rel="stylesheet" type="text/css" href="/public/css/concertfix.css">
		<?php echo $template['metadata']; ?>

	</head>

    <body>

        <div id="bg">
            <img src="/public/img/assets/bg/overlay.png" alt="background image" />
        </div>

        <!-- Fixed navbar -->
	    <header class="header">
	    	<?php /* if($this->caching): ?>
		    	<div class="alert alert-success"><h3>Caching is Enabled!</h3></div>
		    <?php endif; */ ?>
		    <?php echo isset($template['partials']['header']) ? $template['partials']['header'] : ''; ?>
		</header>

		<!-- search -->
		<?php echo isset($template['partials']['search']) ? $template['partials']['search'] : ''; ?>
		<!-- search -->

        <!-- container -->
		<div class="container" role="main">

            <?php if($deezbug = $this->load->get_var('debug-output')): ?>
			<div class="jumbotron">
		        <h5>Pretty Dump:</h5>
				<?php echo $deezbug; ?>
			</div>
			<?php endif; ?>

			<section id="main-content" class="row">

                <div id="content" class="span8">

                    <?php if(isset($template['breadcrumbs'])): ?>

                        <ul class="breadcrumb">

                            <?php foreach ($template['breadcrumbs'] as $key => $crumb): ?>

                                <li class="<?php echo ($key) ? 'typ-pin' : 'typ-home'; ?>" >

                                    <?php if ($crumb['uri']) : ?>
                                        <a href="<?php echo $crumb['uri']; ?>">
                                            <span><?php echo $crumb['name']; ?></span>
                                        </a>
                                        <span class="divider">»</span>
                                    <?php else: ?>
                                        <a href="/<?php echo uri_string(); ?>">
                                            <span><?php echo $crumb['name']; ?></span>
                                        </a>
                                    <?php endif; ?>

								</li>
		    	    		<?php endforeach ?>
					    </ul>
					<?php endif; ?>

					<?php if($this->session->flashdata('type')): ?>
						<div class=" alert <?php echo $this->session->flashdata('type'); ?>">
                            <h3><?php echo $this->session->flashdata('message'); ?></h3>
						</div>
					<?php endif?>

					<div class="post-single" id="post">
       					<div class="content-outer">
          					<div class="content-inner">
	    	    				<?php echo $template['body']; ?>
	    	    			</div>
	    	    		</div>
	    	    	</div>

	    	    </div>

		        <div id="right-sidebar" class="span4">
		        	<?php
		        	foreach ($template['partials'] as $partialName => $partial):
	        			if(preg_match("/sidebar_/i", $partialName)):
	        				echo $partial;
        				endif;
		        	endforeach;
		        	?>
		        </div>

		    </section>

		</div><!-- /.container	 -->

		<footer class="footer">
			<?php echo isset($template['partials']['footer']) ? $template['partials']['footer'] : ''; ?>
		</footer>

	</body>
</html>
