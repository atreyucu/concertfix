<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js ie8 ie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9 ie" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class=" js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths" lang="en">
<!--<![endif]-->
	<head>
		<title><?php echo $template['title']; ?></title>
		<?php echo $template['metadata']; ?>

	</head>
	<body>
		<div id="bg"><img src="/public/img/assets/bg/overlay.png" alt="background image"></div>
		<div id="fb-root"></div>
		<!-- Fixed navbar -->
	    <header class="header">
		<?php echo isset($template['partials']['header']) ? $template['partials']['header'] : ''; ?>
		</header>

		<!-- search -->
		<?php echo isset($template['partials']['search']) ? $template['partials']['search'] : ''; ?>
		<!-- search -->

	    <!-- container -->
		<div class="container" role="main">
			<?php if($deezbug = $this->load->get_var('debug-output')): ?>
			<div class="jumbotron">
		        <h5>Pretty Dump:</h5>
				<?php echo $deezbug; ?>
			</div>
			<?php endif; ?>

			<section id="popular-content" class="row visible-desktop">
				<?php echo isset($template['partials']['popular']) ? $template['partials']['popular'] : ''; ?>
			</section>

			<section id="main-content" class="row home">
	    	    <div id="content" class="span9">
	    	    	<?php echo $template['body']; ?>
	    	    </div>
		        <div id="right-sidebar" class="span3"><?php echo $template['partials']['sidebar']; ?></div>
		    </section>

		</div><!-- /.container	 -->


		<footer class="footer">
			<?php echo isset($template['partials']['footer']) ? $template['partials']['footer'] : ''; ?>
		</footer>

	</body>
</html>
