<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js ie8 ie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9 ie" lang="en"> <![endif]-->

<!--[if lt IE 9]>
<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
<![endif]-->

<!--[if gt IE 9]><!-->
<html class=" js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths" lang="en">
<!--<![endif]-->

	<head>
	    <link href="//mapwidget2.seatics.com/Css/defaultBreakpoint?v=39P2_FQ5S5IJVzQUAnv2jysjH1ShMsSmUE2A7EAP7341" type="text/css" rel="stylesheet">
		<title><?php echo $template['title']; ?></title>
		<!--[if lte IE 9]> <meta http-equiv='X-UA-Compatible' content='IE=7; IE=8; IE=9' /> <![endif]-->
		<?php echo $template['metadata']; ?>

		<?php if(isset($template['partials']['ldjson'])): ?>
			<?php echo $template['partials']['ldjson']; ?>
		<?php endif; ?>
        
		<link rel="shortcut icon" href="/public/img/ico/favicon.ico">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cabin:400,400italic,700,700italic">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=PT+Sans:regular,italic,bold">

		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/public/img/ico/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/public/img/ico/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="/public/img/ico/apple-touch-icon-57-precomposed.png">
		<link rel="stylesheet" type="text/css" href="/public/css/concertfix_tickets.css">
		<link rel="stylesheet" type="text/css" href="/public/css/grid.css">
		<link rel="stylesheet" type="text/css" href="/public/css/temp.css">


	</head>
	<body>
		<div id="bg"><img src="/public/img/assets/bg/overlay.png" alt="background image"></div>
		<!-- Fixed navbar -->
	    <header class="header-tix">
		<?php echo isset($template['partials']['header']) ? $template['partials']['header'] : ''; ?>
		</header>
	    <!-- container -->
		<div class="container" role="main">
			<section id="main-content" class="row">
	    	    <div id="content" class="span12">
	    	    	<?php if(isset($template['breadcrumbs'])): ?>
		    	    	<!--<ul class="breadcrumb mob-dis">
		    	    		<div class="span6 pull-right asob">
        						<img src="/public/img/assets/cf-pr.png" alt="As Seen around the web" style="border: 0 none;height: auto;max-width: 100%;vertical-align: middle;">
        					</div>
		    	    		<div class="span5">
           						<a href="/ticket-guarantee" class="whybuytext pull-left">
              						<img src="/public/img/assets/moneyback-guarantee.png" style="height:75px;" alt="Our 200% Money Back Guarantee">
          						</a>
          						<div class="marg-top pull-left wb-check">
            						<ul class="why-buy">
                						<li>Seats side-by-side, unless otherwise noted</li>
                						<li>Seats come with 200% Money Back Guarantee</li>
                						<li>Look for e-ticket symbol for instant tickets</li>
            						</ul>
        						</div>
        					</div><div style="clear:both;"></div>
					    </ul>-->
					<?php endif; ?>
					<div class="post-single" id="post">
       					<div class="content-outer">
          					<div class="content-inner">
	    	    				<?php echo $template['body']; ?>
	    	    			</div>
	    	    		</div>
	    	    	</div>
	    	    </div>
		    </section>
		</div><!-- /.container	 -->
		<footer class="footer_tix">
			<?php echo isset($template['partials']['footer']) ? $template['partials']['footer'] : ''; ?>

		</footer>
	</body>
</html>
