<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js ie8 ie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9 ie" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class=" js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths" lang="en">
<!--<![endif]-->
	<head>
		<title><?php echo $template['title']; ?></title>
		<?php echo $template['metadata']; ?>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>
		<!-- Fixed navbar -->
	    <header class="header">
		<?php echo isset($template['partials']['header']) ? $template['partials']['header'] : ''; ?>
		</header>
		
	    <!-- container -->
		<div class="container" role="main">
			<?php if($deezbug = $this->load->get_var('debug-output')): ?>
			<div class="jumbotron">
		        <h5>Pretty Dump:</h5>
				<?php echo $deezbug; ?>
			</div>
			<?php endif; ?>

			<?php if($message = $this->session->flashdata('message')): ?>
			<section class="row">
				<div class="span12">
					<div class="alert alert-<?php echo $this->session->flashdata('type'); ?>">
						<h3><?php echo $message; ?> </h3>
					</div>
				</div>
			</section>
			<?php endif; ?>
			
			<section id="main-content" class="row home">

				<?php if(isset($template['partials']['sidebar'])): ?>
				<div id="content" class="span8"> 
	    	    	<?php echo $template['body']; ?>
	    	    </div>
	    	    <div id="sidebar" class="span4">
	    	    	<?php echo $template['partials']['sidebar']; ?>
	    	    </div>
				<?php else: ?>
	    	    <div id="content" class="span12"> 
	    	    	<?php echo $template['body']; ?>
	    	    </div>
		    	<?php endif ?>
		    </section>

		</div><!-- /.container	 -->
		

		<footer class="footer">
			<?php echo isset($template['partials']['footer']) ? $template['partials']['footer'] : ''; ?>
		</footer>
	
	</body>
</html>
